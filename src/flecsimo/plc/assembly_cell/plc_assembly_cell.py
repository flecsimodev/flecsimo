#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for assembly cell; head file. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


from flecsimo.plc.assembly_cell.hw_assembly_cell import HW
import time


class Plc_assembly_cell():
    def __init__(self):
        
        self.hw = HW(self)
        

    def instruction_assemble_one_component_wo_cache(self):
        self.hw.wait_for_component_at_inlet()
        self.hw.move_component_from_inlet_to_gripper()
        self.hw.eject_box_from_stack()
        self.hw.gripper_pick_component()
        self.hw.move_box_from_stack_to_gripper()
        self.hw.gripper_release_component()
        self.hw.move_to_inlet()
        self.hw.wait_for_free_inlet()
    
    def instruction_assemble_one_component(self):                # TODO: check before every conveyor belt movement for new component at inlet, also compensate offset after moving into cache
        if not self.hw.cache_active:
            self.hw.wait_for_component_at_inlet()
            self.hw.move_component_from_inlet_to_gripper()
        else:
            self.hw.cache_eject_component()
            self.hw.move_component_from_cache_to_gripper()
        self.hw.eject_box_from_stack()
        self.hw.gripper_pick_component()
        self.hw.move_box_from_stack_to_gripper()
        self.hw.gripper_release_component()
        while self.hw.is_component_at_inlet():
            if self.hw.cache_active:
                # self.hw.alarm_remove_component_at_inlet()        # currently buggy on hardware side
                self.hw.blink_at_inlet()
            else:
                self.hw.move_component_from_inlet_to_cache_entry()
                self.hw.cache_stock_component()
        self.hw.move_to_inlet()
        self.hw.wait_for_free_inlet()
        if self.hw.cache_active:
            self.instruction_assemble_one_component()
    
    def instruction_cache_demonstration(self):
        self.hw.wait_for_component_at_inlet()
        self.hw.move_component_from_inlet_to_cache_entry()
        self.hw.cache_stock_component()
        time.sleep(2)
        self.hw.cache_eject_component()
        self.hw.move_to_inlet()
        self.hw.wait_for_free_inlet()
    
    def instruction_loop_wo_cache(self):
        while True:
            self.instruction_assemble_one_component_wo_cache()
    
    def instruction_loop_with_cache(self):
        while True:
            self.instruction_assemble_one_component()

    def quick_remote(self, input_string="once"):    # either: once, once_no_cache, cache_demo, loop, loop_no_cache, noise
        # TODO
        pass
        


    

if __name__ == "__main__":
    plc = Plc_assembly_cell()
    plc.instruction_assemble_one_component()
    
    


