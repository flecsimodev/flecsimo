#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for assembly cell; hardware abstraction. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ftrobopy
import time
# from Kinematics import Encoder            # not needed for this cell


class HW():
    
    """
    ftrobopy initializing, 
    hardware state variables etc
    """
    
    def __init__(self, parent):

        try:
            self.txt = ftrobopy.ftrobopy("auto")            # connect to TXT's IO controller
        except:
            self.txt = None                                 # set TXT to "None" of connection failed

        if not self.txt:
            print("HW: Error connecting IO server")                                    
        else:
            print("HW: Connection to TXT's IO Controller seems to work")    # print confirmation for ssh console output
            
            # txt connections
            self.motor_conveyor_belt = self.txt.motor(1)                # M1
            self.valve_cache_pusher = self.txt.output(3)                # O3
            self.valve_stack_pusher = self.txt.output(4)                # O4
            self.valve_gripper_height = self.txt.output(5)              # O5
            self.valve_gripper_suction = self.txt.output(6)             # O6
            self.compressor = self.txt.output(7)                        # O7
            self.lamp_camera_spotlight = self.txt.output(8)             # O8
            
            self.lightbarrier_position_inlet = self.txt.input(1)        # I1
            self.lightbarrier_position_gripper = self.txt.input(2)      # I2
            self.button_cache_status = self.txt.input(3)                # I3
            self.lightbarrier_stack_pusher_status = self.txt.input(4)   # I4
            self.button_gripper_status = self.txt.input(5)              # I5
                       
        self.parent = parent
        
        self.compressor_status = 0  # to be only set by the start|stop_compressor functions (and reset)
        self.compressor_demand = 0  # to be incremented before start_compressor and to be decremented before stop_compressor
        self.cache_active = 0
        
    
    def gripper_pick_component(self):               # one whole process of lifting a component
        self.compressor_demand += 1
        self.start_compressor()
        self.valve_gripper_height.setLevel(512)     # move gripper down, pushed onto the component
        time.sleep(0.6)
        self.valve_gripper_suction.setLevel(512)    # suction cup
        time.sleep(1)
        self.valve_gripper_height.setLevel(0)       # springs push gripper back upwards
        time.sleep(0.5)
        
    def gripper_release_component(self):            # opposite of gripper_pick_component
        self.valve_gripper_suction.setLevel(0)      # release vacuum on suction cup, 
        time.sleep(0.5)                             # so component falls into box underneath        
        self.compressor_demand -= 1
        self.stop_compressor()
        
    def start_compressor(self):
        if self.compressor_status == 0:
            self.compressor.setLevel(512)
            time.sleep(2)
            self.compressor_status = 1
    
    def stop_compressor(self):
        if self.compressor_demand == 0:
            self.compressor.setLevel(0)
            self.compressor_status = 0
    
    def eject_box_from_stack(self):
        self.compressor_demand += 1
        self.start_compressor()
        self.valve_stack_pusher.setLevel(512)       # move box out onto the conveyor belt
        time.sleep(1)
        self.valve_stack_pusher.setLevel(0)         # move pusher back to original position, next box falls on ground
        time.sleep(0.4)
        self.valve_stack_pusher.setLevel(512)       # push again shortly to make sure the box on the conveyor belt is in the right position
        time.sleep(0.1)
        self.valve_stack_pusher.setLevel(0)
        self.compressor_demand -= 1
        self.stop_compressor()
    
    def move_box_from_stack_to_gripper(self):
        self.motor_conveyor_belt.setSpeed(512)      # move til lightbarrier at gripper position
        while self.lightbarrier_position_gripper.state() == 1:
            time.sleep(0.02)
        self.motor_conveyor_belt.stop()             # only stopping for debug observation, can be removed later
        time.sleep(1)
        self.drive_motor_distance(self.motor_conveyor_belt, 45, 360)    # slowly move (step-counted) under gripper to stop centered
        time.sleep(1)
    
    def move_to_inlet(self):
        self.motor_conveyor_belt.setSpeed(512)      # move til lightbarrier at inlet position
        while self.lightbarrier_position_inlet.state() == 1:
            time.sleep(0.02)
        self.motor_conveyor_belt.stop()
        self.drive_motor_distance(self.motor_conveyor_belt, 65, 360)
    
    def move_component_from_inlet_to_gripper(self): 
        # use in fact cache_to_gripper but check free road beforehand: TODO implement check
        self.move_component_from_cache_to_gripper()

    
    def move_component_from_cache_to_gripper(self):
        # do the actual movement that is (currently) also used by inlet_to_gripper
        self.motor_conveyor_belt.setSpeed(-512)     # move til lightbarrier at gripper position
        while self.lightbarrier_position_gripper.state() == 1:
            time.sleep(0.02)
        self.motor_conveyor_belt.stop()             # only stopping for debug observation, can be removed later
        time.sleep(1)
        self.drive_motor_distance(self.motor_conveyor_belt, -42, 360)   # slowly move (step-counted) under gripper to stop centered
        time.sleep(1)
    
    def move_component_from_inlet_to_cache_entry(self):
        self.motor_conveyor_belt.setSpeed(-360)      # move til out of lightbarrier at inlet position
        while self.lightbarrier_position_inlet.state() == 0:
            time.sleep(0.02)
        self.motor_conveyor_belt.stop()             # only stopping for debug observation, can be removed later
        time.sleep(1)
        self.drive_motor_distance(self.motor_conveyor_belt, -70, 360)    # slowly move (step-counted) under gripper to stop centered
        time.sleep(1)
    
    def cache_stock_component(self):                # assumes that the component is already placed in front of the cache on the conveyor belt
        self.cache_active = 1
        self.compressor_demand += 1
        self.start_compressor()
        self.valve_cache_pusher.setLevel(512)
        time.sleep(0.5)
    
    def cache_eject_component(self):
        self.valve_cache_pusher.setLevel(0)
        self.compressor_demand -= 1
        self.stop_compressor()
        time.sleep(0.5)
        self.cache_active = 0
        
    def is_component_at_inlet(self):
        return not self.lightbarrier_position_inlet.state()
        
    def wait_for_component_at_inlet(self):
        while not self.is_component_at_inlet():
            time.sleep(0.02)
        
    def wait_for_free_inlet(self):
        while self.is_component_at_inlet():
            # time.sleep(0.02)
            self.blink_at_inlet()
            
    def alarm_remove_component_at_inlet(self):  # TODO fix bug in relation to play_sound (permission of sound file)
        self.txt.play_sound(2) # play alarm sound once
        time.sleep(2)
        
    def blink_at_inlet(self):
        self.lamp_camera_spotlight.setLevel(512)
        time.sleep(0.15)
        self.lamp_camera_spotlight.setLevel(0)
        time.sleep(0.15)
                    
    
    # NOTE: The following motor control functions are rather basic and for most use cases not to be used for control implementations any longer. For those purposes, consider using new Kinematic classes that provide more advanced logic. 
    # Anyway, at least for rather low-level debug purposes, these functions are kept in here anyway and may be used if appropriate.
    
    def drive_motor_distance(self, mot_identifier, distance, speed=512):
        """
        Moves a ftrobopy.motor object by the given distance in the given direction. 
        mot_identifier is the motor object (note: to be called together with its "self."-prefix)
        distance is 1. the distance value and 2. the direction: positive numbers mean setSpeed(512), negative numbers do setSpeed(-512) 
        Update: Now the speed value can *optionally* be given as well. Default is 512.
        """
        if distance == 0:
            raise "motor_distance: distance value cannot be 0"
        mot_identifier.setDistance(abs(distance), syncto=None)
        if distance > 0:
            mot_identifier.setSpeed(speed)
        else:
            mot_identifier.setSpeed(-speed)        
        while not mot_identifier.finished() == True:
            time.sleep(0.01)
        mot_identifier.stop()
        
    def drive_motor_distance_watch_end_stop(self, mot_identifier, distance, end_stop):
        """
        Moves a ftrobopy.motor object by the given distance in the given direction. 
        mot_identifier is the motor object (note: to be called together with its "self."-prefix)
        distance is 1. the distance value and 2. the direction: positive numbers mean setSpeed(512), negative numbers do setSpeed(-512) 
        Watches for the given end_stop not to be driven over.
        """
        if distance == 0:
            raise "motor_distance: distance value cannot be 0"
        if not end_stop.state() == True:
            mot_identifier.setDistance(abs(distance), syncto=None)
            if distance > 0:
                mot_identifier.setSpeed(512)
            else:
                mot_identifier.setSpeed(-512)        
            while not mot_identifier.finished() and not end_stop.state() == True:
                time.sleep(0.01)
            mot_identifier.stop()
    
    def panic(self): # stop all hardware outputs and motors
        print("PANIC: stopping all hardware outputs")
        self.motor_conveyor_belt.stop()
        self.valve_cache_pusher.setLevel(0)
        self.valve_gripper_height.setLevel(0)
        self.valve_gripper_suction.setLevel(0)
        self.valve_stack_pusher.setLevel(0)
        self.compressor.setLevel(0)
        self.compressor_status = 0
        print("PANIC: stopped all hardware outputs successfully")
        
        