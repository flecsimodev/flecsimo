#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for identification cell; hardware abstraction. 
Just a camera and one lightbarrier inlet as a cell. Returns QR Codes on top of workpieces on request.  

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ftrobopy
import time
from camera import QRCodeCameraReader
import cv2


class HW():
    
    """
    ftrobopy initializing, 
    hardware state variables etc
    """
    
    def __init__(self, parent):

        try:
            self.txt = ftrobopy.ftrobopy("auto")            # connect to TXT's IO controller
        except:
            self.txt = None                                 # set TXT to "None" of connection failed

        if not self.txt:
            print("HW: Error connecting IO server")                                    
        else:
            print("HW: Connection to TXT's IO Controller seems to work")    # print confirmation for ssh console output
            
            # txt connections
            self.lightbarrier_position_inlet = self.txt.input(1)        # I1
        
        self.cell_camera = QRCodeCameraReader(cam_id=0)
        self.last_read_id = None
        self.parent = parent
        
        
        
    # in comparison with other cell HW:
    # no dedicated hardware functions, because there is nothing else than a lightbarrier that is already read in generic functions below 
        
    
    def initialize(self): 
        print("running initialize...")
        # anything else to do here?
        print("initializing done")
        return(self.is_ready_for_production())
        
    def is_ready_for_production(self):
        if self.lightbarrier_position_inlet.state() == 0:
            return False
        else:
            return True
        
    def is_component_at_inlet(self):
        return not self.lightbarrier_position_inlet.state()
        
    def wait_for_component_at_inlet(self):
        if not self.is_component_at_inlet():
            print("please place a component at inlet")
        while not self.is_component_at_inlet():
            time.sleep(0.02)
        print("recognized 1 component at inlet")
        time.sleep(0.5)
        return(self.read_id())
        
    def read_id(self):
        raw_code = []
        try_count = 0
        while len(raw_code) == 0 and try_count < 10:
            try:
                raw_code = self.cell_camera.detectCode()
            except cv2.error as e:
                print(e)
            time.sleep(0.1)
            try_count += 1
            
        if len(raw_code) == 0:
            code = ''
        else:
            code = raw_code[0]
        
        print("reading tries:", try_count)
        print("raw result:", raw_code)
        print("returning 1 code:", code)
        self.last_read_id = code
        return code
            
        
    def wait_for_free_inlet(self):
        while self.is_component_at_inlet():
            # time.sleep(0.02)
            time.sleep(1)
            print("please remove component from inlet")
            
#     def panic(self): # stop all hardware outputs and motors
#         print("PANIC: stopping all hardware outputs")
#         # anything else in case of this cell?
#         print("PANIC: stopped all hardware outputs successfully")
            