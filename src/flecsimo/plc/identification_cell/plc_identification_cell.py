#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for identification cell; head file. 
Just a camera and one lightbarrier inlet as a cell. Returns QR Codes on top of workpieces on request. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


from flecsimo.plc.identification_cell.hw_identification_cell import HW
# import time


class Plc_identification_cell():
    def __init__(self):
        
        self.hw = HW(self)
        
        # self.setup_value = 1     # for the time being, this is enough just as a stored number. for e.g. drilling tool changes, advanced logic needs to be introduced. (current, requested, changing)  

        

    def initialize(self):
        return(self.hw.initialize())
        
#     def setup(self, config_value=1):
#         self.setup_value = config_value
#         return True
    
    def wait_for_component_at_inlet(self):
        self.hw.wait_for_component_at_inlet()
    
    def wait_for_free_inlet(self):
        self.hw.wait_for_free_inlet()
    
#     def instruction_process_one_component(self):
#         pass
#         # TODO
#         return 0
        
        


    

if __name__ == "__main__":
    plc = Plc_identification_cell()
    plc.initialize()
    plc.wait_for_component_at_inlet()
#    plc.instruction_process_one_component()
    plc.wait_for_free_inlet()
    
    


