#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for drilling cell; hardware abstraction. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ftrobopy
import time
# from Kinematics import Encoder            # not needed for this cell (yet)


class HW():
    
    """
    ftrobopy initializing, 
    hardware state variables etc
    """
    
    def __init__(self, parent):

        try:
            self.txt = ftrobopy.ftrobopy("auto")            # connect to TXT's IO controller
        except:
            self.txt = None                                 # set TXT to "None" of connection failed

        if not self.txt:
            print("HW: Error connecting IO server")                                    
        else:
            print("HW: Connection to TXT's IO Controller seems to work")    # print confirmation for ssh console output
            
            # txt connections
            self.motor_conveyor_belt = self.txt.motor(1)                # M1
            self.motor_tool_changer_rotation = self.txt.motor(2)        # M2
            self.motor_tool_vertical = self.txt.motor(3)                # M3
            self.motor_tool_drive = self.txt.motor(4)                   # M4
            
            self.lightbarrier_position_inlet = self.txt.input(1)        # I1
            self.lightbarrier_position_tool = self.txt.input(2)         # I2
                                                                        # no I3 atm; third lightbarrier could be added later
            self.button_tool_changer_code_1  = self.txt.input(4)        # I4
            self.button_tool_changer_code_2 = self.txt.input(5)         # I5
            self.button_tool_vertical_upper = self.txt.input(6)         # I6
            self.button_tool_vertical_lower = self.txt.input(7)         # I7
                       
        self.parent = parent
        
        self.setup_current = "unknown"
        self.setup_possible = [1, 2, 3] # values that can be set as target setups; can also internally be "changing" or "unknown"
        
        self.tool_changer_rotation_speed = 420
        

    def move_component_from_inlet_to_tool(self): 
        self.motor_conveyor_belt.setSpeed(-512)     # move til lightbarrier at tool position
        while self.lightbarrier_position_tool.state() == 1:
            time.sleep(0.02)
        self.motor_conveyor_belt.stop()             # only stopping for debug observation, can be removed later
        time.sleep(1)
        self.drive_motor_distance(self.motor_conveyor_belt, -40, 360)   # slowly move (step-counted) under tool to stop centered
        time.sleep(1)
    
    def move_component_back_to_inlet(self):
        self.motor_conveyor_belt.setSpeed(512)
        while self.lightbarrier_position_inlet.state() == 1:
            time.sleep(0.02)
        self.motor_conveyor_belt.stop()
        time.sleep(1)
    
    def tool_usage_process(self): # TODO in the long run: depending on the tool chosen, 3 different processes?
        self.motor_tool_drive.setSpeed(-512)
        time.sleep(1)
        self.move_tool_down()
        time.sleep(1.5)
        self.move_tool_up()
        time.sleep(1)
        self.motor_tool_drive.stop()
        time.sleep(0.5)        
        
    def move_tool_down(self):
        self.motor_tool_vertical.setSpeed(-360)
        while self.button_tool_vertical_lower.state() == 1:
            time.sleep(0.02)
        self.motor_tool_vertical.stop()
        
    def move_tool_up(self):
        self.motor_tool_vertical.setSpeed(480)
        while self.button_tool_vertical_upper.state() == 1:
            time.sleep(0.02)
        self.motor_tool_vertical.stop()        
    
    def tool_changer_initialization(self):
        self.setup_current = "changing"
        self.motor_tool_changer_rotation.setSpeed(-1 * self.tool_changer_rotation_speed)
        while not (self.button_tool_changer_code_1.state() == 0 and self.button_tool_changer_code_2.state() == 0) :
            time.sleep(0.02)
        self.motor_tool_changer_rotation.stop()
        # self.drive_motor_distance(self.motor_tool_changer_rotation, -10, 128)   # minor position correction to stop exactly vertically
        self.setup_current = 3
        time.sleep(0.5)
                
    def change_tool_to(self, setup_requested):
        # check if machine is already set up correctly
        # actual movement
        if not setup_requested in self.setup_possible:
            raise "invalid setup target number"
        if setup_requested == self.setup_current:
            print("already set up, no setup change necessary")
        else:
            if setup_requested == 1:
                self.setup_current = "changing"
                self.motor_tool_changer_rotation.setSpeed(-1 * self.tool_changer_rotation_speed)
                time.sleep(0.3) # to wait for measure til both buttons are released
                while not (self.button_tool_changer_code_1.state() == 0 and self.button_tool_changer_code_2.state() == 1) :
                    time.sleep(0.02)
                self.motor_tool_changer_rotation.stop()
                # self.drive_motor_distance(self.motor_tool_changer_rotation, -10, 128)   # minor position correction to stop exactly vertically
                self.setup_current = 1
            elif setup_requested == 2:
                self.setup_current = "changing"
                self.motor_tool_changer_rotation.setSpeed(-1 * self.tool_changer_rotation_speed)
                time.sleep(0.3) # to wait for measure til both buttons are released
                while not (self.button_tool_changer_code_1.state() == 1 and self.button_tool_changer_code_2.state() == 0) :
                    time.sleep(0.02)
                self.motor_tool_changer_rotation.stop()
                # self.drive_motor_distance(self.motor_tool_changer_rotation, -10, 128)   # minor position correction to stop exactly vertically
                self.setup_current = 2
            elif setup_requested == 3:
                self.setup_current = "changing"
                self.motor_tool_changer_rotation.setSpeed(-1 * self.tool_changer_rotation_speed)
                time.sleep(0.3) # to wait for measure til both buttons are released
                while not (self.button_tool_changer_code_1.state() == 0 and self.button_tool_changer_code_2.state() == 0) :
                    time.sleep(0.02)
                self.motor_tool_changer_rotation.stop()
                # self.drive_motor_distance(self.motor_tool_changer_rotation, -10, 128)   # minor position correction to stop exactly vertically
                self.setup_current = 3
        print("setup completed, current tool:", self.setup_current)
        time.sleep(0.5)
            
    def initialize(self):
        print("running initialize...")
        if not self.button_tool_vertical_upper.state() == 0:
            self.move_tool_up()
        self.tool_changer_initialization()
        print("tool successfully reset")
        print("lightbarrier states:") 
        print("inlet: ", self.lightbarrier_position_inlet.state()) 
        print("tool: ", self.lightbarrier_position_tool.state())
        return(self.is_ready_for_production())
        
    def is_ready_for_production(self): 
        if self.lightbarrier_position_inlet.state() == 0 or self.lightbarrier_position_tool.state() == 0 or self.button_tool_vertical_upper.state() == 1 or self.setup_current not in self.setup_possible:
            return False
        else:
            return True
        
    def setup(self, config_value): # to be discussed: return error message if invalid setup value?
        if config_value not in self.setup_possible:
            print("invalid setup value")
        else:
            self.change_tool_to(config_value)
        
    def is_component_at_inlet(self):
        return not self.lightbarrier_position_inlet.state()
        
    def wait_for_component_at_inlet(self):
        if not self.is_component_at_inlet():
            print("please place a component at inlet")
        while not self.is_component_at_inlet():
            time.sleep(0.02)
        return(self.read_id())
        
    def read_id(self):
        # in the long run: this is the place for camera id reading functions. 
        # at the moment, just give back a hardcoded number
        return 42
        
    def wait_for_free_inlet(self):
        while self.is_component_at_inlet():
            # time.sleep(0.02)
            time.sleep(1)
            print("please remove component from inlet")
            
    def panic(self): # stop all hardware outputs and motors
        print("PANIC: stopping all hardware outputs")
        self.motor_conveyor_belt.stop()
        self.motor_tool_changer_rotation.stop()
        self.motor_tool_drive.stop()
        self.motor_tool_vertical.stop()
        print("PANIC: stopped all hardware outputs successfully")
                    
    
    # NOTE: The following motor control functions are rather basic and for most use cases not to be used for control implementations any longer. For those purposes, consider using new Kinematic classes that provide more advanced logic. 
    # Anyway, at least for rather low-level debug purposes, these functions are kept in here anyway and may be used if appropriate.
    
    def drive_motor_distance(self, mot_identifier, distance, speed=512):
        """
        Moves a ftrobopy.motor object by the given distance in the given direction. 
        mot_identifier is the motor object (note: to be called together with its "self."-prefix)
        distance is 1. the distance value and 2. the direction: positive numbers mean setSpeed(512), negative numbers do setSpeed(-512) 
        Update: Now the speed value can *optionally* be given as well. Default is 512.
        """
        if distance == 0:
            raise "motor_distance: distance value cannot be 0"
        mot_identifier.setDistance(abs(distance), syncto=None)
        if distance > 0:
            mot_identifier.setSpeed(speed)
        else:
            mot_identifier.setSpeed(-speed)        
        while not mot_identifier.finished() == True:
            time.sleep(0.01)
        mot_identifier.stop()
        
    def drive_motor_distance_watch_end_stop(self, mot_identifier, distance, end_stop):
        """
        Moves a ftrobopy.motor object by the given distance in the given direction. 
        mot_identifier is the motor object (note: to be called together with its "self."-prefix)
        distance is 1. the distance value and 2. the direction: positive numbers mean setSpeed(512), negative numbers do setSpeed(-512) 
        Watches for the given end_stop not to be driven over.
        """
        if distance == 0:
            raise "motor_distance: distance value cannot be 0"
        if not end_stop.state() == True:
            mot_identifier.setDistance(abs(distance), syncto=None)
            if distance > 0:
                mot_identifier.setSpeed(512)
            else:
                mot_identifier.setSpeed(-512)        
            while not mot_identifier.finished() and not end_stop.state() == True:
                time.sleep(0.01)
            mot_identifier.stop()
            