#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for drilling cell; head file. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from flecsimo.plc.drilling_cell.hw_drilling_cell import HW
# import time
import random


class Plc_drilling_cell():
    def __init__(self):
        
        self.hw = HW(self)
        
    def initialize(self):
        return(self.hw.initialize())
        
    def setup(self, config_value=1):
        self.hw.setup(config_value)
        return True
    
    def wait_for_component_at_inlet(self):
        self.hw.wait_for_component_at_inlet()
    
    def wait_for_free_inlet(self):
        self.hw.wait_for_free_inlet()
    
    def instruction_process_one_component(self):
        self.hw.move_component_from_inlet_to_tool()
        self.hw.tool_usage_process()        
        self.hw.move_component_back_to_inlet()
        return 0
        # TODO integrate some tests in sub processes that might return 1 in case of errors
        
    

if __name__ == "__main__":
    plc = Plc_drilling_cell()
    plc.initialize()
    while True: # endless loop of production for offline hardware demonstration use
        plc.wait_for_component_at_inlet()
        plc.setup(random.choice([1, 2, 3]))
        plc.instruction_process_one_component()
        plc.wait_for_free_inlet()
    
    


