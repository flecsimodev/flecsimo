#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for welding cell; hardware abstraction. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ftrobopy
import time
# from Kinematics import Encoder            # not needed for this cell


class HW():
    
    """
    ftrobopy initializing, 
    hardware state variables etc
    """
    
    def __init__(self, parent):

        try:
            self.txt = ftrobopy.ftrobopy("auto")            # connect to TXT's IO controller
        except:
            self.txt = None                                 # set TXT to "None" of connection failed

        if not self.txt:
            print("HW: Error connecting IO server")                                    
        else:
            print("HW: Connection to TXT's IO Controller seems to work")    # print confirmation for ssh console output
            
            # txt connections
            self.motor_conveyor_belt = self.txt.motor(1)                # M1
            self.motor_welding_robot = self.txt.motor(2)                # M2
            self.lamp_welding_nozzle = self.txt.output(5)               # O5
            self.lamp_finishing = self.txt.output(6)                    # O6
            self.valve_finishing_doors = self.txt.output(7)             # O7
            # self.valve_cache_pusher = self.txt.output(8)                # O8
            
            self.lightbarrier_position_inlet = self.txt.input(1)        # I1
            self.lightbarrier_position_welding = self.txt.input(2)      # I2
            self.lightbarrier_position_finishing = self.txt.input(3)    # I3
            self.button_welding_robot_bottom_end = self.txt.input(4)    # I4
            self.button_welding_robot_upper_end = self.txt.input(5)     # I5
                       
        self.parent = parent
        
        # self.cache_active = 0
        
        self.geometry = 'A' # can be 'A', 'B' or 'C'
        self.setuptime = 1  # on physical models ignored for now
        self.color = 'red'  # can be 'red' or 'blue'
        

    def move_component_back_to_inlet(self):
        self.motor_conveyor_belt.setSpeed(512)      # move back to lightbarrier at inlet position
        while self.lightbarrier_position_welding.state() == 1:
            time.sleep(0.02)
        self.finishing_close_doors()                # close doors as soon as the component passed welding position
        while self.lightbarrier_position_inlet.state() == 1:
            time.sleep(0.02)
        self.motor_conveyor_belt.stop()
        # self.drive_motor_distance(self.motor_conveyor_belt, 65, 360)
    
    def move_component_from_inlet_to_welding(self): 
        self.motor_conveyor_belt.setSpeed(-512)     # move til lightbarrier at welding position
        while self.lightbarrier_position_welding.state() == 1:
            time.sleep(0.02)
        # self.motor_conveyor_belt.stop()             # only stopping for debug observation, can be removed later
        # time.sleep(1)
        self.drive_motor_distance(self.motor_conveyor_belt, -85, 360)   # slowly move (step-counted) under welding robot to stop centered
        time.sleep(1)
    
    def move_component_to_finishing(self):  # does NOT include doors
        self.motor_conveyor_belt.setSpeed(-512)
        while self.lightbarrier_position_finishing.state() == 1:
            time.sleep(0.02)
        # self.motor_conveyor_belt.stop()
        # time.sleep(1)
        self.drive_motor_distance(self.motor_conveyor_belt, -65, 360)
    
    def finishing_open_doors(self):
        self.valve_finishing_doors.setLevel(512)
    
    def finishing_close_doors(self):
        self.valve_finishing_doors.setLevel(0)
    
    def finishing_process(self):
        self.lamp_finishing.setLevel(512)
        time.sleep(0.3)
        self.lamp_finishing.setLevel(0)
        time.sleep(0.3)
        if self.color == 'red': # do some variation based on color
            for x in range(3):
                self.lamp_finishing.setLevel(512)
                time.sleep(0.4)
                self.lamp_finishing.setLevel(0)
                time.sleep(0.4)
        elif self.color == 'blue': # do some variation based on color
            for x in range(2):
                self.lamp_finishing.setLevel(420)
                time.sleep(0.7)
                self.lamp_finishing.setLevel(0)
                time.sleep(0.2)
    
    def welding_process(self):
        self.welding_move_down()
        self.welding_nozzle()
        time.sleep(0.2)
        self.welding_move_up()        
        
    def welding_move_down(self):
        self.motor_welding_robot.setSpeed(-512)
        while self.button_welding_robot_bottom_end.state() == 1:
            time.sleep(0.02)
        self.motor_welding_robot.stop()
        
    def welding_move_up(self):
        self.motor_welding_robot.setSpeed(512)
        while self.button_welding_robot_upper_end.state() == 0:     # upper button is wired as closed by default
            time.sleep(0.02)
        self.motor_welding_robot.stop()        
    
    def welding_nozzle(self):
        for x in range(4):
            self.lamp_welding_nozzle.setLevel(512)
            time.sleep(0.03)
            self.lamp_welding_nozzle.setLevel(0)
            time.sleep(0.08)
        time.sleep(0.2)
        self.lamp_welding_nozzle.setLevel(512)
        time.sleep(0.07)
        self.lamp_welding_nozzle.setLevel(0)
        time.sleep(0.15)
        self.lamp_welding_nozzle.setLevel(512)
        time.sleep(0.12)
        self.lamp_welding_nozzle.setLevel(0)
        time.sleep(0.08)
        self.lamp_welding_nozzle.setLevel(512)
        time.sleep(0.04)
        self.lamp_welding_nozzle.setLevel(0)
        time.sleep(0.7)
        self.lamp_welding_nozzle.setLevel(512)
        time.sleep(0.62)
        self.lamp_welding_nozzle.setLevel(0)
        time.sleep(0.25)
        if self.geometry == 'B': # some variation based on geometry TODO perhaps invent something prettier
            variant = 2
        elif self.geometry == 'C':
            variant = 7
        else:
            variant = 4
        for x in range(variant):
            self.lamp_welding_nozzle.setLevel(512)
            time.sleep(0.02)
            self.lamp_welding_nozzle.setLevel(0)
            time.sleep(0.05)
        time.sleep(0.3)
    
    def initialize(self):
        print("running initialize...")
        self.finishing_close_doors()
        print("doors closed")
        if not self.button_welding_robot_upper_end.state() == 1:
            self.welding_move_up()
        print("welding robot at initial position")
        print("lightbarrier states:") 
        print("inlet: ", self.lightbarrier_position_inlet.state()) 
        print("welding: ", self.lightbarrier_position_welding.state())
        print("finishing: ", self.lightbarrier_position_finishing.state())
        return(self.is_ready_for_production())
        
    def is_ready_for_production(self):
        if self.lightbarrier_position_inlet.state() == 0 or self.lightbarrier_position_welding.state() == 0 or self.lightbarrier_position_finishing.state() == 0 or self.button_welding_robot_upper_end.state() == 0:
            return False
        else:
            return True
        
    def setup(self, parameter):
        self.geometry = parameter['parameter']['geom']
        print('geometry = ', self.geometry)
        self.setuptime = parameter['parameter']['setup']
        print('setuptime = ', self.setuptime)
        self.color = parameter['parameter']['col']
        print('color = ', self.color)
        
    def is_component_at_inlet(self):
        # print('is component at inlet? : ', (not self.lightbarrier_position_inlet.state()))
        return not self.lightbarrier_position_inlet.state()
        
    def wait_for_component_at_inlet(self):
        if not self.is_component_at_inlet():
            print("please place a component at inlet")
        while not self.is_component_at_inlet():
            time.sleep(0.02)
        return(self.read_id())
        
    def read_id(self):
        # in the long run: this is the place for camera id reading functions. 
        # at the moment, just give back a hardcoded number
        return 0
        
    def wait_for_free_inlet(self):
        while self.is_component_at_inlet():
            # time.sleep(0.02)
            time.sleep(1)
            print("please remove component from inlet")
        self.is_component_at_inlet()
        # print('inlet free')
            
    def panic(self): # stop all hardware outputs and motors
        print("PANIC: stopping all hardware outputs")
        self.motor_conveyor_belt.stop()
        self.motor_welding_robot.stop()
        self.lamp_welding_nozzle.setLevel(0)
        self.lamp_finishing.setLevel(0)
        self.valve_finishing_doors.setLevel(0)
        # self.valve_cache_pusher.setLevel(0)
        print("PANIC: stopped all hardware outputs successfully")
                    
    
    # NOTE: The following motor control functions are rather basic and for most use cases not to be used for control implementations any longer. For those purposes, consider using new Kinematic classes that provide more advanced logic. 
    # Anyway, at least for rather low-level debug purposes, these functions are kept in here anyway and may be used if appropriate.
    
    def drive_motor_distance(self, mot_identifier, distance, speed=512):
        """
        Moves a ftrobopy.motor object by the given distance in the given direction. 
        mot_identifier is the motor object (note: to be called together with its "self."-prefix)
        distance is 1. the distance value and 2. the direction: positive numbers mean setSpeed(512), negative numbers do setSpeed(-512) 
        Update: Now the speed value can *optionally* be given as well. Default is 512.
        """
        if distance == 0:
            print("motor_distance: distance value cannot be 0")
            raise "motor_distance: distance value cannot be 0"
        refdistance = mot_identifier.getCurrentDistance()
        if distance > 0:
            mot_identifier.setSpeed(speed)
        else:
            mot_identifier.setSpeed(-speed)        
        while (mot_identifier.getCurrentDistance() - refdistance) < abs(distance): # workaround instead of using finished()
            time.sleep(0.01)
            # print('hwdmd still driving')
        mot_identifier.stop()
        
    def drive_motor_distance_watch_end_stop(self, mot_identifier, distance, end_stop):
        """
        Moves a ftrobopy.motor object by the given distance in the given direction. 
        mot_identifier is the motor object (note: to be called together with its "self."-prefix)
        distance is 1. the distance value and 2. the direction: positive numbers mean setSpeed(512), negative numbers do setSpeed(-512) 
        Watches for the given end_stop not to be driven over.
        """
        if distance == 0:
            raise "motor_distance: distance value cannot be 0"
        if not end_stop.state() == True:
            mot_identifier.setDistance(abs(distance), syncto=None)
            if distance > 0:
                mot_identifier.setSpeed(512)
            else:
                mot_identifier.setSpeed(-512)        
            while not mot_identifier.finished() and not end_stop.state() == True:
                time.sleep(0.01)
            mot_identifier.stop()
            
