#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for welding cell; head file. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from flecsimo.plc.welding_cell.hw_welding_cell import HW
#import time


class Plc_welding_cell():
    def __init__(self):
        
        self.hw = HW(self)
        

    def initialize(self):
        return(self.hw.initialize())
        
    def setup(self, parameter):
        self.hw.setup(parameter)
        return True
    
    def wait_for_component_at_inlet(self):
        # TODO: return recognized id
        # return self.hw.wait_for_component_at_inlet()
        return self.hw.wait_for_component_at_inlet()
    
    def wait_for_free_inlet(self):
        self.hw.wait_for_free_inlet()
    
    def instruction_process_one_component(self, parameter=[]):
        self.hw.move_component_from_inlet_to_welding()
        self.hw.welding_process()
        self.hw.finishing_open_doors()
        self.hw.move_component_to_finishing()
        self.hw.finishing_close_doors()
        self.hw.finishing_process()
        self.hw.finishing_open_doors()
        self.hw.move_component_back_to_inlet()
        return 0
        # TODO integrate some tests in sub processes that might return 1 in case of errors
        

if __name__ == "__main__":
    plc = Plc_welding_cell()
    plc.initialize()
    plc.wait_for_component_at_inlet()
    plc.instruction_process_one_component()
    
    


