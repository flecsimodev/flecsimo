#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Advanced Kinematics classes for abstraction of motors and end stops. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import time

# TO BE DEBUGGED (see notes below)


class Encoder():
    
    """
    Encoder motor or any motor with counted steps and one reset end stop at position 0. 
    Combines a ftrobopy motor object with its end stop to one logical unit. 
    At launch, a dict of position alias names and their value can be given, and the range end not to be driven over. 
    """
    
    def __init__(self, motor_identifier, end_stop_identifier, range_end, position_aliases={"HOME": 0}):
        
        self.motor = motor_identifier
        self.end_stop = end_stop_identifier
        
        self.positions = position_aliases  # future: check the input for correct type (dict) and reasonable values (>=0 and <range_end)
        self.range_end = range_end
        
        self._current_absolute_position = None
        
        self._target_position = None  # can be None, int or string for aliases defined in self.positions
        
        
    def move_Encoder_to(self, target_position, speed=512):
        """
        Moves an encoder motor (or similar) to an absolute position. 
        target_position can be either a number or a string defined at class launch via position_aliases (and then stored in self.positions). 
        Uses move_Encoder_relatively internally for actual movements.
        """
        if type(target_position) == str and target_position in self.positions:
            self._target_position = target_position  # still a string, to be interpreted into a number later on
        elif type(target_position) == int and target_position >= 0 and target_position < self.range_end:
            self._target_position = target_position
        else:
            raise TypeError
        
        if type(self._target_position) == str:
            target_position_coordinate = self.positions[self._target_position] # if self._target_position is a string, convert to a number; self._target_position itself is kept as a readable alias name
        else:
            target_position_coordinate = self._target_position
        
        if target_position_coordinate == 0:
            self.move_Encoder_to_reset()
        elif not self._current_absolute_position:
            print("current absolute position unknown, cannot move to absolute positions at the moment; please move to reset to solve")
        elif target_position_coordinate == self._current_absolute_position:
            print("already at target")
        else:
            distance = target_position_coordinate - self._current_absolute_position
            self.move_Encoder_relatively(distance, speed)

        self._target_position = None

    

    def move_Encoder_relatively(self, distance, speed=512):
        """
        Moves by the given distance in the given direction. 
        mot_identifier is the motor object (note: to be called together with its "self."-prefix)
        distance is 1. the distance value and 2. the direction: positive numbers mean setSpeed(512), negative numbers do setSpeed(-512) 
        end_stop is the switch not to be driven over
        also tries to keep self._current_absolute_position up to date
        """
        if distance == 0:
            raise "motor_distance: distance value cannot be 0"
        if not self.end_stop.state() == True and not self._current_absolute_position > self.range_end:      # BUG: don't check for end stop here in general, just later for negative directions
            if self._current_absolute_position:
                start_position = self._current_absolute_position
            self.motor.setDistance(abs(distance), syncto=None)
            if distance > 0:
                self.motor.setSpeed(speed)
                counter_direction = 1
            else:
                self.motor.setSpeed(speed * -1)
                counter_direction = -1        
            while not self.motor.finished() and not self.end_stop.state() == True and not self._current_absolute_position > self.range_end:
                time.sleep(0.01)
                if self._current_absolute_position:
                    self._current_absolute_position = start_position + counter_direction * self.motor.getCurrentDistance()
            self.motor.stop()
            if self._current_absolute_position:
                self._current_absolute_position = start_position + counter_direction * self.motor.getCurrentDistance()
        

    def move_Encoder_to_reset(self, speed=512):
        self._current_absolute_position = None
        if not self.end_stop.state() == 1: 
            self.motor.setSpeed(speed * -1)
            while not self.end_stop.state() == 1:
                time.sleep(0.01)
                # to test: is motor.getCurrentDistance() also counted if not using setDistance() before? If so, current_absolute_position can be counted in here as well
            self.motor.stop()
        self.motor.setDistance(0)
        self._current_absolute_position = 0
        # print(self.motor, "was reset")

    
    def get_Position(self):
        if self.end_stop.state() == True:
            self._current_absolute_position = 0
            self.motor.setDistance(0)
        if self._current_absolute_position:  # BUG: Position 0 returns "unknown" anyway - TODO. -> instead ask for " is not None: " ; also change all similar lines inside this file
            return self._current_absolute_position
        else:
            return "unknown"
            
       
    
    
    # to be added in the future: start-only, finish-only, check-if-ready movement functions to allow more than one movement at a time 
        
    
    
class Motor_with_2_end_stops():
    
    """
    Motor with an end stop on either end of the movement axis.  
    Combines a ftrobopy motor object with its two end stops to one logical unit. 
    At launch, a dict of alias names for the two end stos can be given, as well as a timeout for performing safety halts. [timeout not implemented yet] 
    """
    
    def __init__(self, motor_identifier, end_stop_1_identifier, end_stop_2_identifier, end_stop_aliases={"position_1": 1, "position_2": 2}, timeout=20):
        
        self.motor = motor_identifier
        self.end_stop_1 = end_stop_1_identifier
        self.end_stop_2 = end_stop_2_identifier
        
        self.positions = end_stop_aliases  # future: check the input for correct type (dict), no more than 2 entries and numbers 1 and 2; or rather use something else than dict here? 
        self.timeout = timeout  # not used so far
        
        self._target_position = None
        self._current_position = None
        
    
    def move_Motor_to(self, target_position, speed=512):
        """
        Moves the motor to either end stop given in target_position. 
        target_position can be either a number (1|2) or a string defined at class launch via end_stop_aliases (and then stored in self.positions). 
        """
        if type(target_position) == str and target_position in self.positions:
            self._target_position = target_position  # still a string, to be interpreted into a number later on
        elif target_position == 1 or target_position == 2: 
            self._target_position = target_position
        else:
            raise TypeError
        
        if type(self._target_position) == str:
            target_position_number = self.positions[self._target_position] # if self._target_position is a string, convert to a number; self._target_position itself is kept as a readable alias name
        else:
            target_position_number = self._target_position
        
        if target_position_number == self._current_absolute_position:
            print("already at target")
        elif target_position_number == 1:  # move to 1
            if not self.end_stop_1.state() == 1: 
                self.motor.setSpeed(speed * -1)
            while not self.end_stop_1.state() == 1:
                time.sleep(0.01)
            self.motor.stop()
        elif target_position_number == 2:  # move to 2
            if not self.end_stop_2.state() == 1: 
                self.motor.setSpeed(speed)
            while not self.end_stop_2.state() == 1:
                time.sleep(0.01)
            self.motor.stop()

        self.get_Position()
        self._target_position = None
    
    
    
    def get_Position(self):
        """
        updates self._current_position and returns same value
        """
        if self.end_stop_1.state() == True and self.end_stop_2.state() == False:
            self._current_position = 1
        elif self.end_stop_1.state() == True and self.end_stop_2.state() == True:
            self._current_position = "error"
        elif self.end_stop_2.state() == True:
            self._current_position = 2
        else:
            self._current_position = "in_between"
        return self._current_position
        
    
    
    # in future: actually use timeout function to not drive too far in emergency cases
    
    