#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Camera processing for QR code reading and colour interpreting. 

@author: Leon Schnieber
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import time
import numpy as np
import cv2
from PIL import Image
import zbarlight

# import matplotlib.pyplot as plt # needed for debug-purposes


class QRCodeCameraReader():
    """
    function-wrapper to process e.g. QR-Codes with the TXT-Camera.
    Please note: The TXT-Controller can't use the full resolution of the camera.
    Check manually, if the Pixels in the QR-Codes can be distinguished from each
    other in the downscaled camera image.
    """

    def __init__(self, cam_id=None):
        """
        Initialize the camera and required libraries

        Args:
        cam_id (int): Linux Camera ID to use as the camera-input. Defaults to zero.
        """
        self.__camera_device_ID = 0 if cam_id == None else int(cam_id)
        # further settings see https://docs.opencv.org/master/d4/d15/group__videoio__flags__base.html#gaeb8dd9c89c10a5c63c139bf7c4f5704d
        width = 320
        height = 240
        # Please note: The TXT-Controller can't handle full-size frames sent by
        # the camera so be careful when cranking up the resolution.
        self.__camera_settings = {
            cv2.CAP_PROP_FRAME_WIDTH: width,
            cv2.CAP_PROP_FRAME_HEIGHT: height,
            # cv2.CAP_PROP_FPS: 10
        }

        # Use the frame_crop-Settings to save cpu-time by anaylzing only rele-
        # vant areas in the frame.
        self.frame_crop = {
            "top": 0,
            "bottom": 34,
            "left": 37,
            "right": 40
        }

        # Please note: Coordinates written in the calibration-Area refer to the
        # cropped frame, not to the original one.
        self.calibrationArea = {
            "topLeftX": 210,
            "topLeftY": 9,
            "bottomRightX": 238,
            "bottomRightY": 88
        }

        # Color-Thresholds for the three colors to be detected.
        # Please use the following code-snippet to recalibrate the Color-thresholds
        # https://github.com/opencv/opencv/tree/master/samples/python/tutorial_code/Histograms_Matching/histogram_calculation/calcHist_Demo.py
        # orientation help for values: https://github.com/harbaum/cfw-apps/blob/b08d805123ad9bae0fc4c46627125ef8fc771458/packages/cube/scanner.py#L38
        self.color_thresholds = {
            "red1": {
                "lower": (0, 133, 127),
                "upper": (11, 255, 255)
            },
            "red2": {
                "lower": (248, 133, 127),
                "upper": (255, 255, 255)
            },
            "white1": {
                "lower": (0, 56, 140),
                "upper": (21, 97, 196)
            },
            "white2": {
                "lower": (252, 56, 140),
                "upper": (255, 97, 196)
            },
            # "white3": {
            #     "lower": (209, 0, 245),
            #     "upper": (255, 20, 255)
            # },
            # "white4": {
            #     "lower": (0, 0, 250),
            #     "upper": (255, 3, 255)
            # },
            "blue": {
                "lower": (128, 89, 51),
                "upper": (178, 255, 255)
            }
        }
        self.color_analyzing_kernel = np.ones((5, 5), np.uint8)

        self.__camera_instance = cv2.VideoCapture(self.__camera_device_ID)
        # set camera settings
        if self.__camera_instance.isOpened():
            for config_option in self.__camera_settings:
                self.__camera_instance.set(config_option, self.__camera_settings[config_option])
        else:
            raise ConnectionError("The configured Camera (Port {0}) can't be opened. \
                Please check the connection or your configuration.".format(self.__camera_device_ID))

    def __getFrame(self):
        """
        returns a frame as soon as openCV got a complete (and correct) frame from
        the webcam.
        """
        ret = False
        while not ret:
            ret, frame = self.__camera_instance.read()
            time.sleep(0.01)
        return frame[
                self.frame_crop["top"]:-max(1, self.frame_crop["bottom"]),
                self.frame_crop["left"]:-max(1, self.frame_crop["right"]), :
                ]

    def __getCalibrationColor(self, frame):
        """
        Uses the image given via the frame-parameter and calculate the correction-
        gains to white-balance the frame according to the pixels in the configured
        calibration-area.

        Args:
            frame (openCV-Frame): Camera-Image to be processed
        Returns:
            r_gain, g_gain, b_gain (float): Gain-Values for each color-channel
            (Red, Green and Blue)
        """
        # extract the calibration area out of the frame
        calibration_area_pixels = frame[
            self.calibrationArea["topLeftY"]:self.calibrationArea["bottomRightY"],
            self.calibrationArea["topLeftX"]:self.calibrationArea["bottomRightX"]
        ]
        # split the color-layers and get the mean of each of those layers
        r_pixels, g_pixels, b_pixels = cv2.split(calibration_area_pixels)

        r_med = cv2.mean(r_pixels)[0]
        g_med = cv2.mean(g_pixels)[0]
        b_med = cv2.mean(b_pixels)[0]

        # calculate the gains for each channel
        k_center = (r_med + g_med + b_med) / 3
        r_gain = k_center / r_med
        g_gain = k_center / g_med
        b_gain = k_center / b_med

        return r_gain, g_gain, b_gain

    def __colorBalanceFrame(self, frame):
        r_frame, g_frame, b_frame = cv2.split(frame)
        gain_r, gain_g, gain_b = self.__getCalibrationColor(frame)

        r_balanced = cv2.addWeighted(src1=r_frame, alpha=gain_r, src2=0, beta=0, gamma=0)
        g_balanced = cv2.addWeighted(src1=g_frame, alpha=gain_g, src2=0, beta=0, gamma=0)
        b_balanced = cv2.addWeighted(src1=b_frame, alpha=gain_b, src2=0, beta=0, gamma=0)
        return cv2.merge([r_balanced, g_balanced, b_balanced])

    def detectCode(self, frame=None):
        """
        Waits for the next camera frame to arrive and feeds it after some
        preprocessing into the zbar-detection-system.

        Args:
        frame (openCV-Frame): Optional: Camera frame data (e.g. to use the same
        frame for multiple analyzing functions)

        Returns:
        list: List of strings. Each string represents a detected code.
        """
        codes_detected = []
        if frame is None:
            frame = self.__getFrame()

        # frame_height, frame_width, frame_pixel_depth = frame.shape

        frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        frame_raw = Image.fromarray(frame_gray)
        codes_detected_raw = zbarlight.scan_codes("qrcode", frame_raw)
        if codes_detected_raw != None:
            for code in codes_detected_raw: # convert bytes to string
                codes_detected.append(code.decode())
        return codes_detected

    def __chooseColorName(self, percentage_dict):
        percentage_dict["red"] = percentage_dict["red1"] + percentage_dict["red2"]
        del percentage_dict["red1"]
        del percentage_dict["red2"]
        percentage_dict["white"] = percentage_dict["white1"] + percentage_dict["white2"]
        # + percentage_dict["white3"] + percentage_dict["white4"]
        del percentage_dict["white1"]
        del percentage_dict["white2"]
        # del percentage_dict["white3"]
        # del percentage_dict["white4"]
        key_list = list(percentage_dict.keys())
        value_list = list(percentage_dict.values())
        index_maximum = value_list.index(max(value_list))
        print(percentage_dict)
        return key_list[index_maximum]

    def detectColor(self, min_radius=0, max_radius=10000, timeout=1, frame=None, balance_white=True):
        """
        Tries to examine the color of the workpiece by detecting circles in
        the frame and rendering a color based histogram from the pixels inside
        the circle.

        Args:
        min_radius (int): To strengthen the detection algorithm against misdetections
            a minimal radius can be set so detected workpieces smaller than that
            threshold are disqualified for further analysis. Defaults to zero.
        max_radius (int): See min_radius - but this time the upper limit is controlled.
            Defaults to 10000.
        balance_white (bool): If enabled, a "white" reference-Area must be defined.
        The algorithm tries to color balance the original frame via the
        __colorBalanceFrame-Routine.

        timeout (float): Time in seconds before function resignates finding a color.

        Returns:
        tuple: Tuple of average H, S and V-Values of the workpiece-surface.
        """
        # Create different frames for the processing
        found_frame = False
        timeout_stamp = time.time() + timeout
        while found_frame == False and timeout_stamp > time.time():
            if frame is None:
                frame = self.__getFrame()
            # cv2.imwrite("test0.png", frame)

            if balance_white:
                frame = self.__colorBalanceFrame(frame)

            frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

            circle_mask_frame = np.zeros(frame.shape[0:2], dtype=np.uint8)

            # detect circles in frame_gray and flip the according bits in circle_mask_frame
            circles_raw = cv2.HoughCircles(
                frame_gray,
                cv2.HOUGH_GRADIENT, # detection method. Alternative cv2.HOUGH_GRADIENT
                1, # resolution-ratio. The bigger, the smaller the resolution used for calculation
                minDist=10, # minimum distance between the detected circle-centers
                param2=0.9, # how "circly" the circles need to be. Maximum is 1 for perfect circles.
                minRadius=min_radius, maxRadius=max_radius
                )
            if type(circles_raw) != type(None):
                circles_raw = np.round(circles_raw[0, :]).astype("int")
                x, y, r = circles_raw[0]
                cv2.circle(circle_mask_frame, (x, y), r, (255, 255, 255), -1)

                # extract the masked bits out of the original frame
                frame_masked = cv2.bitwise_or(frame, frame, mask=circle_mask_frame)
                frame_masked_hsv = cv2.cvtColor(frame_masked, cv2.COLOR_BGR2HSV)
                # cv2.imwrite("test1.png", frame)
                # cv2.imwrite("test2.png", frame_masked_hsv) 
                # Sum pixels in masked area up and average them out
                filled_pixels = np.sum(circle_mask_frame)
                matches = {}
                for color_name in self.color_thresholds:
                    color_thresh = self.color_thresholds[color_name]
                    mask = cv2.inRange(frame_masked_hsv, color_thresh["lower"], color_thresh["upper"])
                    # mask = cv2.erode(mask, self.color_analyzing_kernel)
                    # frame_colour = cv2.bitwise_or(frame, frame, mask = mask)
                    # cv2.imwrite("test3.png", frame_colour)
                    matches[color_name] = np.sum(mask) / (filled_pixels)
                return self.__chooseColorName(matches)
        raise UserWarning("detectColor could not find a workpiece in time.")
