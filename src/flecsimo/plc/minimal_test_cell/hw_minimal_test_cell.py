#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for minimal test cell; hardware abstraction. 
Intended to be used as a minimal yet fully functional dummy testing setup rather than an ordinary cell. 
Uses coloursorting robot as hardware. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ftrobopy
import time
# from Kinematics import Encoder            # not needed for this cell


class HW():
    
    """
    ftrobopy initializing, 
    hardware state variables etc
    """
    
    def __init__(self, parent):

        try:
            self.txt = ftrobopy.ftrobopy("auto")            # connect to TXT's IO controller
        except:
            self.txt = None                                 # set TXT to "None" of connection failed

        if not self.txt:
            print("HW: Error connecting IO server")                                    
        else:
            print("HW: Connection to TXT's IO Controller seems to work")    # print confirmation for ssh console output
            
            # txt connections
            self.motor_cranerotation = self.txt.motor(1)                # motor at M1
            self.compressor = self.txt.output(3)                        # compressor at O3
            self.valve_suction = self.txt.output(4)                     # magnetic valve for vacuum gripper at O4
            self.valve_cranearmdown = self.txt.output(5)                # magnetic valve at O5
            self.lamp_lightbarrier = self.txt.output(6)                 # lamp at O6
            
            self.button_pos0 = self.txt.input(1)                        # pushbutton for position 0 reference at I1
            self.phototransistor_lightbarrier = self.txt.input(2)       # photo transistor at I2
            self.coloursensor = self.txt.colorsensor(3)                 # color sensor at I3
                       
        self.parent = parent
        
        self.compressor_status = 0  # to be only set by the start|stop_compressor functions (and reset, initialize)
        self.compressor_demand = 0  # incremented in start_compressor and decremented at the end of stop_compressor, resetted in initialize
        
        self.cranerotation_speed = 384      # speed value used for motor_cranerotation.setSpeed(), 1-512 possible
        
        
    def start_compressor(self):
        self.compressor_demand += 1
        if self.compressor_status == 0:
            self.compressor.setLevel(512)
            time.sleep(2)
            self.compressor_status = 1
    
    def stop_compressor(self):
        self.compressor_demand -= 1
        if self.compressor_demand == 0:
            self.compressor.setLevel(0)
            self.compressor_status = 0
    
    def crane_down(self):
        self.start_compressor()
        self.valve_cranearmdown.setLevel(512)
        time.sleep(1)
                
    def crane_up(self):
        self.valve_cranearmdown.setLevel(0)
        time.sleep(0.5)
        self.stop_compressor()
    
    def pick_part(self):
        self.start_compressor()
        self.valve_suction.setLevel(512)
        time.sleep(0.5)
        
    def release_part(self):
        self.valve_suction.setLevel(0)
        time.sleep(0.3)
        self.stop_compressor()
    
    def pick_process(self):
        self.crane_down()
        self.pick_part()
        self.crane_up()
        
    def release_process(self):
        self.crane_down()
        self.release_part()
        self.crane_up()
    
    def crane_to_colour_detect(self):   # move crane from pos0 to colour_detection
        self.drive_motor_distance(self.motor_cranerotation, -3, self.cranerotation_speed)
    
    def reset_crane(self):  # move crane from anywhere to pos0
        if not self.button_pos0.state() == 1: 
            self.motor_cranerotation.setSpeed(self.cranerotation_speed)
            while not self.button_pos0.state() == 1:
                time.sleep(0.01)
            self.motor_cranerotation.stop()
            self.motor_cranerotation.setDistance(0)
    
    def initialize(self): 
        print("running initialize...")
        self.motor_cranerotation.stop()
        self.release_part()
        self.crane_up()
        self.compressor.setLevel(0)
        self.compressor_status = 0
        self.compressor_demand = 0
        self.reset_crane()
        self.lamp_lightbarrier.setLevel(512)     # switch light of lightbarrier on
        time.sleep(0.4)
        print("initializing done")
        return(self.is_ready_for_production())
        
    def is_ready_for_production(self):
        if self.phototransistor_lightbarrier.state() == 0 or self.button_pos0.state() == 0:
            return False
        else:
            return True
        
    # def setup(self, config_value):            # moved to main class
    #     self.setup_value = config_value    
        
    def is_component_at_inlet(self):
        return not self.phototransistor_lightbarrier.state()
        
    def wait_for_component_at_inlet(self):
        if not self.is_component_at_inlet():
            print("please place a component at inlet")
        while not self.is_component_at_inlet():
            time.sleep(0.02)
        return(self.read_id())
        
    def read_id(self):
        # in the long run: this is the place for camera id reading functions. 
        # at the moment, just give back a hardcoded number
        return 42
        
    def wait_for_free_inlet(self):
        while self.is_component_at_inlet():
            # time.sleep(0.02)
            time.sleep(1)
            print("please remove component from inlet")
            
    def panic(self): # stop all hardware outputs and motors
        print("PANIC: stopping all hardware outputs")
        self.motor_cranerotation.stop()
        self.compressor.setLevel(0)
        self.compressor_status = 0
        self.valve_suction.setLevel(0)
        self.valve_cranearmdown.setLevel(0)
        print("PANIC: stopped all hardware outputs successfully")
                    
    
    # NOTE: The following motor control functions are rather basic and for most use cases not to be used for control implementations any longer. For those purposes, consider using new Kinematic classes that provide more advanced logic. 
    # Anyway, at least for rather low-level debug purposes, these functions are kept in here anyway and may be used if appropriate.
    
    def drive_motor_distance(self, mot_identifier, distance, speed=512):
        """
        Moves a ftrobopy.motor object by the given distance in the given direction. 
        mot_identifier is the motor object (note: to be called together with its "self."-prefix)
        distance is 1. the distance value and 2. the direction: positive numbers mean setSpeed(512), negative numbers do setSpeed(-512) 
        Update: Now the speed value can *optionally* be given as well. Default is 512.
        """
        if distance == 0:
            raise "motor_distance: distance value cannot be 0"
        mot_identifier.setDistance(abs(distance), syncto=None)
        if distance > 0:
            mot_identifier.setSpeed(speed)
        else:
            mot_identifier.setSpeed(-speed)        
        while not mot_identifier.finished() == True:
            time.sleep(0.01)
        mot_identifier.stop()
        
    def drive_motor_distance_watch_end_stop(self, mot_identifier, distance, end_stop):
        """
        Moves a ftrobopy.motor object by the given distance in the given direction. 
        mot_identifier is the motor object (note: to be called together with its "self."-prefix)
        distance is 1. the distance value and 2. the direction: positive numbers mean setSpeed(512), negative numbers do setSpeed(-512) 
        Watches for the given end_stop not to be driven over.
        """
        if distance == 0:
            raise "motor_distance: distance value cannot be 0"
        if not end_stop.state() == True:
            mot_identifier.setDistance(abs(distance), syncto=None)
            if distance > 0:
                mot_identifier.setSpeed(512)
            else:
                mot_identifier.setSpeed(-512)        
            while not mot_identifier.finished() and not end_stop.state() == True:
                time.sleep(0.01)
            mot_identifier.stop()
            