#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""PLC for minimal test cell; head file. 
Intended to be used as a minimal yet fully functional dummy testing setup rather than an ordinary cell. 
Uses coloursorting robot as hardware. 

@author: Bernhard Lehner
"""

""" 
    Copyright and License Notice:

    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


from flecsimo.plc.minimal_test_cell.hw_minimal_test_cell import HW
import time


class Plc_minimal_test_cell():
    def __init__(self):
        
        self.hw = HW(self)
        
        self.setup_value = 1     # for the time being, this is enough just as a stored number. for e.g. drilling tool changes, advanced logic needs to be introduced. (current, requested, changing)  

        

    def initialize(self):
        return(self.hw.initialize())
        
    def setup(self, config_value=1):
        self.setup_value = config_value
        return True
    
    def wait_for_component_at_inlet(self):
        self.hw.wait_for_component_at_inlet()
    
    def wait_for_free_inlet(self):
        self.hw.wait_for_free_inlet()
    
    def instruction_process_one_component(self):
        self.hw.pick_process()
        self.hw.crane_to_colour_detect()
        self.hw.release_process()
        time.sleep(self.setup_value)    # simulate process variation by wasting time depending on setup
        self.hw.pick_process()
        self.hw.reset_crane()
        self.hw.release_process()
        return 0
        # TODO integrate some tests in sub processes that might return 1 in case of errors
        
        


    

if __name__ == "__main__":
    plc = Plc_minimal_test_cell()
    plc.initialize()
    plc.wait_for_component_at_inlet()
    plc.instruction_process_one_component()
    plc.wait_for_free_inlet()
    
    


