"""
Created on 07.09.2021 from callback module, first created on Created on 22.06.2020

@author: Ralf Banning
 
Copyright and License Notice:

    flecsimo statusmessaging module
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import logging
from datetime import datetime

from flecsimo.base import dao
from flecsimo.base import msg
from flecsimo.base.states import ScheduleStates, StationStates

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())


class StatusMessageHandler:
    """ Class defining callback methods, handling flecsimo status messages.
    
    This class initializes all required data access objects and provides message
    handler methods (implementing the mqtt callback interface) for the following 
    process messages:
        enrol: enrolement of an area or cell.
        opstat: status report of for operations
        quit: announcement of end of operation of area or cell.
        reading: meter reading data.
        status: status report for area or cell.
    
    Args:
        conn (sqlite3.Connection): connection handler for sqlite database.
        
    Attributes:
        Instances of DataAccess to operate on the database.
    """

    def __init__(self, database, environment):
        self.database = database
        self.dao = dao.DataAccess(self.database)
        self.env = environment
        self.sender = self.env['settings']['sender']
        self.site = self.env['site']
        self.org = self.env['settings']['org']
        self.supplier = self.env[self.org]

    def _update_schedule(self, site, sfcu, operation, supplier, status):
        """Updateing schedules."""
        self.dao.change('schedule',
                         data={'state': status.value,
                               'statx': status.name,
                               'at': datetime.utcnow()},
                         conditions={'site': site,
                                     'sfcu': sfcu,
                                     'operation': operation,
                                     'supplier': supplier})

    def on_enlist(self, client, userdata, message):
        """Callback handler for 'enlist' messages.
        
        Register facility at next level.
        
        TODO: implement 'on_enlist' message handler."""
        pass

    def on_opstat(self, client, userdata, message):
        """Callback handler for 'opstat' messages.
        
        Updates schedules based on opstat messsages issued at area or cell level.
        """
        opstat_msg = msg.as_dict(message.topic, message.payload)

        op_statx = opstat_msg['statx']

        status_map = {StationStates.ACTIVE: ScheduleStates.WIP,
                      StationStates.DONE: ScheduleStates.DONE}

        try:
            status = status_map[op_statx]
            site = opstat_msg['site']
            sfcu = opstat_msg['sfcu']
            operation = opstat_msg['operation']
            supplier = opstat_msg['supplier']

            self.dao.change('schedule',
                             data={'state': status.value,
                                   'statx': status.name,
                                   'at': datetime.utcnow()},
                             conditions={'site': site,
                                         'sfcu': sfcu,
                                         'operation': operation,
                                         'supplier': supplier})
        except KeyError:
            pass

    def on_delist(self, client, userdata, message):
        """Callback handler for 'delist' messages.
        
        Deregister facility from listing.
        
        TODO: implement 'on_delist' message handler.
        """
        pass

    def on_reading(self, client, userdata, message):
        """Callback handler for 'reading' messages.
                
        Stores or propagates readings from stations in a time-series db.
        
        TODO: implement 'on_reading' message handler.
        """
        pass

    def om_status(self, client, userdata, message):
        """Callback handler for general status messages"""
        pass

    def on_station_status(self, client, userdata, message):
        """Callback handler for 'status' messages.
                
        Stores status information from facilities.        
        """
        status_msg = msg.as_dict(message.topic, message.payload)

        station_status = status_msg['statx']
        sfcu = status_msg['sfcu']
        mhu = status_msg['mhu']
        operation = status_msg['operation']

        if sfcu != 0:
            if station_status in (StationStates.ACTIVE, StationStates.DONE):
                opstat_msg = msg.Opstat(self.sender,
                                        self.site,
                                        sfcu,
                                        mhu,
                                        operation,
                                        self.supplier,
                                        self.org,
                                        station_status)
                self._publish_opstat(opstat_msg)

