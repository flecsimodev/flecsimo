"""
Created on 07.09.2021 from callback module, first created on Created on 22.06.2020

@author: Ralf Banning
 
Copyright and License Notice:

    flecsimo responsehandling module
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import logging

from flecsimo.base import msg
from flecsimo.base import dao

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())


class ResponseMessageHandler:
    """ Class defining callback methods, handling flecsimo response messages.
    
    This class initializes all required data access objects and provides message
    handler methods (implementing the mqtt callback interface) for the following 
    process messages:
        opcfm: confirmation for successfully received opdta.
        quote: offer for execution in response to rfq message.
    
    Args:
        conn (sqlite3.Connection): connection handler for sqlite database.
        
    Attributes:
        Instances of DataAccess to operate on the database.
    """

    def __init__(self, database, mode='AUTO'):
        self.database = database
        self.mode = mode
        self.dao = dao.DataAccess(self.database)

    def _auto_quote_processing(self, sender, site, order, sfcu):
        """Interface definition for automatic quote handling."""
        pass

    def on_opcfm(self, client, userdata, message):
        """Callback handler for 'opcfm' messages.
        
        Updates opstat send status.

        TODO: implement 'on_opcfm' message handler.
        TODO: implement logic to assign not before confirmed opstat.        
        """
        pass

    def on_quote(self, client, userdata, message):
        """Persist quotations and process them if run in AUTO mode."""
        quote_msg = msg.as_dict(message.topic, message.payload)

        _log.info("\nReceived quotation: %s", quote_msg)

        try:
            site = quote_msg['site']
            order = quote_msg['order']
            supplier = quote_msg['supplier']
            # TODO: this is risky...(holds only for a cell quote):
            sfcu = quote_msg['sfcu']
            # TODO: ckeck if operation is needed any longer.
            # operation = quote_msg['spec']
        except BaseException:
            # TODO: exception handling for missing data.
            raise

        self.dao.persist('quotation', quote_msg)

        # Immediate accept quotation in auto mode and send opdta
        if self.mode == 'AUTO':
            self._auto_quote_processing(site, order, sfcu, supplier)
