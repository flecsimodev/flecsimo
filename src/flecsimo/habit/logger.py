"""
Created on 07.09.2021 from callback module, first created on Created on 22.06.2020

@author: Ralf Banning
 
Copyright and License Notice:

flecsimo logger module
Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
of Applied Sciences.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import logging


_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())

class LogHandler:
    """Callback handler for logdta messages"""
    def __init__(self):
        pass


    def on_logdta(self, client, userdata, message):
        """Callback handler for 'logdta' messages.
        
        Stores or propagates log information to logging databases.

        TODO: implement 'on_logdta' message handler.
        """
        pass
