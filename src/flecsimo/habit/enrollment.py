"""
Created on 30.05.2020

@author: Ralf Banning

Copyright and License Notice:

    flecsimo enrollment module
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
# from flecsimo.base import msg


class EnrolMixin:
    """Mixin class for enrollment of facilites to next upper level."""

    def enrol(self):
        """Let a cell join the area controller facilities.

        Args: 
            topic (str): Topic string.
            payload (str): Message payload in json format.        

        TODO: decide if cfmenrol is used or on publish should be
              enough to be evaluated (fire and forget or two-phase..)

        Scope: Cells only
        """

        # TODO: Scope problem: we would need msg here, but it already loaded in mixin...
        #=======================================================================
        # enrol = msg.Ernol(self.sender, self.org, self.cmpt, desc=None, loc=None, oplist=None)
        #
        # publish.single(enrol.topic, enrol.payload, hostname=self._broker,
        #                client_id=self._cid, keepalive=10)
        #=======================================================================
        pass

    def quit(self):
        """Announce quit of cell operation to area control.

        TODO: decide if cfmquit is used or on publish should be
              enough to be evaluated (fire and forget or two-phase..)

        Scope: Cells only
        """

        # TODO: Scope problem: we would need msg here, but it already loaded in mixin...
        #=======================================================================
        # quit = msg.Quit(self.sender)
        #
        # publish.single(topic, payload, hostname=self.MQTT_IP,
        #                client_id=self._cid)
        #=======================================================================
        pass
