#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""
Created on 09.09.2021 from roles.controller module, first created on 25.05.2020

@author: Ralf Banning

Copyright and License Notice:

flecsimo orderprocessing module
Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University
of Applied Sciences.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import datetime
import logging

from flecsimo.base import dao
from flecsimo.base.states import OrderStates, SfcuStates

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())

MAX_ORDER_QTY = 10  # maximum number of items for a new order 


class NoResultException(Exception):
    """Exception thrown if an SQL selection return no rows unexpectedly."""
    pass


class OrderProcessor:
    """Habits for order processing.

    This class contains the methods for
    - order creation
    - planning the order execution (which party 'does it')
    - release of orders to a designated party.
    - additional services and data checker methods

    Args:
        database(str) a path-like string to agents database.
    """

    def __init__(self, database):
        self.database = database
        self.dao = dao.DataAccess(database)
        self.max_order_qty = MAX_ORDER_QTY
        
    def get_products(self):
        """Get a list of all finished materials."""
        result = self.dao.retrieve('material',
                                   columns=['id', 'desc', 'unit'],
                                   conditions={'typ': 'FINISH'})  
        return [dict(_) for _ in result]

    def check_routing(self, material):
        """Check integrity and completeness of order routing data.
        
        The routing describes the set and sequence of operations that has to be 
        executed when manufacturing a product or material. In flecsimo this 
        information is stored in a 'production data structure' (pds) and is used
        during the planning process. 
        
        The purpose of this method is to check the integrity of the pds data for
        a given material and to return a report about the pds status and missing
        order data which will be required for the production planning process.  
        
        Args:
            material (str): the material number.
            
        Returns:
            report (dict): reports the result of checking the routing as:
                {'return_code': int, 
                 'pds': int, , 
                 'param_count': int, 
                 'variants': [], 
                 'params': [], 
                 'msg': str}
            where:
                return_code 0: routing is ok.
                            1: faulty pds structure (none or more than one).
                pds:        pds-structure for this material
                param_count: number of parameters to be set in.
                variants:   list of {'variant': p, 'typ': t, 'value': v}, ...
                params:     list of {'param', p1}, {'param', p2}, ... 
                msg:        a text message        
                
        Note: 
            The params list of the return will NOT contain open parameters which
            are determined by a variant selection,
        """      
        # Report structure; initial values.
        report = {'return_code': None, 'pds': None, 'variants': [], 'params': [],
                  'param_count': None, 'max_lotsize': 0, 'msg': 'No Message'}
        
        check_pds = """
            SELECT count(pds.id) AS pdscount ,
                max(pds.has_variant) AS has_variant,
                max(pds.id) AS pds,
                max(pds.max_lotsize) AS max_lotsize
            FROM pds
            WHERE pds.material  = :material
            AND pds.valid_from < date('now')
            AND pds.valid_to > date('now')"""

        get_variant = """
            SELECT t.variant, t.param_typ AS typ, t.param_value as value
            FROM pds_task as t
            WHERE t.pds = :pds
            AND t.variant is not NULL"""

        # TODO: check if following may be deleted.
        #=======================================================================
        # check_variant = """
        #     SELECT count(pds_task.variant) AS varcount
        #     FROM pds_task
        #     WHERE pds_task.pds = :pds
        #     AND pds_task.variant = :variant"""
        #=======================================================================

        get_parameter = """
            SELECT DISTINCT pds_task.param_typ AS params
            FROM pds_task
            WHERE pds_task.pds = :pds
            AND pds_task.param_typ IS NOT NULL
            AND pds_task.param_value IS NULL"""
        
        check_parameter = """
            SELECT DISTINCT count(pds_task.param_typ) AS pcount
            FROM pds_task
            WHERE pds_task.pds = :pds
            AND pds_task.param_typ IS NOT NULL
            AND pds_task.param_value IS NULL"""

        # Evaluate pds integrity for the material
        conn = self.dao.connect()
        with conn:
            # Find a production data structure (pds) for this material.
            cur = conn.execute(check_pds, {'material': material})
            checkpds = cur.fetchone()
            pds = checkpds['pds']
            max_lotsize = checkpds['max_lotsize']

            # Check whether production data structure is unique for today.  
            if checkpds['pdscount'] != 1:
                message = f"ERROR: no or more than one pds found for " + \
                          f"material {material}."
                report.update({'return_code': 1, 'msg': message})
                return report

            # Get variants and their parameters defined for this pds.
            if checkpds['has_variant']:
                cur = conn.execute(get_variant, {'pds': pds})
                pds_variants = cur.fetchall()
                report.update({'variants': [dict(_) for _ in pds_variants]})

            # Get open parameters of pds (to be set). 
            cur = conn.execute(get_parameter, {'pds': pds})
            pds_parameter = cur.fetchall()
            report.update({'params': [_['params'] for _ in  pds_parameter]})

            # Count number of open parameters of this pds.
            cur = conn.execute(check_parameter, {'pds': pds})
            checkparam = cur.fetchone()
            parcount = checkparam['pcount']
        
        conn.close()
        
        # pds structure was evaluated as ok.
        message = f"... Order data OK with pds {pds} and {parcount} open " \
                  f"parameters."
        report.update({'return_code': 0, 'pds': pds, 'param_count': parcount,
                       'max_lotsize': max_lotsize, 'msg': message})
        
        return report

    def new_order(self, site, material, qty=1, unit='PCE', variant=None):
        """Create a new order by manual data input.

        First checks if the data entered is processable. If true, it is checked
        if parameter data has to be supplied and the order is inserted into the
        database.

        An order is processable, if the following two condition are fulfilled:
        1. exactly one production-data-structure (pds) can be selected from the
           database
        2. if the pds requires to select a variant, the variant argument have
           to match one of these possible variants.

        If the order is processable but contains NULL values in the
        pds_task.param_value column, the open_params counter signals that with
        a value > 1.

        Args:
            site (str):     identifier of the local site.
            material (str): material "number".
            qty (int):      number or quantity of items (default=1)
            unit (str):     unit of measure for qty (default=PCE)
            variant (str):  identifier for a variant to produce (default=None).

        Returns:
            A 4-tupel (return_code:int, order:int, open_params:int, 
            description:str) where:

            status = 0      order was processed successfully,
            status = 1      number of pds records is 0 or >1 (error).
            status = 2      variant missing or does not match.
            status = 3      too much order items (MAX_ORDER_QTY exceeded).
        """
        report = {'return_code': 0, 'order': None, 'open_params': None,
                  'msg': 'NoData'}
        
        # Check for number of order items is in allowed range.
        if qty >= MAX_ORDER_QTY:
            message = f"ERROR: too much order items. {qty} requested, " \
                      f"{MAX_ORDER_QTY} is the maximum allowed."
            return report.update({'return_code': 3, 'msg': message})
       
        # Check routing data of material 
        routing_report = self.check_routing(material)

        # Evaluate routing report
        if routing_report['return_code'] == 0:
            pds = routing_report['pds']
            open_params = routing_report['param_count']
            if open_params:
                status = OrderStates.INITIAL
            else:
                status = OrderStates.CHECKED
        else:
            report.update({'return_code': 1, 'msg': routing_report['msg']})

        # Persist order data and order plan data
        try:
            at = datetime.datetime.utcnow()            
            result = self.dao.persist('order',
                                      data={'site': site,
                                            'material': material,
                                            'variant': variant,
                                            'qty': qty,
                                            'unit': unit,
                                            'pds': pds,
                                            'at': at})
            order = result['rowid']

            self.dao.persist('order_plan',
                             data={'order': order,
                                   'site': site,
                                   'state': status.value,
                                   'statx': status.name,
                                   'at': at})
            
            for param_typ in routing_report['params']:
                self.dao.persist('order_parameter',
                                 data={'order': order,
                                       'site': site,
                                       'typ': param_typ,
                                       'value': None})
            
            # Order created successfully
            message = f"... Order {order} created. Check for open params."
            report.update({'return_code': 0, 'order': order,
                            'open_params': open_params, 'msg': message})

        except BaseException as e:
            # TODO: replace by rc-dict:
            # raise e
            report.update({'return_code': 2, 'msg': e.args})

        return report

    def check_order(self, site:str, order:str) -> dict:
        """Checks for open (not set) parameters for an order.
        
        Args:
            site (str): the site identifier.
            order (str): the order number to check.
            
        Prerequisites:
            Order has to be created.
            
        Returns:
            report (dict): reports the result of checking the order as:
                {'return_code': int, 'params': list, 'msg': message}
            where:
                return_code 0: data is complete
                            1: parameters have to be set (missing)
                params: either an empty list (complete) or a parameter list as
                        returned by get_order_params()
                msg: a text message        
        """
        # Initial setting and form of report.
        message = f"Data for order {order} is complete."
        report = {'return_code': 0, 'params': [], 'msg': message}
        
        # Get open order parameters. 
        # Note: list does not contains unset variant selection!
        params = self.get_order_param(site, order, mode='missing')
        
        if params == []:
            at = datetime.datetime.utcnow()
            status = OrderStates.CHECKED
            self.dao.persist('order_plan',
                             data={'order': order,
                                   'site': site,
                                   'state': status.value,
                                   'statx': status.name,
                                   'at': at},
                             ocdn = True)

            message = f"Order data for {order} complete, state set to checked."
            report.update({'return_code': 0, 'params': params, 'msg': message})
        else:
            message = f"Found {len(params)} open parameters for order {order}."
            report.update({'return_code': 1, 'params': params, 'msg': message})

        return report

    def show_orders(self, status=None):
        """Get a list of orders with a status given.

        Args:
            status:    Order status name or instance of base.OrderStates.
        """
        select_all_orders = """
            SELECT o.site, o.id, IFNULL(o.variant, '') AS variant, o.qty, o.unit, s.state, s.statx
            FROM "order" AS o, order_plan AS s
            WHERE s."order" = o.id """

        select_orders_by_state = """
            SELECT o.site, o.id, IFNULL(o.variant, '') AS variant, o.qty, o.unit, s.state, s.statx
            FROM "order" AS o, order_plan AS s
            WHERE s.statx = :statx
            AND s."order" = o.id"""

        if isinstance(status, OrderStates):
            statx = status.name
        else:
            statx = status

        conn = self.dao.connect()
        with conn:
            if statx is None:
                cur = conn.execute(select_all_orders)
            else:
                cur = conn.execute(select_orders_by_state, {'statx': statx})
            rows = cur.fetchall()
            orders = [ dict(r) for r in rows]
        conn.close()  
        
        return orders

    def set_order_param(self, site, order, param_typ, param_value):
        """Set missing parameters of tasks.

        Set the type and value of a task parameter for a given order and site.

        Args:
            site (str):       identifier of the local site.
            order (in):       oder number to be parameterized.
            param_type (str):  the parameter type to be set.
            param_value (str): a parameter value to be set.
        """
        rc = self.dao.persist('order_parameter',
                              data={'site': site,
                                    'order': order,
                                    'typ': param_typ,
                                    'value': param_value},
                              upsert=['order', 'typ'])
        return rc

    def get_order_param(self, site, order, mode='missing') -> []:
        """List missing (or all) parameters of tasks.

        Selects tasks for a given operation which have parameter values
        declared. In mode "missing", only those with empty values
        will be returned, else all operation having parameter types.

        Args:
            site (str):    identifier of the local site.
            order (in):    order number to be parameterized.
            mode (str):    either 'missing' or 'all.

        Returns:
            A list of dictionaries of parameters.
            
        TODO: W0613: Unused argument 'site' (unused-argument)
        """
        select_missing_parameter = """
            SELECT typ, value
            FROM order_parameter as p
            WHERE p.'order' = :order
            AND site = :site
            AND typ IS NOT NULL
            AND value IS NULL"""

        select_all_parameter = """
            SELECT typ, value
            FROM order_parameter as p
            WHERE p.'order' = :order
            AND site = :site
            AND typ IS NOT NULL"""

        conn = self.dao.connect()
        try:
            with conn:
                if mode == 'missing':
                    cur = conn.execute(select_missing_parameter,
                                       {'order': order, 'site': site})
                else:
                    cur = conn.execute(select_all_parameter,
                                       {'order': order, 'site': site})

                rows = cur.fetchall()
                paramlist = [dict(r) for r in rows]
        except:
            paramlist = None
        finally:
            conn.close()

        return paramlist

    def update_order_state(self, site, order, status):
        """Update order state."""
        if isinstance(status, OrderStates):
            state = status.value
            statx = status.name
        else:
            try:
                state = status
                statx = OrderStates(status).name
            except BaseException:
                raise ValueError(f"Argument status: {status} is neither OrderState nor integer in expected range.")

        rc = self.dao.change('order_plan',
                             data={'state': state,
                                   'statx': statx,
                                   'at': datetime.datetime.utcnow()},
                             conditions={'order': order,
                                         'site': site})
        return rc

    def new_opdta(self, site, order):
        """Prepare the operational data for an order as used by production cells.

        Make a parameterized copy of pds-structure linked to the order. If a
        variant is defined at order level, the respective pds subset will be
        selected.

        TODO: Test if all parameters are set (site and order).
        TODO: Consistent rc and exception handling -> design decision.

        Args:
            site (str):     identifier of the issuing, local site.
            order (int)     order number for which a pds copy should derived.
        """
        select_variant = """
            SELECT variant
            FROM 'order'
            WHERE site = :site
            AND id = :order"""

        insert_task = """
            INSERT INTO task
                (site, id, 'order', step, next, typ, operation, param_typ, param_value, pds_task)
            SELECT :site,
                NULL,
                :order,
                pds_task.step,
                pds_task.next,
                pds_task.typ,
                pds_task.operation,
                pds_task.param_typ,
                pds_task.param_value,
                pds_task.id
            FROM pds_task, 'order'
            WHERE pds_task.pds = 'order'.pds
            AND 'order'.id = :order
            AND (pds_task.variant IS NULL OR pds_task.variant = :variant)"""

        update_task_parameter = """
            WITH plist ('order', site, typ, value) AS
                (SELECT * FROM order_parameter WHERE "order" = :order)
            UPDATE task
               SET param_value = (
                    SELECT plist.value
                    FROM plist
                    WHERE task.param_typ = plist.typ
                )
            WHERE "order" = :order
            AND param_typ in (SELECT typ FROM plist)"""

        insert_part = """
            INSERT INTO part
            SELECT task.id, task.site, pds_part.material, pds_part.usage, pds_part.qty, pds_part.unit
            FROM task, pds_part
            WHERE pds_part.pds_task = task.pds_task
            AND task.site = :site
            AND task."order" = :order"""

        insert_resource = """
            INSERT INTO resource
            SELECT task.id,
                task.site,
                pds_resource.loc,
                pds_resource.prio,
                pds_resource.durvar,
                pds_resource.durfix,
                pds_resource.unit
            FROM task, pds_resource
            WHERE pds_resource.pds_task = task.pds_task
            AND task.site = :site
            AND task."order" = :order"""

        conn = self.dao.connect()
        try:
            with conn:
                # Get the variant
                cur = conn.execute(select_variant, {'site': site, 'order': order})
                row = cur.fetchone()
                variant = row['variant']

                # Create pds copy in database
                conn.execute(insert_task, {'site': site, 'order': order, 'variant': variant})
                conn.execute(update_task_parameter, {'order': order})
                conn.execute(insert_part, {'site': site, 'order': order})
                conn.execute(insert_resource, {'site': site, 'order': order})

            conn.close()

            rc = (0, None)
        except Exception as e:
            rc = (1, e.args)

        return rc

    def get_pds(self, site, order):
        """Get a pds for a given order.
        
        Args:
            site (str): site identifier.
            order (int): order number.
        """
        result = self.dao.retrieve('order',
                                   columns=['pds'],
                                   conditions={'id': order},
                                   single=True)
        if result:
            return result['pds']
        else:
            return None

    def new_sfcu(self, site, order):
        """Split a given order into a set of shop floor control units (SFCUs).

        For a given order of 'n' items for a material 'm', this function
        creates 'n' SFCU records for the material 'm' in the data base.

        TODO: use order_plan.status to prevent double creation of sfcus
        
        Args:
            site (str):     identifier for the local (issuing) site.
            order (int):    the order number to be split in to SFCUs.
            
        TODO: W0613: Unused argument 'site' (unused-argument)
        """
        sfculist = []
        select_order = """
            SELECT 'order'.material, 'order'.qty, 'order'.at
            FROM 'order', order_plan
            WHERE 'order'.site = :site
            AND 'order'.id = :order
            AND 'order'.id = order_plan.'order'
            AND order_plan.state <= :state"""

        insert_sfcu = """
            INSERT INTO sfcu VALUES 
            (NULL, :site, :order, :material, :state, :statx, :at)"""
            
        # TODO: check if this cab be replaced by simple dao persist:
        # Note: outer try to be pertained and extened!
        # row = dao.retrieve(..., single=True)
        # if row:
        #     material = row['material']
        #     qty = int(row['qty'])
        #     at = row['at']
        # else:
        #     raise NoResultException
        # data = {'id': None, 'site': ..., 'at': at}
        # dao.persist('sfcu', data)
        #
        # if qty >= MAX_QTY:
        #     raise ORderQtxError('item quantity in order exceeds allowed limit) 
        _log.debug("orderprocessing:new_sfcu:step1 %s", type(site))

        conn = self.dao.connect()
        try:
            with conn:
                _log.debug("orderprocessing:new_sfcu:step2 %s, %s, %s", site, order, OrderStates.CHECKED.value)
                # Get sfcu data from order
                cur = conn.execute(select_order, {
                    'site': site,
                    'order': order,
                    'state': OrderStates.CHECKED.value})
                row = cur.fetchone()

                if row:
                    material = row['material']
                    qty = int(row['qty'])
                    at = row['at']
                else:
                    raise NoResultException

                # Create on sfcu per ordered item (qty)
                for _ in range(qty):
                    cur = conn.execute(insert_sfcu,
                                       {'site': site,
                                        'order': order,
                                        'material': material,
                                        'state': SfcuStates.INITIAL.value,
                                        'statx': SfcuStates.INITIAL.name,
                                        'at': at})

                    # collect the new sfcu numbers for return list
                    sfculist.append(cur.lastrowid)
            conn.close()

        except NoResultException:
            _log.warning("Order %s is not in state 'initial' or 'checked'."\
                         " Will not create any SFCU.",
                         order)

        return sfculist

    def get_sfcu(self, site, order):
        """Get a list of sfcus for a given order.

        Args:
            site (str):    site identifier.
            order (int):   order number.
        """
        result = self.dao.retrieve('sfcu',
                                   columns=['id'],
                                   conditions={'site': site,
                                               'order': order})

        sfculist = [r['id'] for r in result]
        return sfculist
