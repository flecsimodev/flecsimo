"""
Created on 07.09.2021 from roles.callback module, first created on Created on 22.06.2020

@author: Ralf Banning
 
Copyright and License Notice:

    flecsimo requesthandling module for area facilities
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import logging
from flecsimo.base import dao
from flecsimo.base import msg

# import sqlite3
_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())


class RequestMessageHandler:
    """ Class defining callback methods, handling flecsimo request messages.
    
    This class initializes all required data access objects and provides message
    handler methods (implementing the mqtt callback interface) for the following 
    demand messages:
        asgmt: assign an sfcu to an area or cell.
        opdata: data structure containing all operational data for an sfcu.
        rfq: request quote for processing a demand.
    
    Args:
        database (str): path to a flecsimo sqlite database.
        mode (str): mode of operation, default is 'AUTO'
    """

    def __init__(self, database, myself, mode='AUTO'):
        self.database = database
        self.dao = dao.DataAccess(self.database)
        self.mode = mode
        self.myself = myself

    def _auto_asgmt_processing(self, site, order, sfcu):
        """Interface definition for processing assignments in AUTO mode."""
        pass

    def on_asgmt(self, client, userdata, message):
        """ Callback handler for 'asgmt' (assignment of operations) messages."""
        # Get assignment data from message
        asgmt_msg = msg.as_dict(message.topic, message.payload)

        # Process message only if it was addressed to this facility as supplier.
        try:
            supplier = asgmt_msg['supplier']
        except KeyError as e:
            _log.debug("Malformed message-payload. Skip assignment.", e)
            return

        if supplier == self.myself:
            self.dao.persist('schedule', asgmt_msg)

            # Start re-assignment to cell level in AUTO-mode.
            if self.mode == 'AUTO':
                site = asgmt_msg['site']
                order = asgmt_msg['order']

                try:
                    sfcu = asgmt_msg['sfcu']
                except (KeyError, ValueError):
                    sfcu = None

                self._auto_asgmt_processing(site, order, sfcu)
            else:
                # This will get relevant in RFQ-mode:
                # discard any open quotes for that order/sfcu
                pass

    def on_opdta(self, client, userdata, message):
        """Callback handler for 'opdta' (operation data) messages
           
        This message handler stores the operational data received. 
        
        Args:
            mqtt.Client callback interface
              
        Note:
            TODO: implement a hash value test for the received opdta.
            See also https://stackoverflow.com/questions/19522505/using-sqlite3-in-python-with-with-keyword
        """
        tlrc = 0  # Total row count

        new_opdta_msg = msg.as_dict(message.topic, message.payload)

        if not new_opdta_msg:
            _log.debug("Received not readable payload on_opdta. Skip message.")
            return tlrc

        conn = self.dao.connect()
        with conn:
            tlrc += self.dao.loadmany(conn, 'order',
                                      new_opdta_msg['data']['order'], ocdn=True)
            tlrc += self.dao.loadmany(conn, 'sfcu',
                                      new_opdta_msg['data']['sfcu'], ocdn=True)
            tlrc += self.dao.loadmany(conn, 'task',
                                      new_opdta_msg['data']['task'], ocdn=True)
            tlrc += self.dao.loadmany(conn, 'part',
                                      new_opdta_msg['data']['part'], ocdn=True)
            tlrc += self.dao.loadmany(conn, 'resource',
                                      new_opdta_msg['data']['resource'], ocdn=True)
        conn.close()

        return tlrc

    def on_rfq(self, client, userdata, message):
        """Read request for quote and persist.
        
        This functions defers requests for quote, storing them on a database
        and offers the service, if the operation is available at this level.
        (Does not imply that the service has to be active).
        
        Args:
            as requested by mqtt.Client interface
            
        Examples:
            rfg topic: "FUAS/area1/rfq"
            rfq payload : {"site": "FUAS", "sfcu": 21, "sender": "FUAS", "role": "DEMAND", "oplist": "27-DRILLING", "due": "2020-10-31", "prio": 0, "statx": "initial", "at": "2020-07-03T15:10"}
                       
        Note:
            See also https://stackoverflow.com/questions/19522505/using-sqlite3-in-python-with-with-keyword
        """

        # TODO: implement on_rfq functionality
        pass
#===============================================================================
#         # Prepare SQL statements
#         insert_schedule = """
#             INSERT or REPLACE INTO schedule
#                 (site, "order", sfcu, pds, operation, due, prio, state, statx, at)
#             VALUES (:site, :order, :sfcu, :pds, :operation, :due, :prio, :state, :statx, :at)"""
#
#         insert_quotation = """
#             INSERT or REPLACE INTO schedule
#                 (site, "order", sfcu, pds, operation, due, prio, state, statx, at)
#             VALUES (:site, :order, :sfcu, :pds, :operation, :due, :prio, :state, :statx, :at)"""
#
#         lookup_facility_operation = """
#             SELECT f.operation
#             FROM facility_operation AS f
#             JOIN task AS t ON f.operation = t.operation
#             AND CASE f.value_typ
#                     WHEN 'range' THEN t.param_value BETWEEN f.param_min and f.param_max
#                     WHEN 'value' THEN f.param_min = t.param_value
#                     ELSE 1
#                 END
#             WHERE t.site = :site
#             AND t."order" = :order
#             AND f.operation = :operation
#             """
#
#         # Get request data from message
#         rfq_msg = msg.as_dict(message.topic, message.payload)
#
#         # Prepare request data for insert
#         #=======================================================================
#         # rfq_msg = rfq_msg.update({'state': ScheduleStates.REQUESTED.value,
#         #                           'statx': ScheduleStates.REQUESTED.name})
#         #=======================================================================
#
#         conn = sqlite3.connect(self.database)
#         conn.row_factory = sqlite3.Row
#
#
#         cur = conn.execute(lookup_facility_operation, {'operation': rfq_msg['spec']})
#         r = cur.fetchone()
#
#         if r:
#             # This is true, if service is available in database
#             # in this case we make an offer, otherwise ignore
#             # TODO: think of having also *active* service reported in database
#             # TODO: this is not yet a full implementation of oplist checking
#             # TODO: rework. This is valid only for prototype.
#             demand = dict(r)
#
#
#
#
#             _log.info("Demand:", demand, "for sfcu:", rfq_msg['sfcu'])
#
#             quote = msg.Quote(self.sender, site=self.site, sfcu=rfq_msg['sfcu'])
#
#             try:
#                 with conn:
#                     conn.execute(insert_schedule) #TODO: parameters are missing
#                     conn.execute(insert_quotation, quote.dict)
#             except BaseException:
#                 raise
#
#             _log.info("Send offer:", quote.payload)
#             self.publish(quote.topic, quote.payload)
#
#
#         else:
#             # TODO: there is no message type defined to propagate such a reject...
#             # TODO: rubbish, no entry into dsm, but status update of demand REQUESTED ->  REJECTED
#             _log.info("Requested demand", rfq_msg['spec'], "not deliverable. Reject request for sfcu", rfq_msg['sfcu'])
#
#         conn.close()
#===============================================================================
