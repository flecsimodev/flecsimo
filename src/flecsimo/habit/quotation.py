'''
Created on 03.10.2021

@author: Ralf Banning
'''
import logging
import sqlite3

from flecsimo.base import msg
from flecsimo.base.states import ScheduleStates

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler)


class TenderMixin:
    """Request quotes for order or sfcu execution from other parties."""

    def request_quote(self, site, order):
        """Select data for an rfq-message from dsmbook.
        
        TODO: dsmbook was replace by schedule - analyse this
        """

        select_dsm = """
            SELECT site, "order", sfcu, role, scope, spec, due, prio
            FROM dsmbook
            WHERE site = :site
            AND "order" = :order
            AND role = 'DEMAND'"""

        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row

        with conn:
            cur = conn.execute(select_dsm, {"site": site, "order": order})
            rows = cur.fetchall()

            for row in rows:
                _log.debug("dict of row:", dict(row))

                rfq = msg.Rfq(self.sender, **dict(row))
                rfq.state = ScheduleStates.REQUESTED
                # TODO: set spec with required operation!

                self.publish(rfq.topic, rfq.payload)

        conn.close()
