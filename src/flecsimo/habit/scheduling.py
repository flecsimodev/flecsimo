"""
Created on 09.09.2021 from roles.controller module, first created on 25.05.2020

@author: Ralf Banning

Copyright and License Notice:

flecsimo scheduling module
Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
of Applied Sciences.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import datetime

from flecsimo.base import dao
from flecsimo.base import msg
from flecsimo.base.const import Orglevel
from flecsimo.base.states import ScheduleStates


class Scheduler:
    """Role methods for scheduling operations.
    
    Args:
        database (str): path to database.
        
    Note:
        MQTT sender information will be propagated by method arguments. 
    """

    def __init__(self, database):
        self.database = database
        self.dao = dao.DataAccess(self.database)

    def schedule_supplier(self, site, order, supplier, operation, sfcu=None):
        """Create a schedule for a supplier (as an assigned supplier).
        
        This methods creates a new schedule (or replace an existing if a
        conflict occurs) for a given supplier in state "PLANNED". If an
        sfcu number is supplied, the methods plans a schedule only for 
        this specific sfcu, otherwise for all sfcus related to the order.
        
        Args:
            site (str): site identifier.
            order (int): order number.
            supplier (str): identifier of the assigned supplier.
            operation (str): identifier for the operation assigned to the 
                             supplier.
            sfcu (int): sfcu number (optional)
            
        TODO: return assigned schedules instead of rowid-rowcount-dict?             
        """
        insert_supplier_schedule = """
            INSERT OR REPLACE INTO schedule
            SELECT NULL, s.site, s."order", s.sfcu,
                   :operation,
                   :supplier,
                   :org,
                   NULL, s.due, s.prio, 
                   :state,
                   :statx,
                   :at
            FROM schedule AS s
            WHERE s."order" = :order
            AND CASE
                    WHEN :sfcu IS NULL THEN 1
                    ELSE sfcu = :sfcu
                END"""

        conn = self.dao.connect()
        with conn:
            cur = conn.execute(insert_supplier_schedule, {
                'order': order,
                'sfcu': sfcu,
                'operation': operation,
                'supplier': supplier,
                'org': Orglevel.CELL.value,
                'state': ScheduleStates.PLANNED.value,
                'statx': ScheduleStates.PLANNED.name,
                'at': datetime.datetime.utcnow()})
        conn.close()

        return {'rowid': cur.lastrowid, 'rowcount': cur.rowcount}

    def choose_facility(self, site, order):
        """Choose suppliers for each operation of a scheduled order.
        
        This is a simple "demo" implementation of a planning and optimizing 
        algorithm for selecting a suppliers (cells) out of list of possible
        suppliers (using table facility_operation).
        
        Whenever the operation and task-parameters of an order do not matches 
        with a record facility_operation table, this supplier will be marked as
        a 'NOMATCH' otherwise a marker containing all parameter values of the
        requested operation as a signature. If more than one supplier is 
        offering the same signature, the supplier with lowest load will be 
        selected (which in fact is implemented a random number). 
                
        Args: 
            site (str): site identifier.
            order (int): order number.
            
        Returns:
            List of facilities assignable for operation or empty list.
        
        Note: this algorithm is not only simple, it's "over-simplifying":
        - it does not reflect the load of the cells.
        - it does not prevent from assigning multiple operations to the same 
          cell.
        - it works non predictive if two cells offer same set up time for the 
          same operation.
        """
        choose_stmt = """
            SELECT operation, facility, match 
            FROM (
                SELECT distinct pl.operation, pl.facility, random() AS load, 
                group_concat(ifnull(param_value, 'NOMATCH'), ':') 
                    OVER (PARTITION BY pl.facility order by pl.operation
                    ) AS match
                FROM (
                    SELECT DISTINCT t.operation, f.facility, f.param_typ
                    FROM task as t, facility_operation as f
                    WHERE t.operation = f.operation
                    AND t.param_typ = f.param_typ
                    AND t.site = :site
                    AND t."order" = :order
                    ) AS pl
                LEFT OUTER JOIN (
                    SELECT t.operation, f.facility, t.param_typ, t.param_value
                    FROM task AS t  
                    LEFT OUTER JOIN facility_operation AS f
                    ON f.operation = t.operation
                    WHERE CASE f.value_typ
                        WHEN 'range' THEN t.param_value BETWEEN f.param_min 
                            and f.param_max
                        WHEN 'value' THEN t.param_value = f.param_min
                        END
                    AND t.site = :site
                    AND "order" = :order
                    ) AS po        
                ON (pl.operation, pl.facility, pl.param_typ) 
                    = (po.operation, po.facility, po.param_typ)
                ) AS o
            WHERE match NOT LIKE '%NOMATCH%'
            GROUP by operation having min(load)"""

        facilitylist = []
        conditions = {'site': site, 'order': order}
        conn = self.dao.connect()
        with conn:
            cur = conn.execute(choose_stmt, conditions)
            results = cur.fetchall()
        conn.close()
        if results:
            facilitylist = [dict(r) for r in results]
        return facilitylist

    def get_schedule(self, site, order=None, status=None):
        """Get a schedule record for given criteria.
        
        Retrieves a list of schedules for given parameters: site (requires),
        order (optional) and status (optional). The status may be given as int
        or a qualified Enumeration.
        
        Args:
            site (str):    Identifier for site.
            order (int):   Order id (optional).
            status (int or Enum):  Status id for schedule (optional)
            
        Returns: a list of dicts
        """
        try:
            statx = status.name
        except BaseException:
            statx = status

        conditions = {'site': site, }
        if order:
            conditions['order'] = order
        if statx:
            conditions['statx'] = statx

        result = self.dao.retrieve('schedule',
                                   columns=['id',
                                            'order',
                                            'sfcu',
                                            'operation',
                                            'supplier',
                                            'due',
                                            'prio',
                                            'statx',
                                            'at'],
                                   conditions=conditions)

        return [dict(r) for r in result]

    def get_asgmtlist(self, sender, site, order, sfcu=None):
        """Create assignment messages ready to be published with mqtt.
        
        Args: 
            sender (str):  The sender identification
            site (str):    Identifier of the site that issues the demand.
            order (int):   Order number which is the reason for demand. 
            sfcu (int):    SFCU number - if None, all SFCUs for the given order 
                           will be selected.
        Returns:
            List of Asgmt messages or empty list.
        
        TODO: 
            create a hashvalue of the returned dictionary to be passed 
            along with the assignment message 
            (see: https://docs.python.org/3/library/hashlib.html)   

        Note: was named get_asgmt in earlier versions.
        """
        asgmtlist = []
        conditions = {'site': site, 'order': order}
        if sfcu is not None:
            conditions['sfcu'] = sfcu

        # TODO: This is wrong:: site is not always sender
        rows = self.dao.retrieve('schedule',
                                 columns=['site',
                                          'order',
                                          'sfcu',
                                          'operation',
                                          'supplier',
                                          'org',
                                          'due',
                                          'prio'],
                                 conditions=conditions)

        schedule_list = [dict(r) for r in rows]

        # Update schedule-dicts in list with ASSIGNED state and append an
        # Asgmt object to return list.
        for schedule in schedule_list:
            schedule.update({'state': ScheduleStates.ASSIGNED})
            asgmtlist.append(msg.Asgmt(sender=sender, **schedule))

        return asgmtlist

    def new_opdta_msg(self, sender, site, order, supplier,
                      operation=None, sfcu=None,):
        """Select a complete opdta structure for given parameters.
        
        Note: was named get_opdta in earlier versions.
        """

        # Select - create the single dictionary objects from the database
        try:
            # Get order data
            conditions = {'site': site, 'id': order}
            result = self.dao.retrieve('order', conditions=conditions)
            order_data = [dict(r) for r in result]

            # Get sfcu data
            conditions = {'site': site, 'order': order}
            if sfcu:
                conditions['id'] = sfcu
            result = self.dao.retrieve('sfcu', conditions=conditions)
            sfcu_data = [dict(r) for r in result]

            # Get task data
            conditions = {'site': site, 'order': order}
            if operation is not None:
                conditions['operation'] = operation
            result = self.dao.retrieve('task', conditions=conditions)
            task_data = [dict(r) for r in result]

            # get part data
            columns = ['task', 'site', 'material', 'usage', 'qty', 'unit']
            conditions = {'site': site, 'order': order}
            if operation is not None:
                conditions['operation'] = operation
            result = self.dao.retrieve('task_parts',
                                       columns=columns,
                                       conditions=conditions)
            part_data = [dict(r) for r in result]

            # get resource data
            columns = ['task', 'site', 'loc', 'prio', 'durvar', 'durfix', 'unit']
            conditions = {'site': site, 'order': order}
            if operation is not None:
                conditions['operation'] = operation
            result = self.dao.retrieve('task_resources',
                                       columns=columns,
                                       conditions=conditions)
            resource_data = [dict(r) for r in result]
        finally:
            pass

        # Create the hierarchical dictionary
        opdta = msg.Opdta(sender=sender, receiver=supplier)
        opdta.data['order'] = order_data
        opdta.data['sfcu'] = sfcu_data
        opdta.data['task'] = task_data
        opdta.data['part'] = part_data
        opdta.data['resource'] = resource_data

        return opdta

    def new_schedule(self, site, order, sfcu, operation, supplier, org, mhu,
                     due, prio, state, statx):
        """ Create a new schedule from data.
        
        This method is just a 'facade' to dao.persist and offers the data keys 
        as arguments. 
        
        Args: the columns of a flecsimo-schedule table.
        
        Note:
            The disadvantage is that default value handling for operation and
            supplier is built in to persist but has to duplicated here.  
        """
        operation = operation or 'NODATA'
        supplier = supplier or 'NODATA'
        data = {'site': site,
                'order': order,
                'sfcu': sfcu,
                'operation': operation,
                'supplier': supplier,
                'org': org,
                'mhu': mhu,
                'due': due,
                'prio': prio,
                'state': state,
                'statx': statx,
                'at': datetime.datetime.utcnow()}

        rc = self.dao.persist(
                        'schedule',
                        data=data,
                        upsert=["order", "sfcu", "operation", "supplier"])

        schedule_id = rc['rowid']
        return schedule_id

    def schedule_operation(self, site, order):
        """Schedule the operations for scheduled sfcus.
        
        This method operates internal on the flecsimo database and 
        creates a new schedule for a specific operation as part of
        an order.
        
        Args:
            site (str): site to schedule.
            order (str): order number.
        
        Note:
            Since the data processing in this method is executed within 
            the database, no replacement by dao methods is recommended.        
        """
        insert_operation_schedule = """
            INSERT INTO schedule
                ("site", "order", "sfcu", "operation", "due", "prio", "state", "statx", "at")
            SELECT s.site, s."order", s.sfcu, t.operation, s.due, s.prio, :state, :statx, :at 
            FROM schedule as s, task as t
            WHERE s.site = :site 
            AND t.site = :site
            AND s."order" = :order
            AND t."order" = :order
            AND s.operation IS 'ALL'
            AND statx = 'ASSIGNED'
            ON CONFLICT DO NOTHING"""

        conn = self.dao.connect()
        with conn:
            cur = conn.execute(insert_operation_schedule, {
                'site': site,
                'order': order,
                'state': ScheduleStates.INITIAL.value,
                'statx': ScheduleStates.INITIAL.name,
                'at': datetime.datetime.utcnow()})
        conn.close()

        return  {'lastrowid': cur.lastrowid, 'rowcount': cur.rowcount}

    def update_schedule_state(self, site, order, status):
        """Change the state of a schedule
        
        TODO: rename in update_status?
        
        Args:
            site (str): site identifier.
            order (int): order number.
            status: new state as ScheduleStates member or (int).
        
        Returns:
            numchgs (int): number of changes records.
        
        """
        if isinstance(status, ScheduleStates):
            state = status.value
            statx = status.name
        else:
            try:
                state = status
                statx = ScheduleStates(status).name
            except BaseException:
                raise ValueError(f"Argument status: {status} is neither "\
                                 f"ScheduleState nor integer in expected range.")

        numchgs = self.dao.change('schedule',
                                   data={'state': state,
                                           'statx': statx,
                                           'at': datetime.datetime.utcnow()},
                                   conditions={'site': site, 'order': order})

        return numchgs
