"""Managing flecsimo databases.
Created on 20.12.2021

@author: Ralf Banning
"""
import argparse
import logging
import sqlite3
from pathlib import Path
from flecsimo.util import dbschema

LICENSE_TEXT = """
    Copyright and License Notice:

    flecsimo database manager
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
SUPPORTED_ORG_LEVEL = ('site', 'area', 'cell')

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


class DbSetUp:
    """Create flecsimo databases of various types.
    
    This object provides methods to create of flecsimo databases for a  specific
    organizational level (e.g. site or cell) from central script modules:
    
    create_schema(): 
        create the db tables for a given organizational level (e.g. for a site 
        or a cell). Existing tables will be dropped!
        
    create_masterdata(): 
        insert the master data for a given organizational level to an existing 
        database. 
        
    create_simdata(): 
        insert simulation data for a given organizational level ans a specific
        simulation component (e.g. AREA-1 or CELL-3). This data will be used in 
        simulation applications and provide a basic use case set up for flecsimo
        models.
                         
    create_testdata(): 
        insert test data for a given organizational level. This data should be 
        used for unit testing and will therefore not be in sync with all changes
        in simulation use cases.  
            
    All data inserts may conflict with existing data if tables are not empty)!
    
    The create_table_dict() is for technical use and provides a dictionary of
    the names of all (user defined) tables and views of the object's database.
    """

    def __init__(self, db:str):
        """ Assure db path exit and map scripts to organizational level. 
        
        Args:
            db(str) a "path like" string idnetifying the database.
        """
        self.db = db
        path = Path(db).parent
        try:
            path.mkdir(parents=True)
        except FileExistsError:
            pass

        self.schema = {'site': dbschema.SITE_DDL,
                       'area': dbschema.AREA_DDL,
                       'cell': dbschema.CELL_DDL,
                      }

    def create_schema(self, org:str):
        """Execute a DDL scripts for flecsimo database.
        
        Runs a series of SQL-DDL-Scripts for a selected org. 
        Scripts have to control transactions / commit by them self.

        Args:
            org (str): organizational level like site, area, cell, ...

        Prerequisites:
            org level is supported.
        """
        self._assert_orglevel_supported(org)
        conn = sqlite3.connect(self.db)
        script = {'core': dbschema.CORE_DDL, 'specific': self.schema[org]}
        for typ in script:
            conn.executescript(script[typ])
            log.info("Created %s %s schema in %s", org, typ, self.db)
        conn.close()

    def create_masterdata(self, org:str):
        """Execute a DDL or DML scripts for flecsimo database.
        
        Runs SQL-DML-scripts to insert master . Scripts have to control 
        transactions / commit by them self.
        
        Args:
            org (str): organizational level like site, area, cell, ...
            
        Prerequisites:
            org level is supported.
        """
        self._assert_orglevel_supported(org)

        from flecsimo.util import db_testdata
        self.master_data = {'site': db_testdata.SITE_MASTER_DATA,
                            'area': db_testdata.AREA1_MASTER_DATA,
                            'cell': db_testdata.CELL1_MASTER_DATA
                           }
        conn = sqlite3.connect(self.db)
        conn.executescript(self.master_data[org])
        log.info("Inserted %s master data into %s", org, self.db)
        conn.close()

    def create_testdata(self, org:str):
        """Execute a DDL or DML scripts for flecsimo database.
        
        Runs SQL-DML-scripts to insert test data. Scripts have to control 
        transactions / commit by them self.
        
        Args:
            org (str): organizational level like site, area, cell, ...   

        Prerequisites:
            org level is supported.
        """
        self._assert_orglevel_supported(org)

        from flecsimo.util import db_testdata
        self.test_data = {'site': db_testdata.SITE_TEST_DATA,
                  'area': db_testdata.AREA1_TEST_DATA,
                  'cell': db_testdata.CELL1_TEST_DATA, }

        conn = sqlite3.connect(self.db)
        conn.executescript(self.test_data[org])
        log.info("Inserted %s test data into %s", org, self.db)
        conn.close()

    def create_simdata(self, org:str, name:str):
        """Create a standard set of databases for simulation purposes.
         
        Runs SQL-DDL and SQL-DML scripts to build a set of generated site, area,
        and cell databases. 
        
        Args:
            org (str): organizational level like site, area, cell, ... 
            name (str): name of the facility. Has to be a key of self.sim_data
        
        Prerequisites:
            org level is supported.
        
        Base Sequence:
        1. Create the db schema for the requested organizational level.
        2. Create both master as simulation data for the specified name.
        
        Raises:
            ValueError: if no simulationdata for a component name is known.
        """
        self._assert_orglevel_supported(org)

        from flecsimo.util import db_simdata
        self.sim_data = {'FUAS': [db_simdata.SITE_MASTER_DATA,
                                  db_simdata.SITE_SIM_DATA],
                         'AREA-1': [db_simdata.AREA1_MASTER_DATA,
                                  db_simdata.AREA1_SIM_DATA],
                         'CELL-1': [db_simdata.CELL1_MASTER_DATA,
                                  db_simdata.CELL1_SIM_DATA],
                         'CELL-2': [db_simdata.CELL2_MASTER_DATA,
                                  db_simdata.CELL2_SIM_DATA],
                         'CELL-3': [db_simdata.CELL3_MASTER_DATA,
                                  db_simdata.CELL3_SIM_DATA],
                         'CELL-4': [db_simdata.CELL4_MASTER_DATA,
                                  db_simdata.CELL4_SIM_DATA],
                         }
        if name in self.sim_data:
            conn = sqlite3.connect(self.db)
            for _ in self.sim_data[name]:
                conn.executescript(_)
            log.info("Inserted %s sim master and sim data in %s", org, self.db)
            conn.close()
        else:
            raise(ValueError("No sim data found for name '$s'.", name))

    def create_table_dict(self) -> dict:
        """Create a columns dictionary for all user tables and views of current 
           database.
        
        Create a db_table_columns dictionary with this structure:
        
            {'table1': ('col1', col2',...), 'table2': (...)}.
        
        Notes:
            - A table 't1' having only one column 'c1' will get an dictionary 
              entry {'t1': ('c1',)} as this is the standard formulation of a 
              one element tuple. Any application, using this dictionary has to 
              handle this to be compliant with SQL syntax, where a one-tuple 
              is build as ('c1').
            - Before SQLite version 3.33.0, the schema table was referenced as 
              'sqlite_master'. This name may be used as well in later version of 
              SQLite, but the standard would be 'sqlite_schema', 
              'main.sqlite_schema' or 'temp.sqlite_schema'. 
        """
        table_dict = {}
        if sqlite3.sqlite_version < "3.33.0":
            sqlite_schema = "sqlite_master"
        else:
            sqlite_schema = "sqlite_schema"
        tables_stmt = f"""
            SELECT name FROM {sqlite_schema}
            WHERE type in ('table', 'view')
            AND name NOT LIKE 'sqlit_%'
            ORDER BY name
            """
        columns_stmt = "select name, dflt_value from pragma_table_info (:table)"

        conn = sqlite3.connect(self.db)
        conn.row_factory = sqlite3.Row
        with conn:
            cur = conn.execute(tables_stmt)
            tables = cur.fetchall()

            for tab in tables:
                cur = conn.execute(columns_stmt, {'table': tab['name']})
                columns = cur.fetchall()
                table_dict[tab['name']] = {col['name']: col['dflt_value']
                                           for col in columns}
        conn.close()
        return table_dict

    def _assert_orglevel_supported(self, org):
        if org not in SUPPORTED_ORG_LEVEL:
            raise ValueError(f"Organizatinal level {org} is not supported.")


def main():
    """CLI control for schema and test and simulation data management."""

    parser = argparse.ArgumentParser(description='Manage flecsimo databases.')
    parser.add_argument('--org',
                        choices=['site', 'area', 'cell'],
                        default='site',
                        help='Set database facility type. Default is site.')
    parser.add_argument('--data',
                        choices=['nodata', 'master', 'test', 'sim'],
                        default='nodata',
                        help='Create no data, master data or additional test ' \
                        'data. In sim mode master and sim data will be created.')
    parser.add_argument('--name',
                        default='FUAS',
                        help='Name of a sim data set. Default is "FUAS". ' \
                        'Use --info to get a list of available data sets.')
    parser.add_argument('--info',
                        action='store_true',
                        help='Show list of available simulation data sets.')
    parser.add_argument('--loglevel',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help='Set log level')
    parser.add_argument('--path',
                        help='Define database name and path. Default is ' \
                        'generated-<layer>.db.')
    parser.add_argument('--columns',
                        action='store_true',
                        help='Create a columns dict as used by dao library.')
    parser.add_argument('--license',
                        action='store_true',
                        help='Show license and warranty disclaimer.')
    args = parser.parse_args()

    if args.license:
        print(LICENSE_TEXT)
        return

    if args.loglevel:
        logging.basicConfig(level=args.loglevel)

    if args.path:
        db = args.path
    else:
        db = f"generated-{args.org}.db"

    dbs = DbSetUp(db)

    if args.info:
        print('Available simulation data is:')
        [print(k, end=", ") for k in dbs.sim_data.keys()]
        return

    dbs.create_schema(args.org)

    if args.data == 'master':
        dbs.create_masterdata(args.org)

    if args.data == 'test':
        dbs.create_masterdata(args.org)
        dbs.create_testdata(args.org)

    if args.data == 'sim':
        dbs.create_simdata(args.org, args.name)

    if args.columns:
        tab_dict = dbs.create_table_dict()
        print("{")
        for table, columns in tab_dict.items():
            print(f'''"{table}": {columns},''')
        print("}")


if __name__ == "__main__":
    main()
