"""Managing configurations for flecsimo facilities,
Created on 07.10.2024

@author: Ralf
"""
import argparse
import cmd

from flecsimo.base.config import Configurator


LICENSE_TEXT = """
    Copyright and License Notice:

    flecsimo database manager
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


def _parse(arg, required=False, subcmd=False) -> dict:
    """Normalizes cmd arguments for called functions.
    
    Converts a kwarg argument string (as given by cmd interpreter) in to a
    dictionary. Providing an argument string with non-keyword arguments
    will raise an exception. If the argument string is empty, an empty dictionary
    is returned.
    
    Notes: 
        All argument values can be entered without any quotes. 
        All arguments will be handled internally as strings. 
        All keyword arguments have to be separated by a comma.
        
    Examples:
        Will work:
        show config
        show orders state=PLANNED
        show orders state="PLANNED"  
        create_order material=FXF-1100, variant=col:red, qty=2
        create_order(material=FXF-1100, variant=col:red, qty=2)
        
        Will fail:
        create_order order=FXF-1100 variant=col:red qty=2 (missing commas)
        create_order(FXF-1100, col:red, qty=2) (missing key words)
        
        
    Args:
        arg (str)
        required (bool): raise exception when no argument is provided
        subcmd (bool): partitions arg in a subcommand string and parameters.
        
    Returns:
        None if no parsed arg was found, arg_as_dict(dict) else.
        
    Exception:
        ValueError
        
    """
    arg_as_dict = {}
    partitioned_arg = ()

    parsed_arg = arg.strip('()')
    if parsed_arg == '':
        if required is True:
            raise ValueError("No arguments provided although required.")
        else:
            return arg_as_dict

    try:
        parsed_arg = parsed_arg.replace('\'', '')
        parsed_arg = parsed_arg.replace('\"', '')

        if subcmd is True:
            partitioned_arg = parsed_arg.partition(' ')
            arg_as_dict['subcmd'] = partitioned_arg[0]

            parsed_arg = partitioned_arg[2]

        if parsed_arg != '':
            parsed_arg = parsed_arg.replace(" ", "")
            arg_as_dict.update(dict(_.split("=") for _ in parsed_arg.split(',')))

    except ValueError:
        raise ValueError(f"Malformed arguments: {arg}")

    return arg_as_dict


class ConfigMgr(cmd.Cmd):
    """Creat, change and inspect flecsimo config files."""

    def __init__(self, root, file):
        super().__init__(completekey='TAB')


        self.configurator = Configurator(root)
        self.config_file = self.configurator.get_conf_path(file)
        self.prompt = f"config:>"

    def preloop(self):
        print(f"Config file is: {self.config_file}.")

    def do_show(self, args):
        """Display selected information on site objects.
        
        Syntax: show TARGET [opt1=val1, opt2=val2, ...] 
        
        where TARGET is one of these items:
        config   Show current configuration in this config file.
        profiles Show available profile in the config file.  
        settings Show setting. Filter: profile
        """
        try:
            parsed_arg = _parse(args, subcmd=True)
            target = parsed_arg['subcmd']
        except (ValueError, KeyError) as e:
            print("Error in arguments:", e)
            self.do_help("show")
            return

        targetdict = {
            'config':
                self._show_config,
            'profiles':
                self._show_profiles,
            'setting':
                self._show_setting
            }

        unknown = f"Unknown information target '{target}'. "\
                  f"Chose one of these: {[t for t in targetdict.keys()]}."

        try:
            targetdict[target](parsed_arg)
        except KeyError:
            print(unknown)

    def do_set(self, args):
        """Change information on configuration objects.
        
        Syntax: show TARGET [opt1=val1, opt2=val2, ...] 
        
        where TARGET is one of these items:
        file path=...         work on a (different) config file at path
        current profile=...   Set current profile for this config.
  
        """
        try:
            parsed_arg = _parse(args, subcmd=True)
            target = parsed_arg['subcmd']
        except (ValueError, KeyError) as e:
            print("Error in arguments:", e)
            self.do_help("set")
            return

        targetdict = {
            'current':
                self._set_profile,
            'file':
                self._set_file
            }

        unknown = f"Unknown information target '{target}'. "\
                  f"Chose one of these: {[t for t in targetdict.keys()]}."

        try:
            targetdict[target](parsed_arg)
        except KeyError as e:
            print(unknown, e)

    def do_change(self, args):
        """Change information on configuration objects.
        
        Syntax: show TARGET [opt1=val1, opt2=val2, ...] 
        
        where TARGET is one of these items:

        settings change the settings for a profile=aProfile  
        """
        try:
            parsed_arg = _parse(args, subcmd=True)
            target = parsed_arg['subcmd']
        except (ValueError, KeyError) as e:
            print("Error in arguments:", e)
            self.do_help("change")
            return

        targetdict = {
            'setting':
                self._change_setting
            }

        unknown = f"Unknown information target '{target}'. "\
                  f"Chose one of these: {[t for t in targetdict.keys()]}."

        try:
            targetdict[target](parsed_arg)
        except KeyError:
            print(unknown)

    def do_bye(self, arg):
        """Quit the application."""
        print(f'Leaving configuration.')
        return True

    def _show_config(self, args):
        print("Current config is:")
        confdata = self.configurator.get_config(self.config_file)
        [print(f"    {k}: {v}") for (k, v) in confdata.items()]

    def _show_profiles(self, args):
        print("Available profiles:")
        confdata = self.configurator.get_profiles(self.config_file)
        [print(f"    {k}: {v}") for (k, v) in confdata.items()]
        info = self.configurator.get_info(self.config_file)
        try:
            cp = 'currentprofile'
            print("Current profile is:", info[cp])
        except KeyError:
            print(f"ERROR: {cp} not found in config info.")

    def _show_setting(self, args):
        print("_show_setting:", args)
        try:
            profile = args['profile']
        except (ValueError, TypeError) as e:
            print("Error when parsing argument:", type(e), e)
            return

        print(f"Settings for {profile}:")
        confdata = self.configurator.read_configfile(self.config_file)
        [print(f"    {k}: {v}") for (k, v) in confdata['PROFILE'][profile].items()]

    def _set_profile(self, args):
        try:
            profile = args['profile']
        except (ValueError, TypeError) as e:
            print("Error when parsing argument:", type(e), e)
            return

        confdata = self.configurator.read_configfile(self.config_file)
        confdata['INFO']['currentprofile'] = profile
        self.configurator.write_configfile(self.config_file, confdata)
        print(f"... currentprofile is set to {profile}")

    def _set_file(self, args):
        try:
            file = args['path']
        except (KeyError, ValueError, TypeError) as e:
            print("Error when parsing argument:", type(e), e)
            return
        print(f"Change config file to {file}.")
        self.config_file = file
        self.configuator = Configurator(self.config_file)
        print("... success.")

    def _change_setting(self, args):
        """Change settings of a profile in guided dialog."""
        try:
            profile = args['profile']
        except (KeyError, ValueError, TypeError) as e:
            print("Error when parsing argument:", type(e), e)
            return

        print(f"Change settings for profile: {profile}")

        # Prepare data to be changed
        confdata = self.configurator.read_configfile(self.config_file)
        set_data = confdata['PROFILE'][profile]

        save_config = False
        while save_config is False:

            # Modal dialog 1: select setting to change
            print(f"... settings which can be changed (type 'Q' to quit):")
            i = 0
            for (setting, value) in set_data.items():
                print(f"    Nr. {i}: {setting}: {value}")
                i += 1
            print("    -----------------------")

            setting = None
            while setting is None:
                try:
                    answer = input("    Select a number: ")
                    if answer == "Q":
                        print("    Process aborted by user.")
                        return

                    setting = list(set_data)[int(answer)]
                except:
                    print("    Your selection was not valid. Choose again.")
            print(f"    You have selected {setting}: {set_data[setting]}"
                  f" (current value). ")

            # Modal dialog 2: change setting
            new_value = None
            while new_value is None:
                answer = input("... Enter new value (type 'Q' to abort): ")
                if answer == "Q":
                    print("    Process aborted by user.")
                    return
                new_value = answer

            confdata['PROFILE'][profile][setting] = new_value
            print(f"    You set {setting} to {new_value}")

            print(f"... Do you want to save or continue with other values?")

            answer = input("    Enter 'S' for save or 'C' to continue:")
            if answer == 'S':
                save_config = True

        # Modal dialog 3: save setting
        print(f"... Do you want to save the config with these settings?")
        [print(f"    {k}: {v}") for (k, v) in confdata['PROFILE'][profile].items()]

        decision = None
        while decision is None:
            answer = input("    Enter 'Y' for Yes or 'N' for No:")
            if answer == "N":
                decision = answer
                print("    Process was terminated without saving new value.")
                return
            elif answer == "Y":
                decision = answer
                print(f"    Save config to {self.config_file}.")
                confdata['PROFILE'][profile][setting] = new_value
                self.configurator.write_configfile(self.config_file,
                                                  confdata,
                                                  bkp=True)
                print(f"... success. Recent config saved in .bkp file.")


def main():
    parser = argparse.ArgumentParser(description='Configuration manager. '\
        'Create, inspect and change flecsimo configuration files.')
    parser.add_argument('-r', '--root',
                        help='Set application root directory')
    parser.add_argument('-f', '--file',
                        help='Load configuration from file')
    args = parser.parse_args()

    ConfigMgr(args.root, args.file).cmdloop()


if __name__ == '__main__':
    main()
