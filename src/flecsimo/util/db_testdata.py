"""SQL Scripts for flecsimo master and test data.
Created on 20.12.2021

@author: Ralf Banning

Copyright and License Notice:

    SQL scripts (DML) for flecsimo databases.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

SITE_MASTER_DATA = """
BEGIN TRANSACTION;  
INSERT INTO "material" ("id","desc","typ","unit") VALUES 
    ('FUAS-1005','Universal bike mount (for 20-30 mm handlebars)','FINISH','PCE'),
    ('FUAS-1010','Phone holder package (configurable)','FINISH','PCE'),
    ('FUAS-2011','Phone holder (variant clamp fittings)','SEMIF','PCE'),
    ('FUAS-3012','Customized phone clamp (variant colors)','SEMIF','PCE'),
    ('FUAS-4013','Phone clamp blank (alloy pre-formed)','RAW','PCE'),
    ('FUAS-4610-B','Coating powder blue','RAW','G'),
    ('FUAS-4610-R','Coating powder red','RAW','G'),
    ('FUAS-5031','Storage packaging (storage box)','RAW','PCE'),
    ('FUAS-5041','Transportation packaging for delivery (cardbox with inlay)','RAW','PCE');
INSERT INTO "operation" ("id","desc") VALUES 
    ('ALL','All kind of or unspecified operation'),
    ('10-CUTTG','Laser cutting of alloys'),
    ('11-COATG','Powder coating of alloys'),
    ('12-CUTCO','Combined laser cutting and coating of alloys'),   
    ('27-DRILL','Drilling and reaming'),
    ('50-PACKG','Packaging (all variants)');
INSERT INTO "pds" ("id","site","material","has_variant","valid_from","valid_to") VALUES 
    (1,'FUAS','FUAS-1010',1,'2020-01-01','9999-12-31'),
    (3,'FUAS','FUAS-1005',0,'2020-01-01','2020-03-31'),
    (4,'FUAS','FUAS-1005',0,'2020-04-01','9999-12-31');
---New version
---INSERT INTO "pds" ("id","site","material","strategy", "has_variant","valid_from","valid_to") VALUES 
---    (1,'FUAS','FUAS-1010',20,1,'2020-01-01','9999-12-31'),
---    (2,'FUAS','FUAS-1010',70,1,'2020-01-01','9999-12-31'),
---    (3,'FUAS','FUAS-1003',20,,0,'2020-01-01','2020-03-31'),
---    (4,'FUAS','FUAS-1003',20,,0,'2020-04-01','9999-12-31');
INSERT INTO "pds_task" ("id","pds","step","next","typ","operation","variant","param_typ","param_value") VALUES 
    (1,1,1,2,'start','12-CUTCO',NULL,'geom','A'),
    (2,1,2,3,'var','12-CUTCO','col:red','col','red'),
    (3,1,2,3,'var','12-CUTCO','col:blue','col','blue'),
    (4,1,3,4,'','27-DRILL',NULL,'diam',NULL),
    (5,1,4,0,'final','50-PACKG',NULL,'pkg','storage'),
    (6,1,4,0,'final','50-PACKG',NULL,'pkg','deliver');       
INSERT INTO "pds_part" ("pds_task","material","usage","qty","unit") VALUES
    ---Reduced configuration for make-to-order strategy '20'. 
    (1,'FUAS-4013','in',1.0,'PCE'),
    (2,'FUAS-4610-R','in',3.5,'G'),  
    (3,'FUAS-4610-B','in',3.2,'G'),
    (5,'FUAS-5031','in',1.0,'PCE'),    
    (5,'FUAS-1010','out',1.0,'PCE'),    
    (6,'FUAS-5041','in',1.0,'PCE'),    
    (6,'FUAS-1010','out',1.0,'PCE');    
INSERT INTO "pds_resource" ("pds_task","loc","prio","durvar","durfix","unit") VALUES 
    (1,'',0,30.0,'','S'),
    (2,'',0,125.0,'','S'),
    (3,'',0,125.0,'','S'),
    (4,'',0,35.0,'','S'),
    (5,'',0,30.0,'','S'),
    (6,'',0,55.0,'','S');
COMMIT;    
"""

AREA1_MASTER_DATA = """
BEGIN TRANSACTION;  
INSERT INTO "material" ("id","desc","typ","unit") VALUES 
    ('FUAS-1005','Universal bike mount (for 20-30 mm handlebars)','FINISH','PCE'),
    ('FUAS-1010','Phone holder package (configurable)','FINISH','PCE'),
    ('FUAS-2011','Phone holder (variant clamp fittings)','SEMIF','PCE'),
    ('FUAS-3012','Customized phone clamp (variant colors)','SEMIF','PCE'),
    ('FUAS-4013','Phone clamp blank (alloy pre-formed)','RAW','PCE'),
    ('FUAS-4610-B','Coating powder blue','RAW','G'),
    ('FUAS-4610-R','Coating powder red','RAW','G'),
    ('FUAS-5031','Storage packaging (storage box)','RAW','PCE'),
    ('FUAS-5041','Transportation packaging (cardbox with inlay)','RAW','PCE');
COMMIT;
"""

CELL1_MASTER_DATA = """
BEGIN TRANSACTION;
INSERT INTO "material" ("id","desc","typ","unit") VALUES 
    ('FUAS-3012','Customized phone clamp (variant colors)','SEMIF','PCE'),
    ('FUAS-4013','Phone clamp blank (alloy pre-formed)','RAW','PCE'),
    ('FUAS-4610-B','Coating powder blue','RAW','G'),
    ('FUAS-4610-R','Coating powder red','RAW','G');
COMMIT;    
"""

SITE_TEST_DATA = """
BEGIN TRANSACTION;
INSERT INTO "facility" ("id","org","desc","loc","state","statx","at") VALUES 
    ('AREA-1','area','The flecsimo area 1','A:1',1,'READY','14.07.2020  08:00:00'),
    ('AREA-2','area','The flecsimo area 2','A:2',1,'READY','14.07.2020  08:00:00'),
    ('AREA-3','area','The flecsimo area 3','A:3',1,'READY','14.07.2020  08:00:00'),
    ('AREA-4','area','The flecsimo area 4','A:4',1,'READY','14.07.2020  08:00:00');              
INSERT INTO "facility_operation" ("id","facility","operation","param_typ","param_min","param_max","value_typ","setup","state","statx") VALUES 
    (1,'AREA-1','12-CUTCO','geom','A','','',40,'',''),
    (2,'AREA-1','12-CUTCO','geom','B','','',20,'',''),
    (3,'AREA-1','12-CUTCO','geom','C','','',30,'',''),        
    (4,'AREA-1','12-CUTCO','col','red','','value',10,'',''),
    (5,'AREA-1','12-CUTCO','col','blue','','value',10,'',''),
    (6,'AREA-1','27-DRILL','diam',10,19,'range',25,'',''),
    (7,'AREA-1','27-DRILL','diam',20,39,'range',30,'',''),
    (8,'AREA-1','27-DRILL','diam',40,60,'range',35,NULL,NULL),
    (9,'AREA-1','50-PACKG','pkg','deliver','','value',60,'',''),
    (10,'AREA-1','50-PACKG','pkg','storage','','value',25,'',''),
    (11,'AREA-1','50-PACKG','pkg','nopkg','','value',0,'','');     
INSERT INTO "order" ("id","site","material","variant","qty","unit","pds","at") VALUES 
    (1000020,'FUAS','FUAS-1100','col:red',1.0,'PCE',1,'2024-07-05 08:15:52.636603'),
    (1000021,'FUAS','FUAS-1100','col:blue',1.0,'PCE',1,'2024-07-05 09:29:26.088352'),
    (1000022,'FUAS','FUAS-1100','col:red',2.0,'PCE',1,'2024-07-05 15:30:55.136103'),
    (1000023,'FUAS','FUAS-1100','col:blue',3.0,'PCE',1,'2024-07-08 15:47:57.177334'),
    (1000024,'FUAS','FUAS-1100','col:red',2.0,'PCE',1,'2024-07-08 16:26:32.270911'),
    (1000025,'FUAS','FUAS-1100','col:blue',1.0,'PCE',1,'2024-07-08 17:25:57.504673'),
    (1000026,'FUAS','FUAS-1100','col:red',1.0,'PCE',1,'2024-07-10 10:24:26.088352'),
    (1000027,'FUAS','FUAS-1100','col:blue',2.0,'PCE',1,'2024-07-10 13:10:25.248400'),
    (1000028,'FUAS','FUAS-1100','col:blue',5.0,'PCE',1,'2024-07-10 13:15:06.080033');
INSERT INTO "order_parameter" ("order", "site", "typ","value") VALUES
    (1000020,'FUAS','diam','11'),
    (1000020,'FUAS','pkg','deliver'),
    (1000021,'FUAS','diam','21'),
    (1000021,'FUAS','pkg','deliver'),
    (1000022,'FUAS','diam','45'),
    (1000022,'FUAS','pkg','storage'),
    (1000023,'FUAS','diam','15'),
    (1000023,'FUAS','pkg','storage'),
    (1000024,'FUAS','diam','15'),
    (1000024,'FUAS','pkg','deliver'),
    (1000025,'FUAS','diam','21'),
    (1000025,'FUAS','pkg','deliver'),
    (1000026,'FUAS','diam','21'),
    (1000026,'FUAS','pkg','storage'),
    (1000027,'FUAS','diam','15'),
    (1000027,'FUAS','pkg','storage'),
    (1000028,'FUAS','diam',NULL),
    (1000028,'FUAS','pkg',NULL);
INSERT INTO "order_plan" ("order","site","actual_start","actual_end","plan_start","plan_end","schd_start","schd_end","state","statx","at") VALUES     
    (1000020,'FUAS',NULL,NULL,'2024-07-06','2024-07-10',NULL,NULL,3,'RELEASED','2024-07-05 08:15:52.636603'),
    (1000021,'FUAS',NULL,NULL,'2024-07-06','2024-07-10',NULL,NULL,3,'RELEASED','2024-07-05 09:29:26.088352'),
    (1000022,'FUAS',NULL,NULL,'2024-07-06','2024-07-10',NULL,NULL,3,'RELEASED','2024-07-05 15:30:55.136103'),
    (1000023,'FUAS',NULL,NULL,'2024-07-09','2024-07-13',NULL,NULL,2,'PLANNED','2024-07-08 15:47:57.177334'),
    (1000024,'FUAS',NULL,NULL,'2024-07-09','2024-07-13',NULL,NULL,2,'PLANNED','2024-07-08 16:26:32.270911'),
    (1000025,'FUAS',NULL,NULL,'2024-07-09','2024-07-13',NULL,NULL,2,'PLANNED','2024-07-08 17:25:57.504673'),
    (1000026,'FUAS',NULL,NULL,'2024-07-11','2024-07-15',NULL,NULL,1,'CHECKED','2024-07-10 10:24:26.088352'),
    (1000027,'FUAS',NULL,NULL,'2024-07-11','2024-07-15',NULL,NULL,1,'CHECKED','2024-07-10 13:10:25.248400'),
    (1000028,'FUAS',NULL,NULL,'2024-07-11','2024-07-15',NULL,NULL,0,'INITIAL', '2024-07-10 13:15:06.080033');
INSERT INTO "order_ref" ("order","site","material_ref","extorder_ref","bom_ref","routing_ref","plant_ref") VALUES
    (1000020,'FUAS','6500003234',NULL,NULL,NULL,'HD00'),
    (1000021,'FUAS','6500003234',NULL,NULL,NULL,'HD00'),
    (1000022,'FUAS','6500003234',NULL,NULL,NULL,'HD00'),
    (1000023,'FUAS','6500003234',NULL,NULL,NULL,'HD00'),
    (1000024,'FUAS','6500003234',NULL,NULL,NULL,'HD00'),
    (1000025,'FUAS','6500003234',NULL,NULL,NULL,'HD00'),
    (1000026,'FUAS','6500003234',NULL,NULL,NULL,'HD00'),
    (1000027,'FUAS','6500003234',NULL,NULL,NULL,'HD00'),
    (1000028,'FUAS','6500003234',NULL,NULL,NULL,'HD00');
INSERT INTO "task" ("id","site","order","step","next","typ","operation","param_typ","param_value","pds_task") VALUES
    ('1','FUAS','1000020','1','2','start','12-CUTCO','geom','A','1'),
    ('2','FUAS','1000020','2','3','var','12-CUTCO','col','red','2'),
    ('3','FUAS','1000020','3','4','','27-DRILL','diam','15','4'),
    ('4','FUAS','1000020','4','0','final','50-PACKG','pkg','deliver','6'),
    ('5','FUAS','1000021','1','2','start','12-CUTCO','geom','A','1'),
    ('6','FUAS','1000021','2','3','var','12-CUTCO','col','blue','3'),
    ('7','FUAS','1000021','3','4','','27-DRILL','diam','15','4'),
    ('8','FUAS','1000021','4','0','final','50-PACKG','pkg','deliver','6'),
    ('9','FUAS','1000022','1','2','start','12-CUTCO','geom','A','1'),
    ('10','FUAS','1000022','2','3','var','12-CUTCO','col','red','2'),
    ('11','FUAS','1000022','3','4','','27-DRILL','diam','15','4'),
    ('12','FUAS','1000022','4','0','final','50-PACKG','pkg','storage','5'),
    ('13','FUAS','1000023','1','2','start','12-CUTCO','geom','A','1'),
    ('14','FUAS','1000023','2','3','var','12-CUTCO','col','blue','3'),
    ('15','FUAS','1000023','3','4','','27-DRILL','diam','15','4'),
    ('16','FUAS','1000023','4','0','final','50-PACKG','pkg','storage','5'),
    ('17','FUAS','1000024','1','2','start','12-CUTCO','geom','A','1'),
    ('18','FUAS','1000024','2','3','var','12-CUTCO','col','red','2'),
    ('19','FUAS','1000024','3','4','','27-DRILL','diam','25','4'),
    ('20','FUAS','1000024','4','0','final','50-PACKG','pkg','deliver','6'),
    ('21','FUAS','1000025','1','2','start','12-CUTCO','geom','A','1'),
    ('22','FUAS','1000025','2','3','var','12-CUTCO','col','blue','3'),
    ('23','FUAS','1000025','3','4','','27-DRILL','diam','45','4'),
    ('24','FUAS','1000025','4','0','final','50-PACKG','pkg','deliver','6');          
INSERT INTO "part" ("task","site","material","usage","qty","unit") VALUES 
    ('1','FUAS','FUAS-4013','in','1.0','PCE'),
    ('2','FUAS','FUAS-3610-R','in','3.5','G'),
    ('4','FUAS','FUAS-5031','in','1.0','PCE'),    
    ('4','FUAS','FUAS-1100','out','1.0','PCE'),
    ('5','FUAS','FUAS-4013','in','1.0','PCE'),
    ('6','FUAS','FUAS-3610-B','in','3.2','G'),
    ('8','FUAS','FUAS-5031','in','1.0','PCE'),    
    ('8','FUAS','FUAS-1100','out','1.0','PCE'),
    ('9','FUAS','FUAS-4013','in','1.0','PCE'),
    ('10','FUAS','FUAS-3610-R','in','3.5','G'),
    ('12','FUAS','FUAS-5041','in','1.0','PCE'),    
    ('12','FUAS','FUAS-1100','out','1.0','PCE'),
    ('13','FUAS','FUAS-4013','in','1.0','PCE'),
    ('14','FUAS','FUAS-3610-B','in','3.2','G'),
    ('16','FUAS','FUAS-5041','in','1.0','PCE'),    
    ('16','FUAS','FUAS-1100','out','1.0','PCE'),
    ('17','FUAS','FUAS-4013','in','1.0','PCE'),
    ('18','FUAS','FUAS-3610-R','in','3.5','G'),
    ('20','FUAS','FUAS-5031','in','1.0','PCE'),    
    ('20','FUAS','FUAS-1100','out','1.0','PCE'),
    ('21','FUAS','FUAS-4013','in','1.0','PCE'),
    ('22','FUAS','FUAS-3610-B','in','3.2','G'),
    ('24','FUAS','FUAS-5031','in','1.0','PCE'),    
    ('24','FUAS','FUAS-1100','out','1.0','PCE');  
INSERT INTO resource ("task","site","loc","prio","durvar","durfix","unit") VALUES
    ('1','FUAS','','0','30.0','','S'),
    ('2','FUAS','','0','125.0','','S'),
    ('3','FUAS','','0','35.0','','S'),
    ('4','FUAS','','0','55.0','','S'),
    ('5','FUAS','','0','30.0','','S'),
    ('6','FUAS','','0','125.0','','S'),
    ('7','FUAS','','0','35.0','','S'),
    ('8','FUAS','','0','55.0','','S'),
    ('9','FUAS','','0','30.0','','S'),
    ('10','FUAS','','0','125.0','','S'),
    ('11','FUAS','','0','35.0','','S'),
    ('12','FUAS','','0','30.0','','S'),
    ('13','FUAS','','0','30.0','','S'),
    ('14','FUAS','','0','125.0','','S'),
    ('15','FUAS','','0','35.0','','S'),
    ('16','FUAS','','0','30.0','','S'),
    ('17','FUAS','','0','30.0','','S'),
    ('18','FUAS','','0','125.0','','S'),
    ('19','FUAS','','0','35.0','','S'),
    ('20','FUAS','','0','55.0','','S'),
    ('21','FUAS','','0','30.0','','S'),
    ('22','FUAS','','0','125.0','','S'),
    ('23','FUAS','','0','35.0','','S'),
    ('24','FUAS','','0','55.0','','S');
INSERT INTO "schedule" ("id","site","order","sfcu","operation","supplier","org","mhu","due","prio","state","statx","at") VALUES 
    (1,'FUAS',1000020,1,'ALL','AREA-1','area',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 08:15:52.636603'),
    (2,'FUAS',1000021,2,'ALL','AREA-1','area',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 09:29:26.088352'),
    (3,'FUAS',1000022,3,'ALL','AREA-1','area',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 15:30:55.136103'),
    (4,'FUAS',1000022,4,'ALL','AREA-1','area',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 15:30:55.136103'),    
    (5,'FUAS',1000023,5,'ALL','AREA-1','area',NULL,'2024-07-13',0,1,'PLANNED','2024-07-08 15:47:57.177334'),
    (6,'FUAS',1000023,6,'ALL','AREA-1','area',NULL,'2024-07-13',0,1,'PLANNED','2024-07-08 15:47:57.177434'),
    (7,'FUAS',1000023,7,'ALL','AREA-1','area',NULL,'2024-07-13',0,1,'PLANNED','2024-07-08 15:47:57.177534'),
    (8,'FUAS',1000024,8,'ALL','AREA-1','area',NULL,'2024-07-13',0,1,'PLANNED','2024-07-08 16:26:32.270911'),
    (9,'FUAS',1000024,9,'ALL','AREA-1','area',NULL,'2024-07-13',0,1,'PLANNED','2024-07-08 16:26:32.271011'),
    (10,'FUAS',1000025,10,'ALL','AREA-1','area',NULL,'2024-07-13',0,1,'PLANNED','2024-07-08 17:25:57.504673');   
INSERT INTO "sfcu" ("id", "site","order","material","state","statx","at") VALUES
    (1,'FUAS',1000020,'FUAS-1010',2,'RELEASED','2024-07-05 08:15:52.636603'),
    (2,'FUAS',1000021,'FUAS-1010',2,'RELEASED','2024-07-05 09:29:26.088352'),
    (3,'FUAS',1000022,'FUAS-1010',2,'RELEASED','2024-07-05 15:30:55.136103'),
    (4,'FUAS',1000022,'FUAS-1010',2,'RELEASED','2024-07-05 15:30:55.136103'),    
    (5,'FUAS',1000023,'FUAS-1010',1,'PLANNED','2024-07-08 15:47:57.177334'),
    (6,'FUAS',1000023,'FUAS-1010',1,'PLANNED','2024-07-08 15:47:57.177434'),
    (7,'FUAS',1000023,'FUAS-1010',1,'PLANNED','2024-07-08 15:47:57.177534'),
    (8,'FUAS',1000024,'FUAS-1010',1,'PLANNED','2024-07-08 16:26:32.270911'),
    (9,'FUAS',1000024,'FUAS-1010',1,'PLANNED','2024-07-08 16:26:32.271011'),
    (10,'FUAS',1000025,'FUAS-1010',1,'PLANNED','2024-07-08 17:25:57.504673');
COMMIT;
"""

AREA1_TEST_DATA = """
BEGIN TRANSACTION;
INSERT INTO "facility" ("id","org","desc","loc","state","statx","at") VALUES 
    ('CELL-1','cell','Laser cutting and coating (CUTCO1)',NULL,1,'READY','14.07.2020  08:00:00'),
    ('CELL-2','cell','Laser cutting and coating (CUTCO2)',NULL,1,'READY','14.07.2020  08:00:00'),
    ('CELL-3','cell','Drilling and reaming (REAMR1)',NULL,1,'READY','14.07.2020  08:00:00'),
    ('CELL-4','cell','Packaging station (PACKG1)',NULL,1,'READY','14.07.2020  08:00:00');
INSERT INTO "facility_operation" ("id","facility","operation","param_typ","param_min","param_max","value_typ","setup","state","statx") VALUES 
    (1,'CELL-1','12-CUTCO','geom','A','','value',40,'',''),
    (2,'CELL-1','12-CUTCO','geom','B','','value',20,'',''),    
    (3,'CELL-1','12-CUTCO','col','red','','value',10,'',''),
    (4,'CELL-1','12-CUTCO','col','blue','','value',10,'',''),
    (5,'CELL-2','12-CUTCO','geom','A','','value',40,'',''),
    (6,'CELL-2','12-CUTCO','geom','C','','value',30,'',''),    
    (7,'CELL-2','12-CUTCO','col','red','','value',10,'',''),
    (8,'CELL-2','12-CUTCO','col','blue','','value',10,'',''),
    (9,'CELL-3','27-DRILL','diam',10,19,'range',25,'',''),
    (10,'CELL-3','27-DRILL','diam',20,39,'range',30,'',''),
    (11,'CELL-3','27-DRILL','diam',40,60,'range',35,'',''),
    (12,'CELL-4','50-PACKG','pkg','storage','','value',60,'',''),
    (13,'CELL-4','50-PACKG','pkg','deliver','','value',25,'',''),
    (14,'CELL-4','50-PACKG','pkg','nopkg','','value',0,'',''); 
INSERT INTO "order" ("id","site","material","variant","qty","unit","pds","at") VALUES 
    (1000020,'FUAS','FUAS-1100','col:red',1.0,'PCE',1,'2024-07-05 08:15:52.636603'),
    (1000021,'FUAS','FUAS-1100','col:blue',1.0,'PCE',1,'2024-07-05 09:29:26.088352'),
    (1000022,'FUAS','FUAS-1100','col:red',1.0,'PCE',1,'2024-07-05 15:30:55.136103');
INSERT INTO "task" ("id","site","order","step","next","typ","operation","param_typ","param_value","pds_task") VALUES
    ('1','FUAS','1000020','1','2','start','12-CUTCO','geom','A','1'),
    ('2','FUAS','1000020','2','3','var','12-CUTCO','col','red','2'),
    ('3','FUAS','1000020','3','4','','27-DRILL','diam','15','4'),
    ('4','FUAS','1000020','4','0','final','50-PACKG','pkg','deliver','6'),
    ('5','FUAS','1000021','1','2','start','12-CUTCO','geom','A','1'),
    ('6','FUAS','1000021','2','3','var','12-CUTCO','col','blue','3'),
    ('7','FUAS','1000021','3','4','','27-DRILL','diam','15','4'),
    ('8','FUAS','1000021','4','0','final','50-PACKG','pkg','deliver','6'),
    ('9','FUAS','1000022','1','2','start','12-CUTCO','geom','A','1'),
    ('10','FUAS','1000022','2','3','var','12-CUTCO','col','red','2'),
    ('11','FUAS','1000022','3','4','','27-DRILL','diam','15','4'),
    ('12','FUAS','1000022','4','0','final','50-PACKG','pkg','storage','5');
INSERT INTO "part" ("task","site","material","usage","qty","unit") VALUES 
    ('1','FUAS','FUAS-4013','in','1.0','PCE'),
    ('2','FUAS','FUAS-3610-R','in','3.5','G'),
    ('4','FUAS','FUAS-5031','in','1.0','PCE'),    
    ('4','FUAS','FUAS-1100','out','1.0','PCE'),
    ('5','FUAS','FUAS-4013','in','1.0','PCE'),
    ('6','FUAS','FUAS-3610-B','in','3.2','G'),
    ('8','FUAS','FUAS-5031','in','1.0','PCE'),    
    ('8','FUAS','FUAS-1100','out','1.0','PCE'),
    ('9','FUAS','FUAS-4013','in','1.0','PCE'),
    ('10','FUAS','FUAS-3610-R','in','3.5','G'),
    ('12','FUAS','FUAS-5041','in','1.0','PCE'),    
    ('12','FUAS','FUAS-1100','out','1.0','PCE');
INSERT INTO resource ("task","site","loc","prio","durvar","durfix","unit") VALUES
    ('1','FUAS','','0','30.0','','S'),
    ('2','FUAS','','0','125.0','','S'),
    ('3','FUAS','','0','35.0','','S'),
    ('4','FUAS','','0','55.0','','S'),
    ('5','FUAS','','0','30.0','','S'),
    ('6','FUAS','','0','125.0','','S'),
    ('7','FUAS','','0','35.0','','S'),
    ('8','FUAS','','0','55.0','','S'),
    ('9','FUAS','','0','30.0','','S'),
    ('10','FUAS','','0','125.0','','S'),
    ('11','FUAS','','0','35.0','','S'),
    ('12','FUAS','','0','30.0','','S');
INSERT INTO "schedule" ("id","site","order","sfcu","operation","supplier","org","mhu","due","prio","state","statx","at") VALUES 
    (1,'FUAS',1000020,1,'ALL','AREA-1','area',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 08:15:52.636603'),
    (2,'FUAS',1000021,2,'ALL','AREA-1','area',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 09:29:26.088352'),
    (3,'FUAS',1000022,3,'ALL','AREA-1','area',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 15:30:55.136103'),
    (4,'FUAS',1000020,1,'12-CUTCO','CELL-1','cell',111,'2024-07-10',0,2,'ASSIGNED','2024-07-05 08:15:52.636603'),
    (5,'FUAS',1000021,2,'12-CUTCO','CELL-1','cell',115,'2024-07-10',0,2,'ASSIGNED','2024-07-05 09:29:26.088352'),
    (6,'FUAS',1000022,3,'12-CUTCO','CELL-1','cell',119,'2024-07-10',0,2,'ASSIGNED','2024-07-05 15:30:55.136103'),
    (7,'FUAS',1000020,1,'27-DRILL','CELL-3','cell',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 08:15:52.636603'),
    (8,'FUAS',1000021,2,'27-DRILL','CELL-3','cell',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 09:29:26.088352'),
    (9,'FUAS',1000022,3,'27-DRILL','CELL-3','cell',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 15:30:55.136103'),
    (10,'FUAS',1000020,1,'50-PACKG','CELL-4','cell',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 08:15:52.636603'),
    (11,'FUAS',1000021,2,'50-PACKG','CELL-4','cell',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 09:29:26.088352'),
    (12,'FUAS',1000022,3,'50-PACKG','CELL-4','cell',NULL,'2024-07-10',0,2,'ASSIGNED','2024-07-05 15:30:55.136103');               
INSERT INTO "sfcu" ("id","site","order","material","state","statx","at") VALUES
    (1,'FUAS',1000020,'FUAS-1010',2,'RELEASED','2024-07-05 08:15:52.636603'),
    (2,'FUAS',1000021,'FUAS-1010',2,'RELEASED','2024-07-05 09:29:26.088352'),
    (3,'FUAS',1000022,'FUAS-1010',2,'RELEASED','2024-07-05 15:30:55.136103'); 
COMMIT;
"""

CELL1_TEST_DATA = """
BEGIN TRANSACTION;
INSERT INTO "facility" ("id","org","desc","loc","state","statx","at") VALUES 
    ('CUTCO1','cell','Laser cutting and coating station 1',NULL,1,'READY','14.07.2020  08:00:00');    
INSERT INTO "facility_operation" ("id","facility","operation","param_typ","param_min","param_max","value_typ","setup","state","statx") VALUES 
    (1,'CUTCO1','12-CUTCO','geom','A','','value',5,'',''),
    (2,'CUTCO1','12-CUTCO','geom','B','','value',10,'',''),    
    (3,'CUTCO1','12-CUTCO','col','red','','value',10,'',''),
    (4,'CUTCO1','12-CUTCO','col','blue','','value',10,'','');
INSERT INTO "order" ("id","site","material","variant","qty","unit","pds","at") VALUES 
    (1000020,'FUAS','FUAS-1100','col:red',1.0,'PCE',1,'2024-07-05 08:15:52.636603'),
    (1000021,'FUAS','FUAS-1100','col:blue',1.0,'PCE',1,'2024-07-05 09:29:26.088352');
INSERT INTO "task" ("id","site","order","step","next","typ","operation","param_typ","param_value","pds_task") VALUES
    ('1','FUAS','1000020','1','2','start','12-CUTCO','geom','A','1'),
    ('2','FUAS','1000020','2','3','var','12-CUTCO','col','red','2'),    
    ('5','FUAS','1000021','1','2','start','12-CUTCO','geom','A','1'),
    ('6','FUAS','1000021','2','3','var','12-CUTCO','col','blue','3');
INSERT INTO "part" ("task","site","material","usage","qty","unit") VALUES 
    ('1','FUAS','FUAS-4013','in','1.0','PCE'),
    ('2','FUAS','FUAS-3610-R','in','3.5','G'),
    ('5','FUAS','FUAS-4013','in','1.0','PCE'),
    ('6','FUAS','FUAS-3610-B','in','3.2','G');
INSERT INTO resource ("task","site","loc","prio","durvar","durfix","unit") VALUES
    ('1','FUAS','','0','30.0','','S'),
    ('2','FUAS','','0','125.0','','S'),
    ('5','FUAS','','0','30.0','','S'),
    ('6','FUAS','','0','125.0','','S');
INSERT INTO "schedule" ("id","site","order","sfcu","operation","supplier","org","mhu","due","prio","state","statx","at") VALUES 
    (1,'FUAS',1000020,1,'12-CUTCO','CELL-1','cell',111,'2024-07-10',0,2,'ASSIGNED','2024-07-05 08:15:52.636603'), 
    (2,'FUAS',1000021,2,'12-CUTCO','CELL-1','cell',115,'2024-07-10',0,2,'ASSIGNED','2024-07-05 09:29:26.088352');
INSERT INTO "sfcu" ("id", "site","order","material","state","statx","at") VALUES
    (1,'FUAS',1000020,'FUAS-1100',1,'PLANNED','2024-07-05 08:15:52.636603'),
    (2,'FUAS',1000021,'FUAS-1101',1,'PLANNED','2024-07-05 09:29:26.088352');
COMMIT;    
"""
