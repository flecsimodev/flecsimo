#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" Packaging tool to create zips compatible with CFW app ecosystem out of a 
given local flecsimo repository tree. 

@author: Bernhard Lehner
"""

LICENSE_TEXT = """ 
    Copyright and License Notice:

    flecsimo txt_packager
    Copyright (C) 2022  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import shutil, sys
from pathlib import Path
import click


class FlecsimoPackagerError(Exception):
    pass


@click.command()
@click.option('-c', '--cellid', 'cellid', required=True,
              help='Short name of the cell, as is found in ' + \
              'flecsimo/app/txt/, e.g. REAMR1')
@click.option('-s', '--source', 'source', required=True,
              type=click.Path(exists=True, readable=True, path_type=Path),
              help='Path of local flecsimo source tree. ' + \
              'The path should end with ...src/')
@click.option('-d', '--destination', 'destination', required=True,
              type=click.Path(exists=True, readable=True, path_type=Path),
              help="Destination path to store package during packaging")
@click.option('-n', '--noconfirm', is_flag=True,
              help="Bypass any and all “Are you sure?” messages")
@click.option('-v', '--verbose', is_flag=True, help="Output more detail")
@click.option('--license', 'license_', is_flag=True, help='Print license.')

# Main...
def cli(source, destination, cellid, verbose, noconfirm, license_):
    """Packaging tool to create zips compatible with the CFW app ecosystem out 
    of a given local flecsimo repository tree. The project files will be 
    automatically collected from the tree, including the sources.txt to be 
    interpreted. However, you will be able to edit the contents of the manifest 
    file locally (without altering the version inside your git directory) 
    before it is finally packaged."""
    if license_ is True:
        print(LICENSE_TEXT)
        exit()

    localtree = source
    # check source for typical Flecsimo folder
    if Path.is_dir(localtree / 'flecsimo') == False:
        raise FlecsimoPackagerError('The given source is not a valid' + \
        'flecsimo project tree.')

    # cellid input: check if existing in local git, generate pkg folder path
    # for later and for sources.txt
    cellpkgpath = Path(localtree / 'flecsimo/app/txt' / cellid / 'pkg')
    if Path.is_dir(cellpkgpath) == False:
        raise FlecsimoPackagerError('No cell pkg folder for the given cell ' + \
        'name (cellid) found in app/txt/ of your local Flecsimo git directory.')

    # check that defined content in app/txt/CELLID/pkg folder is present
    pkgfiles = ('manifest', 'icon.png', 'sources.txt', 'startup.py',
                'configure.py')
    for pkgfilename in pkgfiles:
        if verbose:
            print(f"checking if {pkgfilename} is present")
        if not Path.is_file(cellpkgpath / pkgfilename):  # is file present?
            raise FlecsimoPackagerError(f"{pkgfilename} does not exist in " + \
                            "the given local flecsimo tree ({cellpkgpath}).")

    # open and read sources.txt
    sources = open(cellpkgpath / 'sources.txt')
    sourcelines = sources.readlines()  # read sources.txt line by line

    # FILE SOURCING ROUTINE
    # preparing the local temp folder beforehand
    temp_destination = destination / 'temp_destination'
    # check if temp_destination folder is already present from earlier script
    # runs
    if Path.is_dir(temp_destination) == True:
        shutil.rmtree(temp_destination)  # if so, remove it and all its contents
        if verbose:
            print('removed old temporary copytree')
    # tempdesttree = temp_destination / 'Flecsimo/src'
    tempdesttree = temp_destination
    # in any case, there be a freshly made temp_destination folder:
    Path.mkdir(tempdesttree)
    if verbose:
        print('created temporary copy destination')

    # parsing lines from sources.txt
    for line in sourcelines:  # go through lines of sources.txt
        if line.isspace() == True:  # only whitespace, e.g. newlines?
            continue
        if line == '':  # empty line?
            continue
        if line.startswith('#'):  # skip commented lines
            continue
        line = Path(line.strip())  # remove e.g. newlines and convert to Path
        linepath = localtree / line
        # is there a file like this in the given local tree?
        if not Path.is_file(linepath):
            print(f"warning: {linepath} does not exist")
            if noconfirm:
                continue
            elif click.confirm('Continue copying the rest anyways?', 
                               default=True, abort=True):
                continue

        # actual copying
        tempdestpath = tempdesttree / line
        # print(f"linepath: {linepath}")
        # print(f"tempdestpath: {tempdestpath}")
        tempdestdir = tempdestpath.parent
        if not Path.is_dir(tempdestdir):
            Path.mkdir(tempdestdir, parents=True)
        shutil.copy(linepath, tempdestpath)
        if verbose:
            print(f"copied {linepath} \n    to {tempdestpath}")

        # generate all parent paths of this line, recursively
        lineparents = []
        linedir = line.parent
        lineparents.append(linedir)
        # rather hacky solution, might improve some day
        # TODO: use generic pathlib parents() function - it will do it out of
        # the box (Ralf)
        while '/' in str(linedir):
            linedir = linedir.parent
            lineparents.append(linedir)

        # iterate over all the parent paths to collect all init files
        for parentpath in lineparents:
            initpydestpath = tempdesttree / parentpath / '__init__.py'
            if not Path.is_file(initpydestpath):
                initpysourcepath = localtree / parentpath / '__init__.py'
                if not Path.is_file(initpysourcepath):
                    print(f"warning: {initpysourcepath} does not exist; " + \
                          "file could not be copied")
                else:
                    shutil.copy(initpysourcepath, initpydestpath)
                    if verbose:
                        print(f"copied {initpysourcepath} \n    to {initpydestpath}")

    if verbose:
        print('finished copying files from sourcetree')

    # copy startup.py, manifest, icon
    # toplvlfiles = ('manifest', 'icon.png', 'startup.py')
    toplvlfiles = ('manifest', 'icon.png', 'configure.py')
    for toplvlfilename in toplvlfiles:
        shutil.copy(cellpkgpath / toplvlfilename, temp_destination)
        if verbose:
            print(f"copied {toplvlfilename}")
    startuppath = cellpkgpath / 'startup.py'
    startupdestpath = temp_destination / 'startup.py'
    # workaround: rewrite startup.py with unix-style newlines
    with open(startuppath, "r") as i:
        with open(startupdestpath, "w", newline="\n") as o:
            o.writelines(i.readlines())
    if verbose:
        print("finished copying top level files")

    if not noconfirm:
        # ask before final packaging
        print('''\nNote:
        All files are now copied to the temp_destination folder as they are 
        stored on the txt controller. However, they are not yet packed as a 
        zip file. You can still edit the files , e.g. the manifest file or 
        another db file..''')
        click.pause(info='Press any key as soon as you are finished so the ' + \
                    'final packaging will start.')

    # create the zip containing the contents of temp_destination
    zipname = destination / 'packaged_txt_app'
    shutil.make_archive(zipname, "zip", temp_destination)
    if verbose:
        print('created zip file')

    print('Done.')
    # TODO ggf. Statusausgabe und Hilfehinweis für nächsten Schritt
    # -> Webinterface, TXT-IP, Zip hochladen

    # click.pause() #maybe helpful on Windows?
    sys.exit()  # TODO still needed?


if __name__ == '__main__':
    # Run as main.
    cli()