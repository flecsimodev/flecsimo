"""SQL scripts to create flecsimo schemas.
Created on 20.12.2021

@author: Ralf Banning

Copyright and License Notice:

    SQL scripts (DDL / DML) for flecsimo databases.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

# Core database tables used by every factory layer.
CORE_DDL = """
BEGIN TRANSACTION;

DROP TABLE IF EXISTS "facility";
CREATE TABLE IF NOT EXISTS "facility" (
    "id"    TEXT,
    "org"   TEXT,
    "desc"  TEXT,
    "loc"   TEXT,
    "state" INTEGER,
    "statx" TEXT,
    "at"    TIMESTAMP,
    PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "facility_operation";
CREATE TABLE IF NOT EXISTS "facility_operation" (
    "id"        INTEGER,
    "facility"  INTEGER NOT NULL,
    "operation" TEXT NOT NULL,
    "param_typ" TEXT,
    "param_min" INTEGER,
    "param_max" INTEGER,
    "value_typ" INTEGER,
    "setup"     INTEGER,
    "state"     INTEGER,
    "statx"     TEXT,
    PRIMARY KEY("id"),
    FOREIGN KEY("facility") REFERENCES "facility"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "facility_operation";
CREATE TABLE IF NOT EXISTS "facility_operation" (
    "id"        INTEGER,
    "facility"  INTEGER NOT NULL,
    "operation" TEXT NOT NULL,
    "param_typ" TEXT,
    "param_min" INTEGER,
    "param_max" INTEGER,
    "value_typ" INTEGER,
    "setup"     INTEGER,
    "state"     INTEGER,
    "statx"     TEXT,
    PRIMARY KEY("id"),
    FOREIGN KEY("facility") REFERENCES "facility"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "material";
CREATE TABLE IF NOT EXISTS "material" (
    "id"   TEXT,
    "desc" TEXT NOT NULL,
    "typ"  TEXT NOT NULL,
    "unit" TEXT NOT NULL DEFAULT 'PCE',
    PRIMARY KEY("id")
) WITHOUT ROWID;

DROP TABLE IF EXISTS "order";
CREATE TABLE IF NOT EXISTS "order" (
    "id"       INTEGER,
    "site"     TEXT NOT NULL,
    "material" TEXT NOT NULL,
    "variant"  TEXT,
    "qty"      REAL NOT NULL DEFAULT 1,
    "unit"     TEXT NOT NULL DEFAULT 'PCE',
    "pds"      INTEGER,
    "at"       TIMESTAMP,
    PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "task";
CREATE TABLE IF NOT EXISTS "task" (
    "id"          INTEGER,
    "site"        TEXT NUT NULL,
    "order"       INTEGER,
    "step"        INTEGER,
    "next"        INTEGER,
    "typ"         TEXT,
    "operation"   TEXT,
    "param_typ"   TEXT,
    "param_value" TEXT,
    "pds_task"    INTEGER NOT NULL,
    PRIMARY KEY("id"),
    FOREIGN KEY("order") REFERENCES "order"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "part";
CREATE TABLE IF NOT EXISTS "part" (
    "task"     INTEGER NOT NULL,
    "site"     TEXT NOT NULL,
    "material" TEXT,
    "usage"    TEXT,
    "qty"      REAL NOT NULL DEFAULT 1,
    "unit"     TEXT NOT NULL DEFAULT 'PCE',
    PRIMARY KEY("task","material","usage"),
    FOREIGN KEY("task") REFERENCES "task"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "resource";
CREATE TABLE IF NOT EXISTS "resource" (
    "task"   INTEGER,
    "site"   TEXT NOT NULL,
    "loc"    TEXT,
    "prio"   INTEGER,
    "durvar" REAL,
    "durfix" REAL,
    "unit"   TEXT DEFAULT 'S',
    PRIMARY KEY("task"),
    FOREIGN KEY("task") REFERENCES "task"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "quotation";
CREATE TABLE IF NOT EXISTS "quotation" (
    "id"        INTEGER,
    "site"      TEXT NOT NULL,
    "order"     INTEGER NOT NULL,
    "sfcu"      INTEGER NOT NULL,
    "operation" TEXT NOT NULL DEFAULT 'NODATA',
    "supplier"  TEXT NOT NULL DEFAULT 'NODATA',
    "org"       TEXT,
    "mhu"       TEXT,
    "due"       TIMESTAMP,
    "prio"      INTEGER,
    "state"     INTEGER,
    "statx"     TEXT,
    "at"        TIMESTAMP,
    PRIMARY KEY("id"),
    UNIQUE("order","sfcu","operation","supplier")
);

DROP TABLE IF EXISTS "sfcu";
CREATE TABLE IF NOT EXISTS "sfcu" (
    "id"       INTEGER,
    "site"     TEXT NOT NULL,
    "order"    INTEGER,
    "material" TEXT NOT NULL,
    "state"    INTEGER,
    "statx"    TEXT,
    "at"       TIMESTAMP,
    PRIMARY KEY("id"),
    FOREIGN KEY("order") REFERENCES "order"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "schedule";
CREATE TABLE IF NOT EXISTS "schedule" (
    "id"        INTEGER,
    "site"      TEXT NOT NULL,
    "order"     INTEGER NOT NULL,
    "sfcu"      INTEGER NOT NULL,
    "operation" TEXT NOT NULL DEFAULT NODATA,
    "supplier"  TEXT NOT NULL DEFAULT NODATA,
    "org"       TEXT,
    "mhu"       TEXT,
    "due"       TIMESTAMP,
    "prio"      INTEGER NOT NULL DEFAULT '0',
    "state"     INTEGER,
    "statx"     TEXT,
    "at"        TIMESTAMP,
    PRIMARY KEY("id"),
    UNIQUE("order","sfcu","operation","supplier")
);

--Not in use any longer - to be deleted before commit.
--DROP VIEW IF EXISTS "sfcu_taskdata";
--CREATE VIEW IF NOT EXISTS "sfcu_taskdata" (
--    sfcu,
--    operation,
--    param_typ,
--    param_value,
--    setup,
--    statx
--    )
--AS SELECT
--    s.id,
--    t.operation, 
--    t.param_typ, 
--    t.param_min, 
--    t.setup,
--    u.statx 
--FROM sfcu as s, task_operable as t, schedule as u
--WHERE s.'order' = t.'order'
--AND s.id = u.sfcu;

DROP VIEW IF EXISTS "task_resources";
CREATE VIEW IF NOT EXISTS "task_resources" ( 
    task,
    site,
    loc, 
    prio, 
    durvar, 
    durfix, 
    unit,
    'order',
    operation   
    )
AS SELECT 
    r.task, 
    r.site,    
    r.loc, 
    r.prio, 
    r.durvar, 
    r.durfix, 
    r.unit,
    t.'order',
    t.operation
FROM resource r, task t
WHERE r.task = t.id;

DROP VIEW IF EXISTS "task_parts";
CREATE VIEW IF NOT EXISTS "task_parts" ( 
    task,
    site,
    material, 
    usage, 
    qty, 
    unit,
    'order',
    operation
)
AS SELECT 
    p.task, 
    p.site, 
    p.material, 
    p.usage, 
    p.qty, 
    p.unit,
    t.'order',
    t.operation
FROM part p, task t
WHERE p.task = t.id;

--Not in use any longer - to be deleted before commit.
--DROP VIEW IF EXISTS "task_operable";
--CREATE VIEW IF NOT EXISTS "task_operable" (
--    site,
--    'order',
--    id,
--    facility,
--    operation,
--    param_typ,
--    param_min,
--    param_max,
--    value_typ,
--    setup,
--    state,
--    statx)
--AS SELECT 
--    t.site, 
--    t.'order', 
--    f.*
--FROM facility_operation AS f
--JOIN task AS t ON f.operation = t.operation
--WHERE CASE f.value_typ
--    WHEN 'range' THEN t.param_value BETWEEN f.param_min and f.param_max
--    WHEN 'value' THEN f.param_min = t.param_value
--    END;    
COMMIT;
"""

# Specific tables used by the site factory layer.
SITE_DDL = """
BEGIN TRANSACTION;

/* New version for strategy support (MTO / MTS)
DROP TABLE IF EXIST "strategy";
CREATE TABLE IF NOT EXIST "strategy" (
    "id"          INTEGER,
    "description" TEXT,
    PRIMARY KEY("id")
);    
*/

DROP TABLE IF EXISTS "pds";
CREATE TABLE IF NOT EXISTS "pds" (
    "id"          INTEGER,
    "site"        TEXT NOT NULL,
    "material"    TEXT,
    "has_variant" INTEGER NOT NULL DEFAULT 0,
    "min_lotsize" INTEGER NOT NULL DEFAULT 1,
    "max_lotsize" INTEGER NOT NULL DEFAULT 10,
    "valid_from"  TIMESTAMP NOT NULL,
    "valid_to"    TIMESTAMP,
    PRIMARY KEY("id"),
    FOREIGN KEY("material") REFERENCES "material"("id")
);

/* New version for strategy support (MTO / MTS)
DROP TABLE IF EXISTS "pds_task";
CREATE TABLE "pds" (
    "id"          INTEGER,
    "site"        TEXT NOT NULL,
    "material"    TEXT,
    "strategy"    INTEGER DEFAULT 20,
    "has_variant" INTEGER NOT NULL DEFAULT 0,
    "valid_from"  TIMESTAMP NOT NULL,
    "valid_to"    TIMESTAMP,
    PRIMARY KEY("id"),
    FOREIGN KEY("material") REFERENCES "material"("id"),
    FOREIGN KEY("strategy") REFERENCES "strategy"("id")
);
*/

DROP TABLE IF EXISTS "pds_task";
CREATE TABLE IF NOT EXISTS "pds_task" (
    "id"          INTEGER,
    "pds"         INTEGER NOT NULL,
    "step"        INTEGER NOT NULL,
    "next"        INTEGER,
    "typ"         TEXT,
    "operation"   TEXT NOT NULL,
    "variant"     TEXT,
    "param_typ"   TEXT,
    "param_value" TEXT,
    PRIMARY KEY("id"),
    FOREIGN KEY("pds") REFERENCES "pds"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "pds_part";
CREATE TABLE IF NOT EXISTS "pds_part" (
    "pds_task"  INTEGER,
    "material"  TEXT,
    "usage"     TEXT,
    "qty"       REAL NOT NULL DEFAULT 1,
    "unit"      TEXT NOT NULL DEFAULT 'PCE',
    PRIMARY KEY("pds_task","material","usage"),
    FOREIGN KEY("material") REFERENCES "material"("id"),
    FOREIGN KEY("pds_task") REFERENCES "pds_task"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "pds_resource";
CREATE TABLE IF NOT EXISTS "pds_resource" (
    "pds_task" INTEGER,
    "loc"      TEXT,
    "prio"     INTEGER,
    "durvar"   REAL,
    "durfix"   REAL,
    "unit"     TEXT DEFAULT 'S',
    PRIMARY KEY("pds_task"),
    FOREIGN KEY("pds_task") REFERENCES "pds_task"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "operation";
CREATE TABLE IF NOT EXISTS "operation" (
    "id"    TEXT,
    "desc"  TEXT NOT NULL,
    PRIMARY KEY("id")
) WITHOUT ROWID;

DROP TABLE IF EXISTS "order_parameter";
CREATE TABLE IF NOT EXISTS "order_parameter" (
    "order"   INTEGER,
    "site"    TEXT NOT NULL,
    "typ"     TEXT NOT NULL,
    "value"   TEXT,
    PRIMARY KEY("order", "typ"),
    FOREIGN KEY("order") REFERENCES "order"("id") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "order_plan";
CREATE TABLE IF NOT EXISTS "order_plan" (
    "order"        INTEGER NOT NULL,
    "site"         TEXT NOT NULL,
    "actual_start" TIMESTAMP,
    "actual_end"   TIMESTAMP,
    "plan_start"   TIMESTAMP,
    "plan_end"     TIMESTAMP,
    "schd_start"   TIMESTAMP,
    "schd_end"     TIMESTAMP,
    "state"        INTEGER,
    "statx"        TEXT,
    "at"           TIMESTAMP,
    FOREIGN KEY("order") REFERENCES "order"("id") ON DELETE CASCADE
);    

DROP TABLE IF EXISTS "order_ref";
CREATE TABLE IF NOT EXISTS "order_ref" (
    "order"        INTEGER,
    "site"         TEXT NOT NULL,
    "material_ref" TEXT,
    "extorder_ref" TEXT,
    "bom_ref"      TEXT,
    "routing_ref"  TEXT,
    "plant_ref"    TEXT,
    FOREIGN KEY("order") REFERENCES "order"("id") ON DELETE CASCADE
);

COMMIT;
"""

AREA_DDL = """
BEGIN TRANSACTION;

--- check if this table is really required for areas. Don't think so. 
--- DROP TABLE IF EXISTS "pds";
--- CREATE TABLE IF NOT EXISTS "pds" (
---     "id"          INTEGER,
---     "site"        TEXT NOT NULL,
---     "material"    TEXT,
---     "has_variant" INTEGER NOT NULL DEFAULT 0,
---     "valid_from"  TIMESTAMP NOT NULL,
---     "valid_to"    TIMESTAMP,
---     PRIMARY KEY("id"),
---     FOREIGN KEY("material") REFERENCES "material"("id")
--- );

COMMIT;
"""

# Specific tables used by the cell and station factory layer.
CELL_DDL = """
BEGIN TRANSACTION;

DROP TABLE IF EXISTS "consumption";
CREATE TABLE IF NOT EXISTS "consumption" (
    "operation" TEXT,
    "material"  TEXT,
    "desc"      TEXT,
    "qty"       REAL NOT NULL,
    "unit"      TEXT DEFAULT 'PCE',
    "base"      TEXT DEFAULT 'EACH',
    PRIMARY KEY("operation","material")
) WITHOUT ROWID;

DROP TABLE IF EXISTS "inventory";
CREATE TABLE IF NOT EXISTS "inventory" (
    "facility"    TEXT,
    "material"    TEXT,
    "qty"    REAL,
    "unit"    TEXT DEFAULT 'PCE',
    "max"    REAL,
    "reord"    REAL,
    "min"    REAL,
    "at"    TIMESTAMP,
    PRIMARY KEY("facility","material")
);

--- check if this table is really required for cells. Don't think so.
--- DROP TABLE IF EXISTS "pds";
--- CREATE TABLE IF NOT EXISTS "pds" (
---     "id"          INTEGER,
---     "site"        TEXT NOT NULL,
---     "material"    TEXT,
---     "has_variant" INTEGER NOT NULL DEFAULT 0,
---     "valid_from"  TIMESTAMP NOT NULL,
---     "valid_to"    TIMESTAMP,
---     PRIMARY KEY("id"),
---     FOREIGN KEY("material") REFERENCES "material"("id")
--- );

DROP VIEW IF EXISTS "sfcu_setup";
CREATE VIEW IF NOT EXISTS "sfcu_setup" (
    site, 
    'order', 
    sfcu,
    supplier,    
    facility,
    operation,
    param_typ,
    param_value,
    param_min,
    param_max,
    value_typ,
    setup,
    state,
    statx
    )
AS SELECT 
    t.site, 
    t.'order', 
    s.id as sfcu,
    u.supplier,    
    f.facility,
    f.operation,
    f.param_typ,
    t.param_value,
    f.param_min,
    f.param_max,
    f.value_typ,
    f.setup,
    u.state,
    u.statx
FROM facility_operation AS f, sfcu as s, schedule as u
JOIN task AS t ON f.operation = t.operation
WHERE CASE f.value_typ
    WHEN 'range' THEN t.param_value BETWEEN f.param_min and f.param_max
    WHEN 'value' THEN f.param_min = t.param_value
    END
AND s."order" = t."order"
AND s.id = u.sfcu;
COMMIT;
"""
