"""Global constants for flecsimo system.

Created on 21.07.2020

@author: Ralf Banning

Copyright and License Notice:
    flecsimo site control
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
from enum import Enum


class Orglevel(Enum):
    """Constant definition for organizational level within the factory model."""
    SITE = 'site'
    AREA = 'area'
    CELL = 'cell'
    STATION = 'station'
    STOCK = 'stock'
    TRSP = 'transport'
    MIXED = 'mixed'


class Component(Enum):
    """Constant definitions for component types."""
    AGENTTYP = 'agent'
    CONTROLTYP = 'controller'
    STATION = 'station'
    SIMTYP = 'simulation'
    MIXED = 'mixed'
    NODETYP ='node'