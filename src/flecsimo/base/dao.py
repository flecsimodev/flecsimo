"""Data access object methods.

Created on 17.08.2021

@author: Ralf Banning

Copyright and License Notice:

   flecsimo dao base class
   Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
   of Applied Sciences.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import sqlite3


class DataAccess:
    """A general data access object for flecsimo sqlite database.
    
    This object provides a generic persist() and retrieve() method
    to store or read a single object from an sqlite database as it
    is used in a flecsimo lab. Moreover it provides a connect 
    method with a predefined row factory.
      
    Args:
        database (str): the database name (and path). 
        
    Note:
        The database connection will be established and closed with every 
        method call of persist and retrieve to guarantee a thread compliant 
        atomic function behavior. This is a compromise between performance 
        and design stability.
        
        Using the persist method guarantees best stability storing messages
        from the flecsimo mqtt backbone, but comes with the price of consuming
        up to 20% more time for execution. If high throughput is prior to
        safety and stability, use native SQLite execution instead.
"""

    def __init__(self, database:str):
        self.database = database
        self.insert_statement = {}
        self.table_dict = {}

        if sqlite3.sqlite_version < "3.33.0":
            self.sqlite_schema = "sqlite_master"
        else:
            self.sqlite_schema = "sqlite_schema"
        self._create_table_dict()

    def connect(self):
        """Create a SQLite connection with a row factory.
        
        Set the standard row factory and enforces foreign key constraints.
        """
        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row
        conn.execute("PRAGMA foreign_keys=1;")
        return conn

    def persist(self, table, data, upsert=None, strict=False, ocdn=False):
        """A generic single object persist method for SQLite databases.
        
        This method provide an insert interface to the flecsimo sqlite 
        databases. The main purpose is to insert data received from
        a message broker (like mqtt) into the flecsimo databases. Data is 
        expected as a dictionary with the insert column-names as keys and 
        the columns-value as values. This method guarantees that input to 
        the sqlite3.execute method refer only known columns and is 
        independent of the order of data keys of the message.
        
        If the 'data' dictionary is {'colx':valx,'coly':valy, ... ,'colz':valz}
        the insert will be 
        
        INSERT INTO table ("colx","coly",...) VALUES (:colx, :coly, ...)    

        If the 'upsert' argument provides a target list, this method will 
        append an UPSERT clause to the statement, containing an 
        'indexed-column' list for ON CONFLICT targets and a 'columns-name-list' 
        for the DO UPDATE SET clause for SQLite with version 3.2.24 and higher. 
        If the sqlite target has the format (colx, coly) the DO UPDATE SET
        clause will be (...,colz) = (...,:colz), i. e. the data columns and 
        values minus the the upsert columns. This is required to avoid 
        overriding foreign key constraint exception.
        
        Calling this method in strict=True mode will additionally check,
        if the given data keys are a subset of the known table columns.
        If the check fails, a ValueError Exception is thrown.
        
        In strict=False mode, any data keys (and values) not matching a 
        known column will be ignored.
              
        Args:
            table (str): the table name to insert the data.
            data (dict): the data given as dictionary with columns as keys.
            upsert (list): upsert target columns.             
            strict (boolean): if True, check if data columns are subset of 
                table columns. Default is False.
            ocdn (boolean): whether to add a "ON CONFLICT DO NOTHING" clause or
                not. Default is False.
        """
        # Prepare data
        try:
            data_in = data.copy()
            parameters = self.table_dict[table].copy()
        except Exception as e:
            raise TypeError(
                f"Failed to prepare data:{data} for table {table}") from e

        if strict:
            if not data_in.keys() <= parameters.keys():
                raise ValueError(
                    f"Data columns are not a subset of table columns. "
                    f"Data is: {data_in}")
        try:
            parameters.update(data_in)
        except (AttributeError, TypeError, ValueError) as e:
            raise TypeError(
                f"Can not map data to columns. Data is: {data_in}") from e

        # Prepare statement
        stmt = self._get_insert_statement(table)

        if upsert:
            try:
                for k in upsert:
                    del data_in[k]
            except Exception as e:
                raise ValueError(
                    "Upsert target must be a subset of data columns.") from e

            if len(data_in) > 0:
                target = f'''("{'","'.join(upsert)}")'''
                columns, placehld = self._compose_columns_placehld(data_in)
                stmt += f" ON CONFLICT {target} DO UPDATE SET {columns} = {placehld}"

        if ocdn:
            stmt += " ON CONFLICT DO NOTHING"

        # Execute statement
        conn = self.connect()
        with conn:
            cur = conn.execute(stmt, parameters)
        conn.close()

        return {'rowid': cur.lastrowid, 'rowcount': cur.rowcount}

    def loadmany(self, conn, table, data, ocdn=False):
        """A generic multiple insert into a single SQLite table without commit.
        
        This convenient method loads a list of data dictionaries into a single
        SQlite database table with the prepared insert statements. It is useful 
        to load larger amounts of prepared data (preferable with high quality). 
        Connection handling, roll back or commit has to handled outside this 
        method.  
        
        Args:
            conn (sqlite3.Connection): a managed sqlite connection.
            table (str): the table name to insert the data.
            data (dict): the data given as dictionary with columns as keys.
            ocdn (boolean): whether to add a "ON CONFLICT DO NOTHING" clause or
                not. Default is False.
            
            
        Note:
            Be careful when use this in threading - the connection object should 
            be created within the thread which is calling this method.
        """
        insert_stmt = self._get_insert_statement(table)
        
        if ocdn:
            insert_stmt += " ON CONFLICT DO NOTHING"
        
        cur = conn.executemany(insert_stmt, data)

        return cur.rowcount

    def change(self, table, data, conditions=None,):
        """A simple update method for sqlite tables.
        
        This methods supports updating of specific columns and will enforce the
        use of a condition (i. .e. having a WHERE clause), to prevent 
        uncontrolled changes of data. Conditions will only work as 
        (colx, coly, ... , colz) = (:c_colx, :c_coly, ... , :c_colz)
        and does not support WHERE clauses with expressions (like IS NULL). 
        
        Args:
            table (str): the table name to update the data.
            data (dict): the data to be changes given as dictionary with columns 
                         as keys.
            conditions (dict): 
        """
        if conditions is None:
            raise ValueError("Parameter conditions required to be not None.")

        pre = 'c_'
        columns, placehld = self._compose_columns_placehld(data)
        c_columns, c_placehld = self._compose_columns_placehld(conditions,
                                                               prefix=pre)
        stmt = f"UPDATE '{table}' SET {columns} = {placehld} "\
               f"WHERE {c_columns} = {c_placehld}"

        data.update({f"{pre}{key}": value for key, value in conditions.items()})

        conn = self.connect()
        with conn:
            conn.execute(stmt, data)
            numchanges = conn.total_changes
        conn.close()

        return numchanges

    def retrieve(self, table:str, columns=None, conditions=None, inlist=None,
                 null=None, orderby=None, single=False, distinct=False):
        """A generic object retrieve method for flecsimo databases.
        
        This convenience method retrieves a list of flecsimo objects from an
        SQlite database. The result list can be limited by conditions and 
        ordered by given columns. It does not guarantee, that the resulting
        statement is executable.
        
        Args:
            table (str):       the table name to select the data from.
            columns (list):    list of columns to select.   
            conditions (dict): dictionary with 'where' clause content.
            inlist (dict):     dictionary with IN (LIST) clause content.
            null:              dictionary with columns to test on (NOT) NULL     
            orderby (str):     string with 'orderby' clause content. 
            single (boolean):  if true, the cursor will fetch one line, 
                               otherwise all. Default is 'False'.
            distinct (boolean):if true, the result set will contain only 
                               distinct rows, default is 'False'.
                        
        Note:
            If required this method could be extended to work with inlist
            dictionaries of length > 1 by using itertools.zip_longest with
            fillvalue="AND"
        """
        if distinct:
            processing = "DISTINCT"
        else:
            processing = "ALL"
        try:
            collist = f'''"{'","'.join(columns)}"'''
        except TypeError:
            collist = '*'

        conditions = conditions or {}
        inlist = inlist or {}
        where_stmt = []
        parameters = {}

        conn = self.connect()
        stmt = f"SELECT {processing} {collist} FROM '{table}'"

        if conditions:
            columns, placehld = self._compose_columns_placehld(conditions)
            where_stmt.append(f"{columns} = {placehld}")
            parameters.update(conditions)

        if inlist:
            if not isinstance(inlist, dict):
                raise TypeError("The 'inlist' argument has to be of type 'dict'.")

            for key, w_values in inlist.items():
                w_column = key
                w_placehld, w_params = self._compose_placehld_param(w_values,
                                                                    prefix="in")
                where_stmt.append(f"{w_column} IN {w_placehld}")
                parameters.update(w_params)
                # break

        if null:
            if not isinstance(null, dict):
                raise TypeError("The 'null' argument has to be of type 'dict'.")

            for w_column, w_test in null.items():
                if w_test not in ["NULL", "NOT NULL"]:
                    raise ValueError("Test for NULL or NOT NULL allowed only.")

                where_stmt.append(f"{w_column} IS {w_test}")
                # break

        if conditions or inlist or null:
            stmt += f''' WHERE {(" AND ").join(where_stmt)}'''

        if orderby:
            stmt += f" ORDER BY {orderby}"

        with conn:
            cur = conn.execute(stmt, parameters)
            if single:
                rows = cur.fetchone()
            else:
                rows = cur.fetchall()
        conn.close()
        return rows  # this was (rows) before

    # Internal methods
    def _get_insert_statement(self, table:str) -> str:
        """Retrieve a prepared or create a new insert statement for a table."""
        try:
            return self.insert_statement[table]
        except KeyError:
            columns, vparams = self._compose_columns_placehld(self.table_dict[table])
            statement = f"INSERT INTO '{table}' {columns} VALUES {vparams}"
            self.insert_statement[table] = statement
            return statement

    def _compose_columns_placehld(self, keys, prefix="") -> list:
        """Compose a column tuple and a placeholder tuple as a string.
        
        Given a list, tuple with values key1, key2, ... or dictionary with these
        values as keys this method will return two strings:
            
            columns: '("key1","key2",...)'
            placehld: '(:prefixkey1,:prefixkey2,...)'
            
        where prefix defaults to "". The prefix is required to distinguish
        'set' value parameters (vparams) from 'where' value parameters in 
        update statements.
        
        Args:
            keys (Iterable): list of keys to build columns and vparams string.
            prefix (str): Prefix for vparams 
        """
        if not isinstance(keys, (list, tuple, dict)):
            raise TypeError(
                "_compose_columns_placehld() expects 'keys' argument"\
                " of type 'list', 'tuple' or 'dict'.")

        columns = f'''("{'","'.join(keys)}")'''
        placehld = f'''(:{prefix}{(',:'+prefix).join(keys)})'''

        return columns, placehld

    def _compose_placehld_param(self, values, prefix=""):
        """Composes a placeholder tuple and a parameter list from a values.
        
        Given a list or tuple of values: val1, val2, ... this method will 
        compose a formatted string of placeholders "(:pre_1, :pre_2, ...)" and a 
        dictionary of parameters {'prefix1': val1, 'prefix2': val2, ...} which 
        can be used to bind values in SQL queries. Prefix is defaulted to "" and
        may be used to distinguish different placeholder sets. 
        
        Note: 
            using prefix as default "" will nevertheless result in ":1" 
            handled as a string.
        
        Returns:
            placeholders (str): formated placeholders "(:prefix1, :prefix2, ...)"
            parameters (dict): dictionary of binding parameters
        """
        if not isinstance(values, (list, tuple)):
            raise TypeError("_compose_placehld_param() expects 'values' "\
                            "argument of type 'list', 'tuple' or 'dict'.")

        valueindex = 0
        param_keys = []
        parameters = {}

        for value in values:
            param_keys.append(f":{prefix}{valueindex}")
            parameters[f"{prefix}{valueindex}"] = value
            valueindex += 1

        placeholders = (f'''({(',').join(param_keys)})''')

        return placeholders, parameters

    def _create_table_dict(self):
        """Create a table dictionary for all user tables of current database."""
        tables_stmt = f"""
            SELECT name FROM {self.sqlite_schema}
            WHERE type IN ('table','view')
            AND name NOT LIKE 'sqlit_%'
            ORDER BY name
            """
        columns_stmt = "select name, dflt_value from pragma_table_info (:table)"

        self.conn = sqlite3.connect(self.database)
        self.conn.row_factory = sqlite3.Row
        cur = self.conn.execute(tables_stmt)
        tables = cur.fetchall()

        for tab in tables:
            cur = self.conn.execute(columns_stmt, {'table': tab['name']})
            columns = cur.fetchall()
            self.table_dict[tab['name']] = {col['name']: col['dflt_value']
                                            for col in columns}
        self.conn.close()
