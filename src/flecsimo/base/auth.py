#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""Auth-Module used in webserver-component for login and user-based-templating

Created on 05.05.2021

@author: Leon Schnieber

Copyright and License Notice:
    flecsimo web authentication module
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import json
from functools import wraps
from os import path as ospath
from subprocess import check_output as subprocess_check_output

import argon2
from flask import request, abort, render_template

module_location = ospath.dirname(ospath.abspath(__file__))


class Auth():

    def __init__(self, login_data_path=None, webserver_path=None):
        # prepare paths to be used later on
        if login_data_path is None:
            login_data_path = "conf/logins.json"

        if webserver_path is None:
            webserver_path = "conf/webserver.json"

        # start argon2 instance
        self.hasher = argon2.PasswordHasher()
        with open(login_data_path, "r") as f:
            self.userdata = json.load(f)
            f.close()

        with open(webserver_path, "r") as f:
            self.settings = json.load(f)
            f.close()

        try:
            self.git_hash = subprocess_check_output(
                                ['git',
                                 'rev-parse',
                                 '--short', 'HEAD']).decode('ascii').strip()
        except:
            self.git_hash = "unknown"

    def auth_request(self, needed_power=""):

        def decorator(function):

            @wraps(function)
            def wrapper(*arg, **kwarg):
                session = request.cookies.get("flasksimo-session")
                user = self.session_check(session)
                if session and user is not None:  # check if session exists
                    if needed_power not in self.userdata[user]["permissions"]:
                        abort(403)  # forbidden
                    return function(*arg, **kwarg)
                else:
                    abort(401)  # unauthorized

            return wrapper

        return decorator

    def session_check(self, sessionKey):
        for user in self.userdata:
            if sessionKey in self.userdata[user]["sessions"]:
                return user
        return None

    def credentials_check(self, username, password):
        if username not in self.userdata:
            return False
        try:
            user_hash = self.userdata[username]["hash"]
            self.hasher.verify(user_hash, password)
            if self.hasher.check_needs_rehash(user_hash):
                self.userdata[username]["hash"] = self.hasher.hash(password)
            return True
        except argon2.exceptions.VerifyMismatchError:
            return False
        return False

    def render_template_wrap(self, filepath, **kargs):
        session = request.cookies.get("flasksimo-session")
        user = self.session_check(session)
        roles = ""
        username = None
        if user is not None:
            username = self.userdata[user]["name"]
            roles = ", ".join(self.userdata[user]["permissions"])
        if "baseurl" in self.settings:
            baseurl = self.settings["baseurl"]
        else:
            baseurl = ""

        return render_template(
            filepath, username=username, roles=roles,
            baseurl=baseurl, git_hash=self.git_hash, **kargs)
