"""Shared states for flecsimo objects.

This module define enumerated states for objects which are handled from 
multiple classes and have no central instance which is responsible for these
objects. The main purpose is to provide a common state interface for 
transactional objects (or data).

Created on 21.03.2020

@author: Ralf Banning

Copyright and License Notice:
    flecsimo state definitions
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from enum import Enum


class FacilityStates(Enum):
    """States of facilities"""
    ENLISTED = 0
    READY = 1
    MAINTENANCE = 2
    DOWN = 3
    DELISTED = 4


class OrderStates(Enum):
    """States for order processing."""
    INITIAL = 0
    CHECKED = 1
    PLANNED = 2
    RELEASED = 3
    ACCEPTED = 5
    WIP = 6
    DONE = 7
    CANCELLED = 8
    FAILED = 9


class QuotationStates(Enum):
    """States of quotation process."""
    INITIAL = 0
    REQUESTED = 1
    QUOTED = 2
    DROPPED = 4
    PICKED = 5
    ASSIGNED = 6
    CANCELLED = 7


class ScheduleStates(Enum):
    """States of scheduling process."""
    INITIAL = 0
    PLANNED = 1
    ASSIGNED = 2
    ACCEPTED = 3
    ADVISED = 4
    LOADED = 5
    WIP = 6
    PICKABLE = 7
    DONE = 8
    REWORK = 9
    CANCELLED = 10
    FAILED = 11


class SfcuStates(Enum):
    """States for shop floor control unit (sfcu) processing."""
    INITIAL = 0
    PLANNED = 1
    RELEASED = 2
    ACCEPTED = 3
    WIP = 4
    DONE = 5
    REWORK = 6
    CANCELLED = 7
    FAILED = 8
    
class StationStates(Enum):
    """Station states in enumerated form."""
    STOPPED = 0
    READY = 1
    STANDBY = 2
    SETUP = 3
    ACTIVE = 4
    DONE = 5
    HALTED = 6
    
