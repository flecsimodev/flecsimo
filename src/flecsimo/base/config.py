"""Simple parser for json formatted config files.

Created on 29.02.2020

@author: Ralf Banning

Copyright and License Notice:
    flecsimo json configuration base functions
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) anyF later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import json
from pathlib import Path


class Configurator:
    """A class to store and work with configuration paths and functions.
    
    This class will provide methods to locate and access configuration files and
    databases. Instantiation of requires a 'root' argument which sets an entry 
    point independent absolute path hierarchy to access those files. The
    'configuration root' has to be provided as a path-like string pointing
    to an existing file or directory.
    
    Args:
        root (str): configuration root, has to reference an exiting file
                    or directory as a path-like string or byte.
    
    Example:
        If a module which is located at root=/somepath/mod.py creates a 
        Configurator(root) object the class member will be set to
       
        root_path == /somepath  
        conf_path == /somepath/conf  (all configuration files as *.json files)
        conf_path == /modulpath/db   (all sqlite databases as *.db files.)
    
    Note: a Configurator should instantiated only by modules which 
    - which run the "top-level code environment" where the module's main() is 
      invoked by python interpreter directly or
    - which are invoked by other "top-level code" after import but providing the
      actual operational functions which will run or control a system.
    """

    def __init__(self, root:str):
        # set config root
        try:
            root_path = Path(root).resolve()

            if root_path.is_file():
                self.config_root = root_path.parent
            elif root_path.is_dir():
                self.config_root = root_path
            else:
                raise FileNotFoundError("Argument 'root' does not point to " + \
                                        "file or directory")
        except TypeError as t:
            raise TypeError("Wrong type for positional argument 'root'") from t

        # Set derived paths
        self.conf_path = self.config_root.joinpath('conf')
        self.db_path = self.config_root.joinpath('db')

    def get_config(self, file, profile=None) -> dict:
        """Get a section of a json-type config file as dictionary
        
        This method reads configuration data from a file in json format and 
        returns either the complete or partial content as a dict. The return
        will be updated incrementally: first the COMMON_SECTION values are 
        copied, thereafter the selected section's values will update the return.
        
        Args:
            file (path-like str): the name or full path of the config file
            profile (str): selects a specific profile of the config; 
                complete config data is returned if profile == '*'.
             
        Returns:
            confdata (dict) selected config data.
            
        Raises:
            FileNotFoundError
            ValueError
        """
        confdata = self.read_configfile(file)

        if profile is not None:
            this_profile = profile
        else:
            this_profile = confdata['INFO']['currentprofile']

        try:
            _confdata = confdata['IDENTITY']
            _confdata.update(confdata['PROFILE'][this_profile])
            return _confdata
        except KeyError as e:
            raise ValueError(f"Config has no section '{this_profile}'.") from e

    def get_info(self, file):
        """Get meta information about aconfig file."""
        confdata = self.read_configfile(file)
        return confdata['INFO']

    def get_profiles(self, file):
        """Get a dictionary of all profiles of a config file."""
        confdata = self.read_configfile(file)
        return {k: v['name'] for (k, v) in confdata['PROFILE'].items()}

    def get_conf_path(self, configfile:str) -> Path:
        """Return an absolute path to a given configfile.
        
        Configfile should be given as a pure file name. In this case it will be
        returned as the absolute standard root path pointing to that file. 
        
        If configfile is given as an absolute path this path will be returned 
        as an resolved path.
        """
        return self._get_file_path(self.conf_path, configfile)

    def get_db_path(self, dbfile):
        """Return the standard path to a given database file.
        
        Dbfile should be given as a pure file name. In this case it will be
        returned as the absolute standard root path pointing to that file. 
        
        If dbfile is given as an absolute path this path will be returned 
        as an resolved path.
        """
        return self._get_file_path(self.db_path, dbfile)

    def read_configfile(self, file:str) -> dict:
        """Read a JSON configfile and return as dict.
        
        Args:
            file: a path-like str.
        """
        _file = self.get_conf_path(file)
        try:
            confdata = dict(json.loads(_file.read_text(encoding="UTF-8")))
        except FileNotFoundError as e:
            raise FileNotFoundError(f"Config file '{_file}' not found.") from e
        return confdata

    def write_configfile(self, file:str, confdata:dict, bkp=False):
        """Write config data to file
        
        Args:
            file: a pthlike string pointing to a file
            confdata (dict): the configurationtion data to be written
            bkp (bool): creates a '.bkp' file is set True.
            """
        _file = self.get_conf_path(file)
                
        # create optional backup 
        if bkp is not False:
            _bkp = _file.with_suffix('.bkp')
            _bkp.write_text(_file.read_text(), encoding="utf-8")
        
        _file.write_text(json.dumps(confdata, indent=4),
                         encoding="utf-8")

    def _get_file_path(self, root:Path, file:Path):
        """Return an absolute path to file depending on type."""
        _path = Path(file)
        if not _path.is_absolute():
            _path = root.joinpath(_path.name)
        return _path.resolve()

