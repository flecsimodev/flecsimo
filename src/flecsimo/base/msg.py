"""Message structures for machine communication (m2m) in flecsimo.

This module provides message definitions for the machine-to-machine 
communication using the MQTT protocol. Messages in this protocol (so called 
"application messages") consist at least of a topic and a payload. MQTT 
communication in flecsimo is designed in a way, that the topic will express the 
sender identification (written as a topology 'path', e.g. 

    site_name/area_name/cell_name 
    
plus the message type. Messages are sent via a publish protocol and received 
by subscribing to a list of topics. 

Typical usage example:
    (Assumes, a MQTT client c has been instantiated before)
    
    reg = Enrol(cell_name, org='cell', services=['D05','D10'])
    reg.loc = 'X01:Y01-IX02:IY01-OX02:OY01'
    client.publisch(reg.topic, reg.payload) 

Notes:
    This module is designed for MQTT version 3, using paho-mqtt client lib
    and eclipse mosquitto as MQTT server. The MQTT interfaces are not part of 
    this module.
    
    If run with Python version lower as 3.7, make sure to use base.dao module
    in message handling, since this module does not rely on dictionary order.
    
    The classes could also be defined as @dataclasses, but this feature is 
    available only from Python version 3.7 onwards - so this could be a 
    task for later redesign.
    
Requires:
    Python version: 3.7 (guarantee of dictionary order)

TODO:
    Rfq, Quote and Asgmt have nearly the same data structure - derive from 
    common class?


Created on:
     18.02.2020

@author: 
    Ralf Banning   

Copyright and License Notice:
    flecsimo msg language definitions. 
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import datetime
import json


def compose_topic(*args):
    """Compose a mqtt compatible topic string. 
    
    This methods builds a topic string suitable for mqtt application messages.
    Topics may contain '+' and '#' wildcards. Whereas '+' could be used at any
    position, '#' is only allowed as last element.
    
    Args:
        *args: Tuple of topic elements.
        
    Returns:
        The composed topic if args were provided, else None.
        
    Exceptions:
        ValueError: if topic elements would result in malformed topic.
        TypeError: if topic elements are not just strings.
    """
    # TODO: handle execution if args contains list
    topic = None
    if args:
        if '#' in args:
            if args.index('#') < len(args) - 1:
                raise ValueError(
                    "Wildcard '#' is allowed only at the end of a topic!")

        topic = '/'.join(args)
    return topic


def decompose_topic(topic):
    """Split flecsimo topic into sender and message type.
      
    Args: 
        topic (str): A topic string
        
    Returns:
        (sender, sep, message_type): A 3-tupel containing the part before
            the last occurrence of the separator (sender), the separator 
            itself and the part after the last separator (message_type).
    """
    return topic.rpartition('/')


def as_dict(message_topic, message_payload):
    """Decode mqtt message payload as dict and add topic as item.
    
    This function returns a "flat" dictionary of the message and the topic 
    (however preserving nested structures inside the payload). If a message with
    empty payload is received the functions returns only the topic.
    
    Args:
        message_topic (str): topic of the message
        message_payload(str, byte): message as string or byte.
        
    Return:
        data(dict): the payload as dict plus the topic. Returns empty dict on 
                    decoding exceptions.
    """
    data = {}
    try:
        if type(message_payload) == bytes:
            data = dict(json.loads(str(message_payload.decode("utf-8"))))
        else:
            data = dict(json.loads(message_payload))

        data.update({"topic": message_topic})
    except json.decoder.JSONDecodeError:
        pass

    return data


def as_dump(payload):
    """Return a formatted string from a dictionary.
    
    Args:
        payload (dict): payload to dump
    """
    return json.dumps(payload, indent=4)


class Message:
    """Base class for messaging roles.

    Create a message object comprising a topic string and a time-stamp.
    The topic string is composed of the sender identification 
    (sitename/area_id/cell_id) and the message name, which equals the class 
    name in all small caps. The payload is composed of all class attributes 
    beside of '_topic' but with an additional attribute 'at' which contains the
    instantiation time stamp.

    Example:
        If MyMsg is a subclass of Message with attribute 'attr', an instance 
        m = MyMsg(sender= 'src') will have the properties m.topic and m.payload. 
        They will return the topic 'src/mymsg' and the payload of the message: 
        "{'attr': 'value', 'at': [... some ISO date and time ...]}".
        
    Attributes:
        _topic (str): mqtt style topic string of the message 
        topic (property): returns the _topic.
        payload (property) returns the mqtt payload.
        dict (property) returns the payload as dict.

    Args:
        sender (str): name of the sender identification. 
        selector (str): name of a selectable topic part.

    """

    def __init__(self, sender=None, selector=None):
        """Initialize class members."""
        if sender:
            _topiclist = [sender, type(self).__name__.lower()]
            if selector:
                _topiclist.append(selector)
            self._topic = "/".join(_topiclist)
        else:
            raise ValueError("Sender is 'None'.")

    def _format_state(self, dictionary):
        """Expand state attribute for Enum-type state objects
        
        If a dictionary has a 'state' attribute, this function analyzes
        if it's value provide a name and value function (which is typically the 
        case if the 'state' is of Enum type. In this case, it splits the state 
        object into state value and state name (textual description) and maps 
        these components to the 'state' and 'statx' dictionary element 
        respectively.
        
        Args:
            dictionary (dict): the dictionary to be formated.
        """
        if 'state' in dictionary:
            try:
                state = dictionary['state'].value
                descr = dictionary['state'].name
            except AttributeError:
                state = dictionary['state']
                descr = None
            dictionary.update({'state': state, 'statx': descr})

        return dictionary

    @property
    def topic(self):
        """Get internal topic representation."""
        return self._topic

    @property
    def payload(self):
        """Transform class members into a message string.
        
        The function creates a copy of the class dictionary and 'strips off' 
        the topic attribute. If a state attribute is present, it patches a
        'state' and 'descr' field to the message. If the state is given as
        an enumeration member it maps the value to the state-field and the 
        name to the description-field. If this is not the case it tries to 
        map the state to the state-field.
        
        In any case, an 'at-field' is appended to the message containing 
        the current roles and utc-time in iso-Format.
        
        Raises:
            TypeError, if state is an enumeration class instead of a member. 
            
        Return:
            The json formatted message as a string.
        """
        payload = self.__dict__.copy()
        del payload['_topic']
        payload = self._format_state(payload)

        at = datetime.datetime.utcnow()
        payload.update({'at': at.isoformat()})

        return json.dumps(payload)

    @property
    def dict(self):
        """Return message as a dictionary.
        
        The function is similar to the payload property but 
        - includes the topic
        - returns a dictionary instead of string and 
        """
        dictionary = self.__dict__.copy()
        dictionary = self._format_state(dictionary)

        dt = datetime.datetime.utcnow()
        dictionary.update({'at': dt.isoformat()})

        return dictionary


class Asgmt(Message):
    """Assign an operation schedule to an area or cell.

    Args:
        sender (str): the sender identification as shown in topic.
        named args for each attribute.
    
    Attributes:
        sender (str): sender of assignment.
        site (int): flecsimo site identifier.
        sfcu (str): shop floor-control-unit identification.
        operation (str): requested operation (identifier)
        supplier (str): receiver of assignment.
        org (str): 
        due (timestamp): due date of assignment. 
        prio (int): priority indicator.
        dhash (str): hash of the associated opdta message payload.
        state: status as member of a Enum-class or just a string.
    """

    def __init__(self, sender, site, order, sfcu, operation=None,
                 supplier=None, org=None, due=None, prio=0, dhash=None,
                 state=None):
        super().__init__(sender)

        self.site = site
        self.order = order
        self.sfcu = sfcu
        self.operation = operation
        self.supplier = supplier
        self.org = org
        self.due = due
        self.prio = prio
        self.dhash = dhash
        self.state = state


class CfmEnrol(Message):
    """Acknowledgement for cell registration by area controller.
    
    Args:
        sender (str): the sender identification as shown in topic.
        named args for each attribute.
    
    Attributes:
        cell (str): cell name / identification. 
        mid (int): message id of request.
    """

    def __init__(self, sender, cell=None, mid=None):
        super().__init__(sender)
        self.cell = cell
        self.mid = mid


class CfmQuit(Message):
    """Acknowledgement of cell de-registration by area controller
     
    Args:
        sender (str): the sender identification as shown in topic.
        named args for each attribute.   
        
    Attributes:
        cell (str): cell name / identification. 
        mid (int): message id of request.
    """

    def __init__(self, sender, cell=None, mid=None):
        super().__init__(sender)
        self.cell = cell
        self.mid = mid


class Enrol(Message):
    """Enrol a facility at parent level.
    
    Args:
        sender (str): the sender identification as shown in topic.
        named args for each attribute.
    
    Attributes:
        name (str): name of facility (as provided by sender id)
        org (str): identifies the organizational level like area, cell, ...
        cmpt (str): identifies the component type like agent, control, ...
        desc (str): description of facility 
        loc (str): location identifier of cell and cell in/out.
        oplist (list): list of provided operations.
    """

    def __init__(self, sender, org=None, cmpt=None, desc=None, loc=None, 
                 oplist=None):
        super().__init__(sender)
        self.name = sender
        self.org = org
        self.cmpt = cmpt
        self.desc = desc
        self.loc = loc
        self.oplist = oplist


class Opdta(Message):
    """Send operation data (sfcus and tasks).

    Args:
        sender (str): the sender identification as given by the topic.
        receiver (str): the sender identification as given by the topic.
        data (dict): operation data.
    
    Attributes:
        data (dict): operation data. 
    """

    def __init__(self, sender, receiver, data=None):
        super().__init__(sender, selector=receiver)
        if data is None:
            data = {}
        self.data = data


class Opcfm(Message):
    """Send confirmation of received Opdta.

    """

    def __init__(self, sender):
        super().__init__(sender)


class Opstat(Message):
    """Report operational status.

    Args:
        sender (str): the sender identification as shown in topic.
        named args for each attribute.
    
    Attributes:
        site (str): flecsimo site identifier. 
        order (int): order number.
        sfcu (int): shop floor control unit identifier.
        mhu (str): material handling unit
        operation (str): operation id.
        opkey (str): operations qualifier key, default=None
        org (str): type of sender (like 'site', 'area', 'cell', ...). 
        state: status as member of a Enum-class or just a string. 
    """

    def __init__(self, sender, site, sfcu, mhu, operation, supplier, org,
                 state):
        super().__init__(sender)
        self.site = site
        self.sfcu = sfcu
        self.mhu = mhu if mhu is not None else 'NODATA'
        self.operation = operation
        self.supplier = supplier
        self.org = org
        self.state = state


class Reading(Message):
    """Report readings from device.

    Args:
        sender (str): the sender identification as shown in topic.
        named args for each attribute.
    
    Attributes:
        source (str): issuer of reading. 
        value (str): value of reading.
        unit (int): unit of reading.
    """

    def __init__(self, sender, source=None, value=None, unit=None):
        super().__init__(sender)
        self.source = source
        self.value = value
        self.unit = unit


class Rfq(Message):
    """This message requests an quotation to perform an operation.

    Args:
        sender (str): the sender identification as shown in topic.
        named args for each attribute.
    
    Attributes:
        sender (str): sender of assignment.
        site (int): flecsimo site identifier.
        sfcu (str): shop floor-control-unit identification.
        operation (str): requested operation (identifier)
        supplier (str): receiver of assignment.
        org (int): 
        due (timestamp): due date of assignment. 
        prio (int): priority indicator.
        state: status as member of a Enum-class or just a string.
                
    Example:
        Topic: FUAS/area_1/rfq
        Payload: 
        {
            "site": "FUAS", 
            "order": 1000063", 
            "sfcu": 21, 
            "pds": 1,
            "operation": "27-DRILLING", 
            "supplier": "cell-2"
            "org": "cell"
            "due": "2020-10-31", "prio": 0, "statx": "initial", "at": "2020-07-03 15:10"}
        }
    """

    # def __init__(self, sender, site, order, sfcu, pds=None, operation=None,
    # supplier=None, org=None, due=None, prio=0, state=None):
    def __init__(self, sender, site, order, sfcu, operation=None,
                 supplier=None, org=None, due=None, prio=0, state=None):
        super().__init__(sender)
        self.site = site
        self.order = order
        self.sfcu = sfcu
        self.operation = operation
        self.supplier = supplier
        self.org = org
        self.due = due
        self.prio = prio
        self.state = state


class Quit(Message):
    """De-register a cell from an area controller.
    
    This message has to be used, if a a facility should be moved to another 
    location inside an area or to another area.
    
    Args:
        sender (str): the sender identification as shown in topic.
        named args for each attribute.   
        
    Attributes:
        facility (str): facility name / identification. 
    """

    def __init__(self, sender, facility):
        super().__init__(sender)
        self.sender = sender
        self.facility = facility


class Quote(Message):
    """Offering to perform an operation.

    Args:
        sender (str): the sender identification as shown in topic.
        named args for each attribute.
    
    Attributes:
        sender (str): sender of assignment.
        site (int): flecsimo site identifier.
        sfcu (str): shop floor-control-unit identification.
        operation (str): requested operation (identifier)
        supplier (str): receiver of assignment.
        org (int): type of sender
        due (timestamp): due date of assignment. 
        prio (int): priority indicator.
        state: status as member of a Enum-class or just a string.
    """

    def __init__(self, sender, site, order, sfcu, operation=None,
                 supplier=None, org=None, due=None, prio=0, state=None):
        super().__init__(sender)
        self.site = site
        self.order = order
        self.sfcu = sfcu
        self.operation = operation
        self.supplier = supplier
        self.org = org
        self.due = due
        self.prio = prio
        self.state = state
