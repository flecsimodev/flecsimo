"""
Created on 06.09.2021 from app.area_agent.py, first created on 18.02.2020

@author: Ralf Banning

Copyright and License Notice:
    flecsimo area agent.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import logging

from flecsimo.base.const import Orglevel, Component
from flecsimo.base.states import ScheduleStates
from flecsimo.cmpt import edge
from flecsimo.habit import scheduling
from flecsimo.habit.requesthandling import RequestMessageHandler
from flecsimo.habit.responsehandling import ResponseMessageHandler
from flecsimo.habit.statushandling import StatusMessageHandler

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())


class AreaAgent(edge.Agent):
    """Class for area related services.
    
    This class derives from the 'Agent' superclass which handles the 
    connections to the environment (port functionality, i. e. mqtt and 
    database connection). The main purpose of this class itself, is to build 
    the context of the area agent, i.e. the parameterization and the 
    topic mapping of message callback instances.
    
    Args:
            conf (dict): configuration data
            cid (str): Cell identifier of mqtt connection.
            mode (str): operations mode.
    """

    def __init__(self, conf, cid=None, mode='AUTO'):
        # Initialize environment information in self.env
        org = Orglevel.AREA.value
        cmpt = Component.AGENTTYP.value

        # Initialize environment (env) and Node instance
        super().__init__(conf, org, cmpt)

        database = self.env['database']
        self.sender = self.env['settings']['sender']
        site = self.env['site']
        self.myself = area = self.env['area']

        # Get habits.
        self.scheduler = scheduling.Scheduler(database)  # used in auto_asgt
        status_handler = StatusMessageHandler(database, self.env)
        response_handler = ResponseMessageHandler(database, mode)
        request_handler = RequestMessageHandler(database, self.myself, mode)

        # Configure habits
        request_handler.myself = area
        request_handler.mode = mode
        request_handler._auto_asgmt_processing = self._auto_asgmt_processing
        response_handler._auto_quote_processing = self._auto_quote_processing

        # Define subscription topics.
        rfq_topic = "/".join([site, 'rfq'])
        asgmt_topic = "/".join([site, 'asgmt'])
        opdta_topic = "/".join([site, 'opdta', area])

        enlist_topic = "/".join([site, area, '+', 'enlist'])
        delist_topic = "/".join([site, area, '+', 'delist'])
        quote_topic = "/".join([site, area, '+', 'quote'])
        opcfm_topic = "/".join([site, area, '+', 'opcfm'])
        opstat_topic = "/".join([site, area, '+', 'opstat'])
        reading_topic = "/".join([site, area, '+', 'reading'])

        # Create context map: usecase, topic, habit, qos value.
        self.context.add('rfq', rfq_topic, request_handler.on_rfq, 0)
        self.context.add('asgmt', asgmt_topic, request_handler.on_asgmt, 0)
        self.context.add('opdata', opdta_topic, request_handler.on_opdta, 0)
        self.context.add('opcfm', opcfm_topic, response_handler.on_opcfm, 0)
        self.context.add('enrol', enlist_topic, status_handler.on_enlist, 0)
        self.context.add('quit', delist_topic, status_handler.on_delist, 0)
        self.context.add('quote', quote_topic, response_handler.on_quote, 0)
        self.context.add('opstat', opstat_topic, status_handler.on_opstat, 0)
        self.context.add('reading', reading_topic, status_handler.on_reading, 0)

        # Make subscription list and callbacks available to node.
        self.node.subscription = self.context.subscriptions
        self.node.register_callback(self.context.habits)

    def _auto_asgmt_processing(self, site, order, sfcu):
        """Assign tasks to cells and send operational data.
        
        Implements a minimal planning algorithm for assigning sfcu related tasks
        to dedicated cells by using the choose_facility method of the operations
        module. Following steps are executed:
        
        1. Choose a best suited cell for supply of operations.
        1.a) Schedule the operations for those suppliers.
        1.b) Create and publish the operational data to those cells.
        2. Create new assignments to those cells (suppliers).
        3. Create and publish the cell's assignments.
        4. Update the schedule states.
        
        Requirements: execution result depends on schedule data that should be
        persisted when an asgmt message was received before.
        """
        _log.debug("Start auto assignment for order %s at %s.", order, site)

        # 1. Choose a best suited cell per operation.
        facilitylist = self.scheduler.choose_facility(site, order)
        for facility in facilitylist:
            _log.debug("Assign %s.", facility)

            # 1.a) Schedule the operations for those suppliers.
            # This stores new schedules in area db with specific supplier /
            # operation values
            rc = self.scheduler.schedule_supplier(
                    site,
                    order,
                    supplier=facility['facility'],
                    operation=facility['operation'],
                    sfcu=sfcu)

            # 1.b) Create and publish the operational data to those cells.
            if rc['rowcount'] > 0:

                opdta = self.scheduler.new_opdta_msg(
                            self.sender,
                            site,
                            order,
                            supplier=facility['facility'],
                            operation=facility['operation'])

                self.node.mqclient.publish(opdta.topic, opdta.payload)
                _log.info("published opdta for %s.", opdta.topic)

        # 2. Create new assignments to those cells.
        asgmtlist = self.scheduler.get_asgmtlist(self.sender, site, order, sfcu)

        # 3. Create and publish the cell's assignments.
        for asgmt in asgmtlist:
            if asgmt.supplier != self.myself:
                self.node.mqclient.publish(asgmt.topic, asgmt.payload)
                _log.info("published asgmt for %s.", asgmt.supplier)

        # 4. Update the schedule states.
        self.scheduler.update_schedule_state(site, order, ScheduleStates.ASSIGNED)

        # TODO: This could be run in sub-mode 'AUTO-RFQ'
        # self.request_quote(site=site, order=order)

    def _auto_quote_processing(self, site, order, sfcu, supplier):
        # Publish assignment and opdta
        # TODO: this condition make only sense, if moved to central module
        # TODO: check pylint complaints:
        # E1101: Instance of 'list' has no 'topic' member (no-member)
        # E1101: Instance of 'list' has no 'payload' member (no-member))

        if self.org in (Orglevel.SITE, Orglevel.AREA):
            asgmt = self.scheduler.get_asgmtlist(self.sender,
                                                 site,
                                                 order,
                                                 sfcu)
            self.node.mqclient.publish(asgmt.topic, asgmt.payload)

            opdta = self.scheduler.new_opdta_msg(self.sender,
                                                 site,
                                                 order,
                                                 supplier)
            self.node.mqclient.publish(opdta.topic, opdta.payload)

        else:
            _log.info("Wrong facility type: %s.", self.org)
