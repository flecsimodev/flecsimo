#!/usr/bin/env python3
"""Methods for controlling area processes

This module provides a wrapper for the site_context which implements the
interactions of the order processing use cases.

Architectural note:
    This component may be seen as a replacement for a "GUI", and acts mainly
    for the controller-part. The "SiteControlContext" object follows the ideas 
    of Reenskaug and Coplien, to factor out use case algorithms and business 
    logic from pure domain data object (the database in this case) in to 
    abstract role models which are mixed into the data objects at runtime.

Created on  09.09.2020 from app.site_control, first created on 25.05.2020

@author: Ralf Banning

Copyright and License Notice:
    flecsimo site control component
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
from flecsimo.base.const import Orglevel, Component
from flecsimo.cmpt import edge
from flecsimo.habit import orderprocessing, scheduling


class SiteControl(edge.Node):
    """Control functions and habits on site level.
    
    The main purpose of this class, is to build the context of the site control
    agent, i.e. evaluation of the system configuration and the mapping of order 
    processing and scheduling methods.
    
    Args:
        conf (dict): configuration data dictionary.
        cid (str): site identifier of mqtt connection.
    """

    def __init__(self, conf:dict):
        """Initialize configuration of controller."""
        org = Orglevel.SITE.value
        cmpt = Component.CONTROLTYP.value
        
        self.env = edge.map_environment(conf, org, cmpt) 
        super().__init__(self.env['settings'])        

        self.database = self.env['database']

        # Mapping habits
        self.orderproc = orderprocessing.OrderProcessor(self.database)
        self.scheduler = scheduling.Scheduler(self.database)

