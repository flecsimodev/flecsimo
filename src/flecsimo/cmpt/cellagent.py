"""
Created on 06.09.2021 from app.cell_agent.py, first created on 18.02.2020

@author: Ralf Banning

Copyright and License Notice:
    flecsimo cell agent.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import logging

from flecsimo.base.const import Orglevel, Component
from flecsimo.cmpt import edge
from flecsimo.habit.requesthandling import RequestMessageHandler
from flecsimo.habit.statushandling import StatusMessageHandler

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())


class CellAgent(edge.Agent):
    """Class for cell related services.
    
    This class derives from the 'Agent' superclass which handles the 
    connections to the environment (port functionality, i. e. mqtt and 
    database connection). The main purpose of this class, is to build 
    the context of the cell agent, i.e. the parameterization and the 
    topic mapping of message callback instances.

    Args:
        conf (dict): configuration data.
        cid (str): cell identifier of mqtt connection.
        mode (str): operations mode, default is 'AUTO'
    """

    def __init__(self, conf=None, cid=None, mode='AUTO'):
        org = Orglevel.CELL.value
        cmpt = Component.AGENTTYP.value

        # Initialize environment (env) and Node instance
        super().__init__(conf, org, cmpt)

        self.mode = mode
        self.database = self.env['database']
        _log.debug("Use database '%s'.", self.database)

        site = self.env['site']
        area = self.env['area']
        self.myself = cell = self.env['cell']

        # Create message callback handler (habits) and set identity
        request_handler = RequestMessageHandler(self.database, self.myself, 
                                                mode='OFF')
        request_handler.myself = cell

        status_handler = StatusMessageHandler(self.database, self.env)

        # Set agent subscription list
        rfq_topic = "/".join([site, area, 'rfq'])
        asgmt_topic = "/".join([site, area, 'asgmt'])
        opdta_topic = "/".join([site, area, 'opdta', cell])
        status_topic = "/".join([site, area, cell, '+', 'status' , 'STN'])

        # Create context map: usecase, topic, habit, qos value.
        self.context.add('rfq', rfq_topic, request_handler.on_rfq, 0)
        self.context.add('asgmt', asgmt_topic, request_handler.on_asgmt, 0)
        self.context.add('opdta', opdta_topic, request_handler.on_opdta, 0)
        self.context.add('status', status_topic,
                         status_handler.on_station_status, 0)

        # Make subscription list and callbacks available to node.
        self.node.subscription = self.context.subscriptions
        self.node.register_callback(self.context.habits)
        
    def _publish_opstat(self, opstat):
        self.node.mqclient.publish(opstat.topic, opstat.payload)    
        

