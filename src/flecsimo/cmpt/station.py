"""Base class for station models used by flecsimo state machines.

Created on 22.10.2021, major redesign in June 2023

@author: Ralf Banning

Copyright and License Notice:
   flecsimo station base classes
   Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University
   of Applied Sciences.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import datetime
import logging
import json
from abc import ABC, abstractmethod
from enum import Enum
from transitions.core import Machine, MachineError, State
from flecsimo.base import dao
from flecsimo.base.const import Component, Orglevel
from flecsimo.base.states import ScheduleStates, StationStates

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())

MAX_NUNMBER_OF_STATIONS = 4
MAX_SETUP_TIME_SECONDS = 60
LOADABLE_STATES = [ScheduleStates.ASSIGNED.name,
                   ScheduleStates.ACCEPTED.name,
                   ScheduleStates.ADVISED.name,
                   ScheduleStates.REWORK.name]


class FlecsimoMachine(Machine):
    """A pre-configured 'standard' finite state machine for flecsimo models.
    
    This class controls the standard configuration for all variants of a 
    flecsimo station (real models as well as simulations). It is initialized 
    with enum-type states, since numerical state values will be used in 
    messaging on factory level. The machine will publish every state change via 
    the _publish_state_change callback, which should be implemented at the 
    attached model classes. 
    
    This machine will use the event interface to propagate parameters to 
    transition call back functions and inject to_<state> convenience functions 
    at the model (auto_transitions=True). 
    
    We use queued transition to guarantee transition processing finalized before
    state change. 
    """

    def __init__(self):

        self.statelist = [
            State(StationStates.STOPPED,
                  on_enter='prepare_stop'),
            State(StationStates.READY,
                  on_enter='initialize'),
            State(StationStates.STANDBY,
                  on_enter='prepare_standby'),
            State(StationStates.SETUP,
                  on_enter='prepare_setup'),
            State(StationStates.ACTIVE,
                  on_enter='prepare_operation'),
            State(StationStates.DONE,
                  on_enter='prepare_done'),
            State(StationStates.HALTED,
                  on_enter='prepare_halt'),
            ]

        # Note:
        # the sequence order of trigger definitions is important in case of
        # conditional transitions. If a transition is enabled it will prevent
        # other transitions from this state executed even if the condition of
        # the first transition evaluates False.
        self.transitions = [
            {'trigger': 'init',
                        'source': ['STOPPED', 'HALTED'],
                        'dest': 'READY'},
            {'trigger': 'proceed',
                        'source': 'READY',
                        'dest': 'STANDBY'},
            {'trigger': 'load',
                        'source': 'STANDBY',
                        'dest': 'ACTIVE',
                        'prepare': 'get_setup',
                        'before': '_set_sfcu',
                        'conditions': ['_is_set_up', '_not_running']},
            {'trigger': 'load',
                        'source': 'STANDBY',
                        'dest': 'SETUP',
                        'before': '_set_sfcu',
                        'unless': '_is_set_up'},
            {'trigger': 'go',
                        'source': 'SETUP',
                        'dest': 'ACTIVE',
                        'prepare': 'finalize_setup',
                        'conditions': '_not_running'},
            {'trigger': 'proceed',
                        'source': 'ACTIVE',
                        'dest': 'DONE',
                        'conditions': '_not_running'},
            {'trigger': 'unload',
                        'source': 'DONE',
                        'dest': 'STANDBY',
                        'prepare': 'prepare_unloading',
                        'after': '_reset_sfcu'},
            {'trigger': 'halt',
                        'source': '*',
                        'dest': 'HALTED'},
            {'trigger': 'stop',
                        'source': ['STANDBY', 'HALTED', 'STOPPED'],
                        'dest': 'STOPPED'},
            ]

        # Create a queued state machine with auto-transitions
        try:
            super().__init__(
                model=None,
                states=self.statelist,
                transitions=self.transitions,
                initial='STOPPED',
                send_event=True,  # send event with transitions
                queued=True,  # guarantees finalization of transition processing
                auto_transitions=True,  # create to_<state> transitions in model
                after_state_change='_publish_state_change'  # opstat publishing
                # on_exception='on_exception',
            )
        except MachineError as e:
            _log.critical("Initialization of a FlecsimoMachine failed %s.", e)
            raise e


class StationModel(ABC):
    """A an abstract base model for all flecsimo-type station devices.

    Note: self.sfcu is required by twin webapp.

    TODO: Analyze if queue and event parameter would be sufficiently defined
          in implementation context. 

    Args:
        conf (dict): configuration data
        mqclient (mqtt): an mqtt client
        queue: used in specific implementation
        event: used in specific implementation
    """

    def __init__(self, conf, mqclient, queue=None, stopevent=None):

        self.org = Orglevel.STATION.value
        self.cmpt = Component.STATION.value
        self.active_setup = {}
        self.requested_setup = {}
        self.running = False
        self.sfcus = None  # used by txt apps.
        self.sfcu = 0  # used by web_backend_dashboard_handler.py / dashboard.js

        # Get from config
        self.conf = conf
        self.site = self.conf['site']
        self.area = self.conf['area']
        self.cell = self.conf['cell']
        self.station = self.conf['station']

        # Data access interface to station db.
        self.dao = dao.DataAccess(self.conf['database'])

        # mqtt configuration
        if self.conf['online']:
            self.mqclient = mqclient
        else:
            self.mqclient = MqMockup()

        self.sender = "/".join([self.site, self.area, self.cell, self.station])

        # Thread communication
        self.stop_event = stopevent
        try:
            _log.debug("Stop event is: %s", self.stop_event.is_set())
        except AttributeError:
            pass

    # Transition related call backs (guards and effects).
    # These callbacks should use event structure configured at machine.
    def _is_set_up(self, event):
        """Guard: evaluate if current setup configuration is usable for sfcu."""
        _log.debug("Event: %s, active setup: %s",
                   event.event.name, self.active_setup)

        if self.active_setup == self.requested_setup:
            _log.debug("On %s: no set up required", event.event.name)
            return True

        _log.debug("On %s: new set up is required: %s",
            event.event.name, self.requested_setup)
        return False

    def _not_running(self, event):
        """Guard: evaluate if machine is not running."""
        _log.debug("On %s: condition 'not_running' evaluated as %s",
                   event.event.name, not self.running)
        return not self.running

    def get_setup(self, event):
        """Prepare transition to station setup: get setup data for a given sfcu.
        
        This method implements the prepare transition behavior for the `load()'
        trigger. The requested set up configuration of a model is specific for
        the processed sfcu and has to be retrieved from the station's task 
        database.
        """
        sfcu = event.kwargs.get('sfcu', None)

        try:
            self.requested_setup = self._get_setup_by_sfcu(sfcu)

        except TypeError:
            _log.debug("Error in retrieving setup for sfcu '%s'", sfcu)

        _log.info("On %s: sfcu %s needs setup: %s",
                  event.event.name, sfcu, self.requested_setup)

    def finalize_setup(self, event):
        """Prepare transition from SETUP to ACTIVE: update state configuration.
        
        This callback updates the active setup configuration with the setup 
        configured in the the setup process of the machine. This method should
        be used as a prepare statement for the transition traversal in normal
        operation, i.e. targeting the ACTIVE state to avoid side effects when
        multiple transitions may be enabled.
        """
        _log.debug("On %s: finalize setup.", event.event.name)
        self.active_setup = self.requested_setup

    def prepare_unloading(self, event):
        """Prepare transition to DONE: update schedule when unloading an sfcu."""
        unloaded_sfcu = event.kwargs.get('sfcu', None)

        if unloaded_sfcu is not None:
            if self.conf['sfc_active']:
                self._update_schedule(unloaded_sfcu,
                                      ScheduleStates.DONE.value,
                                      ScheduleStates.DONE.name)
            _log.info("Station %s unloaded sfcu: %s.",
                      self.station , unloaded_sfcu)

    # State specific `entry/` (on_enter) callbacks.
    def initialize(self, _event):
        """Re-set state machine data tp prepare machine initialization.
        
        This callback resets the common state configuration. The setup 
        dictionaries are copied from an initial version with None- and 
        0-values to get independent dictionaries for the active and required
        setup-configuration. Finally it call the do/ behavior run_initialize().
        
        Note: _event data is not used in this method.
        """
        _log.info("Initialize state machine data for station %s", self.station)

        initial_setup = {'operation': 'None', 'parameter': {}}

        self.active_setup = initial_setup.copy()
        self.requested_setup = initial_setup.copy()
        self.running = False

        # Call the `do/`action of the state.
        self.run_initialize()

    def prepare_standby(self, _event):
        """Base method to Wait for a specific parts loaded to machine.

        This method implements the entry/ behavior of the STANDBY state. 
        It will only allow to transit to standby state if no out of sync 
        schedules are found in the machines's database. If the transition is
        possible it calls the the do/ behavior await_loading().
        
        Note: _event data is not used in this method.  
        """
        unsyncd = 0

        # Search for un-synched sfcus if sfcu management is switched on
        if self.conf['sfc_active']:
            # Get list of expected sfcus at time of entering state STANDBY
            self.sfcus = self.get_expected_sfcu()
            _log.info("Station %s waits for sfcus: %s", self.station, self.sfcus)

            # Check for out of sync sfcus
            syncstat = self.get_sfcu_sync_state(StationStates.STANDBY)
            for _, sfculist in syncstat.items():
                unsyncd += len(sfculist)

        if unsyncd == 0:
            try:
                self.await_loading()
            except Exception as e:
                self.halt(exception=e)
        else:
            e = "Unsynced sfcu states found."
            self.halt(exception=e)

    def prepare_setup(self, event):
        """Start operation set up process at station.
        
        This method implements the entry/ behavior of the SETUP state. After
        updating the state configuration it calls the the do/ behavior 
        run_setup().  
        """
        self.running = True
        sfcu = int(event.kwargs.get('sfcu'))

        try:
            self.run_setup(sfcu, self.requested_setup)
        except Exception as e:
            self.halt(exception=e, sfcu=sfcu)

    def prepare_operation(self, event):
        """Start a manufacturing process step for an SFCU at station.
        
        This method implements the entry/ behavior of the ACTIVE state. After
        updating the state configuration it calls the the do/ behavior 
        run_operation().  
        """
        self.running = True
        sfcu = event.kwargs.get('sfcu')
        operation = self.active_setup['operation']
        parameter = self.active_setup['parameter']

        if self.conf['sfc_active']:
            self._update_schedule(sfcu,
                                  ScheduleStates.WIP.value,
                                  ScheduleStates.WIP.name)
        try:
            self.run_operation(sfcu, operation, parameter)
        except Exception as e:
            self.halt(exception=e, sfcu=sfcu)

    def prepare_done(self, event):
        """Base method to complete a manufacturing process with unloading.
        
        This is the entry/ action of the DONE state which calls the do/ 
        action await_unloading().
        """
        sfcu = event.kwargs.get('sfcu')
        
        if self.conf['sfc_active']:        
            self._update_schedule(sfcu,
                                  ScheduleStates.PICKABLE.value,
                                  ScheduleStates.PICKABLE.name)
        try:
            self.await_unloading()
        except Exception as e:
            self.halt(exception=e, sfcu=sfcu)

    def prepare_halt(self, event):
        """Save exceptions and enable to make a decision.
        
        This is the entry/ action of the HALTED state which calls the do/ 
        action await_decision().
        """
        self.running = False
        error = event.kwargs.get('exception', None)
        sfcu_with_exception = event.kwargs.get('sfcu', None)
        _log.warning("Station halted when processing sfcu %s.",
                     sfcu_with_exception)

        if error is not None:
            _log.warning("Station %s halted with exception %s.",
                         self.station, error)
        elif event.error is not None:
            _log.warning("Station %s halted with exception %s.",
                         self.station, event.error)
        else:
            _log.warning("Station %s halted.", self.station)

        self.await_decision(proceed=False)

    def prepare_stop(self, _event):
        """Activities when machine stops regulary.
        
        Note: _event data is not used in this method.
        """
        _log.debug("Station %s stopped.", self.station)

    def on_exception(self, event):
        """Central machine's exception handler.
        
        Note:
            Currently not in use.
        """
        _log.debug("Exception reported: %s", event.error)
        sfcu = event.kwargs.get('sfcu', None)
        exception = event.error
        self.halt(exception=exception, sfcu=sfcu)

    # Interfaces for do/ run behavior in state
    @abstractmethod
    def run_initialize(self):
        """Run the model specific initialization behavior."""

    @abstractmethod
    def await_loading(self):
        """Run the model specific behavior to load materials for processing."""

    @abstractmethod
    def run_setup(self, sfcu:int, setup:dict):
        """Run the model specific the setup (behavior).
        
        This method has to implement or call a process which runs the set up of
        the station model and finally set self.running = False."""
        self.running = False

    @abstractmethod
    def run_operation(self, sfcu:int, operation:str, parameter:{}):
        """Run the model specific operational behavior.
        
        This method has to implement or call a process which runs the operation
        and finally set self.running = False."""
        self.running = False

    @abstractmethod
    def await_unloading(self):
        """Run the model specific behavior to unload processed materials."""

    @abstractmethod
    def await_decision(self, proceed=True):
        """Run the model specific behavior when station is halted."""
        return proceed

    # Helper functions
    def get_expected_sfcu(self):
        """ Get a list of expected sfcu's for this machine."""
        expected_sfcus = self.dao.retrieve('schedule',
                                           columns=['sfcu'],
                                           inlist={'statx': LOADABLE_STATES},
                                           distinct=True)
        return [sfcu['sfcu'] for sfcu in expected_sfcus]

    def get_sfcu_state(self, key, typ='sfcu'):
        """Lookup sfcu and it's state, also by material handling unit id (mhu).
               
        Args:
            key (int): Number of sfcu or mhu.
            type (str): sfcu or
            
        Note: 
            if type is set to anything else but 'sfcu', this methods will 
            lookup for an 'mhu' key in the stations database.
        """
        conditions = {'sfcu': key} if typ == 'sfcu' else {'mhu': key}

        sfcu_state = self.dao.retrieve('schedule',
                                       columns=['sfcu', 'statx'],
                                       conditions=conditions,
                                       distinct=True,
                                       single=True)
        if sfcu_state is None:
            return None, None

        return sfcu_state['sfcu'], sfcu_state['statx']

    def get_next_loadable_sfcu(self):
        """Retrieves the next loadable sfcu."""
        sfcu = self.dao.retrieve('schedule',
                                 columns=['sfcu', 'statx'],
                                 inlist={'statx': LOADABLE_STATES},
                                 single=True,
                                 orderby='sfcu')
        return sfcu['sfcu'], sfcu['statx']

    def get_sfcus_by_state(self, state: ScheduleStates):
        """Get a list sfcus in a list of specific state.
        
        Args:
            state (list of ScheduleStates)
        
        Returns:
            List of sfcu (int) in selected state(s)
        """
        statelist = self._compose_sequence(state)
        sfcus = self.dao.retrieve('schedule',
                                 columns=['sfcu'],
                                 inlist={'statx': statelist},
                                 distinct=True)
        return [sfcu['sfcu'] for sfcu in sfcus]

    def get_sfcu_sync_state(self, state):
        """Return a mapping for out of sync sfcu-lists."""
        try:
            currentstate = state.name
        except AttributeError:
            currentstate = state

        # These station- and scheduling-state have to be in sync
        synced_states = {StationStates.STANDBY.name: ScheduleStates.LOADED,
                         StationStates.ACTIVE.name: ScheduleStates.WIP,
                         StationStates.DONE.name: ScheduleStates.PICKABLE}

        # Exclude in-sync states from result for current station state
        oos_keys = set(synced_states.keys()) - {currentstate, }
        oos_stat = [synced_states[key] for key in oos_keys ]

        return {oos.name: self.get_sfcus_by_state(oos) for oos in oos_stat}

    @property
    def loadable_states(self):
        """Show states allowed for loading material to machine."""
        return LOADABLE_STATES

    # Internal functions
    def _set_sfcu(self, event):
        """Sets the sfcu passed by the load(sfcu=sfcu) trigger."""
        self.sfcu = event.kwargs.get('sfcu', 0)

    def _reset_sfcu(self, event):
        """Re-sets the sfcu passed by the unload(sfcu=sfcu) trigger."""
        self.sfcu = 0

    def _status_message(self, state) -> dict:
        """Create a machine status message for edge nodes (topic and payload)."""
        topic = "/".join([self.sender, 'status', self.cmpt])
        payload = json.dumps({'org': self.org,
                              'sfcu': self.sfcu,
                              'mhu': 0,
                              'state': state.value,
                              'statx': state.name,
                              'at': datetime.datetime.utcnow().isoformat()})
        return {'topic':topic, 'payload':payload}

    def _publish_state_change(self, event):
        """ Publish state change to MQTT.
 
        This callback implements the after_stat_change actions on the level of
        the FlecsimoMachine and will be executed after every transitional state
        change. It will be executed after every state- and transition-level
        callback.
        """
        _log.info("entry/ %s --> (%s) %s, call: %s, sfcu: %s",
                  event.transition.source,
                  event.state.value.value,
                  event.state.name,
                  event.state.on_enter,
                  self.sfcu)

        # INFO: transitions.core.State objects are special since transitions
        # core overrides the value operation for enumerations, so a enum value
        # s has to be provided as s.value.
        message = self._status_message(state=event.state.value)
        self.mqclient.publish(message["topic"], message["payload"])

    def _compose_sequence(self, term):
        """Compose a tuple of names for a given string or list of Enums."""
        if isinstance(term, (tuple, list)):
            nametuple = (s.name if isinstance(s, Enum) else s for s in term)
        else:
            nametuple = (term.name,) if isinstance(term, Enum) else (term,)
        return nametuple

    def _get_setup_by_sfcu(self, sfcu:int) -> dict:
        """Retrieve setup request per sfcu and format as dict
        
           This method creates a "packed" structure for multi-parameter 
           stations:
           
           {'operation': operation, 
            'parameters': {param_typ1: param_value1, param_typ2: param_value2,
                           ..., 'setup': sum(setup1,...}}
                           
           where the overall setup time is computed as the sum of the setup 
           times of each single parameterization. Although this might be not
           realistic in manufacturing (neglecting possibilities of 
           parallelization) it offers a direct experience of the influence 
           that different product configuration will have on execution runs on 
           a station. 
        """
        setup_time = 0
        setup = {'operation': None, 'parameter': {}}
        setuplist = self.dao.retrieve('sfcu_setup',
                                        columns=['operation',
                                                 'param_typ',
                                                 'param_value',
                                                 'setup'],
                                        conditions={'sfcu': sfcu},
                                        inlist={'statx': LOADABLE_STATES})

        if setuplist is not None:
            for _ in setuplist:
                setup_time += _['setup']
                setup['operation'] = _['operation']
                setup['parameter'].update({_['param_typ']: _['param_value'],
                                           'setup': setup_time})
        else:
            _log.debug("No setup data found for sfcu %s.", sfcu)
        return setup

    def _recover_sfcu_errors(self, states, mode='Failed'):
        """Reassign sfcus or mark them as failed.
        
        Marks all sfcus scheduled records in a list of given states either as 
        FAILED or as REWORK. 
        
        Args:
            states (Enum, str or list): (List of) states to be recovered from.
            mode (str): Failed (default) or Reassign.
        
        Returns:
            The total number of changed sfcu records.
            
        Note: 
            This method may change a large number of records in the database.
        """
        no_changed_sfcus = 0
        numc = 0

        statelist = self._compose_sequence(states)

        if mode == 'Failed':
            to_state = ScheduleStates.FAILED
        elif mode == 'Reassign':
            to_state = ScheduleStates.REWORK
        else:
            return no_changed_sfcus

        for state in statelist:
            numc += self.dao.change('schedule',
                                    data={'state': to_state.value,
                                          'statx': to_state.name,
                                          'at': datetime.datetime.utcnow()},
                                    conditions={'site': self.site,
                                                'statx': state})
        return numc

    def _test_sfcu(self, sfcu, instate):
        """Test if a given sfcu's schedule is in a list of states.
        
        Args:
            sfcu (int): a single sfcu number
            instate (Enum, or sequence of Enums): states to test for.
            
        Returns:
            (Boolean): True if sfcu's state is in given state list.
        """
        statx = self.dao.retrieve('schedule',
                                  columns=['statx'],
                                  conditions={'sfcu': sfcu})
        return statx in instate

    def _update_schedule(self, sfcu, state, statx):
        """Set schedule to a given state.
        
        Args:
            sfcu (int): number of sfcu.
            state (int): numerical state identifier.
            statx (str): name of state.
            
        Returns:
            Number of updated schedules in the machine's database.
        """
        rc = self.dao.change('schedule',
                             data={'state': state,
                                   'statx': statx,
                                   'at': datetime.datetime.utcnow()},
                             conditions={'site': self.site, 'sfcu': sfcu})
        return rc


class MqMockup:
    """An mqclient mockup to run stations in offline mode."""

    def publish(self, topic, payload):
        _log.debug("State change %s", payload)
