"""Managing and running simulations of flecsimo components

Note:
    This Module was merged on 13.08.2023 from former `simulation` module (which 
    defined a management class for starting and stopping processes needed for
    simulations according to given configuration files) and the `stationsim` 
    module (which definied a class to run a flecsimo station simulation).

Created on 14.05.2021 (simulation, Schnieber) 
        and 23.10.2021 (stationsim, Banning)

@author: Leon Schnieber, Ralf Banning

Copyright and License Notice:
    flecsimo simulation starter
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import logging
from queue import Queue, Empty
from threading import Thread, Event
from time import sleep

from flecsimo.base import config
from flecsimo.base.const import Orglevel
from flecsimo.base.states import ScheduleStates

from flecsimo.cmpt.areaagent import AreaAgent
from flecsimo.cmpt.areacontrol import AreaControl
from flecsimo.cmpt.cellagent import CellAgent
from flecsimo.cmpt.edge import Node
# from flecsimo.cmpt.cellcontrol import CellControl

from flecsimo.cmpt.siteagent import SiteAgent
from flecsimo.cmpt.sitecontrol import SiteControl
from flecsimo.cmpt.station import FlecsimoMachine, StationModel
from flecsimo.util import dbmanager
from transitions.core import MachineError
# from pip._internal.locations import _log_context

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())

SHORTEST_OPERATION_TIME = 1


class SimManager:
    """Management of simulation-processes according to given config files.
    
    This class defines management objects (controller) for setup, starting and
    stopping processes needed in simulation of flecsimo components (aka digital
    flecsimo twin). Setup and configuration of processes is based on 
    configuration files. 
    
    Note: the SimManager should be created by a module which has it's config 
          and database directories at parent level. 
    """

    def __init__(self, root:str, setup="simulation_setup.json", profile="sim"):
        """Read a setup config file and set simulation parameters.
        
        Args:
            root (str):    path-like location of the configuration root for this 
                           module.
            setup (str):   path or file for simulation setup.
            tier (str):    deployment / runtime tier. DEFAULT="Live".
            demo (bool):   Re-create a demo database at start if True. 
                           DEFAULT=False.
                           
        
        """
        self.configurator = config.Configurator(root)
        config_path = self.configurator.get_conf_path(setup)
        confdata = self.configurator.read_configfile(config_path)
        self.setup_dict = confdata['PROFILE']
        self.active_setup_name = ""
        self.profile = profile
        # TODO:
        #  self.demo = demo

        self.sim_cmpt = {}

        self.__stop_event__ = Event()

    def get_simulation_setup_list(self):
        """Create a setup list from a setup dictionary"""
        setup_list = []

        for key in self.setup_dict:
            setup_list.append({
                "value": key,
                "name": self.setup_dict[key]["name"],
                "description": self.setup_dict[key]["description"]
            })
        return setup_list

    def start_simulation(self):
        """Create and start all objects: machine, models and a shared agent.
        
        A complete simulation requires a flecsimo finite state machine, using a 
        shared agent (for sending mqtt messages), a set of models (simulated 
        stations) and a set of agents at cell, area and site level, as defined 
        in the configuration. The normal setup sequence will be:
            1. Create and start a shared agent
            2. Create a flecsimo finite state machine
            3. a) Create and start active agents on cell / area / site level
               b) Create and add Simulated Stations to the Flecsimo Machine
            4. Initialize all Machine-Models by dispatching an `init` trigger.
            
        The steps 3.a and 3.b are delegated to the method `start_cmpt`.
        
        Returns:
            True (boolean): if no exception occurred.
        """
        if self.active_setup_name not in self.setup_dict:
            return "no setup found/active"

        # get list of components to be started for the selected scene
        config_list = self.setup_dict[self.active_setup_name]["files"]

        # One "shared" node for all 1..4 stations of a simulation.
        # TODO: think of more regular sender id.
        stn_settings = {'sender': "Simulation: " + self.active_setup_name,
                        'org': Orglevel.MIXED.name,
                        'cmpt': "SIM",
                        'mqserver': 'localhost',
                        'cid': None}
        self.stn_node = Node(stn_settings)
        self.stn_node.start()

        # One "shared" state machine for 1..4 stations of a simulation
        self.machine = FlecsimoMachine()

        # Start the active components.
        for config_file in config_list:
            self.start_cmpt(config_file)

        # Initialize and run all models of state machine:
        self.machine.dispatch("init")
        return True

    def start_cmpt(self, config_file):
        """Instantiate and start (or add) simulation components.
        
        This method extracts the required information from a given confguration
        file to start the therein defined component. If the demo meber variable 
        is set True it also creates a new component specific database and 
        inserts a set of simulation data (master and transaction data).
        
        Args:
            config_file (str): the file name of the configuration file.
            
        Base Sequence:
        1. Read tier specific configuration data for the components. 
           Default is the "Live" runtime tier (=Production).
        2. Start the components and add their data to the sim_cmpt dictionary.
        
        Alternative Sequences:
        2.a ["demo" is set to "True"]: 
        2.a.1.Create component dabase and simulation data.
        2.a.2 continue with step 2.    
         
        """

        # 1. Read tier specific configuration data.
        confdata = self.configurator.get_config(config_file, self.profile)

        try:
            org = confdata["type"]
        except KeyError as e:
            _log.DEBUG("No data found in configuration for type %s.",
                       self.tier)
            raise(e)

        cmpt_name = confdata[org]
        _log.debug("Starting component of type '%s' with name '%s'.",
                   org, cmpt_name)

        # Prepare component dictionary
        # We need no cid since using clean sessions only.
        self.sim_cmpt.update(
                {cmpt_name:
                    {"agent": None,
                     "controller": None,
                     "type": org ,
                     "log": [],
                     "config": confdata,
                     "status": "unknown", }
                    }
                )

        # 2.a ["demo" is True]
        # Create component database and simulation data.
        if confdata['data'] == 'sim':
            dbname = "".join(['demo_', cmpt_name, '.db'])
            database = str(self.configurator.get_db_path(dbname))
            _log.debug("Create demo db %s", database)

            # Override database setting
            confdata['database'] = database

            # Create demo-database
            dbs = dbmanager.DbSetUp(database)
            dbs.create_schema(org=org)
            dbs.create_simdata(org=org, name=cmpt_name)
            del dbs
        else:
            # Make sure providing an absolute path to database
            database = str(self.configurator.get_db_path(confdata['database']))
            confdata['database'] = database

        # 2. Start the components and add their data to the sim_cmpt dictionary.
        if org == Orglevel.CELL.value:
            self.sim_cmpt[cmpt_name]["agent"] = CellAgent(
                conf=confdata,
                )
            self.sim_cmpt[cmpt_name]["agent"].start()

            self.sim_cmpt[cmpt_name]["controller"] = StationSim(
                conf=confdata,
                mqclient=self.stn_node.mqclient,
                stopevent=self.__stop_event__)

            self.machine.add_model(self.sim_cmpt[cmpt_name]["controller"])

            self.sim_cmpt[cmpt_name]["status"] = "running"

        elif org == Orglevel.AREA.value:
            self.sim_cmpt[cmpt_name]["agent"] = AreaAgent(
                confdata,
                )
            self.sim_cmpt[cmpt_name]["agent"].start()

            self.sim_cmpt[cmpt_name]["controller"] = AreaControl(
                confdata,
            )
            self.sim_cmpt[cmpt_name]["controller"].start()

            self.sim_cmpt[cmpt_name]["status"] = "running"

        elif org == Orglevel.SITE.value:
            self.sim_cmpt[cmpt_name]["agent"] = SiteAgent(
                confdata,
                )
            self.sim_cmpt[cmpt_name]["agent"].start()

            self.sim_cmpt[cmpt_name]["controller"] = SiteControl(
                confdata,
            )
            self.sim_cmpt[cmpt_name]["controller"].start()

            self.sim_cmpt[cmpt_name]["status"] = "running"

        return True

    def stop_simulation(self):
        """Stop all active simulation components."""
        for cmpt_name in self.sim_cmpt:
            try:
                self.sim_cmpt[cmpt_name]["agent"].stop()
            except AttributeError:
                pass
            try:
                self.sim_cmpt[cmpt_name]["controller"].stop()
            except AttributeError:
                pass
            except MachineError:
                self.sim_cmpt[cmpt_name]["controller"].halt()
            try:
                self.sim_cmpt[cmpt_name]["controller"].shut_down(self.__stop_event__)
            except AttributeError:
                pass

        self.sim_cmpt = {}
        self.active_setup_name = ""

    def get_cmpt_status(self):
        """Get the current state of all simulation components.
        
        TODO: to be implemented.
        """
        return []


class StationSim(StationModel):
    """A simulated station model for a flecsimo machine."""

    # @profile(precision=3)
    def __init__(self, conf, mqclient, queue=None, stopevent=None):
        super().__init__(conf, mqclient, queue, stopevent)
        self.conf = conf
        self.queue = Queue(maxsize=1)
        self.processing_time = 25  # Default processing time

    # Implement abstract methods from StationModel
    def run_initialize(self):
        """Simulated station initialization."""
        
        # Search for un-synched sfcus if sfcu management is switched on
        if self.conf['sfc_active']:
            sfcus = self.get_sfcus_by_state(state=ScheduleStates.WIP)
            if len(sfcus) > 0:
                self.halt(sfcus=sfcus)
                return
        self.proceed()

    def await_decision(self, proceed=False):
        """Decision will be made by external process."""
        return proceed

    def await_loading(self):
        """Loading will be controlled by external process."""
        pass

    def await_unloading(self):
        """Unloading will be controlled by external process."""
        pass

    def run_setup(self, sfcu:int, setup:{}):
        """Run the model specific the setup (behavior).
        
           Simulation specific implementation of the abstract method in the 
           super-class: a thread will be started running for the given setup 
           time as passed by the setup parameter. The behaviour is implemented
           in _run_setup_thread().
        """
        setup_time = setup['parameter']['setup']
        _log.info("Run setup for sfcu %s (%s seconds).", sfcu, setup_time)

        set_up = Thread(target=self._run_setup_thread,
                        args=(
                            self.station,
                            sfcu,
                            setup_time,
                            self.stop_event,
                            self.queue),
                        daemon=True,
                        )
        set_up.start()

    def run_operation(self, sfcu:int, operation:str, parameter:dict):
        """Run the production process on station.
        
        This method implements the abstract method in parent class.
        """
        # TODO: store processing time in task
        sleep(0.2)
        _log.info("Start processing sfcu %s in operation %s "\
                  "with parameter %s",
                  sfcu, operation,
                  parameter,)

        operation = Thread(target=self._run_operation_thread,
                                  args=(self.station,
                                        sfcu,
                                        self.processing_time,
                                        self.queue,
                                        self.stop_event,
                                        ),
                                  daemon=True)
        operation.start()

    def _run_setup_thread(self, station, sfcu, setup_time, stop_event, queue):
        """Start a daemon thread to simulate machine set up operation."""
        _log.info("Start machine set up with setup_time %s, sfcu %s.",
                  setup_time, sfcu)

        elapsed = 0
        progress = 0

        while not stop_event.is_set():
            if elapsed < setup_time:

                data = {}
                sleep(1)
                elapsed += 1
                progress = elapsed / setup_time

                data['station'] = station
                data['section'] = "setup"
                data['elapsed'] = elapsed
                data['duration'] = setup_time
                data['progress'] = progress

                if queue.full():
                    queue.get()

                queue.put(data)
            else:
                self.active_setup = self.requested_setup
                self.running = False
                data.update({'elapsed': 0, 'duration': 0, 'progress': 0})
                if queue.full():
                    queue.get()
                queue.put(data)               
                self.go(sfcu=sfcu)
                return

        _log.debug("Setup of station %s was stopped at %s progress.",
                   data['station'], data['progress'])

    def _run_operation_thread(self, station, sfcu, time, queue, stop_event):
        """Start a daemon thread to simulate machine operation."""
        # time = self._int_if_number(processing_time)

        duration = max(int(time), SHORTEST_OPERATION_TIME)
        elapsed = 0
        progress = 0

        while not stop_event.is_set():
            if elapsed < duration:
                data = {}
                sleep(0.5)
                elapsed += 1
                progress = elapsed / duration

                data['station'] = station
                data['section'] = "operation"
                data['elapsed'] = elapsed
                data['duration'] = duration
                data['progress'] = progress

                if queue.full():
                    queue.get()
                queue.put(data)
            else:
                self.running = False
                data.update({'elapsed': 0, 'duration': 0, 'progress': 0})
                if queue.full():
                    queue.get()
                queue.put(data)
                self.proceed(sfcu=sfcu)
                return

        _log.debug("Operation of station %s was stopped at %s progress.",
                   data['station'], data['progress'])

    def get_progress(self):
        """Get the 'manufacturing'-progress of the cell from the queue filled 
           by the worker-thread.
        """
        data = {}
        try:
            # TODO: (low priority - works) problem with queues currently: If
            # using proper blocking .get-Methods there can be problems with
            # e.g. two webserver instances polling the status and each of them
            # emptying the queue. For now this is work-arounded by using a
            # nonblocking direct access to the queue.
            if not self.queue.empty():
                data = self.queue.queue[0]
                # data = controller.queue.get(timeout=2)
        except Empty:
            data = {}
        return data
