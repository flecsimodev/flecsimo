"""
Created on 07.09.2021 from app.site_agent.py, first created on 30.05.2020

@author: Ralf Banning

Copyright and License Notice:

    flecsimo site agent component
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.
    
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
from flecsimo.base.const import Orglevel, Component
from flecsimo.cmpt import edge
from flecsimo.habit.statushandling import StatusMessageHandler
from flecsimo.habit.responsehandling import ResponseMessageHandler


class SiteAgent(edge.Agent):
    """Class for site related services.
    
    This class derives from the 'Agent' superclass which handles the 
    connections to the environment (port functionality, i. e. mqtt and 
    database connection). The main purpose of this class itself, is to build 
    the context of the site agent, i.e. the parameterization and the 
    topic mapping of message callback instances.
            
    Args:
        conf:   dict with configuration data
        cid:    Cell identifier of mqtt connection.
    """

    def __init__(self, conf=None, cid=None):
        # Initialize environment information in self.env
        org = Orglevel.SITE.value
        cmpt = Component.AGENTTYP.value
        
        if cid is not None:
            conf['cid']=cid
        # Initialize environment (env) and Node instance
        super().__init__(conf, org, cmpt)

        self.database = self.env['database']
        site = self.env['site']

        # Get habits.
        response_handler = ResponseMessageHandler(self.database)
        status_handler = StatusMessageHandler(self.database, self.env)


        # Define subscription topics.
        enlist_topic = "/".join([site, '+', 'enlist'])
        delist_topic = "/".join([site, '+', 'delist'])
        quote_topic = "/".join([site, '+', 'quote'])
        opcfm_topic = "/".join([site, '+', 'opcfm'])
        opstat_topic = "/".join([site, '+', 'enrol'])

        # Create context map: usecase, topic, habit, qos value.
        self.context.add('enlist', enlist_topic, status_handler.on_enlist, 0)
        self.context.add('delist', delist_topic, status_handler.on_delist, 0)
        self.context.add('quote', quote_topic, response_handler.on_quote, 0)
        self.context.add('opcfm', opcfm_topic, response_handler.on_opcfm, 0)
        self.context.add('opstat', opstat_topic, status_handler.on_opstat, 0)

        # Make subscription list and callbacks available to node.
        self.node.subscription = self.context.subscriptions
        self.node.register_callback(self.context.habits)

