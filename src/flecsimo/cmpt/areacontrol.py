#!/usr/bin/env python3
"""Methods for controlling area processes

This module provides a wrapper for the area_context which implements the 
interactions of the are based control processing use cases.

Architectural note: 
    This component may be seen as a replacement for a "GUI", and acts mainly 
    for the controller-part. The "order_context" object follows the ideas of 
    Reenskaug and Coplien, to factor out use case algorithms and business logic 
    from pure domain data object (the database in this case) in to abstract 
    role models which are mixed into the data objects at runtime. 

Created on 30.05.2020

@author: Ralf Banning

Copyright and License Notice:

    flecsimo area control component
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
from flecsimo.base.const import Orglevel, Component
from flecsimo.cmpt import edge


class AreaControl(edge.Node):
    """Control functions and habits on area level."""

    def __init__(self, conf:dict):
        """Initialize configuration of controller."""
        org = Orglevel.AREA.value
        cmpt = Component.CONTROLTYP.value
        
        self.env = edge.map_environment(conf, org, cmpt) 
        super().__init__(self.env['settings'])
        
        self.database = self.env['database']

    # TODO: discuss, if AreaControl should provide method to start/stop agents?
    # TODO: define / use in flecsimo.habit.enrollment
    def enrol(self, arg):
        # TODO: there is not order_context element - something is wrong about this!
        pass

    def quit(self, arg):
        # TODO: there is not order_context element - something is wrong about this!
        pass

    # TODO: define / use in flecsimo.habit.quotation
    def quote(self, arg):
        """Quote for a received site rfq."""
        pass

    def request_quote(self, arg):
        """Request a quotation for an sfcu/operation from cells."""
        pass

    # TODO: think if release_sfcu() is further required
    def release_sfcu(self, arg):
        """Assign an sfcu to cell and send opdta"""
        pass

