#!/usr/bin/env python3
"""Moethods for controlling cell processes

This module provides a wrapper for the area_context which implements the 
interactions of the shop floor control processing use cases.

Architectural note: 
    This component may be seen as a replacement for a "GUI", and acts mainly 
    for the controller-part. The "order_context" object follows the ideas of 
    Reenskaug and Coplien, to factor out use case algorithms and business logic 
    from pure domain data object (the database in this case) in to abstract 
    role models which are mixed into the data objects at runtime. 

Created on 04.10.2021

@author: Ralf Banning

Copyright and License Notice:
    flecsimo cell control component
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
from flecsimo.base.const import Orglevel, Component
from flecsimo.cmpt import edge


class CellControl(object):
    """
    TODO: implement CellControl
    """

    def __init__(self, conf=None, cid=None):
        """Initialize configuration of controller."""
        org = Orglevel.CELL.value
        cmpt = Component.CONTROLTYP.value

        self.env = edge.map_environment(conf, org, cmpt)
        self.database = self.env['database']

    # TODO: discuss, if CellControl should provide method to start/stop agents?
