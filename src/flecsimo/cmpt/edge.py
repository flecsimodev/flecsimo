"""This module contains the base classes for flecsimo edge components.

Created on 21.03.2024 based on flecsimo.base.agent, created on 24.08.2021

@author: Ralf Banning

Copyright and License Notice:
    flecsimo edge component classes
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from abc import ABC
import datetime
from enum import Enum
import json
import logging
import paho.mqtt.client as mqtt
from time import sleep
from types import MappingProxyType

from flecsimo.base.const import Orglevel, Component

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())

# Global constants
MQTT_MINQOS = 0  # smallest value for mqtt messages quality of service
MQTT_MAXQOS = 2  # largest value for mqtt messages quality of service

# Templates for composing sender id strings
ORGTREE = MappingProxyType(
                    {Orglevel.SITE.value: (Orglevel.SITE.value,),
                     Orglevel.AREA.value: (Orglevel.SITE.value,
                                           Orglevel.AREA.value),
                     Orglevel.CELL.value: (Orglevel.SITE.value,
                                           Orglevel.AREA.value,
                                           Orglevel.CELL.value),
                     Orglevel.STATION.value: (Orglevel.SITE.value,
                                              Orglevel.AREA.value,
                                              Orglevel.CELL.value,
                                              Orglevel.STATION.value),
                     Orglevel.STOCK.value: (Orglevel.SITE.value,
                                            Orglevel.STOCK.value)})


def map_environment(conf:dict, org:str, cmpt:str) -> dict:
    """Map a environment from a configuration.
    
    Args:
        conf (dict): a flecsimo configuration
        org (str): organizational level of a facility.
        cmpt (str): type of the component.
    
    Return:
        a checked environment data dict (env)
        
    Exception:
        KeyError: if required configuration key is missing
    """
    # Compose required configuration keys for this organizational level,
    required_config_keys = ['mqserver', 'database']
    try:
        required_config_keys += ORGTREE[org]
    except KeyError as e:
        raise Exception("No Orgtree entry found.") from e
    
    env = conf.copy()

    # check data quality
    try:
        for _ in required_config_keys:
            env[_]
    except KeyError as e:
        raise Exception("Incomplete environment information.") from e

    try:
        cid = conf['cid']
    except KeyError:
        cid=None

    # Add node settings to env
    node_settings = {'sender': compose_sender(conf, org),
                     'org': org,
                     'cmpt': cmpt,
                     'mqserver': conf['mqserver'],
                     'cid': cid}
    env['settings'] = node_settings

    return env


def compose_sender(config:dict, org:str) -> str:
    """Compose a sender string from a config dict by a given organization unit.
    
    Args:
        config (dict): a flecsimo configuration
        org (str): organizational level of a facility (like site, area, ...)
        
    Return:
        sender (str): a flecsimo compliant sender string.
    """
    # Select template:
    try:
        template = ORGTREE[org]
    except KeyError as e:
        raise KeyError("No Orgtree entry found.") from e

    try:
        sender = "/".join([config[t] for t in template])
    except (ValueError, KeyError) as e:
        raise KeyError("Can not compose sender for given organizational " \
                        "level (org).") from e

    return sender


class SubscriptionError(Exception):
    """Indicates a subscription to the edge mqtt server has failed."""
    pass


class NodeStates(Enum):
    """Enumerated states for Agent objects."""
    STOPPED = 0
    READY = 1
    SHUTDOWN = 3
    STARTED = 4
    HALTED = 5


class Node:
    """A communication node within a flecsimo edge network.
    
    A node offers a connection to a mqtt server and provides methods start 
    and stop message processing. Moreover it offers to execute callback 
    functions per subscribed topic (as inversion of control).
    
    The node provides a settings dictionary, which keeps essential information
    to identify a node in the environment and in the message communication. 
    """

    def __init__(self, settings:dict):
        self._settings = {'sender': 'FUAS/DEFAULT',
                          'org': "N/A",
                          'cmpt': Component.NODETYP,
                          'mqserver': 'localhost',
                          'cid': None}
        self.settings = settings
        # self.mqserver = self.settings['mqserver']
        self.mqstates = {'connected': False, 'subscribed': False}
        self.mqclient = mqtt.Client(self.settings['cid'])
        self.mqclient.on_connect = self._on_connect
        self.mqclient.on_log = self._on_log
        self.mqclient.on_message = self._on_message
        self.mqclient.on_subscribe = self._on_subscribe
        self.mqclient.on_stop = None
        self.subscription:list[tuple] = None

    @property
    def settings(self):
        """A property object reflecting a node's settings.
        
        A node setting describes the relevant information for a node, to 
        identify itself in the edge mqtt network, i.e. the type of the node, 
        the type of the component used and the sender identification. 
        """
        return self._settings

    @settings.setter
    def settings(self, update:dict):
        try:
            self._settings.update({k:update[k] for k in self._settings.keys()
                                   if k in update})
        except (TypeError, ValueError) as e:
            _log.debug("Update of node settings failed: %s", e)

    def _status_message(self, org, cmpt, sender, state:Enum) -> dict:
        """Create a status message for edge nodes (topic and payload)."""
        topic = "/".join([sender, 'status', cmpt])
        payload = json.dumps({'org': org,
                              'state': state.value,
                              'statx': state.name,
                              'at': datetime.datetime.utcnow().isoformat()})
        return {'topic':topic, 'payload':payload}

    def _wait_for(self, mqclient, state, wait=0.25):
        if state == "connected":
            if mqclient.on_connect:
                while self.mqstates['connected'] is False:
                    _log.info("waiting for CONNACK message.")
                    mqclient.loop()  # check for messages
                    sleep(wait)

        if state == "subscribed":
            if mqclient.on_subscribe:
                while self.mqstates['subscribed'] is False:
                    _log.info("waiting for SUBACK message.")
                    mqclient.loop()  # check for messages
                    sleep(wait)

    def _subscribe(self, subscription):
        """Node wrapper for client subscription."""
        self.mqstates['subscribed'] = False
        rc = self.mqclient.subscribe(subscription)
        if rc[0] != 0:
            _log.critical("Subscription FAILED with code=%s, sender=%s",
                          rc, self.settings['sender'])
            raise SubscriptionError("Subscription failed.")
        else:
            self.mqstates['subscribed'] = True

    def _on_nodestop(self):
        """Publish state on node stop."""
        _log.debug("Node will stop. Sender: %s, organizational level: %s",
                   self._settings['sender'], self._settings['org'])

        message = self._status_message(self._settings['org'],
                                       self._settings['cmpt'],
                                       self._settings['sender'],
                                       NodeStates.STOPPED)
        self.mqclient.publish(message['topic'], message['payload'])

    def _on_connect(self, client, userdata, flags, rc):
        """Publish start message on (re-) connect and subscribe."""
        self.mqstates.update({'connected': False, 'subscribed': False})
        if rc == 0:
            self.mqstates['connected'] = True
            _log.debug("Connect OK, returned code=%s", rc)
            _log.debug("Node started. Sender: %s, organizational level: %s",
                       self._settings['sender'], self._settings['org'])
            message = self._status_message(self._settings['org'],
                                           self._settings['cmpt'],
                                           self._settings['sender'],
                                           NodeStates.STARTED)
            self.mqclient.publish(message['topic'], message['payload'])

            # Handle subscription for clean sessions on reconnect:
            if self.subscription is not None:
                self._subscribe(self.subscription)

        else:
            _log.critical("Connect FAILED with code=%s, sender=%s",
                          rc, self.settings['sender'])

    def _on_subscribe(self, client, userdata, mid, qranted_qos):
        self.mqstates['subscribed'] = True

    def _on_log(self, client, userdata, level, buf):
        """Internal default logger."""
        _log.debug("node: %s", buf)

    def _on_message(self, client, userdata, message):
        """Internal default message handler."""
        _log.debug("Received message id %s with payload %s.",
                   message.mid, str(message.payload.decode('utf-8')))

    def register_callback(self, habits:tuple):
        """Append a (topic, callback)-list to the mqtt client."""
        if habits is not None:
            try:
                for sub, callback in habits:
                    self.mqclient.message_callback_add(sub, callback)
            except BaseException as e:
                raise ValueError("Failed to add callback.") from e

    def start(self, subscription:list=None):
        """Prepare and start receiving or sending of mqtt messages.

        Connect to mqtt-server and subscribe to a topic or list of topics
        (if provided) and start the mqtt-client loop in any case.
        """
        try:
            self.mqclient.connect(self.settings['mqserver'])

            if self.subscription is not None:
                self._wait_for(self.mqclient, 'subscribed', wait=0.5)
            self.mqclient.loop_start()

        except Exception as e:
            _log.critical("Failed to start connection for cid %s",
                          self.settings['cid'])
            raise e

    def stop(self, on_stop=None):
        """Stop node operations and shut down connections gracefully.

        Calls a definable stop function (e.g. to send messages), disconnect 
        from server and stop the mqtt client loop.

        Args:
            on_stop    callback function to be executed before disconnect.
                       Default is _on_agentstop
        """
        on_stopfunc = self._on_nodestop if on_stop is None else on_stop

        _log.debug("Call on_stopfunc...")
        on_stopfunc()

        _log.debug("Call disconnect...")
        self.mqclient.disconnect()

        _log.debug("Stop loop.")
        self.mqclient.loop_stop()


class Agent(ABC):
    """A base configuration of a flecsimo agent object.
    
    This abstract class defines three main concepts of flecsimo agents: habits, 
    i.e. dynamic configurable parts of use cases that an agent may act in, 
    subscriptions, i.e. events of interest raised in the edge network and 
    context information, i.e. a mapping between events and the habits to be 
    triggered. 
    
    Agents act in a given environment, i.e. a level in the production model
    like site, area or cell and many others. Since the environment specification
    may be different for each type of agent implementation the mapping has to be
    implemented by the derived classes.  
    """

    def __init__(self, conf, org, cmpt):
        self.env = map_environment(conf, org, cmpt)
        self.node = Node(self.env['settings'])
        self.context = Context()

    def start(self):
        """Start an edge node."""
        self.node.start()

    def stop(self, onstop=None):
        """Stop an edge node."""
        self.node.stop(onstop)


class Context:
    """Maps the received information type to the required behavior (habit).
    
    Note:
        Technically the context defines a behavior (a.k.a habit) for every topic
        subscribed from the edge network that should be executed when a message 
        on that topic is received. 
    
        Conceptually this can be understood as adding the ability to "perceive
        information" after "receiving data". The term "context" was influenced 
        by the DCI paradigm of Coplien and Renskaugh as it maps the received 
        data to a role-like behavior ("handle this type of data") of the 
        handling agent. The behavior is actually a fractional part of the use 
        cases that an agent implements from the user perspective. Therefore it 
        is named as "habit" following again some ideas of Coplien. 
        
        However, no DCI-like attempts are made to inject "roles into objects".       
    """

    def __init__(self):
        """Prepare the context dictionary."""
        self._context:dict = {}

    def add(self, usecase, topic, habit, qos=0,):
        """Adds or updates a topic-habits map for a given use case name."""
        if usecase is not None and topic is not None and callable(habit) is True:
            rule = {'topic': topic,
                    'qos': max(min(qos, MQTT_MAXQOS), MQTT_MINQOS),
                    'habit': habit, }
            self._context[usecase] = rule
        else:
            raise ValueError(
                "Either usecase or topic is None or func is not a callable.")

    @property
    def context(self) -> dict:
        """Property which returns the full context dictionary."""
        return self._context

    @property
    def habits(self) -> list:
        """Property which returns a list of (topic, habit) tuples."""
        return [(v['topic'], v['habit']) for v in self._context.values()]

    @property
    def subscriptions(self):
        """Property which returns a list of (topic, qos) tuples."""
        return [(v['topic'], v['qos']) for v in self._context.values()]
