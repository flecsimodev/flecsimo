#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""Web-Server of flecsimo-web-simulation tool.

Created on 19.10.2021

@author: Leon Schnieber
"""

LICENSE_TEXT = """
    Copyright and License Notice:

    flecsimo area control
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import uuid
from flask import request, redirect, url_for, jsonify

def flask_frontend(app, auth, sim):
    """
    This function groups all frontend-related endpoints together, mainly
    HTML-Templates that need to be rendered or small processing-routines
    required for user-logins.
    """
    render_template = auth.render_template_wrap

    @app.route("/")
    def root():
        session = request.cookies.get("flasksimo-session")
        if auth.session_check(session) == None:
            return redirect(url_for("login_start_screen"))
        else:
            return render_template("index.html")

    @app.route("/login", methods=["GET", "POST"])
    def login_start_screen():
        formdata = dict(request.form)
        if request.method == "POST" and "username" in formdata and "password" in formdata:
            login_status = auth.credentials_check(formdata["username"], formdata["password"])
            if login_status:
                response = redirect(url_for("root"))
                session_name = str(uuid.uuid4())
                if formdata["username"] not in auth.userdata:
                    auth.userdata[formdata["username"]] = {"sessions":[]}
                # add session-uuid to the database
                auth.userdata[formdata["username"]]["sessions"].append(session_name)
                response.set_cookie("flasksimo-session", session_name)
                return response
            else:
                return render_template(
                    "login.html",
                    message="You entered invalid credentials, please try again.")
        else:
            return render_template("login.html")

    @app.route("/view/simulation_setup")
    @auth.auth_request("instructor")
    def simulation_setup():
        return render_template(
            "simulation_setup.html",
            setups=sim.get_simulation_setup_list(),
            active_setup=sim.active_setup_name,
            units=sim.sim_cmpt
            )

    @app.route("/view/dashboard")
    @auth.auth_request("trainee")
    def simulation_dashboard():
        return render_template(
            "dashboard.html",
            active_setup=sim.active_setup_name,
            units=sim.sim_cmpt
            )
