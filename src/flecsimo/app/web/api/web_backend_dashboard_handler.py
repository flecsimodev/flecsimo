#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""Web-Server of flecsimo-web-simulation tool.

Created on 19.10.2021

@author: Leon Schnieber
"""
from datetime import datetime
from flask import request, redirect, url_for, jsonify, abort

from flecsimo.base.const import Orglevel
from flecsimo.base.states import OrderStates, ScheduleStates

LICENSE_TEXT = """
    Copyright and License Notice:

    flecsimo area control
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


def flask_backend_dashboard(app, auth, sim):

    def __get_cmpt_by_name_or_abort(cmpt_name):
        if cmpt_name in sim.sim_cmpt:
            cmpt = sim.sim_cmpt[cmpt_name]
            return cmpt
        else:
            abort(404)

    @app.route("/api/dashboard/site/<cmpt_name>/new_order", methods=["POST"])
    @auth.auth_request("trainee")
    def dashboard_site_create_new_order(cmpt_name):
        argsdata = dict(request.values)
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)

        data = cmpt["controller"].orderproc.new_order(
            site=argsdata["site"],
            material=argsdata["material"],
            qty=int(argsdata["qty"] if "qty" in argsdata and argsdata["qty"].isnumeric() else 1),
            unit=argsdata["unit"] if "unit" in argsdata else "PCE",
            variant=argsdata["variant"] if "variant" in argsdata and argsdata["variant"] != "" else None)
        return jsonify(data)

    @app.route("/api/dashboard/site/<cmpt_name>/material", methods=["GET"])
    @auth.auth_request("trainee")
    def dashboard_site_get_material_list(cmpt_name):
        argsdata = dict(request.values)
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)

        products = cmpt["controller"].orderproc.get_products()
        return jsonify(products)


    @app.route("/api/dashboard/site/<cmpt_name>/material/<material_name>/variant", methods=["GET"])
    @auth.auth_request("trainee")
    def dashboard_site_get_material_variant_list(cmpt_name, material_name):
        argsdata = dict(request.values)
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)

        variants = cmpt["controller"].orderproc.check_routing(material_name)
        return jsonify(variants)

    @app.route("/api/dashboard/site/<cmpt_name>/list_order", methods=["GET"])
    @auth.auth_request("trainee")
    def dashboard_site_get_order_list(cmpt_name):
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)

        data = cmpt["controller"].orderproc.show_orders()
        return jsonify(data)

    @app.route("/api/dashboard/site/<cmpt_name>/list_schedule", methods=["GET"])
    @auth.auth_request("trainee")
    def dashboard_site_get_order_schedule(cmpt_name):
        argsdata = dict(request.values)
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)
        order = argsdata["order"] if "order" in argsdata and len(argsdata["order"]) >= 1 else None

        data = cmpt["controller"].scheduler.get_schedule(
            site=cmpt["config"]["site"],
            order=order
            )
        return jsonify(data)

    @app.route("/api/dashboard/site/<cmpt_name>/order/<process_verb>", methods=["POST", "GET"])
    @auth.auth_request("trainee")
    def dashboard_site_process_order(cmpt_name, process_verb):
        argsdata = dict(request.values)
        if process_verb not in ["check", "plan", "release"]:
            abort(400)
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)

        controller = cmpt["controller"]
        order_id_list = [_["id"] for _ in controller.orderproc.show_orders()]

        if int(argsdata["order"]) not in order_id_list:
            abort(404)

        order = int(argsdata["order"])
        site = cmpt["config"]["site"]

        outstate = False
        # Please have a look at the do_plan_order and do_release_order-functions in site_control.py
        if process_verb == "check":
            if request.method == "GET":
                missing_types = [_["typ"] for _ in controller.orderproc.get_order_param(site=site, order=order)]
                params_all = controller.orderproc.get_order_param(site=site, order=order, mode="all")
                params = []
                for param in params_all:
                    param["missing"] = True
                    if param["typ"] not in missing_types:
                        param["missing"] = False
                    params.append(param)

                return jsonify(params)
            elif request.method == "POST":
                json_data = request.get_json(force=True)
                if "param" in json_data:
                    returncodes = []
                    for key, value in json_data["param"].items():
                        if value.strip() != "":
                            ret = controller.orderproc.set_order_param(site=site, order=order, param_typ=key, param_value=value)
                            returncodes.append(ret)
                    return_data = controller.orderproc.check_order(site=site, order=order)
                    return jsonify(return_data)
                else:
                    abort(400) # param parameter not set for updates
            else:
                abort(400)
        elif process_verb == "plan":  # follow the steps to plan an order
            # pds = controller.orderproc.get_pds(site, order)

            # controller.orderproc.update_order_state(site, order, OrderStates.CHECKED)
            sfcu_list = controller.orderproc.new_sfcu(site=site, order=order)
            controller.orderproc.new_opdta(site=site, order=order)

            due_date = argsdata["due"] if "due" in argsdata else datetime.now().strftime("%Y-%m-%d")
            for sfcu in sfcu_list:
                if "prio" in argsdata and argsdata["prio"].isnumeric():
                    prio = int(argsdata["prio"])
                else:
                    prio = 0
                controller.scheduler.new_schedule(
                    site=site,
                    order=order,
                    sfcu=sfcu,
                    mhu="NODATA",
                    operation="ALL",
                    supplier=argsdata["supplier"] if "supplier" in argsdata else "",
                    org=Orglevel.AREA.value,
                    due=due_date,
                    prio=prio,
                    state=ScheduleStates.PLANNED.value,
                    statx=ScheduleStates.PLANNED.name
                )
            outstate = controller.orderproc.update_order_state(site, order, OrderStates.PLANNED)
            if outstate:
                return jsonify(sfcu_list)
        elif process_verb == "release":
            asgmtlist = controller.scheduler.get_asgmtlist(site, site, order)
            if asgmtlist:
                # TODO: this is only true if we assign complete orders to an area
                supplier = asgmtlist[0].supplier

                opdta = controller.scheduler.new_opdta_msg(site, site, order, supplier)
                controller.mqclient.publish(opdta.topic, opdta.payload)

                for asgmt in asgmtlist:
                    controller.mqclient.publish(asgmt.topic, asgmt.payload)
                # Bulk update schedule status
                controller.scheduler.update_schedule_state(site, order, ScheduleStates.ASSIGNED)

                # Update order status
                outstate = controller.orderproc.update_order_state(site, order, OrderStates.RELEASED)
            else:
                abort(404)

        return jsonify(outstate)

    @app.route("/api/dashboard/cell/<cmpt_name>/get_status", methods=["GET"])
    # @app.route("/api/dashboard/site/<cmpt_name>/get_status", methods=["GET"])
    # @app.route("/api/dashboard/area/<cmpt_name>/get_status", methods=["GET"])
    # @app.route("/api/dashboard/module/<cmpt_name>/get_status", methods=["GET"])
    @auth.auth_request("trainee")
    def dashboard_module_get_status(cmpt_name):
        data = {}
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)
        data.update({"type": cmpt["type"],
                    "log": cmpt["log"]})
        # add additonal information according to cmpt type to the response
        if cmpt["type"] == Orglevel.CELL.value:
            data["status"] = cmpt["controller"].state.name
            data["progress"] = cmpt["controller"].get_progress()
            data["loaded_sfcu"] = cmpt["controller"].sfcu
        return jsonify(data)

    @app.route("/api/dashboard/cell/<cmpt_name>/list_sfcu", methods=["GET"])
    @auth.auth_request("trainee")
    def dashboard_cell_list_sfcu(cmpt_name):
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)
        controller = cmpt["controller"]
        # data.update(controller.get_expected_sfcu())
        data = controller.get_expected_sfcu()
        return jsonify(data)

    @app.route("/api/dashboard/cell/<cmpt_name>/activate", methods=["POST"])
    @auth.auth_request("trainee")
    def dashboard_cell_startup(cmpt_name):
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)

        # TODO: discuss if necessary, since state control is responsibility of state machine.
        controller = cmpt["controller"]
        if controller.state.name in ("STOPPED", "HALTED"):
            data = controller.init()
        else:
            abort(405)
        return jsonify([data])

    @app.route("/api/dashboard/cell/<cmpt_name>/shutdown", methods=["POST"])
    @auth.auth_request("trainee")
    def dashboard_cell_shutdown(cmpt_name):
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)
        data = cmpt["controller"].halt()
        return jsonify(data)

    @app.route("/api/dashboard/cell/<cmpt_name>/load", methods=["POST"])
    @auth.auth_request("trainee")
    def dashboard_cell_load_sfcu(cmpt_name):
        argsdata = dict(request.values)
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)
        controller = cmpt["controller"]
        try:
            sfcu_int = int(argsdata["sfcu"])
        except ValueError:
            abort(400)

        if controller.state.name == "STANDBY":
            data = controller.load(sfcu=sfcu_int)
            return jsonify(data)
        else:
            abort(405)

    @app.route("/api/dashboard/cell/<cmpt_name>/unload", methods=["POST"])
    @auth.auth_request("trainee")
    def dashboard_cell_unload_sfcu(cmpt_name):
        data = {}
        argsdata = dict(request.values)
        cmpt = __get_cmpt_by_name_or_abort(cmpt_name)
        controller = cmpt["controller"]

        try:
            sfcu_int = int(argsdata["sfcu"])
        except ValueError:
            abort(400)

        if controller.state.name == "DONE":
            data = controller.unload(sfcu=sfcu_int)
        else:
            abort(405)
        return jsonify(data)

