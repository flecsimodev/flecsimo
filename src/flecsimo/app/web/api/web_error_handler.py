#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""Web-Server of flecsimo-web-simulation tool.

Created on 19.10.2021

@author: Leon Schnieber
"""

LICENSE_TEXT = """
    Copyright and License Notice:

    flecsimo area control
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

def flask_error_pages(app, auth, sim):
    render_template = auth.render_template_wrap

    @app.errorhandler(404)
    def not_found_404(error):
        return render_template(
            "error_4xx.html",
            number=404,
            text="Page not found.",
            detailed=""
            ), 404

    @app.errorhandler(403)
    def forbidden_403(error):
        return render_template(
            "error_4xx.html",
            number=403,
            text="Forbidden.",
            detailed="The account you are logged in with is not \
            allowed to access this site"
            ), 403

    @app.errorhandler(401)
    def unauthorized_401(error):
        return render_template(
            "error_4xx.html",
            number=401,
            text="Unauthorized.",
            detailed="You don't have the permission to access \
            this site without being logged in and having the right permissions."
            ), 401
