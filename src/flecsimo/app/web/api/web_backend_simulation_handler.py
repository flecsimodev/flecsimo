#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""Web-Server of flecsimo-web-simulation tool.

Created on 19.10.2021

@author: Leon Schnieber

Copyright and License Notice:

    flecsimo area control
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from flask import request, jsonify, abort
import sqlite3


def flask_backend_simulation(app, auth, sim):

    @app.route("/api/simulation", methods=["GET"])
    @auth.auth_request("instructor")
    def get_simulation():
        units = {}
        for unit in sim.sim_cmpt:
            units[unit] = {
                "type": sim.sim_cmpt[unit]["type"],
                "config": sim.sim_cmpt[unit]["config"],
                "status": sim.sim_cmpt[unit]["status"]
            }
        data = {
            "setups": sim.get_simulation_setup_list(),
            "active_setup": sim.active_setup_name,
            "units": units
        }
        return jsonify(data)

    @app.route("/api/simulation/set_scene", methods=["POST"])
    @auth.auth_request("instructor")
    def set_simulation_scene():
        argsdata = dict(request.args)
        status = {"status": False, "message": ""}
        if "scene" in argsdata and sim.active_setup_name == "":
            setup_list = [x["value"] for x in sim.get_simulation_setup_list()]
            if argsdata["scene"].strip() in setup_list:
                try:
                    sim.active_setup_name = argsdata["scene"].strip()
                    status["status"] = sim.start_simulation()
                    status["message"] = "Startup complete."
                except ConnectionRefusedError as e:
                    status["status"] = False
                    status["message"] = "Error: " + str(e)
                    sim.active_setup_name = ""
                except sqlite3.DatabaseError as e:
                    status["status"] = False
                    status["message"] = "Error: " + str(e)
                    sim.active_setup_name = ""
        return jsonify(status)

    @app.route("/api/simulation/<unit>", methods=["DELETE"])
    @auth.auth_request("instructor")
    def kill_simulation_process(unit):
        if unit in sim.sim_cmpt:
            status = sim.sim_cmpt[unit]["controller"].stop_agent()
            return jsonify(status)
        else:
            abort(404)
            return

    @app.route("/api/simulation/<unit>/log", methods=["GET"])
    @auth.auth_request("instructor")
    # TODO: Implement frontend functionality
    def get_simulation_process_log(**kwargs):
        if "unit" in kwargs:
            unit = kwargs["unit"]
        else:
            return abort(404)
        if unit in sim.sim_cmpt:
            text = sim.sim_cmpt[unit]["log"]
        else:
            return abort(404)
        return jsonify(text)

    @app.route("/api/simulation/stop_scene", methods=["POST"])
    @auth.auth_request("instructor")
    def stop_simulation_scene():

        sim.active_setup_name = ""
        status = sim.stop_simulation()

        return jsonify(status)
