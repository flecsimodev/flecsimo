#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""Main Web-Server-File of flecsimo-web-simulation tool.

Created on 04.05.2021

@author: Leon Schnieber
"""
import argparse
from flask import Flask  # jsonify, request, url_for, redirect, abort
from gevent import pywsgi  # zur Auswahl lassen / define auswahl?
import logging
import werkzeug.serving

from flecsimo.base.auth import Auth
from flecsimo.cmpt.simulation import SimManager

from flecsimo.app.web.api.web_error_handler import flask_error_pages
from flecsimo.app.web.api.web_frontend_handler import flask_frontend
from flecsimo.app.web.api.web_backend_dashboard_handler import flask_backend_dashboard
from flecsimo.app.web.api.web_backend_simulation_handler import flask_backend_simulation

LICENSE_TEXT = """
    Copyright and License Notice:

    flecsimo twin webserver.
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


def run_server():

    """Main process for site control.
    
    Parse arguments, read configuration, assign database, instantiate SiteCmd 
    and start command loop.
    """
    parser = argparse.ArgumentParser(description='Development web server. '\
        'DO NOT USE FOR PRODUCTION!.')
    parser.add_argument('--profile',
                        default='sim',
                        help='Select configuration profile.')
    parser.add_argument('--webdebug',
                        action='store_true',
                        help='Start web server in debug mode.')
    parser.add_argument('--loglevel', help='Set log level as DEBUG, INFO, '\
                        'WANRING, ERROR or CRITICAL.')
    parser.add_argument('--setup',
                        default='simulation_setup.json',
                        help='Setup configuration file for simulation.')
    args = parser.parse_args()

    # TODO: discuss filtering and logging to file for framework logging.
    if args.loglevel:
        logging.basicConfig(level=args.loglevel)  # , stream=sys.stdout)

    print(f"Server configured with profile: {args.profile}.")

    app = Flask("flecsimo-twin", template_folder="www-template",
                static_folder="www-static")

    auth = Auth()
    
    # TODO: if required but not used elsewhere rename -> _render_template 
    render_template = auth.render_template_wrap

    sim = SimManager(root = __file__,
                     setup=args.setup,
                     profile=args.profile)

    # Import all "blueprints"
    # TODO: these are not really blue prints. Why?
    flask_error_pages(app, auth, sim)
    flask_frontend(app, auth, sim)
    flask_backend_dashboard(app, auth, sim)
    flask_backend_simulation(app, auth, sim)

    # ws = pywsgi.WSGIServer(('0.0.0.0', 5002), app)
    # ws.serve_forever()
    app.run("0.0.0.0", 5002, debug=args.webdebug)


if __name__ == "__main__":
    run_server()
