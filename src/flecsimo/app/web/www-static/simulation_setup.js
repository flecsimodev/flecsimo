
function set_simulation_scene() {
  $("#select_simulation_button")
    .attr("onclick", "")
    .attr("disabled", "disabled");
  var data = $("#select_simulation_scene").val();
  toast_show(toast_stencil("Starting…", ""));
  $.post("../api/simulation/set_scene?scene=" + data, function(ret) {
    if(ret["status"] == false) {
      modal_show(modal_stencil("startup failed.", ret["message"], color="bg-warning"));
    } else {
      toast_show(toast_stencil("Startup initiated.", "The bootup of the " + data + "-scene has been initiated.", color="bg-success"));
      $("#select_simulation_modal").html("");
      setTimeout(() => {
        window.location.reload(true);
      }, 50);
    }
  });
}

function stop_simulation_scene() {
  $("#select_simulation_button")
    .attr("disabled", "");
    modal_show(modal_stencil("Stopping scene…", "please leave this winwow opened until page is reloaded", color="bg-warning"));
  $.post("../api/simulation/stop_scene", function(ret) {
    setTimeout(() => {
      window.location.reload(true);
      $("#select_simulation_modal").html("");
    }, 500);
  });
}

function simulation_process_get_log(processName) {
  $.get("../api/simulation/" + processName + "/log", function(ret) {
    var logText = "<code>" + ret.join("<br />") + "</code>";
    var text = modal_stencil("log messages", logText);
    $("#flecsimo_modal_space").html(text);
    $("#flecsimo_modal_space .modal").modal("show")
  });
}

function simulation_process_kill(processName) {
  $.ajax({
    url: "../api/simulation/" + processName,
    type: "DELETE",
    success: function(ret) {
        simulation_process_refresh();
    }});
}

function simulation_process_refresh() {
  $.get("../api/simulation", function(ret) {
    Object.keys(ret["units"]).forEach(element => {
      var el = ret["units"][element];
      var match = "#flecsimo_process_table_" + element.split(".json")[0] + " .flecsimo_process_table_status";
      $(match).html(el["status"]);
      simulation_scene_dropdown_update();
    });
  });
}

function simulation_scene_dropdown_update() {
var current_val = $("#select_simulation_scene").val();
var description = $("#select_simulation_scene option[value='" + current_val + "']").attr("flecsimo-scene-description");
$("#flecsimo_select_simulation_scene_description").html(description);
}

$(function() {
  window.setInterval(simulation_process_refresh, 1000);
  simulation_scene_dropdown_update();
});