function logout() {
  document.cookie = "flasksimo-session=; Path=/; Expires=-1;";
  window.location = "/";
}

function modal_stencil(title, body, width="xl") {
  text = `
  <div class="modal fade">
    <div class="modal-dialog modal-${width}" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">${title}</h5>
          <button type="button" class="btn" onclick="$('#flecsimo_modal_space .modal').modal('hide')">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ${body}
        </div>
      </div>
  </div>
`
return text
}

function modal_show(modal_html) {
  $("#flecsimo_modal_space").html(modal_html)
  $("#flecsimo_modal_space .modal").modal("show");
}

function toast_stencil(title, body, color="bg-warning", time=undefined) {
  if(time == undefined) {
    time_obj = new Date();
    time = time_obj.getHours() + ":" + time_obj.getMinutes() + ":" + time_obj.getSeconds();
  }
  text = `
  <div class="toast mt-2" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="toast-header">
        <div class="${color} rounded me-2" style="width: 1rem; height: 1rem"></div>
        <strong class="me-auto">${title}</strong>
        <small>${time}</small>
        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>
      <div class="toast-body">
        ${body}
      </div>
    </div>
    `
  return text
}

function toast_show(toast_html) {
  // delete "expired" toasts
  $("#flecsimo_toast_space .toast.hide").remove();
  // add and show the new ones.
  var toastcount = $("#flecsimo_toast_space .toast").length;
  if( toastcount >= 3 ) {
    $("#flecsimo_toast_space .toast").last().remove();
  }

  $("#flecsimo_toast_space").append(toast_html);
  $("#flecsimo_toast_space .toast").toast("show");
}


// initialize the error handler for creating toasts and the "you are offline"-modal
$(function() {
  var timeoutErrorCounter = 0;
  var timeoutModal = false;

  $(document).ajaxError(function ajaxErrorHandler(e, xhr, options, err) {
    if (xhr["status"] == 0) {
      timeoutErrorCounter += 1;
      if(timeoutErrorCounter > 10 && timeoutModal == false) {
        var modal = modal_stencil("Connection error", `Please check your internet connection and reload the page.
        <br />
        <button onclick="window.location.reload();" class="btn btn-primary">reload</button>`);
        modal_show(modal);
        timeoutModal = true; // toggle so the modal isn't spawned multiple times.
      } else {
        var stencil = toast_stencil("Timeout", `Please check your internet connection or reload the page.
        If the error still occurs, please contact your administrator.`, "bg-danger");
        toast_show(stencil);
      }
    } else if (xhr["status"] >= 400 && xhr["status"] < 500) {
      var specific_text = xhr["responseText"];
      if(specific_text["status"]) {
        message_text = specific_text["status"]
      } else {
        message_text = "The action you started was errorneous/not allowed or resources you requested were not found.";
      }
      var stencil = toast_stencil("Input error", message_text, "bg-warning");
      toast_show(stencil);
      // client-side-error
    } else if (xhr["status"] >= 500 && xhr["status"] < 600) {
      var stencil = toast_stencil("Server error", "An error in the flecsimo-simulation has occured.", "bg-danger");
      toast_show(stencil);
      // server-side-error
    }
  });
});