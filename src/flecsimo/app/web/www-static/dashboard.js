window.global_blink_state = false;

function removeClassWildcard(index, className) {
  return (className.match (/(^|\s)bg-\S+/g) || []).join(' ');
}

function resolveUnitNameToDOM(unit_name) {
  var result = undefined;
  $(".flecsimo-process").each(function(idx) {
    var dom_object = $(this);
    var unit_name_el = dom_object.attr("flecsimoName");
    if(unit_name === unit_name_el) {
      result = dom_object;
    }
  });
  return result;
}

function translateStatusColor(status, site_name="", elid="") {
  var buttonhtml = "";
  var status_color = "";
  switch (status) {
    case 0: // initial
      status_color = "bg-secondary";
      buttonhtml = `<a class="btn btn-sm btn-secondary" onclick="dashboard_site_order_check('${site_name}','${elid}')">check</a>`;
      break;
    case 1: // checked
      status_color = "bg-secondary";
      buttonhtml = `<a class="btn btn-sm btn-secondary" onclick="dashboard_site_order_plan('${site_name}','${elid}')">plan</a>`;
      break;
    case 2: // planned
      status_color = "bg-info";
      buttonhtml = `<a class="btn btn-sm btn-secondary" onclick="dashboard_site_order_release('${site_name}', '${elid}')">release</a>`;
      break;
    case 3: // released
    case 4: // assigned?
    case 5: // accepted
      status_color = "bg-primary";
      break;
    case 6: // wip
      status_color = "bg-warning";
      break;
    case 7: // done
      status_color = "bg-success";
      break;
    case 8: // cancelled
    case 9: // failed
      status_color = "bg-danger";
      break;
    default:
      break;
  }
  return [status_color, buttonhtml];
}

function dashboard_site_update_order_list(site_name) {
  $.get("../api/dashboard/site/" + site_name + "/list_order", function(data) {
    dom_object = resolveUnitNameToDOM(site_name);
    dom_object.find("div.flecsimo-spinner").remove();
    var orderString = "";
    data.forEach(el => {
      var translatedList = translateStatusColor(el.state, site_name=site_name, elid=el.id);
      var status_color = translatedList[0];
      var buttonhtml = translatedList[1];

      // flecsimo-site-FUAS-tabbing-order-list
      var dom_el = $("div#flecsimo-site-" + site_name + "-tabbing-order-list code:contains('" + el.id + "')");
      if(dom_el.length > 0) {
        //only update
        dom_el.parent().parent().find("span.flecsimo_status")
          .removeClass(removeClassWildcard)
          .addClass(status_color)
          .html(el.statx);
        dom_el.parent().parent().find("span.flecsimo_variant").html(`
          ${el.variant}
        `);
        dom_el.parent().parent().find("span.flecsimo_qtyunit").html(`
          ${el.qty} ${el.unit}
        `);
        dom_el.parent().parent().find("div.flecsimo_action").html(`${buttonhtml}`);
      } else {
        orderString = `
      <tr class="flecsimo_table_row_fixed_height">
        <td>
          <code>${el.id}</code>
        </td>
        <td>
          <span class="flecsimo_status rounded-pill badge ${status_color}">${el.statx}</span>
        </td>
        <td><span class="flecsimo_variant">${el.variant}</span></td>
        <td><span class="flecsimo_qtyunit">${el.qty} ${el.unit}</span></td>
        <td>
        <div class="flecsimo_action">
          ${buttonhtml}
        </div>
        </td>
      </tr>
      `;

      dom_object.find("table.flecsimo-site-order-list tbody").prepend(orderString);
      }

    });

  });
}

function dashboard_site_update_schedule_list(site_name) {
  $.get("../api/dashboard/site/" + site_name + "/list_schedule?order=", function(data) {
    dom_object = resolveUnitNameToDOM(site_name);
    dom_object.find("div.flecsimo-spinner").remove();
    var orderString = "";
    data.forEach(el => {
      var translatedList = translateStatusColor(el.state, site_name=site_name, elid=el.id);
      var status_color = translatedList[0];
      var buttonhtml = translatedList[1];

      var dom_el = $("div#flecsimo-site-" + site_name + "-tabbing-schedule-list code:contains('" + el.id + "')");
        orderString += `
          <tr class="flecsimo_table_row_fixed_height">
            <td>
            <code class="flecsimo_id">${el.id}</code>
            </td>
            <td><i><code class="flecsimo_order" style="cursor: pointer" onclick="dashboard_site_highlight_order('${site_name}','${el.order}')">${el.order}</code></i></td>
            <td><code class="flecsimo_sfcu">${el.sfcu}</code></td>
            <td><span class="flecsimo_operation">${el.operation}</span></td>
            <td><span class="flecsimo_supplier">${el.supplier}</span></td>
            <td><span class="flecsimo_due">${el.due}</span></td>
            <td><span class="flecsimo_prio">${el.prio}</span></td>
            <td>
            <span class="flecsimo_status rounded-pill badge bg-primary ${status_color}">${el.statx}</span>
            </td>
            <td></td>
          </tr>`
    });
    dom_object.find("table.flecsimo-site-schedule-list tbody").html(orderString);
  });
}

function dashboard_site_new_order_update_materials(card_id) {
    $.get("../api/dashboard/site/" + card_id + "/material", function(ret) {
        var current_value = $("#flecsimo_" + card_id + "_dashboard_site_new_order_material").val();
        var html_gen = ret.map(function(el) {
		  return '<option value="' + el.id + '" flecsimo-field-description="' + el.desc + '" flecsimo-field-unit="' + el.unit + '">' + el.id + '</option>';
		}).join('');
        $("#flecsimo_" + card_id + "_dashboard_site_new_order_material")
            .html(html_gen)
            .val(current_value);

        var description = $("#flecsimo_" + card_id + "_dashboard_site_new_order_material option[value='" + current_value + "']").attr("flecsimo-field-description");
        var unit = $("#flecsimo_" + card_id + "_dashboard_site_new_order_material option[value='" + current_value + "']").attr("flecsimo-field-unit");

        $("#flecsimo_" + card_id + "_dashboard_site_new_order_material_description").html(description);
        $("#flecsimo_" + card_id + "_dashboard_site_new_order_count_unit").html(unit);
    });

	var current_material_name = $("#flecsimo_" + card_id + "_dashboard_site_new_order_material").val();
	if(current_material_name != null) {
	    $.get("../api/dashboard/site/" + card_id + "/material/" + current_material_name + "/variant", function(ret) {
	        // update max_lotsize
            $("#flecsimo_" + card_id + "_dashboard_site_new_order_count").attr("max", ret.max_lotsize);

	        var current_value = $("#flecsimo_" + card_id + "_dashboard_site_new_order_variant").val();
	        var html_gen = ret.variants.map(function(el) {
			  return '<option value="' + el.value + '">' + el.variant + '</option>';
			}).join('');
	        $("#flecsimo_" + card_id + "_dashboard_site_new_order_variant")
	            .html(html_gen)
	            .val(current_value);
	    });
	}
}

function dashboard_site_new_order(card_id, card_name) {
  var bodytext = `
  <div class="row mb-2">
      <label for="flecsimo_${card_id}_dashboard_site_new_order_material">material</label>
      <div class="input-group">
        <select
            id="flecsimo_${card_id}_dashboard_site_new_order_material"
            class="form-select"
            type="select" class="form-control"
            onchange="dashboard_site_new_order_update_materials('${card_id}')"
        ></select>
      </div>
      <div id="flecsimo_${card_id}_dashboard_site_new_order_material_description" class="form-text"></div>
  </div>
  <div class="row">
      <label for="flecsimo_${card_id}_dashboard_site_new_order_count">count</label>
      <div class="input-group">
        <input id="flecsimo_${card_id}_dashboard_site_new_order_count" class="form-control" type="number" value="0" min="0" max="99" >
        </input>
        <span id="flecsimo_${card_id}_dashboard_site_new_order_count_unit" class="input-group-text">---</span>
      </div>
  </div>
  <div class="row">
      <label for="flecsimo_${card_id}_dashboard_site_new_order_variant">variant</label>
      <div class="input-group">
        <select id="flecsimo_${card_id}_dashboard_site_new_order_variant" class="form-select" type="select" class="form-control">
        </select>

      </div>
  </div>
  <div class="mt-2 row">
    <div class="col">
      <button
        class="btn btn-primary float-end"
        onclick="dashboard_site_new_order_request('${card_id}', '${card_name}')"
      >create order</button>
    </div>
  </div>
  `

  var modal_title = `
  create new order for
  <span class="badge bg-secondary">
    Site
  </span>
  <b>
    ${card_name}
  </b>
  `
  var text = modal_stencil(modal_title, bodytext, width="md");
  modal_show(text);
  dashboard_site_new_order_update_materials(card_id);
}

function dashboard_site_new_order_request(card_id, site) {
  var material =  $("#flecsimo_" + card_id + "_dashboard_site_new_order_material").val();
  var qty = $("#flecsimo_" + card_id + "_dashboard_site_new_order_count").val();
  var unit = $("#flecsimo_" + card_id + "_dashboard_site_new_order_count_unit").val();
  var variant = $("#flecsimo_" + card_id + "_dashboard_site_new_order_variant").val();
  var data = {
    "material": material,
    "qty": qty,
    "unit": unit,
    "site": site,
    "variant": variant
  }
  $("#flecsimo_modal_space").html(modal_stencil("loading…", "loading…", width="md"));
  $.post("../api/dashboard/site/" + card_id + "/new_order", data=data, function(ret) {
    // remove the input-space
    $("#flecsimo_modal_space .modal").modal("hide");
    $("#flecsimo_modal_space").html("");
    $(".modal-backdrop.fade.show").remove();

	// create a toast with the creation status
	var state = ret[0] // 0 = everything went fine, 2 = baseException
	var toast_color = (state == 0) ? "bg-success" : "bg-error";
	toast_show(toast_stencil("New order processed.", ret[3], color=toast_color));
  });
}

function dashboard_site_order_check(site_name, order) {
	$.get("../api/dashboard/site/" + site_name + "/order/check", data={
        "order": order
    }, function(data) {

		var modal_title = `check order ${order} details`;
        var modal_content = "";

        data.forEach(el => {
            if(el.missing) {
	            modal_content += `
				<div class="row">
					<label for="flecsimo_dashboard_site_check_order_param_field_${el.typ}">${el.typ}</label>
					<div class="input-group">
						<input
						id="flecsimo_dashboard_site_check_order_param_field_${el.typ}"
						class="form-input form-control flecsimo_dashboard_site_check_order_param"
						flecsimo-order-param-name="${el.typ}"
						type="text"></input>
					</div>
				</div>
				`
			} else {
				modal_content += `
				<div class="row">
					<label>${el.typ}</label>
					<div class="input-group">
						<input disabled class="form-input form-control is-valid disabled" type="text"></input>
					</div>
				</div>
				`
			}
        });

		modal_content += `
		<div class="mt-2 row">
			<div class="col">
				<button
				class="btn btn-primary float-end"
				onclick="dashboard_site_order_check_post_data('${site_name}', '${order}')"
				>check order</button>
			</div>
		</div>
		`;
		$("#flecsimo_modal_space").html(modal_stencil(modal_title, modal_content, width="md"));
		$("#flecsimo_modal_space .modal").modal("show");
    });
}

function dashboard_site_order_check_post_data(site_name, order) {
	var retval = {};
	$(".flecsimo_dashboard_site_check_order_param").each(function() {
		var key = $(this).attr("flecsimo-order-param-name");
		var val = $(this).val();
		retval[key] = val;
	});
	$.ajax({url: "../api/dashboard/site/" + site_name + "/order/check?order=" + order, type: "POST",
        dataType: "json", contentType: "application/json",
        data: JSON.stringify({"param": retval}),
        success: function(data) {
            if(data.return_code == 0) {
                toast_show(toast_stencil("Order check successful", data.msg, color="bg-success"));
            } else {
                toast_show(toast_stencil("Order check incomplete", data.msg, color="bg-warning"))
            }
        },
        error: function(data) {
            console.log("E", data);
            toast_show(toast_stencil("Order check", "Error when checking. Please have a look inside of the logs.", color="bg-warning"));
        }
    });

    $("#flecsimo_modal_space .modal").modal("hide");
    $("#flecsimo_modal_space").html("");
    $(".modal-backdrop.fade.show").remove();
}

function dashboard_site_order_plan(site_name, order) {
	var area_options = "";
	$(".flecsimo-process.flecsimo-area").each(function(idx) {
		var area_name = $(this).find("h5").html().split("/span>")[1].trim();
		area_options += `<option value="${area_name}">${area_name}</option>`;
	});

	var today = new Date().toJSON().slice(0, 10);
	var modal_title = `plan order ${order} details`;
	var modal_content = `
	<div class="row">
		<label for="flecsimo_dashboard_site_supplier">supplier</label>
		<div class="input-group">
		<select id="flecsimo_dashboard_site_supplier" class="form-select form-control" type="select">
			${area_options}
		</select>
		</div>
	</div>
	<div class="row">
		<label for="flecsimo_dashboard_site_due_date">due date</label>
		<div class="input-group">
			<input id="flecsimo_dashboard_site_due_date" class="form-input" type="date"  value="${today}" min="${today}"></input>
		</div>
	</div>
	<div class="mt-2 row">
	<div class="col">
	    <button
		    class="btn btn-primary float-end"
		    onclick="dashboard_site_order_plan_after_querying_supplier('${site_name}', '${order}')"
	    >plan order</button>
	</div>
	</div>`;
	$("#flecsimo_modal_space").html(modal_stencil(modal_title, modal_content, width="md"));
	$("#flecsimo_modal_space .modal").modal("show");
}

function dashboard_site_order_plan_after_querying_supplier(site_name, order) {
    var supplier = $("#flecsimo_dashboard_site_supplier").val();
    var dueDate = $("#flecsimo_dashboard_site_due_date").val();
    $("#flecsimo_modal_space").html(modal_stencil("loading…", "loading…", width="md"));

    $.post("../api/dashboard/site/" + site_name + "/order/plan", data={
        "order": order,
        "supplier": supplier,
        "due": dueDate
    }, function(data) {
        $("#flecsimo_modal_space .modal").modal("hide");
        $("#flecsimo_modal_space").html("");
        $(".modal-backdrop.fade.show").remove();
    });
}

function dashboard_site_order_release(site_name, order) {
  $.post("../api/dashboard/site/" + site_name + "/order/release", data={"order": order}, function(data) {});
}
function dashboard_cell_update_sfcu_list(cell_name) {
    $.ajaxSetup({
        timeout: 1000
    });
    $.get("../api/dashboard/cell/" + cell_name + "/list_sfcu", function(data) {
        dom_object = resolveUnitNameToDOM(cell_name);
        var outString = "<option value=''>choose…</option>";
        if(dom_object.find("option").length != data.length +1 ) {
			data.forEach(el => {
			    outString += `<option value="${el}">${el}</option>`;
			});
			dom_object.find("select").html(outString);
			dom_object.find(".flecsimo-cell-sfcu-select-box-countbadge span.badge")
			.html(data.length)
			.show();
        }
    });
}

function dashboard_cell_update_status(cell_name) {
  $.ajaxSetup({
    timeout:1000
  });

  $.get("../api/dashboard/cell/" + cell_name + "/get_status", function(data) {
    dom_object = resolveUnitNameToDOM(cell_name);
    var htmlText = "<span>" + data["log"].join("</span><span>");
    var obj = dom_object.find("pre.flecsimo-log")
      .html(htmlText);
    obj.animate({
      scrollTop: $(obj).prop("scrollHeight")+1000
    }, 100);

    // cell-specific status-update
	if("status" in data) {
		$(dom_object)
			.find(".flecsimo-status-group")
			.attr("flecsimo-state", data["status"]);
	}

    // Do not display the SFCU if the cells status is in standby (because the SFCU is only
    // loaded in the "setup", "active" and "done")
    if("loaded_sfcu" in data && data["loaded_sfcu"] != 0 && data["status"] != "STANDBY") {
      $(dom_object).find(".flecsimo-cell-sfcu-pos").val(data["loaded_sfcu"]);
    } else {
      $(dom_object).find(".flecsimo-cell-sfcu-pos").val("no SFCU loaded");
    }

    if("progress" in data && "section" in data["progress"]){
      progress_obj = $(dom_object).find(".flecsimo-cell-task-progressbox");
      $(progress_obj).find(".flecsimo-cell-task-progress-placeholder").hide();

      var section = data["progress"]["section"];
      var percent = data["progress"]["elapsed"] / data["progress"]["duration"] * 100;

      obj = $(progress_obj).find(".flecsimo-cell-task-progress-" + "section");
      if(obj.length == 0) { // is this subtask not created yet?
        // if so, add the task to the cell
        $(progress_obj).append(`
        <div class="row mb flecsimo-cell-task-progress flecsimo-cell-task-progress-section">
          <label class="col-sm-4 col-form-label pt-0 pb-0">current operation</label>
          <div class="col-sm-8">
              <div class="progress align-middle">
                  <div
                      class="progress-bar progress-bar-striped progress-bar-animated bg-primary flecsimo-bar flecsimo-bar-section"
                      role="progressbar" style="width: ${percent}%">
                  </div>
              </div>
          </div>
        </div>
        `);
      } else {
        // otherwisse update the progressbar-width and it's description (optionally)
        progress_obj.find(`.flecsimo-bar-section`)
          .width(percent + "%")
          .html(section + ":  " + data["progress"]["elapsed"] + "s / " + data["progress"]["duration"] + "s");
        progress_obj.find(`.flecsimo-bar-section`)
          .addClass("progress-bar-animated")
          .addClass("progress-bar-striped");
      }
    } else {
        progress_obj = $(dom_object).find(".flecsimo-cell-task-progressbox");
        $(progress_obj).find(".flecsimo-cell-task-progress-placeholder").show();
        $(progress_obj).find(".flecsimo-cell-task-progress").remove();
    }
  });
}

function dashboard_cell_activate(cell_name) {
  $.post("../api/dashboard/cell/" + cell_name + "/activate", function (data) {
    dashboard_cell_update_status(cell_name);
  });
}

function dashboard_cell_shutdown(cell_name) {
  $.post("../api/dashboard/cell/" + cell_name + "/shutdown", function (data) {
    dashboard_cell_update_status(cell_name);
  });
}

function dashboard_cell_load_sfcu(cell_name) {
  $(this).attr("disabled", "disabled");
  var sfcu = $("#flecsimo-cell-" + cell_name + "-sfcu-box").val();
  $.post("../api/dashboard/cell/" + cell_name + "/load", data={"sfcu": sfcu}, function (data) {
    dashboard_cell_update_status(cell_name);
    dashboard_cell_update_sfcu_list(cell_name);
  });
}

function dashboard_cell_unload_sfcu(cell_name) {
  $(this).attr("disabled", "disabled");
  var sfcu = $("#flecsimo-cell-" + cell_name + "-sfcu-status-box").val();
  $.post("../api/dashboard/cell/" + cell_name + "/unload", data={"sfcu": sfcu}, function (data) {
    dashboard_cell_update_status(cell_name);
  });
}

function dashboard_site_highlight_order(card_id, order_id) {
  // show the "orders"-tab
  var tabElement = document.querySelector('#flecsimo-site-' + card_id + '-tabbing-order-list-tab')
  var tab = new bootstrap.Tab(tabElement);
  tab.show();
  $("table.flecsimo-site-order-list tbody tr").each( function(idx) {
    var id = $(this).find("code").html();
    if(id == order_id) {
      $(this)[0].scrollIntoView();
      $(this)
        .css("border", "2px solid red")
        setTimeout(function() {
					$(this).css("border", "");
				}.bind(this), 3000);
    }
  });
}

// Setup interval-based queries
////////////////////////////////
function fetch_dashboard() {
  // iterate through all sites
  $(".flecsimo-process.flecsimo-site").each(function(idx) {
    var dom_object = $(this);
    var cell_name = dom_object.attr("flecsimoName");

    dashboard_site_update_schedule_list(cell_name);
    dashboard_site_update_order_list(cell_name);
  });

  // iterate through all areas
  // (not applicable at the moment)

  // iterate through all cells
  $(".flecsimo-process.flecsimo-cell").each(function(idx) {
    var dom_object = $(this);
    var cell_name = dom_object.attr("flecsimoName");

    dashboard_cell_update_sfcu_list(cell_name);
    dashboard_cell_update_status(cell_name);
  });

  // iterate through all units/processes
  $(".flecsimo-process").each(function(idx) {
  })
}


function update_cell_statuslight() {
	window.global_blink_state = !window.global_blink_state;

	$(".flecsimo-status-group[flecsimo-state]").each(function() {
		status_attr = $(this).attr("flecsimo-state");
		$(this)
		.find(".flecsimo-status-btn")
		.each(function() {
			var t = $(this);
			var state_list = t.attr("flecsimo-names").replace("_", "").split(";").indexOf(status_attr);
			if (state_list >= 0) {
				t.removeClass("fst-italic").removeClass("fw-normal");
				t.html("<span>" + status_attr + "</span>");

				var blinking = t.attr("flecsimo-names").split(";")[state_list].indexOf("_") !== -1;
				// for "on"-phase of all normal and  blinking states
				if(!blinking || (blinking && window.global_blink_state)) {
					t.removeClass("bg-secondary");
					t.addClass("bg-" + t.attr("flecsimo-color").replace("_", ""));
					// addition for incomplete bootstrap-night-theme: use the color-variable directly instead of using the class itself
					t.attr("style", "background-color: var(--bs-" + t.attr("flecsimo-color") + ")");
				} else { // for "off"-phase of blinking states
					t.removeClass("bg-" + t.attr("flecsimo-color").replace("_", ""));
					t.addClass("bg-secondary");
					t.attr("style", "background-color: var(--bs-secondary)");
				}

			} else { // for "off/inactive state" in general (blinking and non-blinking states)
				t.removeClass("bg-" + t.attr("flecsimo-color").replace("_", ""));
				t.addClass("bg-secondary");
				t.attr("style", "background-color: var(--bs-secondary)");
				t.addClass("fst-italic").addClass("fw-normal");
				if(t.attr("flecsimo-inactive-text") != "") {
					t.html("<span>" + t.attr("flecsimo-inactive-text") + "</span>");
				} else {
					t.html("<span>&nbsp;</span>");
				}
			}
		});
	});
}


$(function() {
    var dashboard_interval = window.setInterval(fetch_dashboard, 2000);
    var cell_status_interval = window.setInterval(update_cell_statuslight, 750);
});