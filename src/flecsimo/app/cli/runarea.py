#!/usr/bin/env python3
"""Run flecsimo area with CLI interface.

This module provides a wrapper for the area_context which implements the 
interactions of the shop floor control processing use cases.

Architectural note: 
    This component may be seen as a replacement for a "GUI", and acts mainly 
    for the controller-part. The "AreaAgent" and "AreaControl" objects follow
    the ideas of Reenskaug and Coplien, to factor out use case algorithms and 
    business logic from pure domain data object (the database in this case) 
    in to abstract role models which are mixed into the data objects at runtime.

Created on 30.05.2020

@author: Ralf Banning
"""
import argparse
import cmd
import logging
import sys
# from flecsimo.app import cli  # use for arg parsing if required.
from flecsimo.base import config
from flecsimo.cmpt.areaagent import AreaAgent
from flecsimo.cmpt.areacontrol import AreaControl

LICENSE_TEXT = """
    Copyright and License Notice:

    flecsimo area (with CLI interface)
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


class AreaCmd(cmd.Cmd):
    """Command wrapper for area control."""

    intro = """
    *************************************************************************
    * Welcome to the flecsimo area controller.                              *
    *                                                                       *
    * Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt       * 
    * University of Applied Sciences.                                       *
    *                                                                       *
    * This program comes with ABSOLUTELY NO WARRANTY                        *
    *                                                                       *
    * Type help or ? to list commands.                                      *
    *                                                                       *
    *************************************************************************
    """

    def __init__(self, conf):
        super().__init__(completekey='TAB')
        self.conf = conf

    def preloop(self):
        # Read configuration
        self.site = self.conf['site']
        self.area = self.conf['area']
        self.prompt = 'Area:{}>'.format(self.area)

        # Start mqtt connection
        print(f"Connecting {self.area} are to mqtt-server "
              f"at {self.conf['mqserver']}...")

        try:
            # Instantiate agent and control context objects
            self.agent = AreaAgent(conf=self.conf)
            self.control = AreaControl(conf=self.conf)

            # Start edge nodes
            self.agent.start()
            self.control.start()

            ######### For threading analysis only ##############################
            # print(f"Agent thread id: {self.agent.mqclient._thread.ident}")
            # print(f"control thread id: {self.control.mqclient._thread.ident}")
            ####################################################################

        except ConnectionRefusedError as e:
            print(e, "\nArea nodes will be stopped.")
            self.close()

    def do_enrol(self, arg):
        # TODO: there is no order_context element - something is wrong about this!
        topic = None
        payload = None

        self.order_context.enrol(topic, payload)

    def do_quote(self, arg):
        """Quote for a received site rfq."""
        pass

    def do_request_quote(self, arg):
        """Request a quotation for an sfcu/operation from cells."""
        pass

    def do_release_sfcu(self, arg):
        """Assign an sfcu to cell and send opdta"""
        pass

    def do_quit(self, arg):
        # FIXME: there is no order_context element - something is wrong about this!
        topic = None
        payload = None

        self.order_context.quit(topic, payload)

    def do_bye(self, arg):
        """ Quit the application."""
        print(f"Thank you for using area: {self.area}")
        self.close()
        return True

    def close(self):
        # Stop the mqtt connection.
        print("Stopping all nodes ...")
        self.control.stop()
        self.agent.stop()


def main():
    """Main process for area control.
    
    Parse arguments, read configuration, assign database, instantiate AreaCmd 
    and start command loop.
    """
    parser = argparse.ArgumentParser(description='Wrapper for area ' + \
                                     'order_context. Implements the ' + \
                                     'interactions of the sfc processing ' + \
                                     'use cases.')
    parser.add_argument('-c', '--config',
                        default='area.json',
                        help='Load configuration from file')
    parser.add_argument('--profile',
                        default='sim',
                        help='Select use case configuration.')
    parser.add_argument('--loglevel', help='Set log level as DEBUG, INFO, ' + \
                        'WANRING, ERROR or CRITICAL.')
    parser.add_argument('--license',
                        action='store_true',
                        help='Show license and warranty disclaimer.')
    args = parser.parse_args()

    if args.license:
        print(LICENSE_TEXT)

    if args.loglevel:
        logging.basicConfig(level=args.loglevel, stream=sys.stdout)

    # Get configuration data.
    configurator = config.Configurator(__file__)
    confdata = configurator.get_config(args.config, args.profile)

    # Set or create databases.
    if confdata['data'] == 'sim':
        from flecsimo.util import dbmanager
        database = configurator.get_db_path(confdata['database'])
        dbs = dbmanager.DbSetUp(database)
        dbs.create_schema('area')
        dbs.create_simdata('area', 'AREA-1')
    
    confdata['database'] = configurator.get_db_path(database)

    # Start command loop
    AreaCmd(confdata).cmdloop()


if __name__ == '__main__':

    main()
