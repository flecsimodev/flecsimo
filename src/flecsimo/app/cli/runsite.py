#!/usr/bin/env python3
"""Command interpreter for controlling site processes

This module provides a wrapper for the site_context which implements the 
interactions of the order processing use cases.

Architectural note: 
    This component may be seen as a replacement for a "GUI", and acts mainly 
    for the controller-part. The "SiteAgent" and "SiteControl" objects follow
    the ideas of Reenskaug and Coplien, to factor out use case algorithms and 
    business logic from pure domain data object (the database in this case) 
    in to abstract role models which are mixed into the data objects at runtime.
    
    TODO: review preceding paragraph 

Created on 25.05.2020

@author: Ralf Banning
"""

import argparse
import cmd
import logging
import sys
from flecsimo.app import cli
from flecsimo.base import config
from flecsimo.base.const import Orglevel
from flecsimo.base.states import OrderStates, ScheduleStates
from flecsimo.cmpt.siteagent import SiteAgent
from flecsimo.cmpt.sitecontrol import SiteControl
# from memory_profiler import profile  # for dev purposes only

LICENSE_TEXT = """ 
    Copyright and License Notice:

    flecsimo runsite with command line interface (cli).
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


class SiteCmd(cmd.Cmd):
    """Command wrapper for site control workflow."""

    intro = """
    *************************************************************************
    * Welcome to the flecsimo site controller.                              *
    *                                                                       *
    * Copyright (C) 2020  Ralf Banning, Bernhard Lehner and                 * 
    * Frankfurt University of Applied Sciences.                             *
    *                                                                       *
    * This program comes with ABSOLUTELY NO WARRANTY                        *
    *                                                                       *
    * Type help or ? to list commands.                                      *
    *                                                                       *
    *************************************************************************
    """

    def __init__(self, conf):
        super().__init__(completekey='TAB')

        self.conf = conf
        self.site = self.conf['site']
        self.prompt = f"Site:{self.site}>"

    # @profile(precision=3)
    def preloop(self):
        """Setup mqtt connection and site habits on startup of command loop."""

        # Start mqtt connection
        print(f"Connecting {self.site} site to mqtt-server "
              f"at {self.conf['mqserver']}...")

        try:
            # Create agent and control objects
            self.agent = SiteAgent(conf=self.conf)
            self.control = SiteControl(conf=self.conf)

            # Start edge nodes
            self.agent.start()
            self.control.start()

        except ConnectionRefusedError as e:
            print(e, "\nSite nodes will be stopped.")
            self.close()

    def do_new_order(self, arg):
        """Create an new order as guided dialog.
        
        This command will do the same as create order but offers a dialog 
        guided data capture, whereas create_order is an 'all-in-once' command.
        
        Args: None.
        """
        materials_list = self.control.orderproc.get_products()
        
        # Modal dialog 1: select material from list
        print("... Materials which can be ordered (type 'Q' to quit:)")
        i = 0
        for _ in materials_list:
            print(f"    Nr. {i} will select: {_['id']}: {_['desc']}.")
            i += 1
        print("    -----------------------")
            
        material = None
        while material is None: 
            try:
                answer = input("    Select a number: ")
                if answer == "Q":
                    print("    Process aborted by user.")
                    return
                
                material = materials_list[int(answer)]
            except:
                print("    Your selection was not valid. Choose again.")
        
        print(f"    Vou have selected: {material['id']}: {material['desc']}.")
        unit = material['unit']
        print(f"    Info: unit of measure is set to: {material['unit']}.\n")
        
        # Check routing for selected material and retrieve routing report.
        routing_report = self.control.orderproc.check_routing(material['id'])
        max_qty = routing_report['max_lotsize']

        # Modal dialog 2: capture rest of data based on routing report
        # a) Set order quantity.
        print(f"... Order quantity has to be set in range 1..{max_qty}:")
        print("    -----------------------")        
       
        qty = 0
        while qty == 0:
            try:
                answer = input(f"    enter quantity:")
                if int(answer) <= max_qty:
                    qty = int(answer)
                elif answer == 'Q':
                    print("    Process aborted by user.")
                    return
            except:
                print("    Your answer is not a number or out of range. Repeat.")
                
        print(f"    Order quantity is set to: {qty}.\n")
        
        # b) Select variant of material from list(if required).
        
        if routing_report['variants'] != []:
            print("... Possible variants for this material:")
            i = 0
            for _ in routing_report['variants']:
                print(f"    Nr. {i} will select: {_['variant']}.")
                i += 1
            print("    -----------------------")
                
            variant = None
            while variant is None: 
                try:
                    answer = input("    choose a number: ")
                    
                    if answer == "Q":
                        print("    Process aborted by user.")
                        return                        
                    variant = routing_report['variants'][int(answer)]
                except:
                    print("    Your selection was not valid. Choose again.")
            
            print(f"    You have selected variant: {variant['variant']}.")
        else:
            variant = {'variant': None}
                
        # Create new order.
        report =self.control.orderproc.new_order(self.site,
                                                 material['id'],
                                                 qty,
                                                 unit,
                                                 variant['variant'])
        
        print(f"{report['msg']}, Return code: {report['return_code']}")
        

    def do_create_order(self, arg):
        """Create a new order in one.
        
        Manually create the minimum data for a flecsimo order.
               
        Args: 
            material (str):    Material number.
            qty (float):       Total quantity of products (default=1.0).
            variant (str):     Variant code (default=None).
            
        Example:
            create_order(material=FXF-1100, qty=1.0, variant=col:red)
        """
        # 0. Parse argumants.
        try:
            arg = cli.parse(arg, required=True)
            material = arg['material']
            qty = int(arg['qty'])
        except (TypeError, ValueError, KeyError) as e:
            print(type(e), e)
            return
        
        try:
            variant = arg['variant']
        except (TypeError, ValueError) as e:
            print(type(e), e)
            return
        except KeyError:
            # Variant is not mandatory. Set default if misssing
            variant = None
            
        # Check if material is a known material
        materials_list = self.control.orderproc.get_products()
        this_material = [_ for _ in materials_list if _['id'] == material]
        
        if this_material == []:
            print(f"    ERROR: material {material} not found in database.")
            return
        else:
            unit = this_material[0]['unit']
            material = this_material[0]['id']
           
        # Check routing data and retrieve report.
        routing_report = self.control.orderproc.check_routing(material)
        return_code = routing_report['return_code']       
        max_qty = int(routing_report['max_lotsize'])

        # Check number of order items is in limits.        
        if qty >= max_qty:
            print(f"    ERROR: {qty} order items requested ({max_qty} allowed).")
            return
        
        # Check variant is known
        known_variants = [_['variant'] for _ in routing_report['variants']]
        if variant not in known_variants:
            print(f"    ERROR: material {material} has no variant {variant}.")
            return

        # Print routing report (may still contain pds errors).
        print(routing_report['msg'])

        # Try to create order 
        if return_code == 0: 
            order_report = self.control.orderproc.new_order(site=self.site,
                                                            material=material,
                                                            qty=qty,
                                                            unit=unit,
                                                            variant=variant)
            if order_report['return_code'] == 0:
                print(f"    Order {order_report['order']} was created successfully.")
            else:
                print("    ERROR: order creation failed")
                print(order_report['msg'])
                
        else:
            print("    Order was not created due to errors in routing.")

    def do_check_order(self, arg):
        """Check the order data before release.
        
        Args:
            order(str): order number to be checked.
        """
        # 1. Parse args .
        try:
            arg = cli.parse(arg, required=True)
            order = arg['order']
            print("order: ", order)
        except (TypeError, ValueError, KeyError) as e:
            print(type(e), e)
            return
        
        order_report = self.control.orderproc.check_order(self.site, order)
        
        return_code = order_report['return_code']
        message = order_report['msg']
                
        if return_code == 1:
            print(f"{order_report['msg']}:\n")
            
            given_params = []
            open_params = order_report['params']
            for _ in open_params:
                value = input(f"    Enter value for parameter {_['typ']}: ")
                given_params.append({'typ': _['typ'], 'value': value})
                
            print("    You have set parameters: ")
            [print(f"{_['typ']}: {_['value']}") for _ in given_params]
            
            answer = input("    Is this ok? Then press 'C', else 'Q'.")
            if answer == 'C':
                for _ in given_params:
                    self.control.orderproc.set_order_param(self.site,
                                                           order,
                                                           _['typ'],
                                                           _['value'])
                re_check = self.control.orderproc.check_order(self.site, order)
                return_code = re_check['return_code']
                message = re_check['msg']
            else:
                print("Setting parameters aborted.")
                
        if return_code == 0:
            print(message)           
        else:
            print("ERROR: ", message)
    
    def do_request_quote(self, arg):
        """Request a quote for a given oder.
        
        Pre-Conditions:
        1. There is at least one facility (area) active. -or-
        2. RFQ Mode was set globally.
        
        TODO: implement."""

    def do_pick_quote(self, arg):
        """Pick best quote.
        
        Pre-Condition: RFQ for order was sent out and response time is elapsed.
        
        Base sequence:
            1. Display the current state of received quotations.
            2. Analyze quotes versus demand requirements (esp. due date)
            3. Select best quote.
            4. Close RFQ.
        
        Alternative Sequences:
            2.A [non sufficient quotes]
                2.A.1 Close RFQ
                2.A.2 Inform orderer
                2.A.3 End sequence.
 
        TODO: implement."""

    def do_plan(self, arg):
        """Plan execution of an order in the flecsimo systems.
                   
        Args:
            order (int):    order number.
            supplier (str): dedicated supplier for task (area-code).
            
        Example:
            plan order=1000020, supplier=area1
            
        Pre-Conditions: 
            1. List of selected suppliers per order is known
            2. RFQ is closed for the order when RFQ mode is set globally.
        
        Base sequence:
            1. Create the sfcus from order and update order status.
            2. Create the order operation data (opdta) and update order status.
            3. Ask for order due date.
            4. Create demands in schedule per sfcu (demand supply matching).
            5. Update order status
            
        TODO: Needs redesign when real planing algorithms are defined.
        TODO: think of multi-area assignments (pre-decided here!) 
        """
        # 0. Initialize by command arguments
        try:
            arg = cli.parse(arg, required=True)
            order = arg['order']
            supplier = arg['supplier']
        except (TypeError, ValueError) as e:
            print(e)
            return
        except KeyError as e:
            print(f"Missing argument in given parameters {arg}")
            return

        try:
            prio = cli.parse(arg)['prio']
        except BaseException:
            prio = 0

        state = ScheduleStates.PLANNED.value
        statx = ScheduleStates.PLANNED.name

        # 1. Create the sfcus from order and update order status
        sfcu_list = self.control.orderproc.new_sfcu(self.site, order)
        print(f"Created sfcus: {sfcu_list}")

        # 2. Create the order operation data (opdta) and update order status.
        self.control.orderproc.new_opdta(self.site, order)

        # 3. Ask for order due date.
        due_date = input("Enter due date [YYYY-MM-DD] for demand: ")

        # 4. Create demands in schedule per sfcu (demand supply matching).
        for sfcu in sfcu_list:
            schedule = self.control.scheduler.new_schedule(
                site=self.site,
                order=order,
                sfcu=sfcu,
                mhu=None,
                operation="ALL",  # all operations are managed by one supplier!
                supplier=supplier,
                org=Orglevel.AREA.value,
                due=due_date,
                prio=prio,
                state=state,
                statx=statx
                )
            # TODO: update sfcu state to 'PLANNED'
            print(f"Created new_schedule {schedule} for sfcu {sfcu} and "\
                   "supplier {supplier}.")

        # 5. Update order status
        self.control.orderproc.update_order_state(self.site,
                                               order,
                                               OrderStates.PLANNED)

    def do_release(self, arg):
        """ Release an order to production.
        
        Args:
            order (int):    Order number.
            
        Examples:
            release order=1000020
        
            Testdata may be found in test/test_order_processing:
                testdata_asgmt.json
                testdata_opdta.json

        Pre-condition: The order has been planned.

        Base sequence:
            1. Retrieve schedule to be assigned:
            2. Publish manufacturing instructions to selected area.
            3. Publish assignment information to all areas to inform who 
               has been selected.
            4. Update the schedule status at site.
            5. Update order status
        """
        # 0. Initialize by command arguments
        try:
            order = cli.parse(arg, required=True)['order']
        except (TypeError, ValueError) as e:
            print(e)
            return

        # 1. Create a list with required scheduling data for assignments.
        asgmtlist = self.control.scheduler.get_asgmtlist(sender=self.site,
                                                         site=self.site,
                                                         order=order)

        # Assign new schedules to suppliers
        if asgmtlist:
            # Select supplier
            # TODO: this is only true if we assign complete orders to an area:
            supplier = asgmtlist[0].supplier

            # 2. Publish unicast operation data to selected area (opdta).
            opdta = self.control.scheduler.new_opdta_msg(sender=self.site,
                                                         site=self.site,
                                                         order=order,
                                                         supplier=supplier)
            self.control.mqclient.publish(opdta.topic, opdta.payload)
            print("Opdta published.")

            # 3. Publish multicast assignment to all areas (schedule).
            for asgmt in asgmtlist:
                self.control.mqclient.publish(asgmt.topic, asgmt.payload)
                print(f"Assignment for sfcu {asgmt.sfcu} published.")

            # 4. Bulk update schedule status at site.
            self.control.scheduler.update_schedule_state(self.site,
                                                         order,
                                                         ScheduleStates.ASSIGNED)

            # 5. Update order status
            self.control.orderproc.update_order_state(self.site,
                                                      order,
                                                      OrderStates.RELEASED)

        else:
            print("No planned schedules found for assignment!")

    def do_show(self, arg):
        # TODO: doesnt work to early eval - find another solution!
        """Display selected information on site objects.
        
        Syntax: show TARGET [opt1=val1, opt2=val2, ...] 
        
        where TARGET is one of these items:
        config   Show configuration of this station.
        schedule Show schedules. Filter options: order, status  
        orders   Show orders list. Filter option: status
        param    Show parameter types of a given order (required argument). 
                 Filter options: mode=all|missing 
        """
        try:
            parsed_arg = cli.parse(arg, subcmd=True)
            target = parsed_arg['subcmd']
        except (ValueError, KeyError) as e:
            print("Error in arguments:", e)
            self.do_help("show")
            return

        targetdict = {
            'config':
                self._show_config,
            'schedule':
                self._show_schedule,
            'orders':
                self._show_orders,
            'param':
                self._show_param
            }

        unknown = f"Unknown information target '{target}'. "\
                  f"Chose one of these: {[t for t in targetdict.keys()]}."

        try:
            targetdict[target](parsed_arg)
        except KeyError:
            print(unknown)

    def do_prepare_opdta(self, arg):
        """ Populate the transfer objects for Shop Floor Control.
        
        Args:
            order (int):   Order number.
            variant (str): Variant code.
            
        TODO: solve problems in create_sfcu if wrong site is passed 
        TODO: this method is not used elsewhere. Why?
        """
        self.control.orderproc.new_opdta(**cli.parse(arg))

    def do_publish_opdta(self, arg):
        """ Publish sfc order_context structure to mqtt server
        
        Args:
            order (int): Order number
            area (str):  Area code
            
        TODO: this method is not used elsewhere. Why?            
        """
        opdta = self.control.scheduler.new_opdta_msg(**cli.parse(arg))
        self.control.mqclient.publish(opdta.topic, opdta.payload)
        print("Published:", opdta.topic, opdta.payload)

    def do_set_param(self, arg):
        """Set missing parameters for a given order.
        
        Args:
            order (int): oder number to be parameterized.
            mode (str):  either 'missing' or 'all.  
        
        Note: 
            This method asks for input of missing order parameter values 
            and set them to a given value.
        """
        try:
            parsed_arg = cli.parse(arg, required=True)
            order = parsed_arg['order']
        except (TypeError, ValueError) as e:
            print(e)
            return

        params = self.control.orderproc.get_order_param(self.site, **parsed_arg)
        for p in params:
            ptyp = p['param_typ']
            pvalue = input(f"Enter parameter value for typ {ptyp}: ")
            if pvalue:
                self.control.orderproc.set_order_param(
                    self.site,
                    order,
                    param_typ=ptyp,
                    param_value=pvalue,
                    )
        return

    def do_create_sfcu(self, arg):
        try:
            parsed_arg = cli.parse(arg)
        except (TypeError, ValueError) as e:
            print(e)
            return

        rc = self.control.orderproc.new_sfcu(self.site, **parsed_arg)
        print(f"Created scfu's: {rc}")
        return rc

    def do_bye(self, arg):
        """Quit the application."""
        print(f'Thank you for using site: {self.site}.')
        self.close()
        return True

    def close(self):
        # Stop the mqtt connection.
        print("Stopping all nodes ...")
        self.agent.stop(None)
        self.control.stop(None)

    def _show_config(self, arg):
        "Print current configuration of site."
        print("Current configuration:")
        # for k, v in self.conf.items():
        [print(f"    {k}: {v}") for k, v in self.conf.items()]

    def _show_schedule(self, args:dict):
        """Display the currently scheduled orders.

        Args:
            order (int):   order number (Optional).
            status (int):  status of scheduled record (Optional).
            
        Example:
            show_schedule(order=1000020)
            show_schedule(status=
        
        Note:
            The display may be restricted to specific orders or schedule
            states by giving the respective arguments (as keyword/value pair).
        """
        try:
            order = args['order']
        except BaseException:
            order = None

        try:
            status = args['status']
        except BaseException:
            status = None

        schedlist = self.control.scheduler.get_schedule(self.site,
                                                        order,
                                                        status)
        tablspec = "{id:<8} {order:>10} {sfcu:>10} {operation:<15} "\
                   "{supplier:<6} {due:<10} {prio:^4} {statx:<10} {at:<12}"
        tablhead = {'id': 'Id', 'order': 'Order', 'sfcu': 'SFCU',
                    'operation': 'Operation', 'supplier': 'Suppl.',
                    'due': 'Due', 'prio': 'prio', 'statx': 'Status', 'at': 'at'}

        print(tablspec.format(**tablhead))

        for schedule in schedlist:
            print(tablspec.format(**schedule))

    def _show_orders(self, args:dict):
        """Display orders for a given status as a table.
        
        Args:
            status (str):    Order state name (optional).
        
        Note: If no order state is given, all orders will be displayed.
        """
        try:
            statx = args['status']
        except (ValueError, TypeError) as e:
            print("Error when parsing argument:", type(e), e)
            return
        except KeyError:
            statx = None

        orderlist = self.control.orderproc.show_orders(status=statx)
        tablspec = "{site:<4} {id:<10} {variant:^20} {qty:>8} {unit:<4} "\
                   "{state:^5} {statx:<10}"
        tablhead = {'site': 'Site', 'id': 'Order Id', 'variant': 'Variant',
                    'qty': 'Qty', 'unit': 'Unit', 'state': 'State', 'statx':
                    'Status Text'}

        print(tablspec.format(**tablhead))

        for order in orderlist:
            print(tablspec.format(**order))

    def _show_param(self, args:dict):
        """Get list of parameter types of an order to be set.
        
        Args:
            order (int):    order number to be parameterized.
            mode (str):     optional: 'missing' or 'all'.
        """
        try:
            order = args['order']
        except (ValueError, TypeError) as e:
            print("Error when parsing argument:", type(e), e)
            return
        except KeyError:
            print("Missing arguments: order number is required.")
            return

        try:
            mode = args['mode']
        except (ValueError, KeyError, TypeError):
            mode = 'missing'

        params = self.control.orderproc.get_order_param(self.site, order, mode)

        if params is not None:
            for p in params:
                print(f"Found parameter typ: {p['param_typ']}")
        else:
            print("No parameters found.")


# @profile
def main():
    """Main process for site control.
    
    Parse arguments, read configuration, assign database, instantiate SiteCmd 
    and start command loop.
    """
    parser = argparse.ArgumentParser(description='Wrapper for site_context. '\
        'Implements the interactions of the order processing use cases.')
    parser.add_argument('-c', '--config', 
                        default='site.json',
                        help='Load configuration from file')
    parser.add_argument('--profile',
                        default='sim',
                        help='Re-create a demo data database at start.')
    parser.add_argument('--loglevel', help='Set log level as DEBUG, INFO, '\
                        'WANRING, ERROR or CRITICAL.')
    parser.add_argument('--license',
                        action='store_true',
                        help='Show license and warranty disclaimer.')
    args = parser.parse_args()

    if args.license:
        print(LICENSE_TEXT)

    if args.loglevel:
        logging.basicConfig(level=args.loglevel, stream=sys.stdout)

    # Get configuration data.
    configurator = config.Configurator(__file__)
    confdata = configurator.get_config(args.config, args.profile)

    # Set or create databases.
    if confdata['data'] == 'sim':
        from flecsimo.util import dbmanager
        database = configurator.get_db_path(confdata['database'])
        dbs = dbmanager.DbSetUp(database)
        dbs.create_schema('site')
        dbs.create_simdata('site', 'FUAS')
        
    confdata['database'] = configurator.get_db_path(database)

    # Instantiate and start command loop.
    SiteCmd(confdata).cmdloop()


if __name__ == '__main__':
    main()
    # Some test data:
    #===========================================================================
    # create_order material=FUAS-1010, qty=2, variant=col:blue
    #
    # plan_order site=FUAS, order=###
    #
    # release_order order=###, area=area1
    #===========================================================================

