#!/usr/bin/env python3
# encoding=utf-8
"""
Device control based on python transitions library

Created on 17.03.2020

@author: Ralf Banning
"""
import argparse
import cmd
import logging
from threading import Thread, Event
from time import sleep

# from memory_profiler import profile
from flecsimo.app import cli
from flecsimo.base import config
from flecsimo.base.const import Orglevel, Component
from flecsimo.base.states import ScheduleStates
from flecsimo.cmpt.edge import Node
from flecsimo.cmpt.station import FlecsimoMachine, MachineError, StationStates
from flecsimo.cmpt.cellagent import CellAgent
from flecsimo.cmpt.simulation import StationSim


LICENSE_TEXT = """ 
    Copyright and License Notice:

    flecsimo single station control.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())

MAX_NUNMBER_OF_STATIONS = 4
MAX_SETUP_TIME_SECONDS = 60
SFCU_SUBSTITUTE = -1

class StationCmd(cmd.Cmd):
    """Command line Interface to control a simulated station model."""

    def __init__(self, states, conf):
        """Initialize configuration of StationCmd.
        
        Args:
            states (enum)    Enumeration of possible state machine states
            conf (dict)      configuration data       
        """
        super().__init__(completekey='TAB')

        self.states = states
        self.conf = conf
        self.stop_event = Event()

    # @profile(precision=3)
    def preloop(self):
        """Initialize cell settings and objects.
        
        Connect to the assigned mqtt server and instantiates a simulated
        station model connected to this server. 
        """
        # Create and start a station agent (one for all stations; only for publishing)
        stn_settings = {'sender': "runstn",
                        'org': Orglevel.STATION.value,
                        'cmpt':Component.SIMTYP.value,
                        'mqserver': 'localhost',
                        'cid': None}
        self.stn_node = Node(stn_settings)
        self.stn_node.start()

        self.machine = FlecsimoMachine()

        # Instantiate twin_cell components (cells and stations)
        try:
            self.cellagent = CellAgent(conf=self.conf)
            self.station = StationSim(self.conf,
                                      self.stn_node.mqclient,
                                      stopevent=self.stop_event)
            self.machine.add_model(self.station)

        except Exception as e:
            logging.CRITICAL("Failed to initialize station_list.")
            raise(e)
            self.close()

    def emptyline(self):
        """Behaviour for 'empty' command line.
        
        Override emptyline() to prevent from repeating last command, print 
        help() instead.
        """
        self.do_help('')

    # @profile(precision=3)
    def do_start(self, arg):
        """ Start all machine's models."""
        self.stop_event.clear()

        try:
            agt_thread = Thread(target=self.cellagent.start(),
                                           args=(),
                                           daemon=True)
            agt_thread.start()

            # Give agent a chance to be connected to MQQT before first
            # messages will be published.
            sleep(1)

            self.machine.dispatch("init")
        except Exception as e:
            _log.critical("Failed to start cell twin:", e)

    def do_show(self, arg):
        """Display selected information on machine or schedules.
        
        Syntax: show TARGET 
        
        where TARGET is one of these items:
        awaited  Show sfcus assigned to this machine.
        pickable Show sfcus waiting to be unloaded
        state    Show the current state of the machine.
        wip      Show sfcus in operation or to unloaded.
        """
        target = arg.lower()

        targetdict = {
            'awaited':
                f"Expected sfcu(s): {self.station.get_expected_sfcu()}.",
            'state':
                f"State of {self.station.station} is {self.station.state.name}.",
            'pickable':
                f"Sfcu(s) waiting to be unloaded: "\
                f"{self.station.get_sfcus_by_state(ScheduleStates.PICKABLE)}.",
            'wip':
                f"Sfcu(s) in operation: "\
                f"{self.station.get_sfcus_by_state(ScheduleStates.WIP)}."}

        unknown = f"Unknown information target '{target}'."\
                  f"Chose one of these: {[t for t in targetdict.keys()]}."

        try:
            print(targetdict[target])
        except:
            print(unknown)

    # @profile(precision=3)
    def do_load(self, arg):
        """Load a specific sfcu to the machine.
        
        Syntax: load sfcu=SFCUNUMBER
        
        TODO: change logic / data to working with parts or pallets.
        TODO: Prevent sfcu from loading which are not expected
        """
        argdict = cli.parse(arg)
        try:
            sfcu = argdict['sfcu']
        except:
            print(f"Malformed argument: {dict(argdict)}")
            return

        if self.conf['sfc_active']:
            loadedsfcu, state = self.station.get_sfcu_state(sfcu)
            _log.debug(f"Found sfcu {loadedsfcu} in state {state}.")
        else:
            loadedsfcu, state = self.station.get_next_loadable_sfcu()
            print(f"No sfcu control active: picked next loadable sfcu {loadedsfcu}")
            _log.debug(f"Selected next loadable sfcu {loadedsfcu} in state {state}.")            

        if loadedsfcu is not None and state in self.station.loadable_states:
            try:
                self.station.load(sfcu=loadedsfcu)

                # Newline after command prompt
                print("\n")

                while not self.stop_event.is_set():
                    sleep(1)
                    data = self.station.get_progress()

                    if data != {}:
                        activity = " ".join([data['station'], data['section'] ])
                        self._display_progress(activity=activity,
                                               progress=data['progress'])
                        if data['section'] == 'setup':
                            if data['elapsed'] >= data['duration']:
                                # Newline to separate from next section.
                                print("\n")
                        if data['section'] == 'operation':
                            if data['elapsed'] >= data['duration']:
                                # Newline to separate from next section.
                                print("\n")
                                break

            except MachineError as e:
                self.station.halt(exception=f"Could not load station: {e}.",
                          sfcu=loadedsfcu)
        else:
            print(f"Sfcu {loadedsfcu} is unknown or not loadable.")

    def do_rework(self, arg):
        """Make a decision, if machine was halted.
               
        Syntax: recover
            
        The rework command starts an analysis if errors or exceptions were
        recorded or if there are schedules in an unsynced state, i.e. those
        schedules with state is in the following list but not in sync with the
        machine's state  
        
        Machine state   In-sync schedule state
        STANDBY         LOADABLE_STATES, REWORK
        SETUP           LOADED
        ACTIVE          WIP
        DONE            PICKABLE
        """
        currentstate = self.station.state
        print(f"Recover process was called in state {currentstate.name}.")

        # Report errors and exceptions.
        try:
            if self.station.error is not None:
                print(f"Station was halted with exception {self.station.error}.")

            if self.station.sfcus_with_exception is not None:
                print(f"Failed sfcus: {self.station.sfcus_with_exception}")

        except:
            print(f"No exceptions were recorded.")

        # Retrieve sync status and filter states with out of sync sfcus.
        oos = self.station.get_sfcu_sync_state(currentstate)
        oos_states = [key for key, val in oos.items() if len(val) > 0]
        print(f"Unsynced sfcus: {oos.items()}")

        # User dialogue
        options = """Decide to:
        [S] Stop the machine.
        [R] Mark the sfcus for re-work.
        [F] Mark the sfcus as failed.
        [C] Continue operation (initialize)
        [X] Leave this dialogue."""

        print(options)
        decision = None
        while True:
            decision = input("Your decision ('X' to leave, '?' for options): ")

            if decision == "?":
                print(options)

            elif decision == "C":
                try:
                    print("Initialize machine.")
                    self.station.init()
                except Exception as e:
                    print(f"Initialization failed: {e}.")

            elif decision == "S":
                print("Stopping station.")
                self.station.stop()

            elif decision == "R":
                print("Marked %s sfcu(s) for re-work." % \
                      self.station._recover_sfcu_errors(oos_states,
                                                        mode='Reassign'))
            elif decision == "F":
                print("Marked %s sfcu(s) as failed." % \
                      self.station._recover_sfcu_errors(oos_states,
                                                        mode='Failed'))
            elif decision == "X":
                return

            else:
                print(f"Undefined decision {decision}. Decide again.")

    # @profile(precision=3)
    def do_unload(self, arg):
        """Unload a finished product from the out bound belt of the machine.
        
        Syntax: unload sfcu=SFCUNUMBER
            
        where SFCUNUMBER is the integer identifier of an sfcu (Shop Floor 
        Control Unit) in status PICKABLE.
        
        TODO: change logic / data to working with parts or pallets.
        TODO: prevent sfcu's from unloading which are not PICKABLE.
        """
        argdict = cli.parse(arg)
        try:
            sfcu = argdict['sfcu']
        except:
            print(f"Malformed argument: {dict(argdict)}")
            return

        if self.conf['sfc_active']:
            pickablesfcu, state = self.station.get_sfcu_state(sfcu)
            _log.debug(f"Found sfcu {pickablesfcu} in state {state}.")
        else:
            pickablesfcu=sfcu
            state=ScheduleStates.PICKABLE.name
            
        if pickablesfcu is not None and state == ScheduleStates.PICKABLE.name:
            try:
                self.station.unload(sfcu=sfcu)
            except MachineError as e:
                _log.warning(f"Could not unload station {e}.")
        else:
            print(f"Sfcu {sfcu} is unknown or not pickable.")

    def do_shutdown(self, arg):
        """ Stop all instances of a StationModel. """
        # TODO: stop cell agents
        try:
            self.station.stop()
        except MachineError as e:
            _log.info("Station is not in stoppable mode:", e)

    def do_halt(self, arg):
        """Halt all running stations in case of exceptions.

        Syntax: halt 
         
        This command will first terminate all running simulation threads and 
        then will bring the state machine of the station into state HALTED. It 
        will neither stop the agent-threads nor disconnect from mqtt server
        """
        self.stop_event.set()  # Stop simulation threads.

        argdict = cli.parse(arg)
        try:
            reason = argdict['reason']
        except:
            reason = "Station halted by user."

        self.station.to_HALTED(reason=reason)
        _log.info("Station is halted!")

    def do_stop(self, arg):
        """Stop station in course of normal operation."""
        self.station.to_STOPPED()
        _log.info("Station is stopped!")

    def do_bye(self, arg):
        """Quit the application."""
        print('Thank you for using site:FUAS')
        self.close()
        return True

    def close(self):
        """Stop the mqtt connection."""
        # TODO: stop twin if still running?
        print("Stopping mqtt connection ...")
        self.cellagent.stop(None)
        self.stn_node.stop(None)

    def _display_progress(self, activity="", progress=0, width=30):
        """Print progress bar.
        
           Args:
               activity (str): a text postfix to the progress bar.
               progress (float or int): the progress x as 0 <= x <= 1.
               width (int): the width of the progress bar.   
        """
        achieved = int(width * progress)
        tobedone = width - achieved
        print(f"\r[", "#" * achieved, " " * tobedone, "]",
              f" {progress*100:.0f} % of {activity}",
              sep='', end='', flush=True)


# @profile(precision=3)
def main():

    print("""
    *************************************************************************
    * Welcome to the flecsimo station control simulation.                   *
    *                                                                       *
    * Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt       * 
    * University of Applied Sciences.                                       *
    * This program comes with ABSOLUTELY NO WARRANTY                        *
    *                                                                       *
    * Type help or ? to list commands.                                      *
    *                                                                       *
    *************************************************************************
    """
    )

    # Parse runtime options
    parser = argparse.ArgumentParser(description='Runs an digital twin for up '\
                                        'to four flecsimo cell simulations.')
    parser.add_argument('-c', '--config',
                        default='cell.json',
                        help='Load configuration from file')
    parser.add_argument('--profile',
                        default='sim',
                        help='Select use case configuration.')
    parser.add_argument('--loglevel', help='Set log level as DEBUG, INFO, '\
                        'WANRING, ERROR or CRITICAL')
    parser.add_argument('--license', action='store_true', help='Show license '\
                        'and warranty disclaimer.')
    args = parser.parse_args()

    if args.license:
        print(LICENSE_TEXT)
        return

    if args.loglevel:
        logging.basicConfig(level=args.loglevel)

    # Only warnings will be shown from state machine
    logging.getLogger('transitions').setLevel(logging.WARNING)
    logging.getLogger('flecsimo.base.edge').setLevel(logging.WARNING)

    # Get configuration data.
    configurator = config.Configurator(__file__)
    confdata = configurator.get_config(args.config, args.profile)

    # Set or create databases.
    if confdata['data'] == 'sim':
        from flecsimo.util import dbmanager
        database = configurator.get_db_path(confdata['database'])
        dbs = dbmanager.DbSetUp(database)
        dbs.create_schema('cell')
        dbs.create_simdata('cell', 'CELL-1')

    confdata['database'] = configurator.get_db_path(database)

    # Instantiate and start command loop.
    StationCmd(StationStates, confdata).cmdloop()


if __name__ == '__main__':

    main()
