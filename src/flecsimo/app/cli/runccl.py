#!/usr/bin/env python3
# encoding=utf-8
"""Run a flecsimo cell cluster with CLI interface. 

FIXME: this cli program is currently not executable due to changed
configuration functionality - it will be recovered by switching to
simlulation.py standards  

Created on 17.03.2020

@author: Ralf Banning
"""
import argparse
import cmd
import logging
from threading import Thread, Event
from time import sleep

from flecsimo.app import cli
from flecsimo.base import config
from flecsimo.base.const import Orglevel
from flecsimo.cmpt.station import FlecsimoMachine, MachineError, StationStates
from flecsimo.cmpt.cellagent import CellAgent
from flecsimo.cmpt.edge import Node
from flecsimo.cmpt.simulation import StationSim

# from collections import deque
LICENSE_TEXT = """ 
    Copyright and License Notice:

    flecsimo cell cluster control.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())

MAX_NUNMBER_OF_STATIONS = 4
MAX_SETUP_TIME_SECONDS = 60


class ClusterCmd(cmd.Cmd):

    def __init__(self, states, conf):
        """Initialize configuration of ClusterCmd.
        
        Args:
            states (enum)    Enumeration of possible state machine states
            conf (dict)      configuration data
                
        Member vars:
        cid        MQTT client Identifier
        config     List of configuration sets for station_list
        numstn     Number of station_list to launch of [1..4]
        
        """
        super().__init__(completekey='TAB')

        self.conf = conf
        self.prompt = 'Cluster:>'
        self.states = states
        self.cid = conf['cid']

        self.config = []
        self.numstn = conf['numstn']

        if self.numstn:
            self.numstn = min(int(self.numstn), MAX_NUNMBER_OF_STATIONS)
        else:
            self.numstn = 1

        self.stop_event = Event()

        # Read configuration files for station_list
        try:
            for _ in range(self.numstn):
                thisname = ''.join(['cell-', str(_ + 1), '.json'])
                # Fixme:
                #===============================================================
                # thisconfig = config.get_config(config.get_conf_path(thisname),
                #                                section='Live')
                #===============================================================
                # Fixme:
                thisconfig={}
                #===============================================================
                self.config.append(thisconfig)
        except Exception as e:
            logging.CRITICAL('Can not initialize station_list configuration.')
            raise(e)

    def preloop(self):
        """Initialize cell settings and objects.
        
        Will connect to the assigned mqtt server and instantiates up to four 
        station_list models connected to this server. 
        """
        # One "shared" node for all 1..4 stations of a simulation.
        # TODO: think of more regular sender id.
        stn_settings = {'sender': "ccl simulation:",
                        'org': Orglevel.MIXED.name,
                        'cmpt': "SIM",
                        'mqserver': 'localhost',
                        'cid': None}
        self.stn_node = Node(stn_settings)
        self.stn_node.start()

        self.machine = FlecsimoMachine()

        # Instatiate twin_cell components (cells and stations)
        try:
            # instantiate cell agents
            self.cell_list = [CellAgent(conf=self.config[_],)  # modified init
                              for _ in range(self.numstn)]

            # instantiate station models
            self.station_list = [StationSim(self.config[_],
                                            self.stn_node.mqclient,
                                            stopevent=self.stop_event,)
                                 for _ in range(self.numstn)]

            # add station model to machine
            self.machine.add_model(self.station_list)

        except Exception as e:
            logging.CRITICAL("Failed to initialize station_list.")
            raise(e)
            self.close()

    def emptyline(self):
        """Behaviour for 'empty' command line.
        
        Override emptyline() to prevent from repeating last command, print 
        help() instead.
        """
        self.do_help('')

    # @profile(precision=3)
    def do_start(self, arg):
        """ Start all machine's models."""
        self.stop_event.clear()

        try:
            # Start mqtt eventloops for agents
            for agt in self.cell_list:
                agt_thread = Thread(target=agt.start(),
                                      args=(),
                                      daemon=True)
                agt_thread.start()

            sleep(1)

            self.machine.dispatch("init")

        except Exception as e:
            _log.critical(f"Failed to start cell twin: {e}")

    def do_showawaited(self, arg):
        """Show list of loadable sfcu's / pallets."""
        argdict = cli.parse(arg)
        try:
            stn = int(argdict['stn'])
            if stn > self.numstn:
                raise ValueError
            else:
                stnlist = [stn]
        except:
            stnlist = range(self.numstn)
        finally:
            for _ in stnlist:
                print(f"Station {_} awaits sfcu {self.station_list[_].get_expected_sfcu()}")

    # @profile(precision=3)
    def do_load(self, arg):
        """Load a specific sfcu to the machine.
        
        Args:
            stn (int): station number to load.
            sfcu (int): sfcu to load.
        
        TODO: change logic / data to working with parts or pallets.
        TODO: move check for setup into model
        """
        argdict = cli.parse(arg)
        stn_nr = int(argdict['stn'])
        sfcu = argdict['sfcu']
        try:
            self.station_list[stn_nr].load(sfcu=sfcu)

        except MachineError as e:
            _log.warning(f"Could not load station {stn_nr}: {e}.")

        for _ in range(1, 10):
            data=self.station_list[stn_nr].get_progress()
            print(f"""Progress in station is {data}""")
            sleep(1)

    # @profile(precision=3)
    def do_unload(self, arg):
        """Unload a finished product from the outbound belt of the machine.
        
        TODO: change logic / data to working with parts or pallets.
        """
        stn_nr = int(cli.parse(arg)['stn'])

        try:
            self.station_list[stn_nr].unload()

        except MachineError as e:
            _log.warning(f"Could not unload station {stn_nr}: {e}.")

    def do_show_config(self, arg):
        "Show current configuration of site."
        print("Current config:")
        for k, v in self.conf.items():
            print(f"{k}: {v}")

    def do_shutdown(self, arg):
        """ Stop all instances of a StationModel. """

        # TODO: stop cell agents
        for _ in range(self.numstn):
            try:
                self.station_list[_].stop()

            except MachineError as e:
                _log.info(f"Station {_} is not in stoppable mode: {e}")

    def do_stop(self, arg):
        """Bring all runnning stations to (emergency) stop-state."""
        self.stop_event.set()
        for _ in range(self.numstn):
            self.station_list[_].to_STOPPED()
            _log.info(f"Station {_} is stopped!")

    def do_bye(self, arg):
        """Quit the application."""
        print('Thank you for using site:FUAS')
        self.close()
        return True

    def close(self):
        """Stop the mqtt connection.
           TODO: stop twin if still running?
        """
        print("Stopping mqtt connection ...")
        self.stn_node.stop(None)
        for cell_agent in self.cell_list:
            cell_agent.stop(None)


def main():

    print("""
    *************************************************************************
    * Welcome to the flecsimo cell-cluster simulation.                      *
    *                                                                       *
    * Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt       * 
    * University of Applied Sciences.                                       *
    * This program comes with ABSOLUTELY NO WARRANTY                        *
    *                                                                       *
    * Type help or ? to list commands.                                      *
    *                                                                       *
    *************************************************************************
    """
    )

    # Parse runtime options
    parser = argparse.ArgumentParser(description='Runs an digital twin for up to four flecsimo cell simulations.')
    parser.add_argument('-c', '--config', 
                        default = 'twin.json',
                        help='Load configuration from file')
    parser.add_argument('--loglevel', help='Set log level as DEBUG, INFO, WANRING, ERROR or CRITICAL')
    parser.add_argument('--license', action='store_true', help='Show license and warranty disclaimer.')
    args = parser.parse_args()

    if args.license:
        print(LICENSE_TEXT)

    if args.loglevel:
        logging.basicConfig(level=args.loglevel)

    # Only warnings will be shown from state machine
    logging.getLogger('transitions').setLevel(logging.WARNING)

    # Get configuration data.
    configurator = config.Configurator()
    config_file = args.config

    confdata = configurator.get_config(config_file, 'twin')

    # Instantiate and start command loop.
    ClusterCmd(StationStates, confdata).cmdloop()


if __name__ == '__main__':
    print("""This cli program is currently not executable due to changed
configuration functionality - it will be recovered by switching to
simlulation.py standards later on.""")
    # main()
