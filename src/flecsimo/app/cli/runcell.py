#!/usr/bin/env python3
"""Run flecsimo cell agent with CLI interface

Created on 18.02.2020

@author: Ralf Banning
"""

LICENSE_TEXT = """
    Copyright and License Notice:

    flecsimo cell agent (with cli interface)
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import cmd
import logging
import sys
# from flecsimo.app import cli  # use for arg parsing if required.
from flecsimo.base import config
from flecsimo.cmpt.cellagent import CellAgent
from flecsimo.cmpt.cellcontrol import CellControl


class CellCmd(cmd.Cmd):
    """Command wrapper for site control workflow."""

    intro = """
    *************************************************************************
    * Welcome to the flecsimo cell controller.                              *
    *                                                                       *
    * Copyright (C) 2020-2024  Ralf Banning, Bernhard Lehner and            * 
    * Frankfurt University of Applied Sciences.                             *
    *                                                                       *
    * This program comes with ABSOLUTELY NO WARRANTY                        *
    *                                                                       *
    * Type help or ? to list commands.                                      *
    *                                                                       *
    *************************************************************************
    """

    def __init__(self, conf, mode):
        super().__init__(completekey='TAB')
        self.conf = conf
        self.mode = mode
        self.site = self.conf['site']
        self.area = self.conf['area']
        self.cell = self.conf['cell']
        self.prompt = f"Cell:{self.cell}>"

    def preloop(self):
        """Setup mqtt connection and cell nodes on startup of command loop."""
        # Start mqtt connection
        print(f"Connecting {self.cell} site to mqtt-server "
              f"at {self.conf['mqserver']}...")

        try:
            # Create agent and control objects
            self.agent = CellAgent(conf=self.conf, mode=self.mode)
            self.control = CellControl(conf=self.conf)

            # Start edge nodes
            self.agent.start()
            # not executed in current version:
            # self.control.start()

        except ConnectionRefusedError as e:
            print(e, "\nSite nodes will be stopped.")
            self.close()

    def do_show_config(self, arg):
        "Show current configuration of site."
        print("Current config:")
        for k, v in self.conf.items():
            print(f"{k}: {v}")

    def do_bye(self, arg):
        """Quit the application."""
        print(f"Thank you for using cell: {self.cell}.")
        self.close()
        return True

    def close(self):
        # Stop the mqtt connection.
        print("Stopping all nodes ...")
        self.agent.stop(None)
        # Not in use in current version
        # self.control.stop(None)


def main():
    """Main process for cell agent.
    
    This code controls the communication between cell-machines and area
    controller and starts operations within the call back functions.
    """
    parser = argparse.ArgumentParser(description='Runs an agent for ' + \
                                                 'manufacturing cells.')
    parser.add_argument('-c', '--config',
                        default='cell.json', 
                        help='Load configuration from file.')
    parser.add_argument('-m', '--mode', 
                        choices=['AUTO', 'USER'],
                        default='AUTO',
                        help='Set mode as AUTO or USER.')
    parser.add_argument('--profile',
                        default='sim',
                        help='Select use case configuration.')
    parser.add_argument('--loglevel', 
                        help='Set log level as DEBUG, INFO, WANRING, ' + \
                             'ERROR or CRITICAL.')
    parser.add_argument('--license',
                        action='store_true',
                        help='Show license and warranty disclaimer.')
    args = parser.parse_args()

    if args.license:
        print(LICENSE_TEXT)

    if args.loglevel:
        logging.basicConfig(level=args.loglevel, stream=sys.stdout)

    # Get configuration data.
    configurator = config.Configurator(__file__)
    confdata = configurator.get_config(args.config, args.profile)
    mode = args.mode

    # Set or create databases.
    if confdata['data'] == 'sim':
        from flecsimo.util import dbmanager
        database = configurator.get_db_path(confdata['database'])
        dbs = dbmanager.DbSetUp(database)
        dbs.create_schema('cell')
        dbs.create_simdata('cell', 'CELL-1')
    
    confdata['database'] = configurator.get_db_path(database)

    # Start command loop
    CellCmd(confdata, mode).cmdloop()


if __name__ == "__main__":

    main()
