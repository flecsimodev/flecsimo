# encoding=utf-8
""" flecsimo command line scripts.

This package provides apps to test, develop and play with flecsimo 
even without having a physical model in place.

Modules:
    runarea    run an area controller and agent with CLI interface.
    runcell    run a cell agent with CLI interface.
    rungrp     run a simulation of 1 to 4 cells together with virtual stations.
    runsite    run a site controller and agent with CLI interface.
    runstn     run a simple single station simulation with CLI interface.
"""

""" 
    Copyright and License Notice:

    flecsimo app.cli package
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

def parse(arg, required=False, subcmd=False) -> dict:
    """Normalizes cmd arguments for called functions.
    
    Converts a kwarg argument string (as given by cmd interpreter) in to a
    dictionary. Providing an argument string with non-keyword arguments
    will raise an exception. If the argument string is empty, an empty dictionary
    is returned.
    
    Notes: 
        All argument values can be entered without any quotes. 
        All arguments will be handled internally as strings. 
        All keyword arguments have to be separated by a comma.
        
    Examples:
        Will work:
        show config
        show orders state=PLANNED
        show orders state="PLANNED"  
        create_order material=FXF-1100, variant=col:red, qty=2
        create_order(material=FXF-1100, variant=col:red, qty=2)
        
        Will fail:
        create_order order=FXF-1100 variant=col:red qty=2 (missing commas)
        create_order(FXF-1100, col:red, qty=2) (missing key words)
        
        
    Args:
        arg (str)
        required (bool): raise exception when no argument is provided
        subcmd (bool): partitions arg in a subcommand string and parameters.
        
    Returns:
        None if no parsed arg was found, arg_as_dict(dict) else.
        
    Exception:
        ValueError
        
    """       
    arg_as_dict={}
    partitioned_arg=()
    
    parsed_arg = arg.strip('()')
    if parsed_arg == '':
        if required is True:
            raise ValueError("No arguments provided although required.")
        else:
            return arg_as_dict
        
    try:
        parsed_arg = parsed_arg.replace('\'', '')
        parsed_arg = parsed_arg.replace('\"', '')
            
        if subcmd is True:
            partitioned_arg = parsed_arg.partition(' ')
            arg_as_dict['subcmd'] = partitioned_arg[0]
              
            parsed_arg = partitioned_arg[2]
              
        if parsed_arg !='':                
            parsed_arg = parsed_arg.replace(" ", "")     
            arg_as_dict.update(dict(_.split("=") for _ in parsed_arg.split(',')))
            
    except ValueError:
        raise ValueError(f"Malformed arguments: {arg}")
        
    return arg_as_dict