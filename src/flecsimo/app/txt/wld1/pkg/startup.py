#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Start up flecsimo welding cell.

This module will be run as 'top-level code environment' when deployed to a
Fischertechnik model with TXT-controller and fct-firmware.

Main purpose is to create a FtcGuiApplication at TXT's screen and to start
the flecsimo station controller in a separate thread. 
   
@author: Leon Schnieber
"""
import argparse  
import os
import sys
from TouchStyle import *

"""
Copyright and License Notice:

    flecsimo txt app startup
    Copyright (C) 2022  Ralf Banning, Bernhard Lehner, Leon Schnieber and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""



class FtcGuiApplication(TouchApplication):

    def __init__(self, args):
        print("starting app ...")
        TouchApplication.__init__(self, args)

        window = TouchWindow("RunStn")

        vbox = QVBoxLayout()
        msg = QLabel("Test Element")
        vbox.addWidget(msg)

        window.centralWidget.setLayout(vbox)

        # Move the Block to the Display.
        window.show()

        # Prepare root.
        flecsimo_root = os.path.dirname(os.path.join(os.getcwd(), __file__))
        os.chdir(flecsimo_root)
               
        parser = argparse.ArgumentParser(
            description='Runs station control for a physical model.')
        parser.add_argument('--loglevel', default='ERROR',
                            help='Set log level as DEBUG, INFO, WANRING, ERROR ' + \
                            'or CRITICAL.')
        args = parser.parse_args()


        # TODO: Overwrite by QT buttons settings in final version.
        #-----------------------------------------------------------------------
        loglevel = args.loglevel
        #-----------------------------------------------------------------------
        
        # Speed up window show by lazy importing...
        import threading     
        station_runner = threading.Thread(target=self.__runStation,
                               args=(loglevel,),
                               daemon=True)
        station_runner.start()

        self.exec_()

    def __runStation(self, loglevel='ERROR'):
        # save some time at initial start of GUI and import the more
        # complicated modules not before this function is called.
        #
        # change next line (wld1) when adapting to other cells...
        from flecsimo.app.txt.wld1 import runstn

        # ... and next line as well. The rest of this file should be generic
        runstn.run_station(loglevel=loglevel)


if __name__ == "__main__":
    FtcGuiApplication(sys.argv)
