#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Configure flecsimo welding cell.

This module will be run as 'top-level code environment' when deployed to a
Fischertechnik model with TXT-controller and fct-firmware.

Main purpose is to import the configmanager module and run a ConfigManager() 
instance. 

Created on 11.10.2024

@author: Ralf
"""
import argparse
from flecsimo.util.configmanager import ConfigMgr


def main():
    parser = argparse.ArgumentParser(description='Configuration manager. '\
        'Create, inspect and change flecsimo configuration files.')
    parser.add_argument('-r', '--root',
                        help='Set application root directory')    
    parser.add_argument('-f', '--file',
                        default='wld1.json',
                        help='Load configuration from file')
    args = parser.parse_args()

    ConfigMgr(args.root, args.file).cmdloop()


if __name__ == '__main__':
    main()
