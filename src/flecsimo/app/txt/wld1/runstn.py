#!/usr/bin/env python3
"""Run flecsimo welding cell controller.

Created on 24.06.2022

@author: Ralf Banning
"""
import argparse
import logging
import sys

from flecsimo.base import config
from flecsimo.base.const import Orglevel, Component
from flecsimo.cmpt.cellagent import CellAgent
from flecsimo.cmpt.edge import Node, compose_sender
from flecsimo.cmpt.station import FlecsimoMachine, StationModel
from flecsimo.plc.welding_cell import plc_welding_cell

LICENSE_TEXT = """
    pwd
    Copyright and License Notice:

    flecsimo welding cell controller (TXT deployment)
    Copyright (C) 2022  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

_log = logging.getLogger(__name__)
_log.addHandler(logging.NullHandler())


class StationControl(StationModel):
    """A hardware specific state machine model.
    
    This class implements the hardware specific functions which have to be 
    called on enter a new state in the general flecsimo state machine model.
    It will overwrite all methods of the StationModel base class which will 
    need specific function of the underlying PLC packages.
    
    Note: 
        StationModel is not implemented as an abstract base class, so it may
        be also in instantiated directly.
     
    Args:
        states: states to be controlled
        conf (dict): a dictionary with configuration data as provided
                     by flecsimo.base.config
        mqclient: a configured mqclient instance for m2m communication
    """

    def __init__(self, conf, mqclient):

        # Call parent class; this will also set self.conf
        super().__init__(conf, mqclient)
        self.plc = plc_welding_cell.Plc_welding_cell()

    # Overload do/ run abstract StationModel methods with physical
    # implementation code
    def run_initialize(self):
        print("run initialize.")
        self.plc.initialize()
        self.proceed()

    def run_setup(self, sfcu, parameter):
        self.plc.setup(parameter)
        self.running = False
        self.go(sfcu=sfcu)

    def run_operation(self, sfcu, operation, parameter):
        self.plc.instruction_process_one_component(parameter)
        self.running = False
        self.proceed(sfcu=sfcu)

    # Overload waiting entry/ methods
    def await_loading(self):
        """Wait for material loaded on station and identify sfcu.
        
        Waits for anything that triggers the light barrier and tries to return
        the material handling unit identifier (mhu) by reading an attached
        QR-code (plc-part). Having a result, we look up the scheduled sfcus.
        If a match is found the station will proceed otherwise requires some
        intervention by operators. 
        """
        # TODO: make next line active when camera is available at model.
        # mhu = self.plc.wait_for_component_at_inlet()

        # TODO: delete next two statements when camera is available at model.

        mhu = self.plc.wait_for_component_at_inlet()
        # Integration test monkey patch
        if self.conf['mhc_active']:
            loadedsfcu, state = self.get_sfcu_state(mhu, typ='mhu')
            _log.debug(f"Found sfcu {loadedsfcu} in state {state} for mhu {mhu}.")

        else:
            loadedsfcu, state = self.get_next_loadable_sfcu()
            _log.debug(f"Selected next loadable sfcu {loadedsfcu} in state {state}.")

        if loadedsfcu is not None and state in self.loadable_states:
            self.load(sfcu=loadedsfcu)
        else:
            self.halt()

    def await_unloading(self):
        self.plc.wait_for_free_inlet()
        self.unload(sfcu=self.sfcu)

    def await_decision(self, proceed=True):
        # TODO: by a "switch" or button at physical model to continue
        #=======================================================================
        # decision = input("... press 'C' for continue, any other key will stop.")
        # if decision == 'C':
        #     proceed = True
        # else:
        #     proceed = False
        #=======================================================================

        return proceed


def run_station(conf='wld1.json', loglevel=None):
    """Main process for station control."""
    # Parse runtime options
    if loglevel:
        _log = logging.getLogger(__name__)
        _log.addHandler(logging.NullHandler())        
        logging.basicConfig(level=loglevel)

    # Read configuration
    configurator = config.Configurator(__file__)
    confdata = configurator.get_config(conf)
    database = configurator.get_db_path(confdata['database'])
    org = Orglevel.STATION.value

    # Set or create databases.
    if confdata['data'] == 'sim':
        from flecsimo.util import dbmanager
        database = configurator.get_db_path(confdata['database'])
        dbs = dbmanager.DbSetUp(database)
        dbs.create_schema('cell')
        dbs.create_simdata('cell', 'CELL-1')
        del dbs

    confdata['database'] = configurator.get_db_path(database)

    cmpt = Component.STATION.value
    stn_settings = {'sender': compose_sender(confdata, org),
                    'org': org,
                    'cmpt': cmpt,
                    'mqserver': confdata['mqserver'],
                    'cid': None}

    # Create a cell agent
    cell_agent = CellAgent(conf=confdata)
    # Create a station agent (only for publishing)
    station_node = Node(stn_settings)

    if confdata['online']:
        try:
            cell_agent.start()
            station_node.start()
        except Exception as e:
            _log.critical("Failed to start agents.")
            raise(e)
    else:
        _log.debug("Station runs in offline mode.")

    # Create and start event loops for hardware (if required).
    pass

    # Create the hardware specific station model
    station = StationControl(confdata, station_node.mqclient,)

    # Create a state machine,add station model and dispatch initialization.
    machine = FlecsimoMachine()
    machine.add_model(station)
    machine.dispatch("init")


def main():
    """Main process for station control."""
    # Parse runtime options
    parser = argparse.ArgumentParser(
        description='Runs station control for a physical model.')
    parser.add_argument('-c', '--config', help='Load configuration from file.')
    parser.add_argument('--offline', action='store_true',
                        help='Set True if station should not connect to ' + \
                        'mqtt server.')
    parser.add_argument('--demo', action='store_true',
                        help='Re-create a demo data database at start.')
    parser.add_argument('--loglevel', default='ERROR',
                        help='Set log level as DEBUG, INFO, WANRING, ERROR ' + \
                        'or CRITICAL.')
    parser.add_argument('--license', action='store_true',
                        help='Show license and warranty disclaimer.')
    args = parser.parse_args()

    if args.license:
        print(LICENSE_TEXT)

    run_station(args.config, args.offline, args.demo, args.loglevel)


if __name__ == "__main__":

    main()
