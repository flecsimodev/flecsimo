# Contributing
If you want to contribute, please visit https://confluence.frankfurt-university.de/display/FFP
and contect the maintainers.
 
The flecsimo project is currently not open for general public contribution, 
due to the fact that a lot design decision are still open. If you want 
contribute, you have to be a registered member of gitlab and the development 
group.