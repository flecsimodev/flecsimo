"""Unit tests for roles/scheduling module.

Created on 29.11.2021

@author: Ralf Banning

Copyright and License Notice:

    flecsimo roles.scheduling unit tests.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import datetime
import json
import sqlite3
import unittest
from pathlib import Path

from flecsimo.base import dao
from flecsimo.base import msg
from flecsimo.base.states import ScheduleStates
from flecsimo.util import dbmanager
from flecsimo.habit import scheduling


class SchedulerMethods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.database = cls.testroot.joinpath("data", "tmp", "test_roles_scheduling.db")
        cls.conf = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": cls.database}

        dbs = dbmanager.DbSetUp(cls.database)
        dbs.create_schema('site')
        dbs.create_masterdata('site')
        dbs.create_testdata('site')

        super(SchedulerMethods, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # cls.database.unklink()
        super(SchedulerMethods, cls).tearDownClass()

    def setUp(self):
        #=======================================================================
        # dbs = dbmanager.DbSetUp(self.database)
        # dbs.create_schema('site')
        # dbs.create_data('test', 'site')
        #=======================================================================
        self.schd = scheduling.Scheduler(self.database)
        self.dao = dao.DataAccess(self.database)

    def tearDown(self):
        self.conn = sqlite3.connect(self.database)
        with self.conn:
            self.conn.execute("DELETE FROM 'schedule' WHERE sfcu=99")
            self.conn.execute("DELETE FROM 'schedule' WHERE supplier = 'NODATA'")
        self.conn.close()

    def testAssignSupplier(self):
        """Assign a supplier to an schedule
        
        Tests:
            Assign supplier test to schedule of TEST_ORDER which should result
            in one new schedule (rowcount: 1) with rowid 31 having 'TEST' as 
            supplier.
            
        Note:
            New record will be deleted from database finally.
            Would be nicer to do in tear down. 
        """
        TEST_ORDER = 1000020

        rc = self.schd.schedule_supplier(site='FUAS',
                                       order=TEST_ORDER,
                                       supplier='TEST',
                                       operation='12-CUTCO',
                                       sfcu=1)
        self.assertEqual(rc, {'rowid': 11, 'rowcount': 1})

        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row
        with conn:
            cur = conn.execute("""SELECT * FROM 'schedule' WHERE "id" = :rowid""",
                               {'rowid': rc['rowid']})
            row = cur.fetchone()
            self.assertEqual([dict(row)['supplier']], ['TEST'])

            conn.execute("""DELETE FROM schedule WHERE "id" = :rowid""",
                         {'rowid': rc['rowid']})

        conn.close()

    @unittest.skip
    def testChooseFacilityOptions(self):
        """Get a list of possible facilities for given order.
        
        The order no. 1000060 comprises a full manufacturing cycle with four
        operations. For each of the operations, a facility should be found
        (on site level execution, this will be an area (or more), on area
        level this will be one or more cells.
        
        Tests:
            1. Choose facilities for an existing order. This will give
               a result list with 4 entries. 
            2. Check that facility data for drilling is found
               and is correct.
            3. Choose facilities for an non existing order. This will give
               an empty result list.
        """
        EXPECTED_DRILLING_FACILITY = {'site': 'FUAS', 'order': 1000060, 'id': 1,
            'facility': 'area1', 'operation': '27-DRILLING',
            'param_typ': 'diam', 'param_min': 10, 'param_max': 19,
            'value_typ': 'range', 'setup': 25, 'state': '', 'statx': ''}

        # Test 1:
        result = self.schd.choose_facility('FUAS', 1000060)
        self.assertEqual(len(result), 4)

        # Test 2:
        matches = 0
        for r in result:
            if dict(r)['operation'] == '27-DRILLING':
                self.assertEqual(dict(r), EXPECTED_DRILLING_FACILITY)
                matches += 1
        self.assertGreater(matches, 0)

        # Test 3:
        result2 = self.schd.choose_facility('FUAS', 1000600)
        self.assertEqual(len(result2), 0)

    def testChooseFacilitySelection(self):
        """Test interval borders for Facility assignment..
                       
        In order no. 10000077 the operation data for '27-Drilling' will be 
        changed in a way that different drilling diameters are required.
        
        Tests:
            1. Diam = 0 should give no result for param_typ 'diam'
               Diam = 9 should give no result for param_typ 'diam'
               Diam = 61 should give no result for param_typ 'diam'
            2. Diam = 10 should give a setup of 25 seconds
               Diam = 21 should give a setup of 30 seconds
               Diam = 41 should give a setup of 35 seconds
               Diam = 60 should give a setup of 35 seconds
        """
        # Test 1:
        # With the testdata given, no operation for drilling should be found,
        # i. e. no line with a param_type as 'diam'.
        DIAMLIST = [0, 9, 61]
        PARAMTYP = ['diam']

        for i in DIAMLIST:
            with self.subTest(i=i):
                result = []
                self.dao.change('task',
                                data={'param_value': int(i)},
                                conditions={'id': 70})
                result = self.schd.choose_facility('FUAS', 1000077)
                for r in result:
                    self.assertTrue(r['param_typ'] not in PARAMTYP,
                                    "Unexpected operation found.")

        # Test 2
        DIAMSETUP = {'10': 25, '21': 30, '41': 35, '60': 35}

        for i in DIAMSETUP.keys():
            with self.subTest(i=i):
                result = []
                self.dao.change('task',
                                data={'param_value': int(i)},
                                conditions={'id': 70})
                result = self.schd.choose_facility('FUAS', 1000077)

                for r in result:
                    if r['param_typ'] == 'diam':
                        self.assertEqual(r['setup'], DIAMSETUP[i],
                                         f"Wrong setup time for diam {i}")

    def testGetAsgmtlist(self):
        """Get an asgmnt list
        
        Creating assignment for order 1000068 should result in two
        assigned schedules fur sfcus 11 and 12.
        
        Tests:
            1. Get assignment list - should contain two elements.
            2. Check that list contain Asgmt message as pyload.
            3. Check topic and payload elements.
        
        """
        # Test 1:
        asgmtlist = self.schd.get_asgmtlist(sender='FUAS',
                                            site='FUAS',
                                            order=1000022,)
        self.assertEqual(len(asgmtlist), 2)

        # Test 2:
        self.assertTrue(isinstance(asgmtlist[0], msg.Asgmt))

        # Test 3:
        self.assertEqual(asgmtlist[0].topic, 'FUAS/asgmt')
        self.assertEqual(json.loads(asgmtlist[0].payload)['sfcu'], 3)
        self.assertEqual(asgmtlist[1].topic, 'FUAS/asgmt')
        self.assertEqual(json.loads(asgmtlist[1].payload)['sfcu'], 4)

    def testGetSchedule(self):
        """Retrieve a list of schedules.
        
        Tests:
            1. Get all schedules for a specific textual state.
            2. Get all schedules for a specific enum state.
            3. Try to get schedules for a non existing state.
            4. Get schedules for a specific order (no condition on state).
            5. Get schedules for a specific order and state.
            6. Try to get schedules for a specific order in wrong state. 
            7. Check content for a single row without defaults.
            8. Check content for a single row with(!) defaults.
            9. Check behavior if no row can be selected.
        """
        site = 'FUAS'
        # Test 1:
        state = "ASSIGNED"
        EXPECTED_ROWCOUNT = 4
        result = self.schd.get_schedule(site, status=state)
        self.assertEqual(len(result), EXPECTED_ROWCOUNT, "Test 1 failed.")

        # Test 2:
        state = ScheduleStates.ASSIGNED
        EXPECTED_ROWCOUNT = 4
        result = self.schd.get_schedule(site, status=state)
        self.assertEqual(len(result), EXPECTED_ROWCOUNT, "Test 2 failed.")

        # Test 3:
        state = 100  # Unidefined
        EXPECTED_ROWCOUNT = 0
        result = self.schd.get_schedule(site, status=state)
        self.assertEqual(len(result), EXPECTED_ROWCOUNT, "Test 3 failed.")

        # Test 4:
        order = 1000023
        EXPECTED_IDS = [5, 6, 7]
        result = self.schd.get_schedule(site, order)
        self.assertEqual([r['id'] for r in result], EXPECTED_IDS,
                         "Test 4 failed.")

        # Test 5:
        state = "ASSIGNED"
        order = 1000022
        EXPECTED_IDS = [3, 4,]
        result = self.schd.get_schedule(site, order, state)
        self.assertEqual([r['id'] for r in result], EXPECTED_IDS,
                         "Test 5 failed.")

        # Test 6:
        state = "PLANNED"
        order = 1000021
        EXPECTED_IDS = []  # no result fetched.
        result = self.schd.get_schedule(site, order, state)
        self.assertEqual([r['id'] for r in result], EXPECTED_IDS,
                         "Test 6 failed.")

        # Test 7:
        state = "ASSIGNED"
        order = 1000020
        EXPECTED_ROW = {'id': 1,
                        'order': 1000020,
                        'sfcu': 1,
                        'operation': 'ALL',
                        'supplier': 'AREA-1',
                        'due': '2024-07-10',
                        'prio': 0,
                        'statx': 'ASSIGNED',
                        'at': '2024-07-05 08:15:52.636603'}

        result = self.schd.get_schedule(site, order, state)
        self.assertEqual(result[0], EXPECTED_ROW)

        # Test 8:
        state = "PLANNED"
        order = 1000077
        EXPECTED_ROW = {'id': 31,
                         'order': 1000077,
                         'sfcu': 1,
                         'operation': 'NODATA',
                         'supplier': 'NODATA',
                         'due': None,
                         'prio': 0,
                         'statx': 'PLANNED',
                         'at': '2021-07-05 08:15:52.636603'}

        DATA = {'id': 31,
                'site': site,
                'order': 1000077,
                'sfcu': 1,
                'prio': 0,
                'state': 1,
                'statx': 'PLANNED',
                'at': '2021-07-05 08:15:52.636603'}

        self.dao.persist('schedule', DATA)

        result = self.schd.get_schedule(site, order, state)
        self.assertEqual(result[0], EXPECTED_ROW, "Test 8 failed.")

        conn = self.dao.connect()

        with conn:
            conn.execute("DELETE FROM schedule WHERE 'id' = 31")
        conn.close

        # Test 9:
        state = 1  # PLANNED
        order = 2000066
        EXPECTED_ROW = [    ]  # no result fetched.
        result = self.schd.get_schedule(site, order, state)
        self.assertEqual(result, EXPECTED_ROW, "Test 9 failed.")

    def testNewOpdtaMsg(self):
        """
        
        Tests:
            1.1 Generate an opdta message with minimal conditions.
            1.2 Verify that result is an opdta message instance.
            1.3 Verify that opdta contains all expected record types.
            1.4 Verify that opdta contains record types in expected 
                multiplicity.
            
            2.1 Generate an opdta message for a specific sfcu.
            2.2-2.3 as 1.3 to 1.4.
            
            3.1 Generate an opdta message for a specific order.
            3.2-3.3 as 1.3 to 1.4.
            
        """
        TEST_ORDER_NO = 1000023
        TEST_SFCU_NO = 5
        TEST_OPERATION = '27-DRILL'

        # Test 1.1:
        result1 = self.schd.new_opdta_msg(sender='FUAS',
                                site='FUAS',
                                order=TEST_ORDER_NO,
                                supplier='area1')
        # Test 1.2:
        self.assertTrue(isinstance(result1, msg.Opdta))

        # Test 1.3:
        payload1 = json.loads(result1.payload)
        self.assertEqual([p for p in payload1['data']],
                         ['order', 'sfcu', 'task', 'part', 'resource'])

        # Test 1.4:
        EXPECTED_OPDATA_NUM_1 = {'order': 1, 'sfcu':3 , 'task': 4, 'part': 4,
                                 'resource': 4}
        for p in payload1['data']:
            self.assertEqual(len(payload1['data'][p]),
                             EXPECTED_OPDATA_NUM_1[p],
                             f"Unexpected number of data records for p={p}.")

        # Test 2.1:
        result2 = self.schd.new_opdta_msg(sender='FUAS',
                                site='FUAS',
                                order=TEST_ORDER_NO,
                                supplier='area1',
                                sfcu=TEST_SFCU_NO)

        # Test 2.2:
        payload2 = json.loads(result2.payload)
        self.assertEqual([p for p in payload1['data']],
                         ['order', 'sfcu', 'task', 'part', 'resource'])

        # Test 2.3:
        EXPECTED_OPDATA_NUM_2 = {'order': 1, 'sfcu': 1 , 'task': 4, 'part': 4,
                                 'resource': 4}
        for p in payload2['data']:
            self.assertEqual(len(payload2['data'][p]),
                             EXPECTED_OPDATA_NUM_2[p],
                             f"Unexpected number of data records for p={p}.")

        # Test 3.1:
        result3 = self.schd.new_opdta_msg(sender='FUAS',
                                site='FUAS',
                                order=TEST_ORDER_NO,
                                supplier='area1',
                                operation=TEST_OPERATION)

        # Test 3.2:
        payload3 = json.loads(result3.payload)
        self.assertEqual([p for p in payload3['data']],
                         ['order', 'sfcu', 'task', 'part', 'resource'])

        # Test 3.3:
        EXPECTED_OPDATA_NUM_3 = {'order': 1, 'sfcu':3 , 'task': 1, 'part': 0,
                                 'resource': 1}
        for p in payload3['data']:
            self.assertEqual(len(payload3['data'][p]),
                             EXPECTED_OPDATA_NUM_3[p],
                             f"Unexpected number of data records for p={p}.")

    def testNewSchedule(self):
        """ Test to create a new schedule from given data."""
        data = {'site': 'FUAS',
                'order': 1000026,
                'sfcu': 99,
                'operation': None,
                'supplier': 'AREA-1',
                'org': 'area',
                'mhu': None,
                'due': '2021-12-24',
                'prio': 0,
                'state': 1,
                'statx': 'TESTED',}
        EXPECTED_RESULT = {'id': 11, 'order': 1000026,
                           'operation': 'NODATA', 'supplier': 'AREA-1',
                           'mhu': None}
        # Test 1:
        self.assertEqual(self.schd.new_schedule(**data), 11)

        # Test 2:
        rc = self.dao.retrieve('schedule',
                        columns=['id', 'order', 'operation', 'supplier', 'mhu'],
                        conditions={'sfcu': 99}, single=True)
        self.assertEqual(dict(rc), EXPECTED_RESULT)

    def testScheduleOperation(self):
        """Test to schedule an operation for a given site and order.
         
        Tests:
            1. Schedule a new operation (order and related task present). 
            2. Negative: no task for given order. 
            3. Negative: order not in db.
        """
        site = 'FUAS'

        # Test 1:
        order_ok = 1000020  # Exists in order and task
        self.assertEqual(self.schd.schedule_operation(site, order_ok)['rowcount'], 
                         3)

        # Test 2:
        order_nok1 = 1000077  # only exists in order
        self.assertEqual(self.schd.schedule_operation(site, order_nok1),
                         {'lastrowid': 0, 'rowcount': 0})

        # test 3:
        order_nok2 = 1000082  # does not exist at all
        self.assertEqual(self.schd.schedule_operation(site, order_nok2),
                         {'lastrowid': 0, 'rowcount': 0})

    def testUpdateSchedule(self):
        """Change the state of a schedule.
        
        Note:
            this function may be replaced by direct all of 
            dao.change('schedule', 
                       data={'state': state, 
                             'statx': statx,
                             'at': ...utc.now()
                       conditions={'order': order,
                                   'site': site})
                                   
        Tests:
            1. Update (two) schedules for order 1000066 to state 9.
            2. Verify that all three schedules are "CANCELLED".
            3. Verify that using a wrong status type fails.
            4. Reset status to ASSIGNED.
        """
        TEST_ORDER_NO = 1000024
        TEST_STATE = ScheduleStates.CANCELLED
        # Test 1:
        result1 = self.schd.update_schedule_state('FUAS',
                                            TEST_ORDER_NO,
                                            TEST_STATE.value)
        self.assertEqual(result1, 2)

        # Test 2:
        result2 = self.dao.retrieve('schedule',
                                    columns=['order', 'statx'],
                                    conditions={'state': TEST_STATE.value})
        self.assertEqual(len(result2), 2)
        for idx in range(len(result2)):
            self.assertEqual(dict(result2[idx])['statx'], 'CANCELLED')
            self.assertEqual(dict(result2[idx])['order'], TEST_ORDER_NO)

        # Test 3:
        with self.assertRaises(ValueError) as te:
            self.schd.update_schedule_state('FUAS', 1000024, None)
        self.assertEqual(te.exception.args, ("Argument status: None is neither ScheduleState nor integer in expected range.",))

        # Test 4:
        result4 = self.schd.update_schedule_state('FUAS', 1000024, ScheduleStates.ASSIGNED)
        self.assertEqual(result4, 2)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
