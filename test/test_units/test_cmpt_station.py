"""Unit test suite for flecsimo.cmpt.station module 
Created on 30.12.2021

@author: Ralf Banning

Copyright and License Notice:

    flecsimo cmpt.station unit tests.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
# import os
import pathlib
from flecsimo.cmpt.station import FlecsimoMachine, StationModel  # , StationStates
from flecsimo.util import dbmanager
from flecsimo.base.dao import DataAccess


class TestStation(StationModel):

    def __init__(self, conf, mqclient, queue=None, stopevent=None):
        super().__init__(conf, mqclient, queue=None, stopevent=None)

    def await_decision(self, proceed):
        pass
        return proceed

    def await_loading(self):
        pass

    def await_unloading(self):
        pass

    def run_initialize(self):
        pass

    def run_operation(self, sfcu, operation, param_typ, param_value):
        pass

    def run_setup(self, sfcu, setup):
        pass


class MachineMethods(unittest.TestCase):

    def setUp(self):
        self.machine = FlecsimoMachine()

    def tearDown(self):
        del self.machine

    def testInitFlecsimoMachine(self):
        pass

    def testStatesDefinition(self):
        """Test minimum states are defined with right numerical values."""

        states_as_is = [_.name for _ in self.machine.states.values()]
        states_to_be = ['STOPPED', 'READY', 'STANDBY', 'SETUP', 'ACTIVE',
                        'DONE', 'HALTED', 'HOLD']
        self.assertTrue(set(states_as_is) <= set(states_to_be))

        # Test if states are defined as Enums with right numbers
        numval_as_is = [_.value.value for _ in self.machine.states.values()]
        numval_to_be = [0, 1, 2, 3, 4, 5, 6]
        self.assertTrue(set(numval_to_be) <= set(numval_as_is))

    def testTriggerDefinition(self):
        """Test if miminum set of triggers is defined"""
        machine_states = [_.name for _ in self.machine.states.values()]

        trigger_as_is = [_ for _ in self.machine.get_triggers(*machine_states,)]
        trigger_to_be = ['to_STOPPED', 'to_READY', 'to_STANDBY', 'to_SETUP',
                         'to_ACTIVE', 'to_DONE', 'to_HALTED', 'init', 'proceed',
                         'load', 'halt', 'stop']

        self.assertTrue(set(trigger_to_be) <= set(trigger_as_is))


class StationMethods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.testpath = (pathlib.Path(__file__).parent.parent.resolve())
        cls.database = cls.testpath.joinpath("data/tmp/test_cmpt_station.db")
        print("Local path of db is:", cls.database)
        cls.conf = {"type": "cell",
                    "site": "FUAS",
                    "area": "AREA-1",
                    "cell": "CELL-1",
                    "station": "CUTCO1",
                    "mqserver": "localhost",
                    "cid": "1234567890",
                    "database": cls.database,
                    "backup": False,
                    "data": "own",
                    "online": True,
                    "check_sfcu": True,
                    "check_mhu": False
                    }

        dbs = dbmanager.DbSetUp(cls.database)
        dbs.create_schema('cell')
        dbs.create_masterdata('cell')
        dbs.create_testdata('cell')

        super(StationMethods, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database)
        super(StationMethods, cls).tearDownClass()

    def setUp(self):
        # self.stnmod = StationModel(StationStates, self.conf, "mqclient")
        self.stnmod = TestStation(self.conf, "mqclient")
        self.stnmod.dao.database = self.database

    def tearDown(self):
        pass

    def testAbstraction(self):
        """StationModel may not instantiated directly"""
        with self.assertRaises(TypeError):
            _ = StationModel(self.conf, "mqclient")

    def testInitStationModel(self):
        """Check member variables."""
        self.assertEqual(self.stnmod.org, "station")
        self.assertEqual(self.stnmod.cmpt, "stn")
        # self.assertEqual(self.stnmod.states, StationStates)
        self.assertEqual(self.stnmod.site, "FUAS")
        self.assertEqual(self.stnmod.area, "AREA-1")
        self.assertEqual(self.stnmod.cell, "CELL-1")
        self.assertEqual(self.stnmod.station, "CUTCO1")
        self.assertTrue(isinstance(self.stnmod.dao, DataAccess))
        self.assertEqual(self.stnmod.mqclient, "mqclient")
        self.assertEqual(self.stnmod.sender, "FUAS/AREA-1/CELL-1/CUTCO1")

    @unittest.skip("Not implemented")
    def testPublishStateChange(self):
        pass

    @unittest.skip("Not implemented")
    def testGetSetup(self):
        pass

    @unittest.skip("Not implemented")
    def testIsSetUp(self):
        pass

    @unittest.skip("Not implemented")
    def testFinalizeSetUp(self):
        pass

    @unittest.skip("Not implemented")
    def testNotRunning(self):
        pass

    @unittest.skip("Not implemented")
    def testPrepareUnloading(self):
        pass

    @unittest.skip("Not implemented")
    def testInitialize(self):
        pass

    @unittest.skip("Not implemented")
    def testPrepareStandby(self):
        pass

    @unittest.skip("Not implemented")
    def testPrepareSetUp(self):
        pass

    @unittest.skip("Not implemented")
    def testPrepareOperation(self):
        pass

    @unittest.skip("Not implemented")
    def testPrepareDone(self):
        pass

    @unittest.skip("Not implemented")
    def testPrepareHalt(self):
        pass

    @unittest.skip("Not implemented")
    def testPrepareStop(self):
        pass

    @unittest.skip("Not implemented")
    def testOnException(self):
        pass

    @unittest.skip("Not implemented")
    def testRunInitialize(self):
        pass

    @unittest.skip("Not implemented")
    def testAwaitLoading(self):
        pass

    @unittest.skip("Not implemented")
    def testRunSetup(self):
        pass

    @unittest.skip("Not implemented")
    def testRunOperation(self):
        pass

    @unittest.skip("Not implemented")
    def testAwaitUnloading(self):
        pass

    @unittest.skip("Not implemented")
    def testAwaitDecission(self):
        pass

    def testGetExpectedSfcu(self):
        """Retrieve a list of expected sfcus."""
        sfculist = self.stnmod.get_expected_sfcu()
        self.assertEqual(sfculist, [1, 2])

    @unittest.skip("Not implemented")
    def testGetSfcusByState(self):
        pass

    def testGetNextLoadableSfcu(self):
        sfcu, state = self.stnmod.get_next_loadable_sfcu()
        self.assertEqual(sfcu, 1)
        self.assertEqual(state, 'ASSIGNED')

    def testGetSfcuState(self):
        """Retrieve a list of expected sfcus by mhu."""
        sfcu, state = self.stnmod.get_sfcu_state(115, typ='mhu')
        self.assertEqual(sfcu, 2)
        self.assertEqual(state, 'ASSIGNED')

        sfcu, state = self.stnmod.get_sfcu_state(2)
        self.assertEqual(sfcu, 2)
        self.assertEqual(state, 'ASSIGNED')

        sfcu, state = self.stnmod.get_sfcu_state(1, typ='mhu')
        self.assertEqual(sfcu, None)
        self.assertEqual(state, None)

    @unittest.skip("Not implemented")
    def testGetSfcuSyncState(self):
        pass

    @unittest.skip("Not implemented")
    def testComposeSequence(self):
        pass

    def testGetSetupBySfcu(self):
        """Get a task for a given sfcu-id."""
        expected_setup = {'operation': '12-CUTCO',
                          'parameter': {
                              'geom': 'A',
                              'setup': 15,
                              'col': 'red'}}

        self.assertEqual(self.stnmod._get_setup_by_sfcu('1'),
                         expected_setup)

    @unittest.skip("Not implemented")
    def testRecoverSfcuErrors(self):
        pass

    @unittest.skip("Not implemented")
    def testSfcu(self):
        pass

    def testUpdateSchedule(self):
        """Update the state for a single schedule"""
        self.stnmod._update_schedule(sfcu='2', state='8', statx='DONE')
        result = self.stnmod.dao.retrieve("schedule", columns=["site", "order"], conditions={'state': 8})
        self.assertEqual([dict(r) for r in result], [{'site': 'FUAS', 'order': 1000021}])


if __name__ == "__main__":

    unittest.main()
