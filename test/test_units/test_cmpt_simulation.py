"""
Created on 06.08.2024

@author: Ralf

Copyright and License Notice:
    flecsimo cmpt.simulation unit tests.
    Copyright (C) 2024  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
from  pathlib import Path
from flecsimo.cmpt import simulation


class TestSiteManager(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__)  # loader independent path
        super(TestSiteManager, cls).setUpClass()

    @classmethod
    def start_cmpt_mpatch(cls, config_file):
        print(config_file)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    # @unittest.skip
    def test_init_plain(self):
        sim_mgr = simulation.SimManager(self.testroot)
        del sim_mgr

    # @unittest.skip
    def test_init_fails(self):
        # Case 1: no root is given
        with self.assertRaises(TypeError) as t1:
            simulation.SimManager()
        self.assertEqual(t1.exception.args[0], 
                         "__init__() missing 1 required positional argument: 'root'" )
        
        # Case 2: setup oints to missing file
        with self.assertRaises(FileNotFoundError):
            simulation.SimManager(self.testroot, setup="missing.json")

    # @unittest.skip
    def test_init_custom_setup_success(self):
        """Test processing of custom setup configs.
        
        Test __init__() method.
        
        Tests:
            1. Instantiation with custom filename.
            2. setup_dict contains custom data.
            3. Providing a path will adress path directly.
        """
        # Test case 1:
        sim_mgr = simulation.SimManager(self.testroot,
                                        setup="simulation_custom_setup.json")
        # Test case 2:
        self.assertTrue("custom_setup" in sim_mgr.setup_dict)

        del sim_mgr

        # Test case 3:
        with self.assertRaises(FileNotFoundError):
            simulation.SimManager(self.testroot, setup="/a/NOTEXISTANT.json")

    # @unittest.skip
    def test_get_simulation_setup_list(self):
        """Test parsing of simulation setup.
        
        Test get_simulation_setup_list()
        
        Tests:
            1. Check correct parsing of simulation setup.
        """

        # Test case 1:
        expected_list = [{'value': 'simple_simulation',
                          'name': 'SimpleSim',
                          'description':
                          'Simple setup consisting of site, area and two cells.'},
                         {'value': 'complex_setup',
                          'name': 'chaotic Expert',
                          'description': "Don't worry, be happy!"}]

        sim_mgr = simulation.SimManager(self.testroot)

        self.assertEqual(sim_mgr.get_simulation_setup_list(), expected_list)

        # Tear down
        del sim_mgr

    # @unittest.skip
    def test_start_simulation(self):
        """Test startup wrapper.
        
        Test start_simulation() w/o call of start_cmpt() (monkey patched)
        
        Tests:
            1. Run a known active setup.
            2. Run an unknown active setup.
        """

        sim_mgr = simulation.SimManager(self.testroot)
        sim_mgr.start_cmpt = self.start_cmpt_mpatch

        # test case 1
        sim_mgr.active_setup_name = "simple_simulation"
        self.assertTrue(sim_mgr.start_simulation())

        # test case 2
        sim_mgr.active_setup_name = "NOTEXISTANT"
        self.assertEqual(sim_mgr.start_simulation(), "no setup found/active")

        # Tear down
        del sim_mgr

    @unittest.skip
    def test_start_cmpt(self):
        pass

    @unittest.skip
    def test_stop_simulation(self):
        pass

    @unittest.skip
    def test_get_cmpt_status(self):
        pass


class TestStationSim(unittest.TestCase):
    """Test the station simulation framework.
    
    TODO: implement.
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @unittest.skip
    def test_run_initialize(self):
        pass


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
