"""Unit test for cmd functions.
 
Created on 07.10.2021

@author: Ralf Banning

Copyright and License Notice:
    flecsimo sfc processing
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import cmd
import unittest
from flecsimo.app import cli

# from memory_profiler import profile


class CmdClass(cmd.Cmd):

    def __init__(self):
        super().__init__(completekey='TAB')

    def do_test1(self, arg):
        return arg

    def do_test2(self, arg):
        return cli.parse(arg)

    def do_test3(self, arg):
        return cli.parse(arg, subcmd=True)

    def do_test4(self):
        return cli.parse()

    def do_test5(self, arg):
        return cli.parse(arg, required=True)


class TestCmdParser(unittest.TestCase):

    def setUp(self):
        self.cmd = CmdClass()

    def testParseKeywordArgs(self):
        # Parantheses are not required
        self.assertEqual(self.cmd.onecmd("test1((a=1))"), '((a=1))')
        self.assertEqual(self.cmd.onecmd("test2((a=1))"), {'a': '1'})
        self.assertEqual(self.cmd.onecmd("test2(a=1)"), {'a': '1'})

        # Single keyword parameter without whitespace
        self.assertEqual(self.cmd.onecmd("test1(a=1)"), '(a=1)')
        self.assertEqual(self.cmd.onecmd("test2(a=1)"), {'a': '1'})

        # Single keyword parameter with whitespace
        self.assertEqual(self.cmd.onecmd("test1(a = 1)"), '(a = 1)')
        self.assertEqual(self.cmd.onecmd("test2(a = 1)"), {'a': '1'})

        # List of keyword parameters without whitespace
        self.assertEqual(self.cmd.onecmd("test1(a=1,b=2)"), '(a=1,b=2)')
        self.assertEqual(self.cmd.onecmd("test2(a=1,b=2)"), 
                                         {'a': '1', 'b': '2'})

        # List of keyword parameters with whitespace
        self.assertEqual(self.cmd.onecmd("test1(a=1, b=2)"), '(a=1, b=2)')
        self.assertEqual(self.cmd.onecmd("test2(a=1, b=2)"), 
                                         {'a': '1', 'b': '2'})

        # List of keyword parameters / mixed whitespace
        self.assertEqual(self.cmd.onecmd("test1(a = 1 ,b=2)"), '(a = 1 ,b=2)')
        self.assertEqual(self.cmd.onecmd("test2(a = 1 ,b=2)"), 
                                         {'a': '1', 'b': '2'})

        # Using simple quotes
        self.assertEqual(self.cmd.onecmd("test2(a='Text1', b=2)"), 
                                         {'a': 'Text1', 'b': '2'})

        # Using double quotes
        self.assertEqual(self.cmd.onecmd("""test2(a="Text1", b=2)"""), 
                                         {'a': 'Text1', 'b': '2'})

        # No args
        self.assertEqual(self.cmd.onecmd("test2()"), {})

        # Missing arguments brackets for test2:
        self.assertEqual(self.cmd.onecmd("test2"), {})
        
    def testParseSubcommandArgs(self):
        # Single sub command
        self.assertEqual(self.cmd.onecmd("test1(mycmd)"), '(mycmd)')
        self.assertEqual(self.cmd.onecmd("test3(mycmd)"), {'subcmd': 'mycmd'})
        
        # Sub command with following key words arguments        
        self.assertEqual(self.cmd.onecmd("test1(mycmd opt1=1, opt2='zeta')"), 
                                         "(mycmd opt1=1, opt2='zeta')")
        self.assertEqual(self.cmd.onecmd("test3(mycmd opt1=1, opt2='zeta')"), 
                                        {'subcmd': 'mycmd',
                                         'opt1': '1',
                                         'opt2': 'zeta'})
   
    def testParseMalformedArgs(self):

        # Non keyword first parameter
        with self.assertRaises(ValueError) as ve:
            self.cmd.onecmd("test2(1, b=2)")
        self.assertEqual(ve.exception.args, ('Malformed arguments: (1, b=2)',))

        # Non keyword last parameter
        with self.assertRaises(ValueError) as ve:
            self.cmd.onecmd("test2(a=1, b)")
        self.assertEqual(ve.exception.args, ('Malformed arguments: (a=1, b)',))

        # Missing list separator
        with self.assertRaises(ValueError) as ve:
            self.cmd.onecmd("test2(a=1 b=2)")
        self.assertEqual(ve.exception.args, ('Malformed arguments: (a=1 b=2)',))

        # Using parse for function having no arguments (test3)
        with self.assertRaises(TypeError):
            self.cmd.onecmd("test4")

        # Missing list separator for test3
        with self.assertRaises(ValueError) as ve:
            self.cmd.onecmd("test5")
        self.assertEqual(ve.exception.args, ('No arguments provided although required.',))


if __name__ == "__main__":
    unittest.main()
