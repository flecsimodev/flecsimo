#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""API-tests for main-simulation-setup-endpoints of flask-webserver 
Created on 13.01.2022

@author: Leon Schnieber

Copyright and License Notice:

    flecsimo app.web.flask.simulation_handler unit tests.
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
import os
import time
from flask import Flask
from flecsimo.app.web.api import web_backend_simulation_handler
from flecsimo.base import auth
from flecsimo.cmpt import simulation
from flecsimo.db import dbmanager

class SimulationHandler(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        # set up auth-environment

        cls.config_folder = os.path.join(os.getcwd(), "../data/")
        
        # set up databases
        cls.dbs = {}
        for _ in ["site", "area", "cell"]:
            cls.dbs[_] = dbmanager.DbSetUp(cls.config_folder + f"test-{_}.db")
            cls.dbs[_].create_schema(_)
            cls.dbs[_].create_data("test", _)


        cls.username = "john_doe"
        cls.admin_username = "admin"
        cls.password = "foobar"
        cls.admin_password = "bazbar"
        cls.session_key = "sessionHash"
        cls.admin_session_key = "AdminSessionHash"


        super(SimulationHandler, cls).setUpClass()
        
    @classmethod
    def tearDownClass(cls):
        for _ in ["site", "area", "cell"]:
            os.remove(os.path.join(cls.config_folder, f"test-{_}.db"))
        super(SimulationHandler, cls).tearDownClass()    

    def setUp(self):
        
        self.auth = auth.Auth(login_data_path=self.config_folder + "testdata_auth_logins.json")

        # set up simulation-environment
        sim_path = self.config_folder + "../data/testdata_simulation_setup.json"
        self.sim = simulation.SimManager(path=sim_path)
        
        # start up a minimal flask-instance for module-simulation
        resources_folder = os.path.join(os.path.dirname(web_backend_simulation_handler.__file__), "../")
        self.flask_app = Flask("test-app", template_folder=resources_folder + "www-template", static_folder=resources_folder + "www-static")
        
        self.module = web_backend_simulation_handler.flask_backend_simulation(self.flask_app, self.auth, sim=self.sim)
        

    def tearDown(self):
        pass
    
    @unittest.skip("Not implemented")
    def testGetUnitByName(self):
        unit = "test_sim"
        self.assertIn("test_sim", unit)

    def testBaseSimulation(self):
        with self.flask_app.test_client() as client:
            client.set_cookie("*","flasksimo-session", self.admin_session_key)
            url = "/api/simulation"
            response = client.get(url, follow_redirects=True)
            self.assertIn("active_setup", response.json)
            self.assertIn("setups", response.json)
            self.assertEqual(response.json["active_setup"], "")
            self.assertEqual("test_sim", response.json["setups"][0]["value"])

    def testSetSimulationScene(self):
        with self.flask_app.test_client() as client:
            client.set_cookie("*","flasksimo-session", self.admin_session_key)
            # start session
            url = "/api/simulation/set_scene?scene=test_sim"
            response = client.post(url, follow_redirects=True)
            self.assertEqual(response.json, True)

            # check if session is marked as the 'active_setup'
            url = "/api/simulation"
            response = client.get(url, follow_redirects=True)
            self.assertEqual(response.json["active_setup"], "test_sim")
            self.assertIn("test_config_site.json", response.json["units"])

    def testKillSimulationProcess(self):
        self.testSetSimulationScene() # run the process-setup because its resetted for every check
        unit_list = list(self.sim.sim_cmpt.keys())
        with self.flask_app.test_client() as client:
            client.set_cookie("*","flasksimo-session", self.admin_session_key)

            url = f"/api/simulation/{unit_list[0]}"
            response = client.delete(url, follow_redirects=False)
            state = str(self.sim.sim_cmpt[unit_list[0]]["controller"].agentstate.state)
            self.assertEqual(state, "AgentStates.STOPPED")

    def testLogSimulationProcess(self):
        self.testSetSimulationScene() # run the process-setup because its resetted for every check
        unit_list = list(self.sim.sim_cmpt.keys())
        with self.flask_app.test_client() as client:
            client.set_cookie("*","flasksimo-session", self.admin_session_key)

            url = f"/api/simulation/{unit_list[0]}/log"
            response = client.get(url, follow_redirects=False)
            self.assertTrue((type(response.json) == type([])))

    def testStopSimulationScene(self):
        self.testSetSimulationScene()
        with self.flask_app.test_client() as client:
            client.set_cookie("*","flasksimo-session", self.admin_session_key)

            url = f"/api/simulation/stop_scene"
            response = client.post(url, follow_redirects=False)
            self.assertTrue(True) # TODO - implement test and functionality correctly 
        

    
if __name__ == "__main__":

    unittest.main()