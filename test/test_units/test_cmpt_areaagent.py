"""Unit tests for cmpt.areaagent module.

Created on 27.01.2022

@author: Ralf Banning
"""

"""
    Copyright and License Notice:

    flecsimo area agent unit tests
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import pathlib
import sqlite3
import sys
import unittest


from flecsimo.base import dao
from flecsimo.util import dbmanager
from flecsimo.cmpt import areaagent

class AreaAgentMethods(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.testpath = (pathlib.Path(__file__).parent.resolve())
        print("Local path of test is:", cls.testpath)
        cls.dbpath = cls.testpath.parent.joinpath("data/tmp").resolve()
        #cls.dbpath = pathlib.Path("../data/tmp").resolve()
        print("Database will be created in:", cls.dbpath)
        cls.database = "test_cmpt_arreaagent.db"
        cls.db_abs_path = cls.dbpath / cls.database
        cls.conf = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": cls.db_abs_path}
        cls.site = cls.conf['site']
        cls.area = cls.conf['area']
        dbs = dbmanager.DbSetUp(cls.dbpath.joinpath(cls.database))
        dbs.create_schema('area')
        dbs.create_masterdata('area')
        dbs.create_testdata('area')
        
        logging.basicConfig(level="DEBUG", stream=sys.stdout)       
        super(AreaAgentMethods, cls).setUpClass()
        
    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database)
        super(AreaAgentMethods, cls).tearDownClass()         

    def setUp(self):        
        """Setup up a test fixture.
        
        This set up creates an areaagent instance, patching the data base path
        in the config module before. In standard operation, an areaagent expects
        the database located in ../db, but here we use the temporary db path 
        from setUpClass, local at unit test path.
        """
        self.agent = areaagent.AreaAgent(conf=self.conf)
        
        # TODO: check why commented out.
        #self.agent.start_agent()
        self.dao = dao.DataAccess(self.dbpath.joinpath(self.database))

    def tearDown(self):
        #self.agent.stop_agent()
        teardown_script = """
            BEGIN TRANSACTION;
            COMMIT;
        """
        self.conn = sqlite3.connect(self.database)
        self.conn.execute("PRAGMA foreign_keys=1")
        with self.conn:
            self.conn.executescript(teardown_script)
        self.conn.close()
        
    #@unittest.skip('Test not implemented')
    def test_AutoAsgmtProcessing(self):
        """Test _auto_asgmt_processing method.
        
        This method overrides the roles.requesthandler (empty) method of the
        same name. This function will be called if a an mqtt agent receives
        an asgmt message abd triggers the registered on_message handler for this
        message (which is normally is roles.requesthandler). 
        
        This test requires:
        - opdta for the tested order has been received and processed before, 
          including order, sfcu, task, part and resources.
        - The sfcu(s) for the test order should be in statx "RELEASED", and no
          operation related schedules should be present for this order.  
        - a schedule for the tested order number has been persisted on base of
          the assignment, i. e. operation is "ALL" and supplier is "area1" in this case. 

        Test cases:
        
        1. Assert test data in database meets requirements.
        2. Test assignment processing for an order with one sfcu; should yield
           four additional schedules (one for each operation).
    
        """
        TEST_ORDER=1000020
        
        #=======================================================================
        # # Test case 1
        # r1 = self.dao.retrieve('sfcu', conditions={'order': TEST_ORDER})
        # self.assertEqual(len(r1), 1, "Exactly on sfcu should be found.")
        # self.assertEqual(r1[0]['statx'], "RELEASED") 
        #=======================================================================
        
        #=======================================================================
        # r2 = self.dao.retrieve('schedule', conditions={'order': TEST_ORDER})
        # self.assertEqual(len(r2), 1, "Exactly on schedule should be found.")
        # self.assertEqual(r2[0]['operation'], "ALL") 
        # self.assertEqual(r2[0]['supplier'], self.conf['area1'])
        #=======================================================================
        
        # Test case 2
        self.agent._auto_asgmt_processing(site='FUAS',
                                          sfcu=1, 
                                          order=TEST_ORDER)
        #=======================================================================
        # r3 = self.dao.retrieve('schedule', conditions={'order': TEST_ORDER})
        # self.assertEqual(len(r3), 6, "Expect schedule for each operation+ALL.")      
        #=======================================================================

        
    @unittest.skip('Test not implemented')
    def test_AutoQuoteProcessing(self):
        self.agent._auto_quote_processing(site='FUAS', 
                                          order=1000065, 
                                          sfcu=6, 
                                          supplier=None)

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
