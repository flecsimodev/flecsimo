# codiung: utf-8
"""Unit tests for flecsimo.base.dao class.

Created on 16.09.2021

@author: Ralf Banning

Copyright and License Notice:
    flecsimo base.dao unit tests.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import datetime
import sqlite3
import unittest
import time
# from memory_profiler import profile
from pathlib import Path
from flecsimo.base import dao
from flecsimo.util import dbmanager
from flecsimo.base.states import ScheduleStates

LICENSE_TEXT = """

"""

# Constants
MODULVERSION_MIN = "2.6.0"
SQLITEVERSION_MIN = "3.27.2"
# correct input data
data_ok_1 = {'site': 'FUAS', 'material': 'ZZZ', 'qty': 2, 'unit': 'PCE',
             'at': '2021-09-16 21:18:32'}
data_ok_2 = {'id': None, 'site': 'FUAS', 'material': 'ZZZ', 'variant': None,
             'qty': 1, 'unit': 'PCE', 'pds': None, 'at': '2021-09-16 22:18:32'}

# faulty input data: wrong column name 'material1' / unknown column name 'blue'
data_fault_1 = {'material1': 'ZZZ', 'qty': 2, 'at': '2021-09-16 21:18:32'}
data_fault_2 = {'site': 'FUAS', 'material': 'ZZZ', 'qty': 2, 'unit': 'PCE',
                'at': '2021-09-16 21:18:32', 'blue': 'red' }
data_fault_3 = ('a', 'b')


class DaoEnvironment(unittest.TestCase):
    """Test case to verify SQLite environment."""

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.database = cls.testroot.joinpath("data", "tmp", "test_base_dao.db")
        dbs = dbmanager.DbSetUp(cls.database)
        dbs.create_schema('site')
        dbs.create_masterdata('site')
        dbs.create_testdata('site')
        cls.dao = None
        super(DaoEnvironment, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # cls.database.unlink(missing_ok=True)
        super(DaoEnvironment, cls).tearDownClass()

    def test_module_version(self):
        """Check python version of sqlite library."""
        mv = sqlite3.version
        self.assertGreaterEqual(mv, MODULVERSION_MIN, msg=f"Modul version is "
                            f"{mv} but should be at least {MODULVERSION_MIN}.")

    def test_sqlite_version(self):
        """Check sqlite version."""
        sv = sqlite3.sqlite_version
        self.assertGreaterEqual(sv, SQLITEVERSION_MIN, msg=f"SQLite version is "
                            f"{sv} but should be at least {SQLITEVERSION_MIN}.")
        print(f"SQLite version: {sv}")

    def test_pragmas(self):
        """Test important PRAGMAS for dao connections."""
        self.dao = dao.DataAccess(self.database)
        conn = self.dao.connect()

        # dao connect has to ensure that foreign keys enforcement is on.
        result = conn.execute('PRAGMA foreign_keys;').fetchone()
        conn.close()
        pfk = dict(result)['foreign_keys']
        self.assertEqual(pfk, 1)
        print(f"PRAGMA foreign_keys: {pfk}")


class DaoMethods(unittest.TestCase):
    """Tests case for dao methods.
    
    Test-fixture:
        Set up a standard flecsimo site database which will be (not be) deleted 
        after test run. For a better re-use of test data, specific inserts to 
        order table will be deleted after each test. 
    """

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.database = cls.testroot.joinpath("data", "tmp", "test_base_dao.db")
        super(DaoMethods, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database) # use, when tearDonwClass should remove database.
        super(DaoMethods, cls).tearDownClass()

    def setUp(self):
        """Create an sqlite dao_test database with an order table."""
        dbs = dbmanager.DbSetUp(self.database)
        dbs.create_schema('site')
        dbs.create_masterdata('site')
        dbs.create_testdata('site')
        self.dao = dao.DataAccess(self.database)

    def tearDown(self) -> None:
        # self.database.unlink(missing_ok=True)
        pass

    # @unittest.skip
    def test_connect(self):
        """Test dao database connection."""
        conn = self.dao.connect()
        self.assertIsInstance(conn, sqlite3.Connection)
        conn.close

    # @unittest.skip
    def test_create_tabledict(self):
        """Check table columns against data definition."""
        dbsetup = dbmanager.DbSetUp(self.database)
        self.assertEqual(self.dao.table_dict, dbsetup.create_table_dict())

    # @unittest.skip
    def test_get_insert_stmt(self):
        """Introspect the stmt builder and management for inserts.
        
        Test _get_insert_stmt() method.
         
        Tests:
            1. Make sure, no statement is stored in insert_statement list
               at beginning.
            2. A new retrieved statement should be stored on the list.
            3. Repeated retrieval of the same statement will not affect 
               the list (idempotent operation behavior).
            4. Retrieval of an other statement will add one more member to 
               the insert_statement list.
            At all reasonable stages: check whether stored statement matches 
            expected values.
        """
        test_insert_stmt_order = """INSERT INTO 'order' ("id","site","material","variant","qty","unit","pds","at") VALUES (:id,:site,:material,:variant,:qty,:unit,:pds,:at)"""
        test_insert_stmt_facility = """INSERT INTO 'facility' ("id","org","desc","loc","state","statx","at") VALUES (:id,:org,:desc,:loc,:state,:statx,:at)"""

        # Test 1: insert_statement list should be empty.
        self.assertEqual(len(self.dao.insert_statement), 0)

        # Test 2: retrieving an order insert statement ...
        self.assertEqual(test_insert_stmt_order,
                         self.dao._get_insert_statement('order'))

        # ... results in one entry in the insert_statement list.
        self.assertEqual(len(self.dao.insert_statement), 1)

        # Test 3: retrieving an other order insert statement ...
        self.assertEqual(test_insert_stmt_order,
                         self.dao._get_insert_statement('order'))

        # ... does not affect the list which contains the expected statement.
        self.assertEqual(len(self.dao.insert_statement), 1)
        self.assertEqual(test_insert_stmt_order,
                         self.dao.insert_statement['order'])

        # Test 4: same procedure for 'facility' results in two entries in insert_statement list.
        self.assertEqual(test_insert_stmt_facility,
                         self.dao._get_insert_statement('facility'))
        self.assertEqual(test_insert_stmt_facility,
                         self.dao.insert_statement['facility'])
        self.assertEqual(len(self.dao.insert_statement), 2)

    # @unittest.skip
    def test_get_columns_params(self):
        """Test composing sql column-name-lists and parameter lists from 
           list-like arguments.
         
        Test _compose_columns_placehld() method.
         
        Tests:
            1. Compose from a single-item tuple, list or dictionary.
            2. Compose from a multi-item tuple, list or dictionary.
            3. Exception handling for arguments with wrong type.
        """
        # Test 1:
        self.assertEqual(self.dao._compose_columns_placehld(
            ('col',)),
            ('("col")', '(:col)'))
        self.assertEqual(self.dao._compose_columns_placehld(
            ['col']),
            ('("col")', '(:col)'))
        self.assertEqual(self.dao._compose_columns_placehld(
            {'col': 'val'}),
            ('("col")', '(:col)'))
        # Test 2:
        self.assertEqual(self.dao._compose_columns_placehld(
            ('colx', 'coly')),
            ('("colx","coly")', '(:colx,:coly)'))
        self.assertEqual(self.dao._compose_columns_placehld(
            ['colx', 'coly']),
            ('("colx","coly")', '(:colx,:coly)'))
        self.assertEqual(self.dao._compose_columns_placehld(
            {'colx': 'valx', 'coly': 'valy'}),
            ('("colx","coly")', '(:colx,:coly)'))
        # Test 3:
        with self.assertRaises(TypeError) as te1:
            self.dao._compose_columns_placehld("astring")
        self.assertEqual(te1.exception.args, (
            "_compose_columns_placehld() expects 'keys' argument of type"\
            " 'list', 'tuple' or 'dict'.",))

    # @unittest.skip
    def test_change(self):
        """Change objects in db.
         
        Test change() method.
         
        Tests:
            1. Change date for two columns w.r.t. another columns value .
            2. Change value for a column also included in condition.
        """
        test_order = 1000020
        # Test 1:
        data = {'schd_start': '2021-07-06 12:30:00',
                'schd_end': '2021-07-10 17:30:00', }
        conditions = {'order': test_order}
        self.dao.change('order_plan', data, conditions=conditions)

        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row
        with conn:
            row = conn.execute(
                """select * from "order_plan" WHERE "order" = :order""",
                ({'order': test_order})
                ).fetchone()
        conn.close()

        # Test 2:
        self.assertEqual(row['schd_start'], '2021-07-06 12:30:00')
        self.assertEqual(row['schd_end'], '2021-07-10 17:30:00')

        data_2 = {'statx': 'blue'}
        conditions2 = {'statx': 'RELEASED'}
        self.dao.change('order_plan', data_2, conditions=conditions2)

        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row
        with conn:
            row = conn.execute('select * from "order_plan"').fetchone()
        conn.close()
        self.assertEqual(row['statx'], 'blue')

    # @unittest.skip
    def test_load_many(self):
        """Load data from a list of dictionaries.
         
        Test loadmany() method. This "load from a list of dicts" is used for 
        bulk insert of data into the flecsimo database. To gain more 
        flexibility and performance, the loadmany() method does not handle 
        connections or context managers by itself.
         
        Test:
            Load a list of three resp. two data sets and test that the 
            return values (rowcount of cursor) is equivalent.
        """
        conn = self.dao.connect()
        data_list_1 = [data_ok_2, data_ok_2, data_ok_2]
        data_list_2 = [data_ok_2, data_ok_2]
        with conn:
            rc1 = self.dao.loadmany(conn, 'order', data_list_1)
            rc2 = self.dao.loadmany(conn, 'order', data_list_2)
        conn.close

        self.assertEqual(rc1, 3)
        self.assertEqual(rc2, 2)

    @unittest.skip("to be implemented.")
    def test_load_many_on_conflict(self):
        """Load data from a list of dictionaries.
         
        Test loadmany() method. This "load from a list of dicts" is used for 
        bulk insert of data into the flecsimo database. To gain more 
        flexibility and performance, the loadmany() method does not handle 
        connections or context managers by itself.
         
        Test:
            Load a list of three resp. two data sets and test that the 
            return values (rowcount of cursor) is equivalent.
        """
        conn = self.dao.connect()
        data_list_1 = [data_ok_2, data_ok_2, data_ok_2]
        data_list_2 = [data_ok_2, data_ok_2]
        with conn:
            rc1 = self.dao.loadmany(conn, 'order', data_list_1)
            rc2 = self.dao.loadmany(conn, 'order', data_list_2)
        conn.close

        self.assertEqual(rc1, 3)
        self.assertEqual(rc2, 2)        

    # @unittest.skip
    def test_persist_non_strict(self):
        """Persist a single order record in non strict mode.
         
        Test persist() method in non strict mode. Data should be 
        inserted even if an additional unknown column is addressed - 
        the unknown column data should be ignored, but an sqlite3 
        exceptions should be raise in case of integrity issues.
         
        Tests:
            1. The data_fault_2 set contains an unknown column - should be
               inserted in non strict mode.
            2. The data 2 contains an misspelled column (material1)
               and should fail, since a NOT NULL constraint on the
               material column is set (sqlite3 Integrity violation).
            3. The data 4 is not mappable at all... Check if this raises
               a qualified TypeError exception.              
        """
        # Test 1:
        rc = self.dao.persist("order", data_fault_2)
        self.assertTrue(rc['rowid'] > 0)
        self.assertEqual(rc['rowcount'], 1)

        # Test 2:
        with self.assertRaises(sqlite3.IntegrityError):
            self.dao.persist("order", data_fault_1)

        # Test 3:
        with self.assertRaises(TypeError) as te:
            self.dao.persist("order", data_fault_3)
        self.assertEqual(te.exception.args,
                         ("Failed to prepare data:('a', 'b') for table order",))

    # @unittest.skip
    def test_persist_strict(self):
        """Persist a single order record in non strict mode.
         
        Test persist(..., strict=True) method. Insert should be successful 
        if and only if the addressed columns are a subset of the known columns 
        in the table definition.
         
        Tests:
            1. Positive test with data_ok_1
            2. The data_fault_1 columns (keys) are no subset of the table columns.
               Should raise a qualified ValueExcpetion.
            3. The data_fault_2 contains a unknown column ('blue'). Although this
               will be inserted in non strict mode, here it should raise a 
               qualified ValueExcpetion.
        """
        # Test 1:
        rc = self.dao.persist("order", data_ok_1, strict=True)
        self.assertTrue(rc['rowid'] > 0)
        self.assertEqual(rc['rowcount'], 1)

        # Test 2:
        with self.assertRaises(ValueError) as ve1:
            self.dao.persist("order", data_fault_1, strict=True)
        self.assertEqual(ve1.exception.args, (
            "Data columns are not a subset of table columns. "\
            "Data is: {'material1': 'ZZZ', 'qty': 2, 'at': "\
            "'2021-09-16 21:18:32'}",))

        # Test 3:
        with self.assertRaises(ValueError) as ve2:
            self.dao.persist("order", data_fault_2, strict=True)
        self.assertEqual(ve2.exception.args, (
            "Data columns are not a subset of table columns. "\
            "Data is: {'site': 'FUAS', 'material': 'ZZZ', 'qty': 2, 'unit': 'PCE', 'at': '2021-09-16 21:18:32', 'blue': 'red'}",))

    # @unittest.skip
    def test_persist_on_table_constraints(self):
        """Insert data that violates table constraints.
         
        Tests:
            1. Insert a compliant data set.
            2. Inserting the same data set fails. 
        """
        test_order=1000026
        # Test 1:
        data_con = {'site': 'FUAS', 'order': 1000090, 'sfcu': 11, 'operation': 'ALL', 'supplier': 'NODATA'}
        rc = self.dao.persist('schedule', data_con)
        self.assertEqual(dict(rc), {'rowid': 11, 'rowcount': 1})

        # Test 2:
        with self.assertRaises(sqlite3.IntegrityError) as ie1:
            self.dao.persist('schedule', data_con)
        self.assertEqual(ie1.exception.args, ("UNIQUE constraint failed: schedule.order, schedule.sfcu, schedule.operation, schedule.supplier",))

    # @unittest.skip
    def test_persist_on_defaults(self):
        """Insert data relying on default values.
         
        The data_dflt does no contain values for the operation and supplier 
        columns, which are defined as NOT NULL DEFAULT 'NODATA'. The new row 
        should contain the default values. 
         
        Note:
            The default values defined in the database are fetched by the dao
            library during object initialization by calling 
            _create_table_dict(). 
         
        Test:
            1. Minimal data set should be insertable.
            2. Database row should contain the default values.
 
        """
        test_order = 1000026
        # Test 1:
        data_dflt = {'site': 'FUAS', 'order': test_order, 'sfcu': 11}
        result = self.dao.persist('schedule', data_dflt)
        self.assertEqual(result, {'rowid': 11, 'rowcount': 1})

        # Test 2:
        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row
        with conn:
            row = conn.execute(
                "select operation, supplier from schedule where id=:id",
                {'id': result['rowid']}).fetchone()
        conn.close()

        self.assertEqual(dict(row), {'operation': 'NODATA', 'supplier': 'NODATA'})

    # @unittest.skip
    def test_persist_with_upsert(self):
        """Test ON CONFLICT situations handled by an UPSERT clause
        
        Test persist(..., upsert=[...]) conflict handling.
        
        Tests:
            1. Test the current schedule situation - all prio values are 0.
            2. Conflict on existing schedule primary key 'id' should not result 
               in a new row but in one row affected; proved by values of result 
               dictionary.
            3. The database row should contain an updated variant value 
               'col:blue'.
            4. Conflict on schedule UNIQUE constraint on "site", "order", 
               "sfcu", "operation" and "supplier" should not result in a new 
               row but in one row affected; proved by values of result 
               dictionary.
            5. The database should contain an updated prio value 1.
            6. Assert, upsert list which are no real subset of data keys will
               raise an exception.
        """
        test_order = 1000020
        # Test 1:
        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row
        with conn:
            row = conn.execute(
                'select max(prio) as prio from "schedule"').fetchone()
        conn.close()
        self.assertEqual(row['prio'], 0)

        # Test 2:
        order_data = {'id': test_order, 'site': 'FUAS', 'material': 'FXF-1100',
                      'qty': 1, 'unit': 'PCE', 'variant': 'col:blue',
                      'at': '2021-12-24'}
        order_upsert = ['id']
        result = self.dao.persist('order', order_data, upsert=order_upsert)

        # No row inserted, but one row affected (updated)
        self.assertEqual(result, {'rowid': 0, 'rowcount': 1})

        # Test 3
        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row
        with conn:
            row = conn.execute('select "variant" from "order"').fetchone()
            self.assertEqual(row['variant'], 'col:blue')
        conn.close()

        # Test 4:
        schedule_data = {'site': 'FUAS', 'order': test_order, 'sfcu': 1,
                         'operation': 'ALL', 'supplier': 'AREA-1',
                         'prio': 1}
        schedule_upsert = ["order", "sfcu", "operation", "supplier"]

        result_4 = self.dao.persist('schedule',
                             schedule_data, upsert=schedule_upsert)

        # No row inserted, but one row affected (updated)
        self.assertEqual(result_4, {'rowid': 0, 'rowcount': 1})

        # Test 5
        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row
        with conn:
            row = conn.execute(
                'select max(prio) as prio from "schedule"').fetchone()
            self.assertEqual(row['prio'], 1)
        conn.close()

        # Test 6
        param_data_1 = {'order': test_order, 'site': 'FUAS'}
        param_data_2 = {'order': test_order, 'site': 'FUAS', 'typ': 'test',
                        'value': 'test2'}

        upsert = ["order", "typ"]

        with self.assertRaises(ValueError) as ve:
            self.dao.persist('order_parameter',
                              data=param_data_1,
                              upsert=upsert)

        self.assertEqual(ve.exception.args,
                         ('Upsert target must be a subset of data columns.',))

        self.dao.persist('order_parameter',
                              data=param_data_2,
                              upsert=upsert)

        result = self.dao.persist('order_parameter',
                              data=param_data_2,
                              upsert=upsert)
        self.assertEqual(result, {'rowid': 0, 'rowcount': 1})

    # @unittest.skip
    def test_retrieve_all(self):
        """Retrieve all order records.
         
        Test the 'plain' retrieve(table) method - get all records. 
        """
        expected_rows = 9
        rows = self.dao.retrieve('order')
        self.assertEqual(len(rows), expected_rows,
                         "Number of retrieved rows is unexpected.")
        # For debug only
        # for r in rows: print(f"Retrieve all: {dict(r)}")

    # @unittest.skip
    def test_retrieve_by_condition(self):
        """Retrieve a conditional set of order records.
         
        Test retrieve(..., conditions=...) conditional selection.
        """
        expected_rows1 = 5
        rows1 = self.dao.retrieve('order',
                                  conditions={'variant': 'col:blue'})
        self.assertEqual(len(rows1), expected_rows1,
                         "Number of retrieved rows is unexpected.")
        # For debug only
        # for r in rows: print(f"Retrieve by condition: {dict(r)}")

        expected_rows2 = 4
        rows2 = self.dao.retrieve('task',
                                  conditions={'operation': '27-DRILL',
                                              'param_typ': 'diam',
                                              'param_value': 15})
        self.assertEqual(len(rows2), expected_rows2)

    # @unittest.skip
    def test_retrieve_columns(self):
        """Retrieve a columns-subset only.
                
        Test retrieve(..., columns=...) conditional selection.
        """
        test_order = 1000021
        rows = self.dao.retrieve('order',
                                 columns=['id', 'material', 'variant'],
                                 conditions={'id': test_order}
                                )

        self.assertEqual([dict(r) for r in rows],
                         [{'id': test_order, 'material': 'FUAS-1100',
                           'variant': 'col:blue'}])

    # @unittest.skip
    def test_retrieve_in_list(self):
        """Retrieve a columns-subset only.
                 
        Test retrieve(..., range=...) conditional selection.
        """
        valuelist = (1000020, 1000027)
        rows = self.dao.retrieve('order',
                                 columns=['id', 'material', 'variant'],
                                 inlist={'id': valuelist}
                                )

        self.assertEqual([dict(r) for r in rows],
                         [{'id': 1000020, 'material': 'FUAS-1100',
                           'variant': 'col:red'},
                          {'id': 1000027, 'material': 'FUAS-1100',
                           'variant': 'col:blue'}])

        rows = self.dao.retrieve('order',
                                 columns=['id', 'material', 'variant'],
                                 conditions={'id': 1000027},
                                 inlist={'id': (1000020, 1000027)}
                                )

        self.assertEqual([dict(r) for r in rows],
                         [{'id': 1000027,
                           'material': 'FUAS-1100',
                           'variant': 'col:blue'}])

    def test_retrieve_null_selectors(self):
        """Retrieve data with IS NULL or IS NOT NULL where clause.
        
        Test retrieve(..., null={...})
        """
        row = self.dao.retrieve('pds_task',
                                 null={'variant': 'NULL'},
                                 single=True)


        self.assertEqual(dict(row),
                         {'id': 1, 'pds': 1, 'step': 1, 'next': 2, 
                          'typ': 'start', 'operation': '12-CUTCO', 
                          'variant': None, 'param_typ': 'geom', 
                          'param_value': 'A'})

    # @unittest.skip
    def test_retrieve_distinct(self):
        """Test retrieve(..., distinct=True) - fetch only one record.
         
        Tests:
            1. Select ALL gives 30 schedule rows (with duplicate order numbers).
            2. Select DISTINCT give only 17 disitinct rows. 
        """
        # Test 1:
        expected_all_rowcount = 10
        rows = self.dao.retrieve('schedule', columns=['order'])
        self.assertEqual(len(rows), expected_all_rowcount)

        # Tst 2:
        expected_distinct_rowcount = 6
        expected_order_list = [1000020, 1000021, 1000022, 1000023, 1000024,
                               1000025]
        rows = self.dao.retrieve('schedule', columns=['order'], distinct=True)
        self.assertEqual(len(rows), expected_distinct_rowcount)
        self.assertEqual([dict(r)['order'] for r in rows], expected_order_list)

    # @unittest.skip
    def test_retrieve_order_by(self):
        """Test retrieve(..., orderby=...) ordered selection
         
        Retrieve an ordered set of order records.
         
        Tests:
            1. Ordered selection should retrieve same number of records
               as plain select.
            2. Order of records should be as expected: expected_order contains
               the expected sequence of id's. 
        """
        expected_rows = 9
        expected_order = [1000021, 1000023, 1000025, 1000027, 1000028, 1000020,
                          1000022, 1000024, 1000026]
        rows = self.dao.retrieve('order', orderby='variant, id', single=False)

        # Test 1:
        self.assertEqual(len(rows), expected_rows, "Unexpected number of rows.")
        self.assertTrue(isinstance(rows, list))

        # Test 2:
        self.assertEqual([r['id'] for r in rows], expected_order)

    # @unittest.skip
    def test_retrieve_single(self):
        """Test retrieve(..., single=True) - fetch only one record.
         
        Tests:
            1. Returned data is of type sqlite3.Row
            2. Result is a single line, that matches the first row of the table.
             
        """
        expected_row = {'id': 1000020, 'site': 'FUAS', 'material': 'FUAS-1100',
                        'variant': 'col:red', 'qty': 1.0, 'unit': 'PCE',
                        'pds': 1, 'at': '2024-07-05 08:15:52.636603'}
        row = self.dao.retrieve('order', single=True)

        # Test 1:
        self.assertTrue(isinstance(row, sqlite3.Row))

        # Test 2:
        self.assertEqual(dict(row), expected_row)

    @unittest.skip("not implemented")
    def test_retrieve_special(self):
        """Test exotic situations."""
        # TODO: retrieve "non-existing" columns
        # TODO: retrieve ['a','b',] type lists
        pass

    # @unittest.skip('Not comparable to dao.')
    def test_retrieve_schedule_native(self):
        """Test plain SQL select."""
        select_schedule = """
            SELECT id, 
                "order", 
                sfcu, 
                ifnull(operation, 'N/A') AS operation, 
                ifnull(supplier,'N/A') AS supplier, 
                ifnull(due, 'N/A') as due, 
                ifnull(prio, 0) AS prio, 
                statx, 
                at  
            FROM schedule
            WHERE site = :site
            AND CASE
                    WHEN :order IS NULL THEN 1
                    ELSE "order" = :order
                END
            AND CASE
                    WHEN :state IS NULL THEN 1
                    ELSE state = :state
                END
        """
        site = 'FUAS'
        order = '1000060'
        state = 'TEST'

        conn = sqlite3.connect(self.database)
        conn.row_factory = sqlite3.Row

        with conn:
            cur = conn.execute(select_schedule, {'site': site, 'order': order,
                                                 'state': state })
            _ = cur.fetchall()

        conn.close()


class DaoPerfomance(unittest.TestCase):
    """Performance tests for dao operations."""

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.database = cls.testroot.joinpath("data", "tmp", "test_base_dao.db")
        dbs = dbmanager.DbSetUp(cls.database)
        cls.dao = None
        cls.site = None
        dbs.create_schema('site')
        dbs.create_masterdata('site')
        dbs.create_testdata('site')
        super(DaoPerfomance, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(DaoPerfomance, cls).tearDownClass()

    def setUp(self):
        """Create an sqlite dao_test database with an order table."""
        dbs = dbmanager.DbSetUp(self.database)
        dbs.create_schema('site')
        dbs.create_masterdata('site')
        dbs.create_testdata('site')
        self.dao = dao.DataAccess(self.database)

    def tearDown(self):
        # self.database.unlink(missing_ok=True)
        pass

    # @unittest.skip
    def test_persist_ten_native(self):
        """Insert 10 order records w/o using dao."""
        stmt = "INSERT INTO 'order' ('site', 'material', 'qty', 'at') " + \
               "VALUES (:site, :material, :qty, :at)"
        testdata = [data_ok_1,
                    data_ok_2,
                    data_ok_2,
                    data_ok_1,
                    data_ok_1,
                    data_ok_1,
                    data_ok_2,
                    data_fault_2,
                    data_ok_2,
                    data_ok_1, ]

        for data in testdata:
            conn = sqlite3.connect(self.database)
            with conn:
                conn.execute(stmt, data)
            conn.close()

    # @unittest.skip
    def test_persist_ten_with_dao(self):
        """persist 10 records - to compare with testNativeInsert unit test."""
        self.dao.persist("order", data_ok_1)
        self.dao.persist("order", data_ok_2)
        self.dao.persist("order", data_ok_2)
        self.dao.persist("order", data_ok_1)
        self.dao.persist("order", data_ok_1)
        self.dao.persist("order", data_ok_1)
        self.dao.persist("order", data_ok_2)
        self.dao.persist("order", data_fault_2)
        self.dao.persist("order", data_ok_2)
        self.dao.persist("order", data_ok_1)

    # @unittest.skip
    def test_change_ten_native(self):
        """Change 10 schedule records w/o using dao."""
        update_schedule_state = """
            UPDATE schedule
            SET state = :state,
                statx = :statx,
                at = :at
            WHERE site = :site
            AND sfcu = :sfcu"""

        self.site = 'FUAS'
        state = 2
        statx = 'PLANNED'
        at = datetime.datetime.utcnow()

        for sfcu in range(11, 20):
            conn = self.dao.connect()
            with conn:
                conn.execute(update_schedule_state, {"site": self.site,
                                               "sfcu": sfcu,
                                               "state": state,
                                               'statx': statx,
                                               'at': at})
            conn.close()

    # @unittest.skip
    def test_change_ten_with_dao(self):
        """Change 10 schedule records using dao."""
        for sfcu in range(11, 20):
            data = {'state': 0, 'statx': 'INITIAL',
                    'at': datetime.datetime.utcnow()}
            conditions = {'id': sfcu}
            self.dao.change('schedule',
                            data,
                            conditions)

    # @unittest.skip
    def test_select_in_list_native(self):
        """Native select with WHERE ... IN (...) clause."""
        vallist = ('CHECKED', 'INITIAL')

        stmt = """
            SELECT "order" FROM order_plan
            WHERE statx IN (:in1, :in2)"""

        parameters = dict(zip(['in1', 'in2'], vallist))

        tic = time.perf_counter()

        conn = self.dao.connect()
        conn.row_factory = sqlite3.Row

        with conn:
            cur = conn.execute(stmt, parameters)
            cur.fetchall()
        toc = time.perf_counter()
        print(f"Retrieve in list native: {toc - tic:0.6f} seconds")

    def test_select_in_list_with_dao(self):
        """Dao select with WHERE ... IN (...) clause."""
        vallist = ('CHECKED', 'INITIAL')

        tic = time.perf_counter()
        self.dao.retrieve("order_plan",
                          columns=["order"],
                          inlist={'statx': vallist})
        toc = time.perf_counter()
        print(f"Retrieve in list with dao: {toc - tic:0.6f} seconds")

    def test_compose_placehld_param_time(self):
        """Get time for placeholder creation."""
        tic = time.perf_counter()
        self.dao._compose_placehld_param(('a', 'b',
                                          ScheduleStates.ADVISED.name))
        toc = time.perf_counter()
        print(f"_compose_placehld_param needs: {toc - tic:0.6f} seconds")


if __name__ == "__main__":

    unittest.main()
