#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""Application tests for error-page module of flask-webserver 
Created on 09.01.2022

@author: Leon Schnieber

Copyright and License Notice:

    flecsimo sfc processing
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
import os
from flask import Flask
from flecsimo.app.web.api import web_frontend_handler
from flecsimo.base import auth
from flecsimo.cmpt import simulation


class AppFrontendHandler(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(AppFrontendHandler, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(AppFrontendHandler, cls).tearDownClass()

    def setUp(self):
        # set up auth-environment

        self.config_login_file_prod = os.path.join(os.path.dirname(auth.__file__), "../conf/logins.json")
        self.config_login_file_test = os.path.join(os.path.dirname(__file__), "../data/testdata_auth_logins.json")
        self.config_login_file_temp = os.path.join(os.path.dirname(__file__), "../data/testdata_auth_logins_temp.json")

        os.rename(self.config_login_file_prod, self.config_login_file_temp)
        os.rename(self.config_login_file_test, self.config_login_file_prod)

        self.username = "john_doe"
        self.admin_username = "admin"
        self.password = "foobar"
        self.admin_password = "bazbar"
        self.session_key = "sessionHash"
        self.admin_session_key = "AdminSessionHash"

        self.auth = auth.Auth()

        # set up simulation-environment
        self.sim = simulation.SimManager()

        # start up a minimal flask-instance for module-simulation
        resources_folder = os.path.join(os.path.dirname(web_frontend_handler.__file__), "../")
        self.flask_app = Flask("test-app", template_folder=resources_folder + "www-template", static_folder=resources_folder + "www-static")

        self.module = web_frontend_handler.flask_frontend(self.flask_app, self.auth, sim=self.sim)

    def tearDown(self):
        # undo auth-setup for testing
        os.rename(self.config_login_file_prod, self.config_login_file_test)
        os.rename(self.config_login_file_temp, self.config_login_file_prod)

    def testRoot(self):
        with self.flask_app.test_client() as client:

            # check root without login
            response = client.get("/")
            self.assertEqual(response.status_code, 302)

            # check root with session-cookie
            client.set_cookie("*", "flasksimo-session", self.session_key)
            response = client.get("/")
            self.assertEqual(response.status_code, 200)
            self.assertIn("<title>overview</title>", str(response.data))

    def testLogin(self):
        with self.flask_app.test_client() as client:
            # GET login-template
            response = client.get("/login")
            self.assertIn("<title>Login</title>", str(response.data))

            # POST check login with correct credentials
            response = client.post("/login", data={
                "username": self.username,
                "password": self.password
            }, follow_redirects=True)
            self.assertIn("<title>overview</title>", str(response.data))
            self.assertIn("hi, John Doe", str(response.data))

            response = client.post("/login", data={
                "username": self.username,
                "password": self.password
            })
            self.assertIn("Set-Cookie: flasksimo-session=", str(response.headers))

            # POST check login with correct credentials (and check if cookie is set)
            response = client.post("/login", data={
                "username": self.username,
                "password": "wrongPassword"
            }, follow_redirects=True)
            self.assertIn("invalid credentials", str(response.data))

    def testSimulationSetupTemplate(self):
        with self.flask_app.test_client() as client:
            # GET login-template without session-key
            response = client.get("/view/simulation_setup")
            self.assertEqual(response.status_code, 401)

            # GET login-template with session-key
            client.set_cookie("*", "flasksimo-session", self.session_key)
            response = client.get("/view/simulation_setup", follow_redirects=True)
            self.assertIn("simulation settings", str(response.data))

    def testSimulationSetupTemplate(self):
        with self.flask_app.test_client() as client:
            # GET login-template without session-key
            response = client.get("/view/simulation_setup")
            self.assertEqual(response.status_code, 401)

            # GET login-template with session-key
            client.set_cookie("*", "flasksimo-session", self.admin_session_key)
            response = client.get("/view/simulation_setup", follow_redirects=True)
            self.assertIn("simulation settings", str(response.data))

    def testDashboardTemplate(self):
        with self.flask_app.test_client() as client:
            # GET login-template without session-key
            response = client.get("/view/dashboard")
            self.assertEqual(response.status_code, 401)

            # GET login-template with session-key
            client.set_cookie("*", "flasksimo-session", self.session_key)
            response = client.get("/view/dashboard", follow_redirects=True)
            self.assertIn("dashboard", str(response.data))


if __name__ == "__main__":

    unittest.main()
