#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""API-tests for main-dashboard-endpoints of flask-webserver 
Created on 01.02.2022

@author: Leon Schnieber

Copyright and License Notice:

    flecsimo sfc processing
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
import os
import time
from flask import Flask
from flecsimo.app.web.api import web_backend_simulation_handler
from flecsimo.app.web.api import web_backend_dashboard_handler
from flecsimo.base import auth
from flecsimo.cmpt import simulation
from flecsimo.util import dbmanager

class DashboardHandler(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        # set up auth-environment

        cls.config_folder = os.path.join(os.getcwd(), "../data/")
        
        # set up databases
        cls.dbs = {}
        for _ in ["site", "area", "cell"]:
            cls.dbs[_] = dbmanager.DbSetUp(cls.config_folder + f"test-{_}.db")
            cls.dbs[_].create_schema(_)
            cls.dbs[_].create_testdata(_)


        cls.username = "john_doe"
        cls.admin_username = "admin"
        cls.password = "foobar"
        cls.admin_password = "bazbar"
        cls.session_key = "sessionHash"
        cls.admin_session_key = "AdminSessionHash"
        cls.unit_name = "sim_config_site"
        cls.cell_name = "sim_config_cell"


        super(DashboardHandler, cls).setUpClass()
        
    @classmethod
    def tearDownClass(cls):
        for _ in ["site", "area", "cell"]:
            os.remove(os.path.join(cls.config_folder, f"test-{_}.db"))
        super(DashboardHandler, cls).tearDownClass()    

    def setUp(self):
        self.auth = auth.Auth(login_data_path=self.config_folder + "testdata_auth_logins.json",
                              webserver_path=self.config_folder + "webserver.json")

        # set up simulation-environment
        sim_path = self.config_folder + "../data/testdata_simulation_setup.json"
        self.sim = simulation.SimManager(setup=sim_path)
        
        # start up a minimal flask-instance for simulation purposes
        resources_folder = os.path.join(os.path.dirname(web_backend_dashboard_handler.__file__), "../")
        self.flask_app = Flask("test-app", template_folder=resources_folder + "www-template", static_folder=resources_folder + "www-static")
        
        self.simulation_module = web_backend_simulation_handler.flask_backend_simulation(self.flask_app, self.auth, sim=self.sim)
        self.module = web_backend_dashboard_handler.flask_backend_dashboard(self.flask_app, self.auth, sim=self.sim)
        

    def tearDown(self):
        pass
    
    def flaskClientAuthed(self):
        """
        is used by the test-functions to setup the simulation-webclient and starts a simulation
        for later testing. Depends on passed tests in  test_app_web_flask_simulation_handler.py!
        """
        with self.flask_app.test_client() as client:
            client.set_cookie("*", "flasksimo-session", self.admin_session_key)
            
            url = "/api/simulation/set_scene?scene=test_sim"
            response = client.post(url, follow_redirects=True)

            client.set_cookie("*", "flasksimo-session", self.session_key)

            return client

    @unittest.skip("Not implemented")
    def testGetUnitByName(self):
        unit = "test_sim"
        self.assertIn("test_sim", unit)


    def testSiteCreateNewOrder(self):
        with self.flaskClientAuthed() as client:
            
            url = f"/api/dashboard/site/{self.unit_name}/new_order"
            data = {
                "site": "",
                "material": "",
                "qty": "2",
                "unit": "PCE",
                "variant": "red"
            }
            response = client.post(url, data=data, follow_redirects=True)
            self.assertIn("2", str(response.data)) 
    
    def testSiteGetOrderList(self):
        # check list for test-data
        with self.flaskClientAuthed() as client:
            url = f"/api/dashboard/site/{self.unit_name}/list_order"
            response = client.get(url, follow_redirects=True)
            self.assertEqual(type(response.json), type([]))
            self.assertEqual(response.json[0], {
                'id': 1000060,
                'qty': 1.0,
                'sfcu': [1],
                'site': 'FUAS',
                'state': 3,
                'statx': 'RELEASED',
                'unit': 'PCE',
                'variant': 'col:red'}
            )
    
    def testSiteGetScheduleList(self):
        with self.flaskClientAuthed() as client:
            url = f"/api/dashboard/site/{self.unit_name}/list_schedule"
            response = client.get(url, follow_redirects=True)
            self.assertEqual(type(response.json), type([]))
            self.assertEqual(response.json[0], {
                'at': '2021-07-05 08:15:52.636603',
                'due': '2021-07-10',
                'id': 1,
                'operation': 'ALL',
                'order': 1000060,
                'prio': 0,
                'sfcu': 1,
                'statx': 'ASSIGNED',
                'supplier': 'area1'}
            )
    
    def testSitePlanOrder(self):
        with self.flaskClientAuthed() as client:
            url = f"/api/dashboard/site/{self.unit_name}/order/plan"
            
            data = {
                "order": 1000077,
                "supplier": "area1",
                "due": "2022-02-17"
            }   
            response = client.post(url, data=data, follow_redirects=True)
            self.assertEqual(type(response.json), type([]))
            self.assertEqual(response.status_code, 200)
    
    def testSiteReleaseOrder(self):
        with self.flaskClientAuthed() as client:
            url = f"/api/dashboard/site/{self.unit_name}/order/release"
            
            data = {
                "order": 1000077,
            }   
            response = client.post(url, data=data, follow_redirects=True)
            self.assertEqual(response.json, 1)
            self.assertEqual(response.status_code, 200)

    def testUnitGetStatus(self):
        with self.flaskClientAuthed() as client:
            url = f"/api/dashboard/cell/{self.cell_name}/get_status"
            response = client.get(url, follow_redirects=True)
            self.assertEqual(response.json, {
                'loaded_sfcu': 0,
                'log': [],
                'progress': {},
                'status': 'STANDBY',
                'type': 'Cell'
                }
            )
            
    def testUnitListSfcu(self):
        with self.flaskClientAuthed() as client:
            url = f"/api/dashboard/cell/{self.cell_name}/list_sfcu"
            response = client.get(url, follow_redirects=True)
            self.assertEqual(type(response.json), type([]))
            self.assertIn(1, response.json)
            self.assertEqual(len(response.json), 4)
        
    def testUnitShutdownAndStartupCell(self):
        with self.flaskClientAuthed() as client:
            url = f"/api/dashboard/cell/{self.cell_name}/shutdown"
            response = client.post(url, follow_redirects=True)
            time.sleep(0.5)
            url = f"/api/dashboard/cell/{self.cell_name}/get_status"
            response = client.get(url, follow_redirects=True)
            self.assertEqual(response.json["status"], "STOPPED")
            time.sleep(0.5)
            url = f"/api/dashboard/cell/{self.cell_name}/activate"
            response = client.post(url, follow_redirects=True)
            time.sleep(0.5)
            url = f"/api/dashboard/cell/{self.cell_name}/get_status"
            response = client.get(url, follow_redirects=True)
            self.assertEqual(response.json["status"], "STANDBY")
            
    # POST /api/dashboard/cell/<unit_name>/load
    # POST /api/dashboard/cell/<unit_name>/unload
    def testUnitLoadUnloadSfcu(self):
        with self.flaskClientAuthed() as client:
            url = f"/api/dashboard/cell/{self.cell_name}/list_sfcu"
            response = client.get(url, follow_redirects=True)
            self.assertGreaterEqual(len(response.json), 1)

            url = f"/api/dashboard/cell/{self.cell_name}/load"
            data = {
                "sfcu": response.json[0]
            }
            response = client.post(url, data=data, follow_redirects=True)
            self.assertEqual(response.status_code, 200)
            url = f"/api/dashboard/cell/{self.cell_name}/unload"
            response = client.post(url, follow_redirects=True)
            self.assertEqual(response.status_code, 200)
 

if __name__ == "__main__":

    unittest.main()