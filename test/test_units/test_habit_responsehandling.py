"""Unit tests for roles.responsehandling module.

Created on 31.01.2022

@author: Ralf Banning

Copyright and License Notice:

    flecsimo roles.responsehandling unit tests.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import os
import sqlite3
import sys
import unittest
from pathlib import Path

from flecsimo.base import dao
from flecsimo.util import dbmanager
from flecsimo.habit import responsehandling
from paho.mqtt import client as mqtt

class ResponsehandlingMethods(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.testroot=Path(__file__).parents[1] # loader independent path
        cls.database = cls.testroot.joinpath(
            "data",
            "tmp",
            "test_roles_requesthandling_methods.db")        
        cls.conf = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": cls.database}
        cls.site = cls.conf['site']
        cls.area = cls.conf['area']
        dbs = dbmanager.DbSetUp(cls.database)
        dbs.create_schema('area')
        dbs.create_masterdata('area')
        dbs.create_testdata('area')

        logging.basicConfig(level="DEBUG", stream=sys.stdout)       
        super(ResponsehandlingMethods, cls).setUpClass()
        
    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database)
        super(ResponsehandlingMethods, cls).tearDownClass()         

    def setUp(self): 
        self.rsh = responsehandling.ResponseMessageHandler(self.database)
        self.dao = dao.DataAccess(self.database)
        self.client = None
        self.userdata = None

    def tearDown(self):
        teardown_script = """
            BEGIN TRANSACTION;
            DELETE FROM 'order' WHERE "id"=1000068;
            DELETE FROM 'sfcu' WHERE "id" BETWEEN 11 AND 12;
            DELETE FROM 'task' WHERE "id" BETWEEN 33 AND 36;
            COMMIT;
        """
        self.conn = sqlite3.connect(self.database)
        self.conn.execute("PRAGMA foreign_keys=1")
        with self.conn:
            self.conn.executescript(teardown_script)
        self.conn.close()
        
    unittest.skip('Test not implemented')
    def test_AutoQuoteProcessing(self):
        pass
    
    unittest.skip('Test not implemented')
    def test_OnOpcfm(self):
        pass
  

    unittest.skip('Test not implemented')
    def test_OnQuote(self):
        pass
    
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
