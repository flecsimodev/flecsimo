#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""Application tests for base-auth-module 
Created on 08.01.2022

@author: Leon Schnieber

Copyright and License Notice:

    flecsimo base.auth unit tests.
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
from pathlib import Path
import unittest
import os
# TODO: change to pathlib
# import pathlib
from flecsimo.base import auth
from flecsimo.app.web import webserver

"""

WARNING: this test does not work correctly - renaming auth files may fail and
         will compromise the login data.
         
FIXME

"""

class BaseAuthTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(BaseAuthTest, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(BaseAuthTest, cls).tearDownClass()

    def setUp(self):
        # prepare the config-files
        
        # TODO: remove after successful test-
        #=======================================================================
        # self.config_login_file_prod = os.path.join(os.path.dirname(auth.__file__),
        #                                     "../conf/logins.json")
        # self.config_login_file_test = os.path.join(os.path.dirname(__file__),
        #                                     "../data/testdata_auth_logins.json")
        # self.config_login_file_temp = os.path.join(os.path.dirname(__file__),
        #                                     "../data/testdata_auth_logins_temp.json")
        #=======================================================================
        logins_path = Path(webserver.__file__).parent.joinpath('conf')
        test_data = Path(__file__).parent.parent.joinpath('data')
        
        self.config_login_file_prod = logins_path.joinpath('logins.json').resolve()
        self.config_login_file_test = test_data.joinpath('testdata_auth_logins.json').resolve()
        self.config_login_file_temp = test_data.joinpath('testdata_auth_logins_temp.json').resolve()      

        # FIXME: This is dangerous: if test fails, auth will be corrupted! Change!
        #=======================================================================
        # os.rename(self.config_login_file_prod, self.config_login_file_temp)
        # os.rename(self.config_login_file_test, self.config_login_file_prod)
        #=======================================================================

        self.username = "john_doe"  # admin
        self.password = "foobar"  # bazbar
        self.session_key = "sessionHash"

        self.auth = auth.Auth()

    def tearDown(self):
        # undo the move of the config-files
        
        # FIXME:
        #=======================================================================
        # os.rename(self.config_login_file_prod, self.config_login_file_test)
        # os.rename(self.config_login_file_temp, self.config_login_file_prod)
        #=======================================================================

        # FIXME:
        # del self.auth
        pass

    @unittest.skip("Not implemented")
    def test_auth_request(self):
        pass

    def test_session_check(self):
        # correct session key
        self.assertEqual(self.username, self.auth.session_check(self.session_key))
        # wrong session key
        self.assertIsNone(self.auth.session_check("wrongsessionHash"))
        # emmpty session key
        self.assertIsNone(self.auth.session_check(""))

    def test_credentials_check(self):
        # correct credentials
        self.assertTrue(self.auth.credentials_check(self.username, self.password))
        # wrong password
        self.assertFalse(self.auth.credentials_check(self.username, "test_password"))
        # wrong username
        self.assertFalse(self.auth.credentials_check("test_user", self.password))
        # empty username
        self.assertFalse(self.auth.credentials_check("", self.password))
        # empty password
        self.assertFalse(self.auth.credentials_check(self.username, ""))

    @unittest.skip("Not implemented")
    def test_render_template_wrap(self):
        pass

    def test_git_hash_function(self):
        git_hash = self.auth.git_hash
        self.assertEqual(7, len(git_hash))
        self.assertEqual(type(""), type(git_hash))


if __name__ == "__main__":

    unittest.main()
