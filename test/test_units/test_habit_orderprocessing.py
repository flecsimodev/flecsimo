"""Unit tests for roles/order_processing module.

FIXME: ... almost everything...

Note: currently main problem, that all SQL-statements are prepared as local string,
    which makes them inaccessible for unit-testing. So: only "black-box"testing is
    possible.

Created on 10.07.2020

@author: Ralf Banning

Copyright and License Notice:

    flecsimo roles.orderprocessing unit tests.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sqlite3
import unittest
from pathlib import Path

from flecsimo.base import dao
from flecsimo.base.states import OrderStates, SfcuStates
from flecsimo.util import dbmanager
from flecsimo.habit import orderprocessing


class OrderprocessingMethods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.database = cls.testroot.joinpath(
            "data",
            "tmp",
            "test_roles_orderprocessing.db")
        cls.conf = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": cls.database}
        cls.site = cls.conf['site']
        cls.area = cls.conf['area']
        dbs = dbmanager.DbSetUp(cls.database)
        dbs.create_schema('site')
        dbs.create_masterdata('site')
        dbs.create_testdata('site')

        print(f"Set up sqlite test data in sqlite version {sqlite3.sqlite_version}")

        super(OrderprocessingMethods, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database)
        super(OrderprocessingMethods, cls).tearDownClass()

    def setUp(self):
        self.op = orderprocessing.OrderProcessor(self.database)
        self.dao = dao.DataAccess(self.database)

    def tearDown(self):
        teardown_script = """
            BEGIN TRANSACTION;
            DELETE FROM 'order' WHERE "unit"='TEST';
            DELETE FROM 'order_parameter' WHERE "typ" = 'test';
            DELETE FROM 'sfcu' WHERE "state" = 0;
            COMMIT;
        """
        self.conn = sqlite3.connect(self.database)
        self.conn.execute("PRAGMA foreign_keys=1")
        with self.conn:
            self.conn.executescript(teardown_script)
        self.conn.close()
    
    @unittest.skip('Outdated')
    def testCeckOrderdata(self):
        """Check order data before insert.
        
        Tests:
            1. Test variant material (test_material_ok).
            2. Test variant material with with wrong variant (test_variant_fault).
            3. Test variant material without providing variant.
            4. Test non-variant material with pds time slice.
            5. Test non-variant material with wrong variant data.
            6. Test material without valid pds.
        """
        test_material_ok = 'FUAS-1010'
        test_material_timeslice = 'FUAS-1005'
        test_material_fault = 'FXF-1120'
        test_variant_ok = 'col:blue'
        test_variant_fault = 'col:green'

        # Test 1:
        result1 = self.op.check_routing(test_material_ok,
                                          test_variant_ok)
        self.assertEqual(result1,
            (0, 1, 1, '... Order data OK with pds 1 and 1 open parameters.'))

        # Test 2:
        result2 = self.op.check_routing(test_material_ok,
                                           test_variant_fault)
        self.assertEqual(result2,
            (2, None, None, 'ERROR: no pds found for variant col:green.'))

        # Test 3:
        result3 = self.op.check_routing(test_material_ok)
        self.assertEqual(result3,
            (2, None, None, 'ERROR: no pds found for variant None.'))

        # Test 4:
        result4 = self.op.check_routing(test_material_timeslice,
                                           test_variant_ok)
        self.assertEqual(result4,
            (0, 4, 0, '... Order data OK with pds 4 and 0 open parameters.'))

        # Test 5:
        result5 = self.op.check_routing(test_material_timeslice,
                                           test_variant_fault)
        self.assertEqual(result5,
            (0, 4, 0, '... Order data OK with pds 4 and 0 open parameters.'))

        # Test 6:
        result6 = self.op.check_routing(test_material_fault)
        self.assertEqual(result6,
            (1, None, None, 'ERROR: no or more than one pds found for material FXF-1120.'))    

    # @unittest.skip('Test not implemented')    
    def testCeckRouting_new(self):
        """Check order data before insert.
        
        Tests:
            1. Test variant material (test_material_ok).
            2. Test variant material with with wrong variant (test_variant_fault).
            3. Test variant material without providing variant.
            4. Test non-variant material with pds time slice.
            5. Test non-variant material with wrong variant data.
            6. Test material without valid pds.
        """
        test_material_ok = 'FUAS-1010'
        test_material_timeslice = 'FUAS-1005'
        #test_material_fault = 'FXF-1120'
        #test_variant_ok = 'col:blue'
        #test_variant_fault = 'col:green'

        # Test 1:
        result1 = self.op.check_routing(test_material_ok)

        self.assertEqual(result1,
            {'return_code': 0, 'pds': 1, 'param_count': 1,
             'variants': [{'variant': 'col:red', 'typ': 'col', 'value': 'red'},
                          {'variant': 'col:blue', 'typ': 'col', 'value': 'blue'}],
             'params': ['diam'],
             'max_lotsize': 10,
             'msg': '... Order data OK with pds 1 and 1 open parameters.'})

        # Test 2:
        #=======================================================================
        # result2 = self.op.check_routing(test_material_ok)
        # self.assertEqual(result2,
        #     {'return_code': 2, 'pds': None, 'param_count': None,
        #      'msg': 'ERROR: no pds found for variant col:green.'})
        #=======================================================================

        # Test 3:
        result3 = self.op.check_routing(test_material_ok)
        self.assertEqual(result3,
            {'return_code': 0, 'pds': 1, 'param_count': 1,
             'variants': [{'variant': 'col:red', 'typ': 'col', 'value': 'red'},
                          {'variant': 'col:blue', 'typ': 'col', 'value': 'blue'}],
             'params': ['diam'],
             'max_lotsize': 10,
             'msg': '... Order data OK with pds 1 and 1 open parameters.'})

        # Test 4:
        result4 = self.op.check_routing(test_material_timeslice)
        self.assertEqual(result4,
            {'return_code': 0, 'pds': 4, 'param_count': 0, 'variants': [],
             'params': [], 'max_lotsize': 10,
             'msg': '... Order data OK with pds 4 and 0 open parameters.'})

        # Test 5:
        result5 = self.op.check_routing(test_material_timeslice)
        self.assertEqual(result5,
            {'return_code': 0, 'pds': 4, 'param_count': 0, 'variants': [],
             'params': [], 'max_lotsize': 10,
             'msg': '... Order data OK with pds 4 and 0 open parameters.'})

        # Test 6:
        #=======================================================================
        # result6 = self.op.check_routing(test_material_fault)
        # print("XXX", result6)
        # self.assertEqual(result6,
        #     {'return_code': 1, 'pds': None, 'param_count': None,
        #      'msg': 'ERROR: no or more than one pds found for material FXF-1120.'})
        #=======================================================================

    # @unittest.skip('Test not implemented')
    def testGetOrderParam(self):
        test_order = 1000028
        params = self.op.get_order_param(self.site, test_order)
        self.assertEqual([dict(p) for p in params],
                         [{'typ': 'diam', 'value': None}, 
                          {'typ': 'pkg', 'value': None}])

        params = self.op.get_order_param(self.site, test_order, mode='all')

        self.assertEqual([dict(p) for p in params],
                         [{'typ': 'diam', 'value': None}, 
                          {'typ': 'pkg', 'value': None}])

        test_order_fault = 1000600
        params = self.op.get_order_param(self.site, test_order_fault, mode='all')
        self.assertEqual(params, [])

    # @unittest.skip('Test not implemented')
    def testGetPds(self):
        """Retrieve pds for a given order."""
        test_order_ok = 1000020
        test_order_fault = 1000200

        pds = self.op.get_pds(self.site, test_order_ok)
        self.assertEqual(pds, 1)

        pds = self.op.get_pds(self.site, test_order_fault)
        self.assertEqual(pds, None)

    # @unittest.skip('Test not implemented')
    def testGetSfcu(self):
        test_order_ok = 1000023
        test_order_fault = 1000200

        sfculist1 = self.op.get_sfcu(self.site, test_order_ok)
        self.assertEqual(sfculist1, [5, 6, 7])

        sfculist2 = self.op.get_sfcu(self.site, test_order_fault)
        self.assertEqual(sfculist2, [])

    # @unittest.skip('Test not implemented')
    def testNewOpdta(self):
        """Create new opdta for an order.
        
        Tests:
            1. Test with an order in state CHECKED (test_order_checked).
            2. Assure, orders with higher state raise an exception.   
        """
        test_order_checked = 1000026
        test_order_released = 1000020
        test_order_planned = 1000025

        # Test 1:
        rc1 = self.op.new_opdta(self.site, test_order_checked)
        self.assertEqual(rc1, (0, None))

        # Test 2:
        for _ in [test_order_planned, test_order_released]:
            with self.subTest(order=_):
                rc2 = self.op.new_opdta(self.site, test_order_released)
                self.assertEqual(rc2,
                    (1,
                     ('UNIQUE constraint failed: part.task, part.material, part.usage',)
                    )
                )

    # @unittest.skip('Test not implemented')
    def testNewOrder(self):
        # Test 1:
        report1 = self.op.new_order(self.site,
                               material='FUAS-1010',
                               qty=2,
                               unit='TEST',
                               variant='col:blue')

        self.assertEqual(report1,
           {'return_code': 0, 'order': 1000029, 'open_params':1,
            'msg': '... Order 1000029 created. Check for open params.'})
        
        # Test 2:
        report2 = self.op.new_order(self.site,
                               material='FUAS-1010',
                               qty=2,
                               unit='TEST')

        # TODO: testing of exceptions / _chech_order.

    # @unittest.skip('Test not implemented')
    def testNewSfcu(self):
        """Create new sfcu for unplanned orders.
        
        Tests:
            1. Test with a order in planning state checked.
            2. Test with a order in planning state initial.
            3. Test with a order in planning state checked, which should not
               create any new sfcu.
            4. Re-call new sfcu for test 1 (checked order) - currently will
               create a new set of sfcu's - rework!
        """
        test_order_checked = 1000026
        test_order_initial = 1000028
        test_order_planned = 1000024

        # Test 1:
        sfculist = self.op.new_sfcu(self.site, test_order_checked)
        self.assertEqual([s for s in sfculist], [11])

        # Test 2:
        sfculist = self.op.new_sfcu(self.site, test_order_initial)
        self.assertEqual([s for s in sfculist], [12, 13, 14, 15, 16])

        # Test 3:
        sfculist = self.op.new_sfcu(self.site, test_order_planned)
        self.assertEqual([s for s in sfculist], [])

        #=======================================================================
        # # Test 4:
        # sfculist = self.op.new_sfcu(self.site, test_order_checked)
        # self.assertEqual([s for s in sfculist], [31, 32])
        #=======================================================================

    # @unittest.skip('Test not implemented')
    def testSetOrderParam(self):
        """Enter parameters for orders.
        
        Tests:
            1. Create a new parameter row (rowid 41, one row affected).
            2. Create again with same parameters - should result
               in update (rowid 0, one row affected.)
            3. Create with unknown order id -should result in exception
               due to foreign key contraint.
        """
        test_order_ok = 1000020
        test_order_fault = 1000200

        # Test 1:
        rc = self.op.set_order_param(self.site,
                                     order=test_order_ok,
                                     param_typ='test',
                                     param_value='testvalue')
        self.assertEqual(dict(rc), {'rowid': 19, 'rowcount': 1})

        result1 = self.dao.retrieve('order_parameter',
                                   columns=['value'],
                                   conditions={'order':test_order_ok,
                                               'typ':'test'})

        self.assertEqual(len(result1), 1)
        self.assertEqual([dict(r) for r in result1], [{'value': 'testvalue'}])

        # Test 2:
        rc = self.op.set_order_param(self.site,
                                     order=test_order_ok,
                                     param_typ='test',
                                     param_value='testvalue2')
        self.assertEqual(dict(rc), {'rowid': 0, 'rowcount': 1})

        result2 = self.dao.retrieve('order_parameter',
                                   columns=['value'],
                                   conditions={'order':test_order_ok,
                                               'typ':'test'})

        self.assertEqual(len(result2), 1)
        self.assertEqual([dict(r) for r in result2], [{'value': 'testvalue2'}])

        # test 3:
        with self.assertRaises(sqlite3.IntegrityError) as ie:
            self.op.set_order_param(self.site,
                                    order=test_order_fault,
                                    param_typ='test',
                                    param_value='testvalue2')
        self.assertEqual(ie.exception.args, ('FOREIGN KEY constraint failed',))

    # @unittest.skip('Test not implemented')
    def testSetOrderState(self):
        """Set order planning state.
        
        Tests:
            1. Set an order planning state with OrderStates Instance.
            2. Try to set an order planning stes with wrong status type - should
               raise exception.
            3. Set an order planning state with integer.
        """
        test_order = 1000020

        # Test 1:
        rc = self.op.update_order_state(self.site, test_order, OrderStates.FAILED)
        self.assertEqual(rc, 1)

        result1 = self.dao.retrieve('order_plan',
                                    columns=['state', 'statx'],
                                    conditions={'order': test_order})

        self.assertEqual([dict(r) for r in result1],
                         [{'state': 9, 'statx': 'FAILED'}])

        # Test 2:
        with self.assertRaises(ValueError) as ve:
            self.op.update_order_state(self.site, test_order, "TYPEFAULT")
        self.assertEqual(ve.exception.args, ('Argument status: TYPEFAULT is neither OrderState nor integer in expected range.',))

        result2 = self.dao.retrieve('order_plan',
                                    columns=['state', 'statx'],
                                    conditions={'order': test_order})
        self.assertEqual([dict(r) for r in result2],
                         [{'state': 9, 'statx': 'FAILED'}])

        # Test 3:
        rc = self.op.update_order_state(self.site, test_order, 0)
        self.assertEqual(rc, 1)

        result3 = self.dao.retrieve('order_plan',
                                    columns=['state', 'statx'],
                                    conditions={'order': test_order})

        self.assertEqual([dict(r) for r in result3],
                         [{'state': 0, 'statx': 'INITIAL'}])

    # @unittest.skip('Test not implemented')
    def testShowOrders(self):
        orders = self.op.show_orders()
        self.assertEqual(len(orders), 9)

        orders = self.op.show_orders(OrderStates.CHECKED.name)
        self.assertEqual(len(orders), 2)


class OrderprocessingPerformance(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.database = cls.testroot.joinpath(
            "data",
            "tmp",
            "test_roles_orderprocessing_performance.db")
        cls.conf = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": cls.database}
        cls.site = cls.conf['site']
        cls.area = cls.conf['area']
        dbs = dbmanager.DbSetUp(cls.database)
        dbs.create_schema('site')
        dbs.create_masterdata('site')
        dbs.create_testdata('site')

        super(OrderprocessingPerformance, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # FIXME: remove fails since db is in use by other process.
        # os.remove(cls.database)
        super(OrderprocessingPerformance, cls).tearDownClass()

    def setUp(self):
        self.op = orderprocessing.OrderProcessor(self.database)
        self.dao = dao.DataAccess(self.database)
        self.dao.change('order_plan',
                        data={'state': 1, 'statx': 'CHECKED'},
                        conditions={'site': 'FUAS'})

    def tearDown(self):
        self.conn = sqlite3.connect(self.database)
        with self.conn:
            self.conn.execute("DELETE FROM 'sfcu' WHERE state=0")
        self.conn.close()

    def testNewSfcuDao(self):
        """Split a given order into a set of shop floor control units (SFCUs).
        """
        site = self.site
        first_order = 1000060

        for order in range(first_order, first_order + 10):
            self.op.new_sfcu(site, order)

    def testNewSfcuNative(self):
        """Split a given order into a set of shop floor control units (SFCUs).
        """
        site = self.site
        first_order = 1000060

        for order in range(first_order, first_order + 10):
            self._new_sfcu(site, order)

    def _new_sfcu(self, site, order):
        """Benchmark method for new sfcu."""
        sfculist = []
        select_order = """
            SELECT 'order'.material, 'order'.qty, 'order'.at
            FROM 'order', order_plan
            WHERE 'order'.site = :site
            AND 'order'.id = :order
            AND 'order'.id = order_plan.'order'
            AND order_plan.state <= :state
            """
        insert_sfcu = """
            INSERT INTO sfcu VALUES 
            (NULL, :site, :order, :material, :state, :statx, :at)"""
        conn = self.dao.connect()

        with conn:
            # Get the order data
            cur = conn.execute(select_order, {
                'site': site,
                'order': order,
                'state': OrderStates.CHECKED.value})
            row = cur.fetchone()
            if row:
                material = row['material']
                qty = int(row['qty'])
                at = row['at']
            else:
                return None
            for _ in range(qty):
                cur = conn.execute(insert_sfcu,
                                   {'site': site,
                                    'order': order,
                                    'material': material,
                                    'state': SfcuStates.INITIAL.value,
                                    'statx': SfcuStates.INITIAL.name,
                                    'at': at})
                sfculist.append(cur.lastrowid)
        conn.close()
        return sfculist


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
