"""
Created on 30.12.2021

@author: Ralf Banning

Copyright and License Notice:
    flecsimo base.config unit tests
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import json
from pathlib import Path, WindowsPath
import unittest
import sys
from flecsimo.base import config


class ConfigMethods(unittest.TestCase):
    """Test suite for base.config.py functions."""

    @classmethod
    def setUpClass(cls):
        cls.cpath = Path("./conf/")
        cls.cpath.mkdir(exist_ok=True)
        cls.configfile = cls.cpath.joinpath("base_config.json")
        cls.configdict_old = {
            "About": {"version": "2024-10-05-1", "organization": "FUAS"},
            "COMMON": {
                "type": "site",
                "description": "Common configuration.",
                "mqserver": "localhost",
                "site": "FUAS",
                "database": "site.db",
                "backup": False,
                "data": "sim",
                "online": True,
                "sfcu": True,
                "mhu": False },
            "Test": {
                "description": "Unit-test configuration.",
                "mqserver": "123.456.789.1",
                "site": "FUAS",
                "database": "test_site.db",
                "sfcu": False,
                "data": "sim"
                }
            }
        cls.configdict = {
            "INFO": {
                    "version": "2024-10-05 10:24:07",
                    "organization": "FUAS",
                    "currentprofile": "default"
            },
            "IDENTITY": {
                    "type": "cell",
                    "site": "FUAS",
                    "area": "AREA-1",
                    "cell": "CUTCO1"
            },
            "PROFILE": {
                "default": {
                    "name": "Default profile.",
                    "description": "Laser cutting and coating station 1.",    
                    "mqserver": "localhost",
                    "database": "cutco1.db",
                    "backup": False,
                    "data": "own",
                    "online": True,
                    "sfcu": True,
                    "mhu": False
                },
                "single": {
                    "name": "Stand-alone profile.",
                    "description": "Stand alone: laser cutting and coating station 1.",
                    "mqserver": None,
                    "database": "cutco1.db",
                    "backup": False,
                    "data": "sim",
                    "online": False,
                    "sfcu": False,
                    "mhu": False
                },
                "lab": {
                    "name": "Lab scope profile.",
                    "description": "Lab: laser cutting and coating station 1.",
                    "mqserver": "192.168.100.100",
                    "database": "cutco1.db",
                    "backup": False,
                    "data": "sim",
                    "online": False,
                    "sfcu": False,
                    "mhu": False
                }                
            }
        }
        cls.configfile.write_text(json.dumps(cls.configdict), encoding="utf-8")
        # with open(cls.configfile, 'w') as f:
        #    f.write(json.dumps(cls.configdict))
        if sys.platform.startswith("win"):
            cls.sys_root = Path(__file__).drive
        else:
            cls.sys_root = Path(__file__).root

        print(f"INFO: System path root is set to '{cls.sys_root}'.")

        super(ConfigMethods, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.configfile.unlink()
        super(ConfigMethods, cls).tearDownClass()

    def setUp(self) -> None:
        self.configurator = config.Configurator(__file__)

    def tearDown(self) -> None:
        del self.configurator

    def test_path(self):
        """Test path configuration."""
        # TODO: How to test pathes independent from test setup
        self.assertEqual(self.configurator.config_root,
                         Path(__file__).parent.resolve())
        self.assertEqual(self.configurator.db_path,
                         Path(__file__).parent.resolve() / 'db')
        self.assertEqual(self.configurator.conf_path,
                         Path(__file__).parent.resolve() / 'conf')

    def test_new_configurator(self):
        """Behavior of constructor.
        
        Tests:
            1. Test instantiation and variables of Configurator.
               a) set root to current modules file (this unit_test!)
               b) set root to current modules name (this unit_test!)
               c) set root to a string (filename w/o path) should also point 
                  to this unit_test location.
        """
        # Test case 1a:
        test_root_1 = Path(__file__).parent.resolve()

        cfgr1 = config.Configurator(__file__)
        self.assertEquals(cfgr1.config_root, test_root_1)
        self.assertEqual(cfgr1.conf_path, test_root_1 / 'conf')
        self.assertEqual(cfgr1.db_path, test_root_1 / 'db')
        del(cfgr1)

        # Test case 1b:
        test_root_2 = Path(__name__).parent.resolve()
        cfgr2 = config.Configurator(__file__)
        self.assertEquals(cfgr2.config_root, test_root_2)
        self.assertEqual(cfgr2.conf_path, test_root_2 / 'conf')
        self.assertEqual(cfgr2.db_path, test_root_2 / 'db')
        del(cfgr2)

        # Test case 1c:
        test_root_3 = Path('c:/tmp')
        cfgr3 = config.Configurator('c:/tmp')
        self.assertEquals(cfgr3.config_root, test_root_3)
        self.assertEqual(cfgr3.conf_path, test_root_3 / 'conf')
        self.assertEqual(cfgr3.db_path, test_root_3 / 'db')
        del(cfgr3)

    def test_new_configurator_fails(self):
        """Faulty initialization of constructor.
        
        Tests: Test exception if init fails
        1) root parameter is missing
        2) wrong type of root argument: int
        3) wrong type of root argument: dict
        4) root does not point to existing file
        5) root does not point to existing directory
        """
        # Test cases
        # 1) root parameter is missing
        with self.assertRaises(TypeError) as te1:
            config.Configurator()
        self.assertEqual(te1.exception.args[0],
                         "__init__() missing 1 required positional " + \
                         "argument: 'root'")
        # 2) wrong type of root argument: int
        with self.assertRaises(TypeError) as te2:
            config.Configurator(0)
        self.assertEqual(te2.exception.args[0],
                         "Wrong type for positional argument 'root'")

        # 3) wrong type of root argument: dict
        with self.assertRaises(TypeError) as te3:
            config.Configurator({'a': 1})
        self.assertEqual(te3.exception.args[0],
                         "Wrong type for positional argument 'root'")

        # 4) root does not point to existing file
        non_existing_file = 'xxxx.py'
        with self.assertRaises(FileNotFoundError) as f4:
            config.Configurator(non_existing_file)
        self.assertEqual(f4.exception.args[0],
                         "Argument 'root' does not point to file or directory")

        # 5) root does not point to existing directory
        non_exiting_path = Path(__file__).with_name(non_existing_file)
        with self.assertRaises(FileNotFoundError) as f4:
            config.Configurator(non_exiting_path)
        self.assertEqual(f4.exception.args[0],
                         "Argument 'root' does not point to file or directory")

    def test_get_config(self):
        """Get a configuration from file.
        
        Tests:
            1. Test selection of different profiles
            2. Test incremental value setting
            3. Test exception handling from wrong path or section
            4. Test handling of file arg (as a Path object).
        """
    
        # Test 1:
        expected_default = self.configdict['IDENTITY'].copy()
        expected_default.update(self.configdict['PROFILE']['default'])
        
        self.assertEqual(
            self.configurator.get_config(self.configfile),
            expected_default)

       
        # test 2:
        test_dict = self.configdict['IDENTITY'].copy()
        test_dict.update(self.configdict['PROFILE']['lab'])
        retrieved = self.configurator.get_config(self.configfile, 'lab')
        
        self.assertEqual(retrieved, test_dict) 
        self.assertEqual(retrieved['sfcu'], False)        
        self.assertEqual(retrieved['data'], "sim")

        # Test 3:
        with self.assertRaises(FileNotFoundError) as ve1:
            self.configurator.get_config('Nofile')
        self.assertRegex(ve1.exception.args[0], r"not found.$")

        with self.assertRaises(ValueError) as ve2:
            self.configurator.get_config(self.configfile, 'NoSection')
        self.assertEqual(ve2.exception.args, ("Config has no section 'NoSection'.",))

        # Test 4:
        p = Path(self.configfile)
        self.configurator.get_config(p)

        with self.assertRaises(FileNotFoundError) as ve1:
            q = Path('Nofile')
            self.configurator.get_config(q)
        self.assertRegex(ve1.exception.args[0], r"not found.$")

    def test_get_info(self):
        """Test info getter."""
        expected = {'version': '2024-10-05 10:24:07', 
                    'organization': 'FUAS', 
                    'currentprofile': 'default'}
        self.assertEqual(self.configurator.get_info(self.configfile),
                         expected)

    def test_get_profiles(self):
        """Test keys getter."""
        expected = {'default': 'Default profile.',
                    'lab': 'Lab scope profile.',
                    'single': 'Stand-alone profile.'}
        self.assertEqual(self.configurator.get_profiles(self.configfile),
                         expected)

    def test_get_conf_path(self):
        """Test conf_path getter.
        
        Tests:
        1. Assert that incomplete paths will be mapped to standard path + file 
           name
        2.Assert that absolute paths will be taken as they are.
        """
        # Test case 1
        path1 = "/a/b/conf.json"
        self.assertEqual(self.configurator.get_conf_path(path1),
                         self.configurator.conf_path / Path(path1).name)

        # Test case 2
        path2 = "D:/Local/git/flecsimo/conf/config.json"
        self.assertEqual(self.configurator.get_conf_path(path2),
                         Path(path2).resolve())

    def test_get_db_path(self):
        """Test db_path synthesis."""
        # TODO: How to test pathes independent from test setup
        path1 = "test.db"
        self.assertEqual(self.configurator.get_db_path(path1),
                         self.configurator.db_path / path1)

    def test_get_db_path_absolut(self):
        """Test db_path synthesis."""
        # TODO: How to test pathes independent from test setup
        self.assertEqual(self.configurator.get_db_path("C:/tmp/test.db"),
                         WindowsPath("C:/tmp/test.db"))
        self.assertEqual(self.configurator.get_db_path("C:\\tmp\\test.db"),
                         WindowsPath("C:/tmp/test.db"))

    def test_get_path(self):
        """Test private path mangler
        
        Tests:
        1) relative paths are ignored and 'name' is added to root path.
        2) a pure filename is added to root path
        3) an absolute path is given as Pat
        """

        # Test case 1
        new_sub_path1 = self.configurator._get_file_path(
            self.configurator.config_root, '../new/blue.stem')
        expected_path1 = self.configurator.config_root / 'blue.stem'
        self.assertEqual(new_sub_path1, expected_path1)

        # Test case 2
        new_sub_path2 = self.configurator._get_file_path(
            self.configurator.config_root, 'blue.stem')
        expected_path2 = self.configurator.config_root / 'blue.stem'
        self.assertEqual(new_sub_path2, expected_path2)

        # Test case 3
        new_sub_path3 = self.configurator._get_file_path(
            self.configurator.config_root, 'd:/tmp/blue.stem')
        expected_path3 = Path(self.sys_root).joinpath('/tmp/blue.stem')

        self.assertEqual(new_sub_path3, expected_path3)
        
    def test_read_configfile(self):
        """Test reading config file."""
        self.assertEqual(
            self.configurator.read_configfile(self.configfile),
            self.configdict)
        
        with self.assertRaises(FileNotFoundError):
            self.configurator.read_configfile('/tmp/nofile.json')

    def test_write_configfile(self):
        """Test writing a config file"""
        test_file_name = 'test_write_configfile.json'
        self.configurator.write_configfile(test_file_name, 
                                           self.configdict)
               
        self.configurator.write_configfile(test_file_name, 
                                           self.configdict,
                                           bkp=True)
        test_path=self.configurator.conf_path / test_file_name 
        self.assertTrue(test_path.with_suffix('.bkp').is_file())
