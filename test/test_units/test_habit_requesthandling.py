"""Unit tests for roles.requesthandling module.

Created on 27.01.2022

@author: Ralf Banning

Copyright and License Notice:
    flecsimo roles.requesthandling unit tests.
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import os
import sqlite3
import sys
import unittest
import json
from pathlib import Path

from flecsimo.base import dao
from flecsimo.util import dbmanager
from flecsimo.habit import requesthandling
from paho.mqtt import client as mqtt


class RequesthandlingMethods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.database = cls.testroot.joinpath(
            "data",
            "tmp",
            "test_roles_requesthandling_methods.db")
        cls.conf = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": cls.database}
        cls.site = cls.conf['site']
        cls.area = cls.conf['area']
        dbs = dbmanager.DbSetUp(cls.database)
        dbs.create_schema('area')
        dbs.create_masterdata('area')
        dbs.create_testdata('area')

        logging.basicConfig(level="DEBUG", stream=sys.stdout)
        super(RequesthandlingMethods, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database)
        super(RequesthandlingMethods, cls).tearDownClass()

    def setUp(self):
        self.rqh = requesthandling.RequestMessageHandler(self.database,
                                                         myself = "AREA-1")
        self.dao = dao.DataAccess(self.database)
        self.client = None
        self.userdata = None

    def tearDown(self):
        teardown_script = """
            BEGIN TRANSACTION;
            DELETE FROM 'order' WHERE "id"=1000068;
            DELETE FROM 'sfcu' WHERE "id" BETWEEN 11 AND 12;
            DELETE FROM 'task' WHERE "id" BETWEEN 33 AND 36;
            COMMIT;
        """
        self.conn = sqlite3.connect(self.database)
        self.conn.execute("PRAGMA foreign_keys=1")
        with self.conn:
            self.conn.executescript(teardown_script)
        self.conn.close()

    unittest.skip('Test not implemented')

    def test_AutoAsgmtProcessing(self):
        pass

    # unittest.skip('Test not implemented')
    def test_OnAsgmt(self):
        """Test assignments on area level.
        
        Test cases:
            1. Receive an asgmt message and assert a record is inserted
        """
        topic = b"FUAS/asgmt"
        payload = """{"site": "FUAS", 
                   "order": 1000023, 
                   "sfcu": 12, 
                   "operation": "ALL", 
                   "supplier": "AREA-1", 
                   "org": "area", 
                   "due": "2021-07-13", 
                   "prio": 0, 
                   "state": 4, 
                   "dhash": null, 
                   "statx": "ASSIGNED", 
                   "at": "2022-01-28T14:24:05.664690"}"""

        message = mqtt.MQTTMessage(topic=topic)
        message.payload = payload

        # test case 2
        self.rqh.myself = "AREA-1"
        rows = self.dao.retrieve('schedule', conditions={'order': 1000023})
        self.assertEqual(len(rows), 0, "Schedule exists before inserted.")
        self.rqh.on_asgmt(self.client, self.userdata, message)
        rows = self.dao.retrieve('schedule', conditions={'order': 1000023})
        self.assertEqual(len(rows), 1, "No schedule found.")

    # unittest.skip('Test not implemented')
    def test_OnOpdta(self):
        """Test opdta messages on area level.
        
        Test cases:
            1. None message logs 'DEBUG:flecsimo.habit.requesthandling:Skip non-valid opdta message...'
            2. Empty payload logs JSONDecodeError...
            3. Receive an opdta message with 20 records and assert total count of records is 20.
        
        """
        #=======================================================================
        # # test case 1
        # message
        # with self.assertLogs(level="DEBUG") as lg:
        #     tlrc = self.rqh.on_opdta(self.client, self.userdata, message)
        # #self.assertEqual(tlrc, 0)
        # #self.assertEqual(lg.output, ["DEBUG:flecsimo.habit.requesthandling:Skip non-valid opdta message: 'NoneType' object has no attribute 'topic'"])
        #=======================================================================

        # Test case 2:
        topic = b"FUAS/opdta/area1"
        payload = ""
        message = mqtt.MQTTMessage(topic=topic)
        message.payload = payload
        with self.assertLogs(level="DEBUG") as lg:
            tlrc = self.rqh.on_opdta(self.client, self.userdata, message)
        self.assertEqual(tlrc, 0)
        self.assertEqual(lg.output, ['DEBUG:flecsimo.habit.requesthandling:Received not readable payload on_opdta. Skip message.'])

        # Test case 3
        topic = b"FUAS/opdta/area1"
        payload = """{
            "data":{
                "order":[
                    {"id": 1000068, "site": "FUAS", "material": "FXF-1100", "variant": "col:blue", "qty": 2.0, "unit": "PCE", "pds": 1, "at": "2021-07-08 13:13:11.932082"}], 
                "sfcu":[
                    {"id": 11, "site": "FUAS", "order": 1000068, "material": "FXF-1110", "state": 2, "statx": "PLANNED", "at": "2021-07-08 13:13:11.932082"}, 
                    {"id": 12, "site": "FUAS", "order": 1000068, "material": "FXF-1111", "state": 2, "statx": "PLANNED", "at": "2021-07-08 13:13:11.932182"}],
                "task":[
                    {"id": 33, "site": "FUAS", "order": 1000068, "step": 1, "next": 2, "typ": "start", "operation": "1-PUABE", "param_typ": null, "param_value": null, "pds_task": 1}, 
                    {"id": 34, "site": "FUAS", "order": 1000068, "step": 2, "next": 3, "typ": null, "operation": "27-DRILLING", "param_typ": "diam", "param_value": null, "pds_task": 2}, 
                    {"id": 35, "site": "FUAS", "order": 1000068, "step": 3, "next": 4, "typ": "var", "operation": "11-ANODIZATION", "param_typ": "col", "param_value": "blue", "pds_task": 4}, 
                    {"id": 36, "site": "FUAS", "order": 1000068, "step": 4, "next": 0, "typ": "final", "operation": "5-FINASS", "param_typ": "pkg", "param_value": null, "pds_task": 5}], 
                "part":[
                    {"task": 33, "site": "FUAS", "material": "FXF-1120", "usage": "out", "qty": 1.0, "unit": "PCE"}, 
                    {"task": 33, "site": "FUAS", "material": "FXF-5642", "usage": "in", "qty": 0.1, "unit": "M"}, 
                    {"task": 34, "site": "FUAS", "material": "FXF-1110", "usage": "in", "qty": 1.0, "unit": "PCE"}, 
                    {"task": 35, "site": "FUAS", "material": "FXF-1110", "usage": "out", "qty": 1.0, "unit": "PCE"}, 
                    {"task": 35, "site": "FUAS", "material": "FXF-3610-B", "usage": "in", "qty": 3.5, "unit": "G"}, 
                    {"task": 36, "site": "FUAS", "material": "FXF-1100", "usage": "out", "qty": 1.0, "unit": "PCE"}, 
                    {"task": 36, "site": "FUAS", "material": "FXF-1110", "usage": "in", "qty": 1.0, "unit": "PCE"}, 
                    {"task": 36, "site": "FUAS", "material": "FXF-1120", "usage": "in", "qty": 1.0, "unit": "PCE"}, 
                    {"task": 36, "site": "FUAS", "material": "FXF-5611", "usage": "in", "qty": 2.0, "unit": "PCE"}], 
                "resource": [
                    {"task": 33, "site": "FUAS", "loc": "", "prio": 0, "durvar": 30.0, "durfix": "", "unit": "S"}, 
                    {"task": 34, "site": "FUAS", "loc": "", "prio": 0, "durvar": 35.0, "durfix": "", "unit": "S"}, 
                    {"task": 35, "site": "FUAS", "loc": "", "prio": 0, "durvar": 125.0, "durfix": "", "unit": "S"}, 
                    {"task": 36, "site": "FUAS", "loc": "", "prio": 0, "durvar": 30.0, "durfix": "", "unit": "S"}]}, 
                "at": "2022-01-28T14:24:05.663687"}"""

        message = mqtt.MQTTMessage(topic=topic)
        message.payload = payload
        tlrc = self.rqh.on_opdta(self.client, self.userdata, message)
        self.assertEqual(tlrc, 20 , "Unexpected total rowcount of inserted opdta.")

    unittest.skip('Test not implemented')

    def test_OnRfq(self):
        pass


class RequesthandlingPerformance(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.testpath = (Path(__file__).parent.resolve())
        print("Local path of test is:", cls.testpath)
        cls.database = "../data/tmp/test_roles_requesthandling_performance.db"
        cls.conf = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": cls.database}
        cls.site = cls.conf['site']
        cls.area = cls.conf['area']
        dbs = dbmanager.DbSetUp(cls.database)
        dbs.create_schema('site')
        dbs.create_data('test', 'site')

        super(RequesthandlingPerformance, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        os.remove(cls.database)
        super(RequesthandlingPerformance, cls).tearDownClass()

    def setUp(self):
        self.rqh = requesthandling.RequestMessageHandler(self.database)
        self.dao = dao.DataAccess(self.database)

    def tearDown(self):
        self.conn = sqlite3.connect(self.database)
        with self.conn:
            self.conn.execute("DELETE FROM 'sfcu' WHERE state=0")
        self.conn.close()


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
