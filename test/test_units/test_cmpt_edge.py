"""
Created on 07.10.2021

@author: Ralf Banning

Copyright and License Notice:
    flecsimo base.edge unit tests (module renamed from base.agent).
    Copyright (C) 2020  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
import json
import socket
# from memory_profiler import profile
from pathlib import Path, WindowsPath
from time import sleep

from flecsimo.util import dbmanager
from flecsimo.base.const import Orglevel, Component
from flecsimo.cmpt import edge

MAXQOS = 2


class TestContext(unittest.TestCase):
    """Test cases for Context objects."""

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.rule = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": 'area1.db'}
        cls.usecase = 'test'
        cls.topic = 'a/test/topic'
        cls.habit = cls.aTestFunction
        cls.qos = 0

        super(TestContext, cls).setUpClass()

    @classmethod
    def aTestFunction(cls):
        return 0

    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database)
        super(TestContext, cls).tearDownClass()

    # @unittest.skip
    def test_add_rule_success(self):
        """Test adding rules to a context.
        
        Test add() method
        
        Tests:
            1. Add a well formed rule.
            2. Boundary testing of qos levels  
        """
        # Test 1:
        expected_context = {'test':
                                {'topic': 'a/test/topic',
                                 'habit': self.aTestFunction,
                                 'qos': 0
                                }
                            }
        context = edge.Context()
        context.add(self.usecase, self.topic, self.habit, self.qos)

        self.assertEqual(context.context, expected_context,)

        # Test case 2:
        for i in range(-1, 3):
            with self.subTest(i=i):
                context.add(self.usecase, self.topic, self.habit, qos=i)
                self.assertTrue(
                    0 <= context.context['test']['qos'] <= 2,
                    'Unexpected qos level')

    # @unittest.skip
    def test_add_rule_exception(self):
        """Test exceptions in rule adding.
        
        Test add() method.
        
        Tests:
            1. Test None values for use case or topic.
            2. Test non callable habit.
        """
        context = edge.Context()

        # Test case 1:
        with self.assertRaises(ValueError):
            context.add(None, self.topic, self.habit, self.qos)

        with self.assertRaises(ValueError):
            context.add(self.usecase, None, self.habit, self.qos)

        with self.assertRaises(ValueError):
            context.add(None, None, self.habit, self.qos)

        # Test case 2:
        with self.assertRaises(ValueError):
            context.add(self.usecase, self.topic, 'nofunc', self.qos)

    # @unittest.skip
    def test_properties(self):
        """Test properties.
        
        Tests:
            1. context properties getter.
            2. habits properties getter.
            3. subscriptions properties getter.
        """
        context = edge.Context()
        context.add('test_1', 'a/test/topic', self.aTestFunction, 0)
        context.add('test_2', 'another/test/topic', self.aTestFunction, 1)
        expected_context = {'test_1':
                                {'topic': 'a/test/topic',
                                 'habit': self.aTestFunction,
                                 'qos': 0
                                },
                            'test_2':
                                {'topic': 'another/test/topic',
                                 'habit': self.aTestFunction,
                                 'qos': 1
                                },
                            }
        expected_habits = [('a/test/topic', self.aTestFunction),
                           ('another/test/topic', self.aTestFunction)]

        expected_subscriptions = [('a/test/topic', 0),
                                  ('another/test/topic', 1)]

        # Test case 1
        self.assertEqual(context.context, expected_context)

        # Test case 2
        self.assertEqual(context.habits, expected_habits)

        # Test case 3
        self.assertEqual(context.subscriptions, expected_subscriptions)


class TestModuleFunctions(unittest.TestCase):
    "Test cases for the edge module functions."

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.database = cls.testroot.joinpath(
            "data",
            "tmp",
            "test_base_agent.db")
        cls.conf = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": cls.database}
        cls.settings = {'sender': 'FUAS/EDGEUNITTEST',
                    'typ': 'MIX',
                    'cmpt': 'NOD',
                    'broker': 'localhost',
                    'cid': None}

        cls.usecase = 'test'
        cls.topic = 'a/test/topic'
        cls.qos = 0

        super(TestModuleFunctions, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database)
        super(TestModuleFunctions, cls).tearDownClass()

    @unittest.skip("To be implemented.")
    def test_checked_environment(self):
        pass

    @unittest.skip("To be implemented.")
    def test_map_environment(self):
        pass

    def test_compose_sender(self):
        # Test 1
        snd = edge.compose_sender(self.conf,
                                  org=Orglevel.AREA.value)

        self.assertEqual(snd, 'FUAS/area1')

        # Test 2 (Fail: org as Enum instead of str.)
        with self.assertRaises(KeyError):
            edge.compose_sender(self.conf, org=Orglevel.AREA)

class TestNode(unittest.TestCase):
    """Test cases for Node objects."""

    @classmethod
    def setUpClass(cls):
        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.settings = {'sender': 'FUAS/EDGEUNITTEST',
                    'org': 'mixed',
                    'cmpt': 'NOD',
                    'mqserver': 'localhost',
                    'cid': None}

        cls.usecase = 'test'
        cls.topic = "FUAS/EDGEUNITTEST/node"
        cls.payload = json.dumps({'test': 0})
        cls.qos = 0

        super(TestNode, cls).setUpClass()

    @classmethod
    def aTestFunction_1(cls):
        return 1

    @classmethod
    def aTestFunction_2(cls):
        return 2

    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database)
        super(TestNode, cls).tearDownClass()

    # @unittest.skip
    def test_node_start_successful(self):
        """Start an agent and check log for success.
        
        Test node.start() method.
        
        Tests:
            1. Node can publish a message.
            2. Debug message is issued.
        """
        node = edge.Node(self.settings)

        with self.assertLogs(level="DEBUG") as lg:
            node.start()
            # The __str__ method of the info class returns (rc, mid); if rc is
            # zero publishing were performed w/o errors.
            info = node.mqclient.publish(self.topic, self.payload)

            # Test case 1
            self.assertEqual(info[0], 0)

            # Test case 2
            expected_log = "DEBUG:flecsimo.cmpt.edge:Node started. " + \
                           "Sender: FUAS/EDGEUNITTEST, " + \
                           "organizational level: mixed"

            print("Wait for node start ...")
            node._wait_for(node.mqclient, 'connected', 0.5)

            self.assertTrue(expected_log in lg.output)

        node.stop()

    # @unittest.skip
    def test_node_start_fault(self):
        """Start an agent with non exisiting server and check log for failure.
        
        Test node.start() method. 
               
        Test:
            1. Test empty mqserver setting.
            2. Test non existing server name for
                a) raising a specific exception and
                b) issuing a specific log.
        """
        node = edge.Node(self.settings)

        # Test case 1
        node.settings['mqserver'] = ""
        with self.assertRaises(ValueError):
            print("Wait for connection fails (1) ...")
            node.start()

        # Test case 2.a)
        node.settings['mqserver'] = "just_phantasy"
        with self.assertRaises(socket.gaierror):
            print("Wait for connection fails (2) ...")
            node.start()

        # Test case 2.b)
        log_on_connect_fail = "CRITICAL:flecsimo.cmpt.edge:Failed to start " + \
                              "connection for cid None"
        with self.assertLogs(level="DEBUG") as lg:
            # This test is not really inside the rc testing
            # the exception occurs before _on_connect is called.
            try:
                print("Wait for connection fails (3) ...")
                node.start()
            except:
                self.assertTrue(log_on_connect_fail in lg.output)

    # @unittest.skip
    def test_node_on_connect(self):
        """Test on_connect callback.
        
        Test node._on_connect() method.
        
        Tests: 
            1. Test successful connect (log and mqstate['connected'].
            2. Test failure of connect (log and mqstate['connected'].
            3. Test if subscription is started and subscription state is set 
               correctly.
            4. Test if subscription is started and subscription failure 
               (node not connected) is logged.
        """
        node = edge.Node(self.settings)

        # Test case 1
        expected_log_1 = "DEBUG:flecsimo.cmpt.edge:Connect OK, " + \
                         "returned code=0"
        with self.assertLogs(level="DEBUG") as lg:
            node._on_connect(node.mqclient, None, None, rc=0)
            self.assertTrue(expected_log_1 in lg.output)
            self.assertEqual(node.mqstates['connected'], True,
                             "connection state was not set correctly.")

        # Test case 2
        expected_log_2 = "CRITICAL:flecsimo.cmpt.edge:Connect FAILED with " + \
                         "code=1, sender=FUAS/EDGEUNITTEST"
        with self.assertLogs(level="DEBUG") as lg:
            node._on_connect(node.mqclient, None, None, rc=1)
            self.assertTrue(expected_log_2 in lg.output)
            self.assertEqual(node.mqstates['connected'], False,
                             "connection state was not set correctly.")

        # Test case 4
        # rc=0 ist set although the mqclient is not connected - this provokes
        # a subscription failure with code=(4, None)
        node.subscription = [(self.topic, 0)]

        expected_log_4 = "CRITICAL:flecsimo.cmpt.edge:Subscription FAILED " + \
                         "with code=(4, None), sender=FUAS/EDGEUNITTEST"
        with self.assertRaises(edge.SubscriptionError):
            with self.assertLogs(level="DEBUG") as lg:
                node._on_connect(node.mqclient, None, None, rc=0)
                self.assertTrue(expected_log_4 in lg.output)
                self.assertEqual(node.mqstates['subscribed'], False,
                             "subscription state was not set correctly.")

    # @unittest.skip
    def test_node_on_stop(self):
        """Test of on_stop method injection in node.stop case.
        
        Test node.stop(on_stop) method.
        
        Note: for test puposes a meber "test_stop_flag" is added
        
        Tests:
            1. Standard _on_stop function will not change test_stop_flag.
            2. Custom _on_stop function test_on_stop will be executed (proved
               by changing test_stop_flag to False.
            3. Check standard logs of node.stop() method.
        """
        node = edge.Node(self.settings)
        node.test_stop_flag = False

        def test_on_stop():
            node.test_stop_flag = True

        # Test case 1
        node.stop()
        self.assertFalse(node.test_stop_flag)

        # Test case 2
        node.stop(test_on_stop)
        self.assertTrue(node.test_stop_flag)

        # Test case 3
        expected_log_1 = "DEBUG:flecsimo.cmpt.edge:Call on_stopfunc..."
        expected_log_2 = "DEBUG:flecsimo.cmpt.edge:Call disconnect..."
        expected_log_3 = "DEBUG:flecsimo.cmpt.edge:Stop loop."
        with self.assertLogs(level="DEBUG") as lg:
            node.stop()
            self.assertIn(expected_log_1, lg.output, "Expected log not found")
            self.assertIn(expected_log_2, lg.output, "Expected log not found")
            self.assertIn(expected_log_3, lg.output, "Expected log not found")

    # @unittest.skip("To be implemented.")
    def test_node_on_nodestop(self):
        """Test of on_noddestop callback.
        
        Test node._on_nodestop() method.
        
        Tests:
            1. Check for defined "will stop" log entry.
            2. Check status message published at edge mqtt network. 
        """
        node = edge.Node(self.settings)

        # Test case 1
        expected_log = "DEBUG:flecsimo.cmpt.edge:Node will stop. " + \
                       "Sender: FUAS/EDGEUNITTEST, organizational level: mixed"
        with self.assertLogs(level="DEBUG") as lg:
            node._on_nodestop()
            self.assertIn(expected_log, lg.output, "Wrong log content")

        # test case 2
        expected_str = "DEBUG:flecsimo.cmpt.edge:node: Sending PUBLISH " + \
                       "(d0, q0, r0, m2), 'b'FUAS/EDGEUNITTEST/status/NOD"
        with self.assertLogs(level="DEBUG") as lg:
            node.start()
            # give client a chance to receive message.
            print("Wait for _on_message receiving a message...")
            sleep(1)
            self.assertTrue(expected_str in ",".join(lg.output))
        node.stop()

    # @unittest.skip
    def test_node_on_log(self):
        """Test mqtt client log will be handled by _on_log.
        
        Test node._on_log method.
        
        Test: generic log request from client will issue a log entry as in 
            expected_log.            
        """
        expected_log = "DEBUG:flecsimo.cmpt.edge:node: Sending CONNECT " + \
                       "(u0, p0, wr0, wq0, wf0, c1, k60) client_id=b''"

        node = edge.Node(self.settings)

        with self.assertLogs(level="DEBUG") as lg:
            node.start()
            self.assertTrue(expected_log in lg.output)
        node.stop()

    # @unittest.skip
    def test_node_on_message(self):
        """Test _on_message log function.
        
        Test node._on_message method.
        
        Tests: Debug log of received message. 
        """
        node = edge.Node(self.settings)

        # test_topic = 'FUAS/EDGEUNITTEST/test'
        # test_payload = json.dumps({'test': 0})
        node.subscription = [(self.topic, 0)]

        expected_log = 'DEBUG:flecsimo.cmpt.edge:Received message id 0 with ' + \
                       'payload {"test": 0}.'

        with self.assertLogs(level="DEBUG") as lg:
            node.start()
            node.mqclient.publish(self.topic, self.payload)
            # give client a chance to receive message.
            print("Wait for _on_message receiving a message...")
            sleep(1)
            self.assertTrue(expected_log in lg.output)
        node.stop()

    # @unittest.skip
    def test_register_callback(self):
        """Test callback propagation to client.
        
        Test node.register_callback(msgmap) method.
        
        Tests:
            1. Check MQTTMatcher list for registered callbacks.
            2. Test that a None habits does not change registration
            3. Test ValueError exception is raised with malformed habits.
        """
        node = edge.Node(self.settings)
        topic_1 = 'a/test/topic'
        topic_2 = 'another/test/topic'
        habits = [(topic_1, self.aTestFunction_1),
                  (topic_2, self.aTestFunction_2)]

        # Test case 1
        node.register_callback(habits)
        matcher_1 = [repr(_) for _ in
                     node.mqclient._on_message_filtered.iter_match(topic_1)]
        matcher_2 = [repr(_) for _ in
                     node.mqclient._on_message_filtered.iter_match(topic_2)]
        expected_method_1 = "<bound method TestNode.aTestFunction_1 of " + \
                         "<class 'test.test_units.test_cmpt_edge.TestNode'>>"
        expected_method_2 = "<bound method TestNode.aTestFunction_2 of " + \
                         "<class 'test.test_units.test_cmpt_edge.TestNode'>>"

        self.assertIn(expected_method_1, matcher_1, "No matching callback.")
        self.assertIn(expected_method_2, matcher_2, "No matching callback.")

        # Test case 2
        node.register_callback(None)
        self.assertIn(expected_method_1, matcher_1, "No matching callback.")
        self.assertIn(expected_method_2, matcher_2, "No matching callback.")

        # Test case 3
        with self.assertRaises(ValueError):
            node.register_callback('IAmNotAListOfTuples')


class TestAgent(unittest.TestCase):
    """Testing of Agent base class."""

    @classmethod
    def setUpClass(cls):
        # TODO:Try to defin a local Agent class instead having a dependency to AreaAgent

        cls.testroot = Path(__file__).parents[1]  # loader independent path
        cls.database = cls.testroot.joinpath(
            "data",
            "tmp",
            "test_base_agent.db")
        cls.conf = {"mqserver": "localhost",
                    "site": "FUAS",
                    "area": "area1",
                    "database": cls.database,
                    "type": 'area'}
        cls.site = cls.conf['site']
        cls.area = cls.conf['area']
        dbs = dbmanager.DbSetUp(cls.database)
        dbs.create_schema('site')
        dbs.create_masterdata('site')
        dbs.create_testdata('site')

        super(TestAgent, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # os.remove(cls.database)
        super(TestAgent, cls).tearDownClass()

    class CompleteAgent(edge.Agent):
        """An agent with _map_enironment implementation."""

        def __init__(self, conf=None, cid=None,):
            org = Orglevel.AREA.value
            cmpt = Component.AGENTTYP.value
            super().__init__(conf, org, cmpt)

            self.database = self.env['database']

    class IncompleteAgent(edge.Agent):
        """An agent without implementing the abstract method."""

        def __init__(self, conf=None, cid=None,):
            super().__init__(conf, cid=cid)

    # @unittest.skip
    def test_agent_is_not_instatiable(self):
        """Make sure that node.agent can not instantiated directly
        
        Test node.agent()
        """
        with self.assertRaises(TypeError):
            edge.Agent()

    # @unittest.skip
    def test_agent_instance_reqires_implement_method(self):
        """Test that an instance of node.Agent() has to implement 
        _map_environment.
        
        Test self.IncompleteAgent()
        """
        with self.assertRaises(TypeError):
            self.IncompleteAgent(self.conf)

    # @unittest.skip
    def test_map_environment(self):
        """Test propagation of environment information.
        
        Test self.CompleteAgent.
        """
        expected_settings = {'cid': None,
                             'cmpt': 'agt',
                             'mqserver': 'localhost',
                             'sender': 'FUAS/area1',
                             'org': 'area'}
        env_db_type = type(WindowsPath)
        agent = self.CompleteAgent(self.conf)

        self.assertEqual(expected_settings, agent.env['settings'])
        self.assertTrue(env_db_type, type(agent.env['database']))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testAgent']
    unittest.main()
