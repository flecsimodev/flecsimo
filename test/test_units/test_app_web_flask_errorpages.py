#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""Application tests for error-page module of flask-webserver 
Created on 09.01.2022

@author: Leon Schnieber

Copyright and License Notice:

    flecsimo sfc processing
    Copyright (C) 2021  Ralf Banning, Bernhard Lehner and Frankfurt University 
    of Applied Sciences.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
import os
from flask import Flask
from flecsimo.app.web.api import web_error_handler
from flecsimo.base.auth import Auth

class AppWebErrorHandler(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        super(AppWebErrorHandler, cls).setUpClass()
        
    @classmethod
    def tearDownClass(cls):
        super(AppWebErrorHandler, cls).tearDownClass()    

    def setUp(self):
        # start up a minimal flask-instance for module-simulation
        resources_folder = os.path.join(os.path.dirname(web_error_handler.__file__), "../")
        self.flask_app = Flask("test-app", template_folder=resources_folder + "www-template", static_folder=resources_folder + "www-static")
        auth = Auth()
        
        self.module = web_error_handler.flask_error_pages(self.flask_app, auth, sim=None)
        

    def tearDown(self):
        pass
    
    def testNotFound(self):
        with self.flask_app.test_client() as client:
            response = client.get("/not_existing_url")
            self.assertEqual(response.status_code, 404)
    
    @unittest.skip("Not implemented")  
    def testForbidden(self):
        with self.flask_app.test_client() as client:
            response = client.get("/index.html")
            self.assertEqual(response.status_code, 403)
    
    @unittest.skip("Not implemented")  
    def testUnauthorized(self):
        with self.flask_app.test_client() as client:
            response = client.get("/index.html")
            self.assertEqual(response.status_code, 401)
          
if __name__ == "__main__":

    unittest.main()