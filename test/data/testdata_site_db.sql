BEGIN TRANSACTION;
DROP TABLE IF EXISTS "pds_resource";
CREATE TABLE IF NOT EXISTS "pds_resource" (
	"pds_task"	INTEGER,
	"loc"	TEXT,
	"prio"	INTEGER,
	"durvar"	REAL,
	"durfix"	REAL,
	"unit"	TEXT DEFAULT 'S',
	PRIMARY KEY("pds_task"),
	FOREIGN KEY("pds_task") REFERENCES "pds_task"("id")
);
DROP TABLE IF EXISTS "pds_part";
CREATE TABLE IF NOT EXISTS "pds_part" (
	"pds_task"	INTEGER,
	"material"	TEXT,
	"usage"	TEXT,
	"qty"	REAL NOT NULL DEFAULT 1,
	"unit"	TEXT NOT NULL DEFAULT 'PC',
	PRIMARY KEY("pds_task","material","usage"),
	FOREIGN KEY("pds_task") REFERENCES "pds_task"("id"),
	FOREIGN KEY("material") REFERENCES "material"("id")
);
DROP TABLE IF EXISTS "operation";
CREATE TABLE IF NOT EXISTS "operation" (
	"id"	TEXT,
	"desc"	TEXT NOT NULL,
	PRIMARY KEY("id")
) WITHOUT ROWID;
DROP TABLE IF EXISTS "material";
CREATE TABLE IF NOT EXISTS "material" (
	"id"	TEXT,
	"desc"	TEXT,
	"typ"	TEXT,
	"unit"	TEXT DEFAULT 'PCE',
	PRIMARY KEY("id")
) WITHOUT ROWID;
DROP TABLE IF EXISTS "order_ref";
CREATE TABLE IF NOT EXISTS "order_ref" (
	"site"	TEXT,
	"order"	INTEGER,
	"material_ref"	TEXT,
	"extorder_ref"	TEXT,
	"bom_ref"	TEXT,
	"routing_ref"	TEXT,
	"plant_ref"	TEXT,
	FOREIGN KEY("site","order") REFERENCES "order"("site","id")
);
DROP TABLE IF EXISTS "order_part";
CREATE TABLE IF NOT EXISTS "order_part" (
	"site"	TEXT,
	"order_task"	INTEGER,
	"material"	TEXT,
	"usage"	TEXT,
	"qty"	REAL NOT NULL DEFAULT 1,
	"unit"	TEXT NOT NULL DEFAULT 'PC',
	PRIMARY KEY("order_task","material","usage","site")
);
DROP TABLE IF EXISTS "order_resource";
CREATE TABLE IF NOT EXISTS "order_resource" (
	"site"	TEXT,
	"order_task"	INTEGER,
	"loc"	TEXT,
	"prio"	INTEGER,
	"durvar"	REAL,
	"durfix"	REAL,
	"unit"	TEXT DEFAULT 'S',
	PRIMARY KEY("site","order_task")
);
DROP TABLE IF EXISTS "pds_task";
CREATE TABLE IF NOT EXISTS "pds_task" (
	"id"	INTEGER,
	"pds"	INTEGER NOT NULL,
	"step"	INTEGER NOT NULL,
	"next"	INTEGER,
	"typ"	TEXT,
	"operation"	TEXT NOT NULL,
	"variant"	TEXT,
	"paramtyp"	TEXT,
	"paramvalue"	TEXT,
	PRIMARY KEY("id"),
	FOREIGN KEY("pds") REFERENCES "pds"("id")
);
DROP TABLE IF EXISTS "order_task";
CREATE TABLE IF NOT EXISTS "order_task" (
	"site"	TEXT,
	"id"	INTEGER,
	"order"	INTEGER,
	"step"	INTEGER,
	"next"	INTEGER,
	"typ"	TEXT,
	"operation"	TEXT,
	"paramtyp"	TEXT,
	"paramvalue"	TEXT,
	"pds_task"	INTEGER NOT NULL,
	PRIMARY KEY("id"),
	FOREIGN KEY("site","order") REFERENCES "order"("site","id")
);
DROP TABLE IF EXISTS "order_parameter";
CREATE TABLE IF NOT EXISTS "order_parameter" (
	"order"	INTEGER,
	"typ"	TEXT,
	"value"	TEXT,
	PRIMARY KEY("order","typ")
);
DROP TABLE IF EXISTS "pds";
CREATE TABLE IF NOT EXISTS "pds" (
	"id"	INTEGER,
	"material"	TEXT NOT NULL,
	"has_variant"	INTEGER NOT NULL DEFAULT 0,
	"valid_from"	TIMESTAMP NOT NULL,
	"valid_to"	TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("material") REFERENCES "material"("id")
);
DROP TABLE IF EXISTS "facility";
CREATE TABLE IF NOT EXISTS "facility" (
	"id"	TEXT,
	"typ"	TEXT,
	"desc"	TEXT,
	"loc"	TEXT,
	"state"	INTEGER,
	"statx"	TEXT,
	"at"	TIMESTAMP,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "facility_operation";
CREATE TABLE IF NOT EXISTS "facility_operation" (
	"id"	INTEGER,
	"facility"	INTEGER TEXT NOT NULL,
	"operation"	TEXT,
	"param"	TEXT,
	"setup"	INTEGER,
	"state"	INTEGER,
	"statx"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("facility") REFERENCES "facility"("id")
);
DROP TABLE IF EXISTS "order_status";
CREATE TABLE IF NOT EXISTS "order_status" (
	"site"	TEXT,
	"order"	INTEGER,
	"state"	INTEGER NOT NULL,
	"statx"	TEXT,
	"actual_start"	TIMESTAMP,
	"actual_end"	TIMESTAMP,
	"plan_start"	TIMESTAMP,
	"plan_end"	TIMESTAMP,
	"schd_start"	TIMESTAMP,
	"schd_end"	TIMESTAMP,
	"at"	TIMESTAMP,
	FOREIGN KEY("site","order") REFERENCES "order"("site","id")
);
DROP TABLE IF EXISTS "sfcu";
CREATE TABLE IF NOT EXISTS "sfcu" (
	"site"	TEXT,
	"id"	INTEGER,
	"order"	INTEGER,
	"material"	TEXT NOT NULL,
	"state"	INTEGER NOT NULL,
	"statx"	TEXT NOT NULL,
	"at"	TIMESTAMP NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT),
	UNIQUE("site","id"),
	FOREIGN KEY("order") REFERENCES "order"("id")
);
DROP TABLE IF EXISTS "order";
CREATE TABLE IF NOT EXISTS "order" (
	"site"	TEXT,
	"id"	INTEGER,
	"material"	TEXT NOT NULL,
	"variant"	TEXT,
	"qty"	REAL NOT NULL DEFAULT 1,
	"unit"	TEXT DEFAULT 'PCE',
	"pds"	INTEGER,
	"at"	TIMESTAMP NOT NULL,
	PRIMARY KEY("id"),
	UNIQUE("site","id")
);
DROP TABLE IF EXISTS "dsmbook";
CREATE TABLE IF NOT EXISTS "dsmbook" (
	"id"	INTEGER,
	"site"	TEXT NOT NULL,
	"order"	INTEGER NOT NULL,
	"sfcu"	INTEGER NOT NULL,
	"party"	TEXT,
	"role"	TEXT,
	"scope"	TEXT,
	"spec"	TEXT,
	"due"	TIMESTAMP,
	"prio"	INTEGER,
	"state"	INTEGER,
	"statx"	TEXT,
	"at"	TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT),
	UNIQUE("site","order","sfcu","party","role")
);
INSERT INTO "pds_resource" ("pds_task","loc","prio","durvar","durfix","unit") VALUES 
 (1,'',0,30.0,'','S'),
 (2,'',0,35.0,'','S'),
 (3,'',0,125.0,'','S'),
 (4,'',0,125.0,'','S'),
 (5,'',0,30.0,'','S');
 
INSERT INTO "pds_part" ("pds_task","material","usage","qty","unit") VALUES 
 (1,'FXF-5642','in',0.1,'M'),
 (1,'FXF-1120','out',1.0,'PCE'),
 (2,'FXF-1110','in',1.0,'PCE'),
 (3,'FXF-3610-R','in',3.5,'G'),
 (4,'FXF-3610-B','in',3.5,'G'),
 (3,'FXF-1110','out',1.0,'PCE'),
 (4,'FXF-1110','out',1.0,'PCE'),
 (5,'FXF-1110','in',1.0,'PCE'),
 (5,'FXF-1120','in',1.0,'PCE'),
 (5,'FXF-5611','in',2.0,'PCE'),
 (5,'FXF-1100','out',1.0,'PCE');
 
INSERT INTO "operation" ("id","desc") VALUES 
 ('1-PUABE','Punching and Bend Steel Clamps'),
 ('11-ANODIZATION','Anodization of Alloys (Size Group A)'),
 ('27-DRILLING','Drilling and Thread Cutting'),
 ('5-FINASS','Final Assembly (Material Group C)');
 
INSERT INTO "material" ("id","desc","typ","unit") VALUES 
 ('FXF-1100','Device Mount (for 20- 30 mm handlebars)','FINISH','PCE'),
 ('FXF-1110','Mount plate alloy (20-30 mm)','SEMIF','PCE'),
 ('FXF-1120','Steel clamp for 20-30 mm hadlebars','SEMIF','PCE'),
 ('FXF-1130','Deluxe Packaging','RAW','PCE'),
 ('FXF-1131','Standard Packaging','RAW','M'),
 ('FXF-3610-B','Anodization powder ''blue''','RAW','G'),
 ('FXF-3610-R','Anodization powder ''red''','RAW','G'),
 ('FXF-5611','M-5 allen bolts','RAW','PCE'),
 ('FXF-5642','INOX steel strap 0,2 mm thickness','RAW','M');
 
INSERT INTO "pds_task" ("id","pds","step","next","typ","operation","variant","paramtyp","paramvalue") VALUES 
 (1,1,1,2,'start','1-PUABE',NULL,NULL,NULL),
 (2,1,2,3,'','27-DRILLING',NULL,'diam',NULL),
 (3,1,3,4,'var','11-ANODIZATION','col:red','col','red'),
 (4,1,3,4,'var','11-ANODIZATION','col:blue','col','blue'),
 (5,1,4,0,'final','5-FINASS',NULL,'pkg',NULL);
 
INSERT INTO "pds" ("id","material","has_variant","valid_from","valid_to") VALUES (1,'FXF-1100',1,'2020-01-01 00:00:00','9999-12-31 23:29:59'),
 (2,'FXF-1110',0,'2020-01-01 00:00:00',NULL),
 (3,'FXF-1110',0,'2020-01-01 00:00:00',NULL);
 
INSERT INTO "facility" ("id","typ","desc","loc","state","statx","at") VALUES 
 ('area1','C','The flecsimo area 1','A:1',1,'ready','2020-07-14');
 
INSERT INTO "facility_operation" ("id","facility","operation","param","setup","state","statx") VALUES 
 (1,'area1','1-PUABE',NULL,20,NULL,NULL),
 (2,'area1','27-DRILLING','10-60',30,NULL,NULL),
 (3,'area1','11-ANODIZATION',NULL,NULL,NULL,NULL),
 (4,'area1','5-FINASS',NULL,NULL,NULL,NULL);

INSERT INTO "order_status" ("site","order","state","statx","actual_start","actual_end","plan_start","plan_end","schd_start","schd_end","at") VALUES 
 ('FUAS',1000063,0,'initial',NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-20 10:05:00'),
 ('FUAS',1000064,0,'initial',NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-20 10:05:45');

INSERT INTO "order" ("site","id","material","variant","qty","unit","pds","at") VALUES 
 ('FUAS',1000063,'FXF-1100','col:red',1.0,'PCE',1,'2020-05-20 10:05:00'),
 ('FUAS',1000064,'FXF-1100','col:blue',2.0,'PCE',1,'2020-05-20 10:05:45');

COMMIT;
