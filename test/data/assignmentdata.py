        #=======================================================================
        # opdta =json.loads("""
        #     {"data": 
        #         {"order": [
        #             {"id": 1000070, "site": "FUAS", "material": "FXF-1100", 
        #             "variant": "col:red", "qty": 1.0, "unit": "PCE", "pds": 1, 
        #             "at": "2021-07-08 16:26:32.270911"}], 
        #         "sfcu": [
        #             {"id": 16, "site": "FUAS", "order": 1000070,
        #              "material": "FXF-1115", "state": 2, "statx": "PLANNED", 
        #             "at": "2021-07-08 16:26:32.270911" }], 
        #         "task": [
        #             {"id": 41, "site": "FUAS", "order": 1000070, "step": 1, 
        #              "next": 2, "typ": "start", "operation": "1-PUABE", 
        #              "param_typ": null, "param_value": 15, "pds_task": 1}, 
        #             {"id": 42, "site": "FUAS", "order": 1000070, "step": 2, 
        #              "next": 3, "typ": null, "operation": "27-DRILLING", 
        #              "param_typ": "diam", "param_value": null, "pds_task": 2}, 
        #             {"id": 43, "site": "FUAS", "order": 1000070, "step": 3, 
        #              "next": 4, "typ": "var", "operation": "11-ANODIZATION", 
        #              "param_typ": "col", "param_value": "red", "pds_task": 3}, 
        #             {"id": 44, "site": "FUAS", "order": 1000070, "step": 4, 
        #              "next": 0, "typ": "final", "operation": "5-FINASS", 
        #              "param_typ": "pkg", "param_value": "deluxe", "pds_task": 5}], 
        #         "part": [
        #             {"task": 41, "site": "FUAS", "material": "FXF-1120", 
        #              "usage": "out", "qty": 1.0, "unit": "PCE" }, 
        #             {"task": 41, "site": "FUAS", "material": "FXF-5642", 
        #              "usage": "in", "qty": 0.1, "unit": "M"}, 
        #             {"task": 42, "site": "FUAS", "material": "FXF-1110", 
        #              "usage": "in", "qty": 1.0, "unit": "PCE"}, 
        #             {"task": 43, "site": "FUAS", "material": "FXF-1110", 
        #              "usage": "out", "qty": 1.0, "unit": "PCE"}, 
        #             {"task": 43, "site": "FUAS", "material": "FXF-3610-R", 
        #              "usage": "in", "qty": 3.5, "unit": "G"}, 
        #             {"task": 44, "site": "FUAS", "material": "FXF-1100", 
        #              "usage": "out", "qty": 1.0, "unit": "PCE"}, 
        #             {"task": 44, "site": "FUAS", "material": "FXF-1110", 
        #              "usage": "in", "qty": 1.0, "unit": "PCE"}, 
        #             {"task": 44, "site": "FUAS", "material": "FXF-1120", 
        #              "usage": "in", "qty": 1.0, "unit": "PCE"}, 
        #             {"task": 44, "site": "FUAS", "material": "FXF-5611", 
        #              "usage": "in", "qty": 2.0, "unit": "PCE"}], 
        #         "resource": [
        #             {"task": 41, "site": "FUAS", "loc": "", "prio": 0, 
        #              "durvar": 30.0, "durfix": "", "unit": "S"}, 
        #             {"task": 42, "site": "FUAS", "loc": "", "prio": 0, 
        #              "durvar": 35.0, "durfix": "", "unit": "S"}, 
        #             {"task": 43, "site": "FUAS", "loc": "", "prio": 0, 
        #              "durvar": 125.0, "durfix": "", "unit": "S"}, 
        #             {"task": 44, "site": "FUAS", "loc": "", "prio": 0, 
        #              "durvar": 30.0, "durfix": "", "unit": "S"}]}, 
        #     "at": "2022-02-02T15:06:21.077600"}""")
        #     
        # assignment = json.loads("""{"site": "FUAS", "order": 1000070, "sfcu": 16,
        #               "operation": "ALL", "supplier": "area1", "typ": "A", 
        #               "due": "2021-07-12", "prio": 0, "dhash": null, 
        #               "state": 4, "statx": "ASSIGNED", 
        #               "at": "2022-02-02T15:56:57.652966"}""")
        #                      
        # conn = self.dao.connect()      
        # with conn:
        #     self.dao.loadmany(conn, 'order', opdta['data']['order'])
        #     self.dao.loadmany(conn, 'sfcu', opdta['data']['sfcu'])
        #     self.dao.loadmany(conn, 'task', opdta['data']['task'])
        #     self.dao.loadmany(conn, 'part', opdta['data']['part'])
        #     self.dao.loadmany(conn, 'resource', opdta['data']['resource'])           
        # conn.close()
        #
        #self.dao.persist('schedule', assignment)
        #=======================================================================